/*
 * BitronixImpl.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jta;

import java.lang.reflect.Method;
import javax.transaction.TransactionManager;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class BitronixImpl implements JTAImplementation {
   
   private static TransactionManager transactionManager;
   
   public static JTAImplementation isLibraryAvailable() {
      try {
         Class c = Class.forName("bitronix.tm.TransactionManagerServices");
         
         Method getTm = c.getMethod("getTransactionManager");
         transactionManager = (TransactionManager)getTm.invoke(null);
         
         return new BitronixImpl();
      } catch(Exception ex) {
         return null;
      }
   }

   @Override
   public String getImplementationName() {
      return "Bitronix - bitronix.tm.BitronixTransactionManager";
   }

   @Override
   public TransactionManager getTransactionManager() {
      return transactionManager;
   }


   
}
