/*
 * CDIUserTransaction.java
 * 
 * Defines a class to implement a UserTransaction 
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jta;

import javax.enterprise.inject.Vetoed;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class UserTransaction implements javax.transaction.UserTransaction {

   private TransactionManager tm;

   public UserTransaction(TransactionManager tm) {
      this.tm = tm;
   }

   @Override
   public void begin() throws NotSupportedException, SystemException {
      tm.begin();
   }

   @Override
   public void commit() throws RollbackException, HeuristicMixedException, HeuristicRollbackException, SecurityException, IllegalStateException, SystemException {
      tm.commit();
   }

   @Override
   public void rollback() throws IllegalStateException, SecurityException, SystemException {
      tm.rollback();
   }

   @Override
   public void setRollbackOnly() throws IllegalStateException, SystemException {
      tm.setRollbackOnly();
   }

   @Override
   public int getStatus() throws SystemException {
      return tm.getStatus();
   }

   @Override
   public void setTransactionTimeout(int seconds) throws SystemException {
      tm.setTransactionTimeout(seconds);
   }

   public Transaction getTransaction() throws SystemException {
      return tm.getTransaction();
   }
}
