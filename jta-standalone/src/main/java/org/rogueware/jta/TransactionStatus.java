/*
 * TransactionStatus.java
 * 
 * Defined a class to initialize the JTA library
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jta;

import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TransactionStatus {

   public static String getTransactionStatusString(int status) {
      switch (status) {
         case Status.STATUS_ACTIVE:
            return "ACTIVE";
         case Status.STATUS_COMMITTED:
            return "COMMITED";
         case Status.STATUS_COMMITTING:
            return "COMMITING";
         case Status.STATUS_MARKED_ROLLBACK:
            return "MARKED ROLLBACK";
         case Status.STATUS_NO_TRANSACTION:
            return "NO TRANSACTION";
         case Status.STATUS_PREPARED:
            return "PREPARED";
         case Status.STATUS_PREPARING:
            return "PREPARING";
         case Status.STATUS_ROLLEDBACK:
            return "ROLLEDBACK";
         case Status.STATUS_ROLLING_BACK:
            return "ROLLING BACK";
         default:
            return "UNKNOWN";
      }
   }

   public static String getCurrentTransactionStatusString() {
      try {
         return getTransactionStatusString(JTAStandAlone.getTransactionManager().getStatus());
      } catch (SystemException ex) {
         return "SystemException";
      }
   }

   public static String getTransactionId(Transaction t) {
      String res = String.format("%s (%d)", Thread.currentThread().getName(), Thread.currentThread().getId());
      if (null != t) {
         res += String.format(" - %d", t.hashCode());
      }
      return res;
   }

   public static String getCurrentTransactionId() {
      try {
         return getTransactionId(JTAStandAlone.getTransactionManager().getTransaction());
      } catch (SystemException ex) {
         return "SystemException";
      }
   }
}
