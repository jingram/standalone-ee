/*
 * JTAStandAlone.java
 * 
 * Defined a class to initialize the JTA library
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jta;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.TransactionalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class JTAStandAlone {

   private static final Logger log = LoggerFactory.getLogger(JTAStandAlone.class);
   private static JTAImplementation jtaImpl;

   public static boolean initJTAStandAloneApplication() throws TransactionalException {
      boolean registeredWithJNDI = false;

      // Try and find an implementation from known JTA libs
      jtaImpl = AtomikosImpl.isLibraryAvailable();
      if (null == jtaImpl) 
         jtaImpl = BitronixImpl.isLibraryAvailable();
      if (null == jtaImpl) {
         jtaImpl = JBossTSArjunaImpl.isLibraryAvailable();
      }
      // TODO: Add implementations as required

      if (null == jtaImpl) {
         throw new TransactionalException("Unable to find a valid JTA implementation", new ClassNotFoundException("No implementation for javax.transaction.TransactionManager"));
      }

      // Register the transaction manager with JNDI
      try {
         Context ctx = new InitialContext();

         // Transaction Manager
         // --------------------
         ctx.rebind("java:/TransactionManager", jtaImpl.getTransactionManager());
         log.info(String.format("JTA - Bound transaction manager to JNDI context java:/TransactionManager"));
         ctx.rebind("/TransactionManager", jtaImpl.getTransactionManager());
         log.info(String.format("JTA - Bound transaction manager to JNDI context /TransactionManager"));

         // Some libs want transaction manager at JBoss AS server location :(
         Context cJboss = ctx.createSubcontext("java:jboss");
         cJboss.rebind("TransactionManager", jtaImpl.getTransactionManager());
         log.info(String.format("JTA - Bound transaction manager to JNDI context java:jboss/TransactionManager"));

         // Some libs want transaction manager at Glassfish AS server location :(
         Context appServer = ctx.createSubcontext("java:appserver");
         appServer.rebind("TransactionManager", jtaImpl.getTransactionManager());
         log.info(String.format("JTA - Bound transaction manager to JNDI context java:appserver/TransactionManager"));
  
         // User Transaction
         // --------------------         
         Context comp = ctx.createSubcontext("java:comp");
         
         comp.rebind("UserTransaction", new org.rogueware.jta.UserTransaction(jtaImpl.getTransactionManager()));
         log.info(String.format("JTA - Bound user transaction to JNDI context java:comp/UserTransaction"));
         
         
         
         registeredWithJNDI = true;
      } catch (Exception ex) {
         registeredWithJNDI = false;  // Inited, but not registered
         log.warn(String.format("JTA - Unable to bind transaction manager to JNDI context java:/TransactionManager or /TransactionManager or  java:jboss/TransactionManager or java:appserver/TransactionManager"));
      }

      return registeredWithJNDI;   // Registered with JNDI ?
   }

   public static TransactionManager getTransactionManager() {
      return jtaImpl.getTransactionManager();
   }

   public static String getImplementationName() {
      return jtaImpl.getImplementationName();
   }

   public Transaction getCurrentTransaction() throws SystemException {
      return jtaImpl.getTransactionManager().getTransaction();
   }
}
