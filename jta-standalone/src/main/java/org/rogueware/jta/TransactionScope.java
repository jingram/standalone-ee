/*
 * TransactionScope.java
 * 
 * Defined a class to wrap a transaction as a resource
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jta;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.InvalidTransactionException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TransactionScope implements AutoCloseable {

   public enum Scope {

      REQUIRED,
      REQUIRES_NEW
   }

   private boolean createdTransaction;
   private Transaction suspended;

   public TransactionScope(Scope scope) throws SystemException, NotSupportedException {
      TransactionManager tm = JTAStandAlone.getTransactionManager();
      if (null == tm) {
         throw new NotSupportedException("No available transaction manager. Transaction manager is null");
      }
      switch (scope) {
         case REQUIRED:
            if (tm.getStatus() == Status.STATUS_NO_TRANSACTION) {
               tm.begin();
               createdTransaction = true;
            }
            break;

         case REQUIRES_NEW:
            if (tm.getStatus() == Status.STATUS_ACTIVE
                  || tm.getStatus() == Status.STATUS_COMMITTED
                  || tm.getStatus() == Status.STATUS_ROLLEDBACK) {
               suspended = tm.suspend();
            }
            tm.begin();
            createdTransaction = true;
            break;
      }
   }

   public boolean rollback() {
      TransactionManager tm = JTAStandAlone.getTransactionManager();
      try {
         if (null != tm && null != tm.getTransaction()) {
            tm.getTransaction().setRollbackOnly();
         }
      } catch (Exception ex) {
         return false;
      }
      return true;
   }

   public Transaction getTransaction() throws SystemException, NotSupportedException {
      TransactionManager tm = JTAStandAlone.getTransactionManager();
      if (null == tm) {
         throw new NotSupportedException("No available transaction manager. Transaction manager is null");
      }
      return tm.getTransaction();
   }

   @Override
   public void close() throws NotSupportedException, SystemException {
      TransactionManager tm = JTAStandAlone.getTransactionManager();
      if (null == tm) {
         throw new NotSupportedException("No available transaction manager. Transaction manager is null");
      }

      // If we created the transaction, commit or roll it back
      try {
         if (createdTransaction) {
            switch (tm.getStatus()) {
               case Status.STATUS_ACTIVE:
                  tm.commit();
                  break;

               default:
                  tm.rollback();
                  break;
            }
         }

         // If we suspended a transaction, resume it
         if (null != suspended) {
            tm.resume(suspended);
         }
      } catch (HeuristicMixedException | HeuristicRollbackException | IllegalStateException | InvalidTransactionException | RollbackException | SecurityException ex) {
         throw new SystemException(ex.getMessage());
      }
   }
}
