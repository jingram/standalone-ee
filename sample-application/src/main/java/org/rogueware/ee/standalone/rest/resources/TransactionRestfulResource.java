/*
 * TransactionRestfulResource.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.rest.resources;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.TransactionalException;
import javax.transaction.UserTransaction;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.rogueware.ee.standalone.tansaction.bean.ExistingTransactionWrapperBean;
import org.rogueware.ee.standalone.tansaction.bean.TransactionBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Path("transaction")
public class TransactionRestfulResource {

   private static final Logger log = LoggerFactory.getLogger(TransactionRestfulResource.class);

   @Context
   private HttpServletRequest servletRequest;

   @Inject
   TransactionBean transactionBean;

   @Inject
   ExistingTransactionWrapperBean existingTransactionWrapperBean;

   @Inject
   UserTransaction ut;

   @Path("/required")
   @GET
   @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
   public void required() {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting REQUIRED Test");
      log.info("-------------------------------------------------------------------------");

      assertNoTransaction("start");

      // Test with no existing transaction
      transactionBean.required(null);

      // Test using existing transction
      existingTransactionWrapperBean.required();

      assertNoTransaction("end");
      log.info("-------------------------------------------------------------------------");
   }

   @Path("/requiresNew")
   @GET
   @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
   public void requiresNew() {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting REQUIRES-NEW Test");
      log.info("-------------------------------------------------------------------------");

      assertNoTransaction("start");

      // Test with no existing transaction
      transactionBean.requiresNew(null);

      // Test using existing transction
      existingTransactionWrapperBean.requiresNew();

      assertNoTransaction("end");
      log.info("-------------------------------------------------------------------------");
   }

   @Path("/supports")
   @GET
   @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
   public void supports() {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting SUPPORTS Test");
      log.info("-------------------------------------------------------------------------");

      assertNoTransaction("start");

      // Test with no existing transaction
      transactionBean.supports(null);

      // Test using existing transction
      existingTransactionWrapperBean.supports();

      assertNoTransaction("end");
      log.info("-------------------------------------------------------------------------");
   }

   @Path("/notSupported")
   @GET
   @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
   public void notSupported() {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting NOT_SUPPORTED Test");
      log.info("-------------------------------------------------------------------------");

      assertNoTransaction("start");

      // Test with no existing transaction
      transactionBean.notSupported(null);

      // Test using existing transction
      existingTransactionWrapperBean.notSupported();

      assertNoTransaction("end");
      log.info("-------------------------------------------------------------------------");
   }

   @Path("/never")
   @GET
   @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
   public void never() {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting NEVER Test");
      log.info("-------------------------------------------------------------------------");

      assertNoTransaction("start");

      // Test with no existing transaction
      transactionBean.never(null);

      // Test using existing transction
      existingTransactionWrapperBean.never();

      assertNoTransaction("end");
      log.info("-------------------------------------------------------------------------");
   }

   @Path("/mandatory")
   @GET
   @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
   public void mandatory() {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting MANDATORY Test");
      log.info("-------------------------------------------------------------------------");

      assertNoTransaction("start");

      // Test with no existing transaction
      try {
         transactionBean.mandatory(null);
         log.info("MANDATORY - Missing transaction did not cause an exception");
         throw new InternalServerErrorException("MANDATORY - Missing transaction did not cause an exception");
      } catch (TransactionalException ex) {
         // Yup, what we want :)
      }

      // Test using existing transction
      existingTransactionWrapperBean.mandatory();

      assertNoTransaction("end");
      log.info("-------------------------------------------------------------------------");
   }

   private void assertNoTransaction(String position) {
      try {
         if (ut.getStatus() != Status.STATUS_NO_TRANSACTION) {
            log.error(String.format("No transaction should exist at the %s of the test", position));
            throw new InternalServerErrorException(String.format("No transaction should exist at the %s of the test", position));
         }
      } catch (SystemException ex) {
         log.error("Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("Error occured while determining status of transaction", ex);
      }
   }

}
