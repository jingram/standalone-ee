/*
 * ServerResourceConfig.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.rest.resources;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class ServerResourceConfig extends Application {

   @Override
   public Set<Class<?>> getClasses() {
      Set<Class<?>> s = new HashSet<>();

      // TODO: Add new classes REST classes as defined 
      s.add(TransactionRestfulResource.class);
      s.add(XAResourceRestfulResource.class);

      
      return s;
   }

   @Override
   public Set<Object> getSingletons() {
      Set<Object> s = new HashSet<>();

      // TODO: Add new providers as defined
      return s;
   }

}
