/*
 * App.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.naming.NamingException;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.webapp.WebAppContext;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.rogueware.ee.standalone.jpa.JPAStandAlone;
import org.rogueware.jta.JTAStandAlone;
import org.rogueware.xadisk.XADiskStandAlone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class App {

   private static final Logger log = LoggerFactory.getLogger(App.class);

   private static AppConfiguration config;
   private static Weld weld;
   private static Server webServer;

   public static void main(String[] args) {
      log.info("EIQube Server Starting ...");

      try {
         init();
      } catch (Exception ex) {
         System.exit(-1);
      }

      shutdown();

      log.info("EIQube Server Exiting");
   }

   public static void init() throws Exception {
      try {
         // Init the configuration
         initConfig();

         // Init Transaction Management
         initTransactionManagement();

         // Init XADisk
         initXADisk();

         // Init the database
         //initDatabase();
         // Init the CDI container
         initCDIContainer();

         // Init the web server
         initWebServer();

      } catch (Throwable ex) {
         log.error("Unable to init system", ex);
         throw new Exception("Unable to init system", ex);
      }
   }

   public static void shutdown() {
      // Web server
      shutdownWebServer();

      // CDI Container
      shutdownCDIContainer();

      // Database
      //shutdownDatabase();
      // XADisk
      shutdownXADisk();
   }

   public static void initConfig() {
      config = AppConfiguration.getSingleton();
      log.info("Configuration loaded");
      log.info("Running from path " + config.getMainJarPath().getAbsolutePath());
   }

   public static void initTransactionManagement() throws NamingException {
      boolean registeredWithJNDI = JTAStandAlone.initJTAStandAloneApplication();
      log.info(String.format("Initialised JTA - %s, registered with JNDI %s", JTAStandAlone.getImplementationName(), registeredWithJNDI));
   }

   public static void initXADisk() throws InterruptedException {
      XADiskStandAlone.initJPAStandAloneApplication(config.getXaDiskInstances());
      log.info("Initialised XADisk");
      for (String instanceId : XADiskStandAlone.getXADiskInstanceIds()) {
         log.info(String.format("  Created XADisk instance: %s", instanceId));
      }
   }

   public static void shutdownXADisk() {
      log.info("Shutting down XADisk...");
      for (String instanceId : XADiskStandAlone.getXADiskInstanceIds()) {
         try {
            XADiskStandAlone.getXAFileSystem(instanceId).shutdown();
         } catch (IOException ex) {
            log.error(String.format("Unable to shutdown XA Disk instance %s", instanceId), ex);
         }
         log.info(String.format("  Shutdown XADisk instance: %s", instanceId));
      }
      XADiskStandAlone.destroySingleton();
   }

   public static void initCDIContainer() throws Exception {
      weld = new Weld();
      WeldContainer weldContainer = weld.initialize();

      BeanManager bm = CDI.current().getBeanManager();
      if (null == bm) {
         throw new Exception("Unable to initialize CDI container");
      }
      
      log.info(String.format("  Initialised CDI container"));
   }

   public static void shutdownCDIContainer() {
      try {
         if (null != weld) {
            weld.shutdown();
            log.info(String.format("  Shutdown CDI container"));
         }
      } catch (Exception ex) {
         log.trace("Unable to stop CDI container", ex);
      }
   }

   public static void initDatabase() throws NamingException {
      // JPA
      JPAStandAlone.initJPAStandAloneApplication(config.getPersistenceUnits());
      log.info("Initialised JPA");
      for (String puName : JPAStandAlone.getPersistenceUnitNames()) {
         log.info(String.format("  Created Persistence Unit: %s", puName));
      }

      // Run init script
    /*  EntityManager em = null;
      try {
         JTAStandAlone.getTransactionManager().begin();
         em = JPAStandAlone.createEntityManager();
         
         InputStream is = ExecuteSQLScript.class.getResourceAsStream("/sql/init.sql");
         if (null == is) {
            throw new PersistenceException("Unable to execute DML script /sql/init.sql");
         }
         ExecuteSQLScript.execute(em, is);
      } catch (Exception ex) {
         try {
            JTAStandAlone.getTransactionManager().rollback();
         } catch (Exception ex2) {
            log.error("Unable to commit DML script /sql/init.sql", ex2);
         }

         log.error("Unable to execute DML script /sql/init.sql", ex);
         throw new PersistenceException("Unable to execute DML script /sql/init.sql", ex);
      } finally {
         try {
            JTAStandAlone.getTransactionManager().commit();
         } catch (Exception ex2) {
            log.error("Unable to rollback DML script /sql/init.sql", ex2);
         }
         if (null != em)
            em.close();
      }*/
   }

   public static void shutdownDatabase() {
      log.info("Shutting down JPA...");
      for (String puName : JPAStandAlone.getPersistenceUnitNames()) {
         JPAStandAlone.getEntityManagerFactory(puName).close();
         log.info(String.format("  Shutdown Persistence Unit: %s", puName));
      }
      JPAStandAlone.destroySingleton();
   }   
   
   public static void initWebServer() throws Exception {
      try {
         webServer = new Server();
         webServer.setStopTimeout(5000);
         webServer.setStopAtShutdown(true); // shut down when VM shuts down

         // Create connectors for all ports listed
         List<Connector> connectors = new ArrayList<>();
         for (int port : config.getWebServerPorts()) {
            ServerConnector connector = new ServerConnector(webServer);
            connector.setHost(config.getWebServerHost());
            connector.setPort(port);
            connector.setIdleTimeout(15000);

            connectors.add(connector);
         }
         webServer.setConnectors(connectors.toArray(new Connector[0]));

         // Root context handler
         WebAppContext webApp = new WebAppContext();
         webApp.setDescriptor(config.getWebAppPath() + "/WEB-INF/web.xml");
         webApp.setWar("/");
         webApp.setResourceBase(config.getWebAppPath());      // Static site content
         webApp.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");
         webApp.setClassLoader(Thread.currentThread().getContextClassLoader());

         HandlerList handlers = new HandlerList();
         handlers.setHandlers(new Handler[]{webApp, new DefaultHandler()});
         webServer.setHandler(handlers);

         webServer.start();
         log.info("Started embedded web server");
      } catch (Exception ex) {
         log.error("Unable to start embedded web server " + ex.getMessage(), ex);
         throw new Exception("Unable to start embedded web server " + ex.getMessage(), ex);
      }
   }

   public static void shutdownWebServer() {
      if (null != webServer) {
         try {
            webServer.stop();
            log.info("Stopped embedded web server");
         } catch (Exception ex) {
            log.error("Unable to stop web server", ex);
         }
      }
   }
}
