/*
 * XAResourceRestClient.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.rest.client;

import java.net.MalformedURLException;
import java.net.UnknownHostException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.rogueware.jaxrs.client.AbstractRestClient;
import org.rogueware.jaxrs.client.ErrorResponseHelper;
import org.rogueware.jaxrs.client.RemoteRestException;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class XAResourceRestClient extends AbstractRestClient {

   public XAResourceRestClient(String baseUri, boolean useSsl) throws MalformedURLException, UnknownHostException {
      super(baseUri, "rest/xaResource", useSsl);
   }

   public void commit() throws RemoteRestException {
      Response response = webTarget.path("commit").request(MediaType.APPLICATION_JSON).get();
      new ErrorResponseHelper(response).checkAndRaiseSingle(RemoteRestException.class); 
   }
   
}
