/*
 * AppConfiguration.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.rogueware.ee.standalone.jpa.PUEntry;
import org.rogueware.xadisk.FileSystemConfigurationEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class AppConfiguration {

   private static final Logger log = LoggerFactory.getLogger(AppConfiguration.class);
   private static AppConfiguration singleton;
   private static XMLConfiguration config;

   private String configFile;

   // Web Server
   private String webServerHost;
   private List<Integer> webServerPorts;
   private String webAppPath;

   // Database
   private List<PUEntry> persistenceUnits = new ArrayList<>();

   // XADisk
   private List<FileSystemConfigurationEntry> xaDiskInstances = new ArrayList<>();
   
   public AppConfiguration() {
      try {
         // Check if a config file has been specified
         configFile = "configuration.xml";
         if (null != System.getProperty("ConfigFile")) {
            configFile = System.getProperty("ConfigFile");
            log.info("Using specified configuration file " + configFile);
         } else {
            log.info("Using default configuration");
         }

         config = new XMLConfiguration(configFile);
         config.setThrowExceptionOnMissing(true);

         // Set the autosave to be enabled so changes are written to config file
         readConfiguration();
      } catch (Exception ex) {
         log.error("Unable to load configuration file, exiting", ex);
         System.err.println("Unable to load configuration file, exiting");
         ex.printStackTrace(System.err);
         System.exit(-101);
      }
   }

   private void readConfiguration() {

      // Web Server
      webServerHost = config.getString("WebServer[@host]", "0.0.0.0");

      List<Object> tmp = config.getList("WebServer[@ports]", Arrays.asList("8282"));
      webServerPorts = new ArrayList<>();
      for (Object o : tmp) {
         if (!webServerPorts.contains(Integer.valueOf((String) o))) {
            webServerPorts.add(Integer.valueOf((String) o));
         }
      }

      webAppPath = config.getString("WebServer[@webAppPath]");

      // Database PU's
      for (HierarchicalConfiguration hc : config.configurationsAt("Database.JPA.PU")) {
         persistenceUnits.add(new PUEntry(hc.configurationAt("")));
      }
      
      // XA Disk Instances
      for (HierarchicalConfiguration hc : config.configurationsAt("XADisk.Instance")) {
         xaDiskInstances.add(new FileSystemConfigurationEntry(hc.configurationAt("")));
      }      
   }

   public static synchronized AppConfiguration getSingleton() {
      if (null == singleton) {
         singleton = new AppConfiguration();
      }
      return singleton;
   }

   public static synchronized void destroySingleton() {
      if (null == singleton) {
         return;
      }
      singleton = null;
   }

   public static synchronized XMLConfiguration getConfigurator() {
      return config;
   }

   public final File getMainJarPath() {
      return new File(App.class.getProtectionDomain().getCodeSource().getLocation().getPath());
   }

   public String getWebServerHost() {
      return webServerHost;
   }

   public List<Integer> getWebServerPorts() {
      return webServerPorts;
   }

   public List<PUEntry> getPersistenceUnits() {
      return persistenceUnits;
   }

   public List<FileSystemConfigurationEntry> getXaDiskInstances() {
      return xaDiskInstances;
   }

   public String getWebAppPath() {
      return webAppPath;
   }
}
