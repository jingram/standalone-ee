/*
 * TransactionBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.tansaction.bean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;
import javax.ws.rs.InternalServerErrorException;
import org.rogueware.jta.JTAStandAlone;
import org.rogueware.jta.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
@RequestScoped
public class TransactionBean {

   private static final Logger log = LoggerFactory.getLogger(TransactionBean.class);

   @Inject
   UserTransaction ut;

   @Transactional(Transactional.TxType.REQUIRED)
   public void required(String existingTransactionToString) {
      // Make sure user transaction is correct
      assertUserTransaction();

      try {
         // Make sure there is an active transaction
         if (ut.getStatus() != Status.STATUS_ACTIVE) {
            log.error("REQUIRED - An active transaction is required");
            throw new InternalServerErrorException("REQUIRED - An active transaction is required");
         }

         // If there was a transaction before, make sure they are the same as required in not supposed to create a new transaction if one exists,
         //   only create one if no transaction exist
         if (null != existingTransactionToString && !existingTransactionToString.equals(((org.rogueware.jta.UserTransaction) ut).getTransaction().toString())) {
            log.error("REQUIRED - A new transaction was created even though there was an existing transaction");
            throw new InternalServerErrorException("REQUIRED - A new transaction was created even though there was an existing transaction");
         }
      } catch (SystemException ex) {
         log.error("REQUIRED - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("REQUIRED - Error occured while determining status of transaction", ex);
      }

      log.info(String.format("REQUIRED - Using transaction : %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));
   }

   @Transactional(Transactional.TxType.REQUIRES_NEW)
   public void requiresNew(String existingTransactionToString) {
      // Make sure user transaction is correct
      assertUserTransaction();

      try {
         // Make sure there is an active transaction
         if (ut.getStatus() != Status.STATUS_ACTIVE) {
            log.error("REQUIRES_NEW - An active transaction is required");
            throw new InternalServerErrorException("REQUIRES_NEW - An active transaction is required");
         }

         // If there was a transaction before, make sure they are NOT the same as requires new in not supposed to suspend the existing one and,
         //   create a new one if a transaction exists or create a new one if no transaction exist
         if (null != existingTransactionToString && existingTransactionToString.equals(((org.rogueware.jta.UserTransaction) ut).getTransaction().toString())) {
            log.error("REQUIRES_NEW - A new transaction was NOT created when there was an existing transaction");
            throw new InternalServerErrorException("REQUIRES_NEW - A new transaction was NOT created when there was an existing transaction");
         }
      } catch (SystemException ex) {
         log.error("REQUIRES_NEW - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("REQUIRES_NEW - Error occured while determining status of transaction", ex);
      }

      log.info(String.format("REQUIRES_NEW - Created transaction : %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));
   }

   @Transactional(Transactional.TxType.SUPPORTS)
   public void supports(String existingTransactionToString) {
      // Make sure user transaction is correct
      assertUserTransaction();

      try {
         // If there was no transaction, make sure there is no active transaction
         if (null == existingTransactionToString) {
            if (ut.getStatus() != Status.STATUS_NO_TRANSACTION) {
               log.error("SUPPORTS - No transaction should be available");
               throw new InternalServerErrorException("SUPPORTS - No transaction should be available");
            }

         }

         // If there was a transaction before, make sure its still available
         if (null != existingTransactionToString && !existingTransactionToString.equals(((org.rogueware.jta.UserTransaction) ut).getTransaction().toString())) {
            log.error("SUPPORTS - The existing transaction was not maintained");
            throw new InternalServerErrorException("SUPPORTS - The existing transaction was not maintained");
         }
      } catch (SystemException ex) {
         log.error("SUPPORTS - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("SUPPORTS - Error occured while determining status of transaction", ex);
      }

      log.info(String.format("SUPPORTS - Using transaction : %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));
   }

   @Transactional(Transactional.TxType.NOT_SUPPORTED)
   public void notSupported(String existingTransactionToString) {
      // Make sure user transaction is correct
      assertUserTransaction();

      try {
         // There should never be a transaction (It should be suspended if there was one)
         if (ut.getStatus() != Status.STATUS_NO_TRANSACTION) {
            log.error("NOT_SUPPORTED - No transaction should be available");
            throw new InternalServerErrorException("NOT_SUPPORTED - No transaction should be available");
         }

         if (null != existingTransactionToString && null != ((org.rogueware.jta.UserTransaction) ut).getTransaction()) {
            log.error("NOT_SUPPORTED - The existing transaction was not suspended");
            throw new InternalServerErrorException("NOT_SUPPORTED - The existing transaction was not suspended");
         }
      } catch (SystemException ex) {
         log.error("NOT_SUPPORTED - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("NOT_SUPPORTED - Error occured while determining status of transaction", ex);
      }

      log.info(String.format("NOT_SUPPORTED - Using transaction : %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));
   }   
   
   @Transactional(Transactional.TxType.NEVER)
   public void never(String existingTransactionToString) {
      // Make sure user transaction is correct
      assertUserTransaction();

      try {
         // There should never be a transaction (It should throw exception if there was one)
         if (ut.getStatus() != Status.STATUS_NO_TRANSACTION) {
            log.error("NEVER - No transaction should be available");
            throw new InternalServerErrorException("NEVER - No transaction should be available");
         }

         if (null != existingTransactionToString) {
            log.error("NEVER - The existing transaction did not cause an exception");
            throw new InternalServerErrorException("NEVER - The existing transaction did not cause an exception");
         }
      } catch (SystemException ex) {
         log.error("NEVER - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("NEVER - Error occured while determining status of transaction", ex);
      }

      log.info(String.format("NEVER - Using transaction : %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));
   }    
   
   
   @Transactional(Transactional.TxType.MANDATORY)
   public void mandatory(String existingTransactionToString) {
      // Make sure user transaction is correct
      assertUserTransaction();

      try {
         // There should always be a transaction (It should throw exception if there was not one)
         if (ut.getStatus() != Status.STATUS_ACTIVE) {
            log.error("MANDATORY - Transaction should be available");
            throw new InternalServerErrorException("MANDATORY - Transaction should be available");
         }

         if (null == existingTransactionToString) {
            log.error("MANDATORY - The missing transaction did not cause an exception");
            throw new InternalServerErrorException("MANDATORY - The missing transaction did not cause an exception");
         }
         
         if (!existingTransactionToString.equals(((org.rogueware.jta.UserTransaction) ut).getTransaction().toString())) {
            log.error("MANDATORY - A new transaction was created even though there was an existing transaction");
            throw new InternalServerErrorException("MANDATORY - A new transaction was created even though there was an existing transaction");
         }         
      } catch (SystemException ex) {
         log.error("MANDATORY - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("MANDATORY - Error occured while determining status of transaction", ex);
      }

      log.info(String.format("MANDATORY - Using transaction : %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));
   }   
   
   private void assertUserTransaction() {
      // Make sure the user transaction injected
      if (null == ut) {
         log.error("User transaction not injected");
         throw new InternalServerErrorException("User transaction not injected");
      }

      if (!(ut instanceof UserTransaction)) {
         log.error("User transaction not of type CDIUserTransaction");
         throw new InternalServerErrorException("User transaction not of type CDIUserTransaction");
      }

      // Make sure the user transaction matches the current thread transaction
      try {
         TransactionManager tm = JTAStandAlone.getTransactionManager();
         org.rogueware.jta.UserTransaction cut = (org.rogueware.jta.UserTransaction) ut;

         // Check transaction status the same
         if (null != tm.getTransaction() && null != cut.getTransaction()) {
            if (!tm.getTransaction().toString().equals(cut.getTransaction().toString())) {
               log.error("User transaction does not match the current thread transaction");
               throw new InternalServerErrorException("User transaction does not match the current thread transaction");
            }
         } else if (null != tm.getTransaction() && null != cut.getTransaction()) {
            log.error("User transaction does not match the current thread transaction");
            throw new InternalServerErrorException("User transaction does not match the current thread transaction");
         }
      } catch (SystemException ex) {
         log.error("Error occured while comparing user transaction to current thread transaction", ex);
         throw new InternalServerErrorException("Error occured while comparing user transaction to current thread transaction", ex);
      }
   }
}
