/*
 * XAResourceRestfulResource.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.rest.resources;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.rogueware.ee.standalone.xaresource.bean.XAResourceWrapperBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Path("xaResource")
public class XAResourceRestfulResource {

   private static final Logger log = LoggerFactory.getLogger(XAResourceRestfulResource.class);

   @Context
   private HttpServletRequest servletRequest;

   @Inject
   private XAResourceWrapperBean xaResourceWrapperBean;
   
   
   @Path("/commit")
   @GET
   @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
   public void required() {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting XA Reource Commit Test");
      log.info("-------------------------------------------------------------------------");

      xaResourceWrapperBean.createAndCommitResources();
      
      log.info("-------------------------------------------------------------------------");
   }

}
