/*
 * XAResourceWrapperBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.xaresource.bean;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.TransactionalException;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Transactional(value = Transactional.TxType.REQUIRES_NEW)
public class XAResourceWrapperBean {

   @Inject
   private XADiskResourceBean xaDisk;

   public void createAndCommitResources() {
      xaDisk.createAndCommitResources();
   }

   @Transactional(value = Transactional.TxType.NOT_SUPPORTED)
   public void validateCommitResources() {
      if (!xaDisk.validateCommitedResources()) {
         throw new TransactionalException("XADisk did not commit", null);
      }
   }

}
