/*
 * ExistingTransactionWrapperBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.tansaction.bean;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transactional;
import javax.transaction.TransactionalException;
import javax.transaction.UserTransaction;
import javax.ws.rs.InternalServerErrorException;
import org.rogueware.jta.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
@Transactional(Transactional.TxType.REQUIRES_NEW)
public class ExistingTransactionWrapperBean {

   private static final Logger log = LoggerFactory.getLogger(ExistingTransactionWrapperBean.class);

   @Inject
   TransactionBean transactionBean;

   @Inject
   UserTransaction ut;

   public void required() {
      try {
         // Make sure there is an active transaction
         if (ut.getStatus() != Status.STATUS_ACTIVE) {
            log.error("REQUIRED - An active transaction is required");
            throw new InternalServerErrorException("REQUIRED - An active transaction is required");
         }

         log.info(String.format("REQUIRED - Created transaction: %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));

         org.rogueware.jta.UserTransaction cdiUt = (org.rogueware.jta.UserTransaction) ut;
         transactionBean.required(cdiUt.getTransaction().toString());
      } catch (SystemException ex) {
         log.error("REQUIRED - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("REQUIRED - Error occured while determining status of transaction", ex);
      }
   }

   public void requiresNew() {
      try {
         // Make sure there is an active transaction
         if (ut.getStatus() != Status.STATUS_ACTIVE) {
            log.error("REQUIRES-NEW - An active transaction is required");
            throw new InternalServerErrorException("REQUIRES-NEW - An active transaction is required");
         }

         log.info(String.format("REQUIRES-NEW - Created transaction: %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));

         org.rogueware.jta.UserTransaction cdiUt = (org.rogueware.jta.UserTransaction) ut;
         transactionBean.requiresNew(cdiUt.getTransaction().toString());
      } catch (SystemException ex) {
         log.error("REQUIRES-NEW - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("REQUIRES-NEW - Error occured while determining status of transaction", ex);
      }
   }

   public void supports() {
      try {
         // Make sure there is an active transaction
         if (ut.getStatus() != Status.STATUS_ACTIVE) {
            log.error("SUPPORTS - An active transaction is required");
            throw new InternalServerErrorException("SUPPORTS - An active transaction is required");
         }

         log.info(String.format("SUPPORTS - Created transaction: %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));

         org.rogueware.jta.UserTransaction cdiUt = (org.rogueware.jta.UserTransaction) ut;
         transactionBean.supports(cdiUt.getTransaction().toString());
      } catch (SystemException ex) {
         log.error("SUPPORTS - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("SUPPORTS - Error occured while determining status of transaction", ex);
      }
   }

   public void notSupported() {
      try {
         // Make sure there is an active transaction
         if (ut.getStatus() != Status.STATUS_ACTIVE) {
            log.error("NOT_SUPPORTED - An active transaction is required");
            throw new InternalServerErrorException("NOT_SUPPORTED - An active transaction is required");
         }

         log.info(String.format("NOT_SUPPORTED - Created transaction: %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));

         org.rogueware.jta.UserTransaction cdiUt = (org.rogueware.jta.UserTransaction) ut;
         transactionBean.notSupported(cdiUt.getTransaction().toString());
      } catch (SystemException ex) {
         log.error("NOT_SUPPORTED - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("NOT_SUPPORTED - Error occured while determining status of transaction", ex);
      }
   }

   public void never() {
      try {
         // Make sure there is an active transaction
         if (ut.getStatus() != Status.STATUS_ACTIVE) {
            log.error("NEVER - An active transaction is required");
            throw new InternalServerErrorException("NEVER - An active transaction is required");
         }

         log.info(String.format("NEVER - Created transaction: %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));

         try {
            org.rogueware.jta.UserTransaction cdiUt = (org.rogueware.jta.UserTransaction) ut;
            transactionBean.never(cdiUt.getTransaction().toString());

            log.info("NEVER - Existing transaction did not cause an exception");
            throw new InternalServerErrorException("NEVER - Existing transaction did not cause an exception");
         } catch (TransactionalException ex) {
            // Yup, what we want :)
         }
      } catch (SystemException ex) {
         log.error("NOT_SUPPORTED - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("NOT_SUPPORTED - Error occured while determining status of transaction", ex);
      }
   }
   
   public void mandatory() {
      try {
         // Make sure there is an active transaction
         if (ut.getStatus() != Status.STATUS_ACTIVE) {
            log.error("MANDATORY - An active transaction is required");
            throw new InternalServerErrorException("MANDATORY - An active transaction is required");
         }

         log.info(String.format("MANDATORY - Created transaction: %s - %s ", TransactionStatus.getCurrentTransactionId(), TransactionStatus.getCurrentTransactionStatusString()));

         org.rogueware.jta.UserTransaction cdiUt = (org.rogueware.jta.UserTransaction) ut;
         transactionBean.mandatory(cdiUt.getTransaction().toString());
      } catch (SystemException ex) {
         log.error("MANDATORY - Error occured while determining status of transaction", ex);
         throw new InternalServerErrorException("MANDATORY - Error occured while determining status of transaction", ex);
      }
   }   
   
}
