/*
 * SecurityDenyAllBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.security.bean;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
@DenyAll
@RequestScoped
public class SecurityDenyAllBean implements SecurityInterface {

   private transient static final Logger log = LoggerFactory.getLogger(SecurityDenyAllBean.class);

   @PermitAll
   @Override
   public void permitAll() {

   }

   @Override
   public void denyAll() {

   }

   @RolesAllowed({"roleA", "roleC"})  // Also test on class
   @Override
   public void rolesAllowed() {

   }
}
