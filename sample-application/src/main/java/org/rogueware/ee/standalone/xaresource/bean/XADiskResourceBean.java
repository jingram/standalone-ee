/*
 * XAResourceRestfulResource.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.xaresource.bean;

import java.io.File;
import java.io.PrintStream;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import javax.ws.rs.InternalServerErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xadisk.additional.XAFileOutputStreamWrapper;
import org.xadisk.bridge.proxies.interfaces.XASession;
import org.xadisk.filesystem.exceptions.FileAlreadyExistsException;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Transactional(value = Transactional.TxType.REQUIRED, rollbackOn = {ResourceRollbackException.class})
@Named
public class XADiskResourceBean {

   private transient static final Logger log = LoggerFactory.getLogger(XADiskResourceBean.class);

   private final File helloFile = new File("target/xaResource/XADisk/hello.txt");
   private final File goodbyeFile = new File("target/xaResource/XADisk/goodbye.txt");

   @Inject
   private XASession xaDisk;

   public void createAndCommitResources() {
      try {
         xaDisk.createFile(new File("target/xaResource"), true);
         xaDisk.createFile(new File("target/xaResource/XADisk"), true);
         xaDisk.createFile(new File("target/xaResource/XADisk/touched.txt"), false);

         xaDisk.createFile(helloFile, false);
         try (XAFileOutputStreamWrapper is = new XAFileOutputStreamWrapper(xaDisk.createXAFileOutputStream(helloFile, false))) {
            PrintStream ps = new PrintStream(is);
            ps.append("Hello World\n");
            ps.append("This file has been created within a transaction");
         }

         xaDisk.createFile(goodbyeFile, false);
         try (XAFileOutputStreamWrapper is = new XAFileOutputStreamWrapper(xaDisk.createXAFileOutputStream(goodbyeFile, false))) {
            PrintStream ps = new PrintStream(is);
            ps.append("Goodbye World\n");
            ps.append("This file has been created within a transaction");
         }
      } catch (FileAlreadyExistsException ex) {
         log.error("CREATE & COMMIT - File already exists", ex);
         throw new InternalServerErrorException("CREATE & COMMIT - File already exists", ex);
      } catch (Exception ex) {
         log.error("CREATE & COMMIT - Unable to create and commit", ex);
         throw new InternalServerErrorException("CREATE & COMMIT - Unable to create and commit", ex);
      }
   }

   @Transactional(value = Transactional.TxType.NOT_SUPPORTED)
   public boolean validateCommitedResources() {
      return (helloFile.exists() && !goodbyeFile.exists());
   }

}
