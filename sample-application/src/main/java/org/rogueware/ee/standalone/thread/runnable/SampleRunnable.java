/*
 * RunAsRunnable.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.thread.runnable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import org.rogueware.ee.standalone.security.bean.SecurityDenyAllBean;
import org.rogueware.ee.standalone.security.bean.SecurityInterface;
import org.rogueware.ee.standalone.security.bean.SecurityPermitAllBean;
import org.rogueware.ee.standalone.security.bean.SecurityRolesAllowedBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@RequestScoped
public class SampleRunnable implements Runnable {

   private static final Logger log = LoggerFactory.getLogger(SampleRunnable.class);

   @Inject
   private SecurityPermitAllBean allowAllBean;

   @Inject
   private SecurityDenyAllBean denyAllBean;

   @Inject
   private SecurityRolesAllowedBean rolesAllowedBean;

   @Override
   public void run() {
      try {
         testBean(allowAllBean);

         testBean(denyAllBean);

         testBean(rolesAllowedBean);

      } catch (Exception ex) {
         throw new IllegalStateException("Thread test failed", ex);
      }
   }

   private void testBean(SecurityInterface bean) throws Exception {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting Security PermitAll");
      log.info("-------------------------------------------------------------------------");
      bean.permitAll();
      log.info("-------------------------------------------------------------------------");

      log.info("-------------------------------------------------------------------------");
      log.info("Starting Security RolesAllowed");
      log.info("-------------------------------------------------------------------------");
      bean.rolesAllowed();
      log.info("-------------------------------------------------------------------------");

      log.info("-------------------------------------------------------------------------");
      log.info("Starting Security DenyAll");
      log.info("-------------------------------------------------------------------------");
      try {
         bean.denyAll();
         throw new Exception("DenyAll did not throw NotAuthorizedException exception");
      } catch (NotAuthorizedException ex) {
      }
      log.info("-------------------------------------------------------------------------");
   }

}
