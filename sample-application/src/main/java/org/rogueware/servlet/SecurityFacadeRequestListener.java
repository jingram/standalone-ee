/*
 * ContextThreadReferenceRequestListener.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.servlet;

import org.rogueware.cdi.runnable.security.SecurityFacade;
import java.lang.reflect.Method;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class SecurityFacadeRequestListener implements ServletRequestListener {
   private static final Logger log = LoggerFactory.getLogger(SecurityFacadeRequestListener.class);

   @Override
   public void requestDestroyed(ServletRequestEvent sre) {
      SecurityFacade.setSecurityFacade(null); // Destroy the reference      
      log.trace(String.format("Cleared security facade from thread id %d", Thread.currentThread().getId()));
   }

   @Override
   public void requestInitialized(ServletRequestEvent sre) {
      HttpServletRequest hsr = (HttpServletRequest)sre.getServletRequest();
      Method isUserInRole = null;
      try {
         // Add the security facade thread var to the thread
         isUserInRole = hsr.getClass().getMethod("isUserInRole", String.class);
      } catch (Exception ex) {
      }
      
      SecurityFacade.setSecurityFacade(new SecurityFacade(hsr, isUserInRole, hsr.getUserPrincipal()));
      log.trace(String.format("Injected security facade into thread id %d", Thread.currentThread().getId()));
   }
}
