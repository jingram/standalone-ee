/*
 * TimerServiceRESTFulTransactionTest.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.tests;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.rogueware.ee.standalone.App;
import org.rogueware.ee.standalone.rest.client.TransactionRestClient;
import org.rogueware.ee.standalone.rest.client.XAResourceRestClient;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestRESTFul {

   @BeforeClass
   public static void init() throws Exception {
      // Cleanup XA resource dir
      FileUtils.deleteDirectory(new File("target/xaResource"));

      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.INFO);

      Logger jtaLogger = (Logger) LoggerFactory.getLogger("org.rogueware.cdi.interceptor.transaction");
      jtaLogger.setLevel(Level.TRACE);

      Logger timerLogger = (Logger) LoggerFactory.getLogger("org.rogueware.ee.standalone.timer");
      timerLogger.setLevel(Level.TRACE);

      System.setProperty("ConfigFile", "src/test/resources/configuration.xml");
      App.init();
   }

   @AfterClass
   public static void shutdown() {
      App.shutdown();
   }

   //*******************************************************************
   // Transactions
   //*******************************************************************   
   @Test
   public void testRequired() throws Exception {
      try (TransactionRestClient client = new TransactionRestClient("http://localhost:8080", false)) {
         client.required();
      }
   }

   @Test
   public void testRequiresNew() throws Exception {
      try (TransactionRestClient client = new TransactionRestClient("http://localhost:8080", false)) {
         client.requiresNew();
      }
   }

   @Test
   public void testSupports() throws Exception {
      try (TransactionRestClient client = new TransactionRestClient("http://localhost:8080", false)) {
         client.supports();
      }
   }

   @Test
   public void notSupported() throws Exception {
      try (TransactionRestClient client = new TransactionRestClient("http://localhost:8080", false)) {
         client.notSupported();
      }
   }

   @Test
   public void never() throws Exception {
      try (TransactionRestClient client = new TransactionRestClient("http://localhost:8080", false)) {
         client.never();
      }
   }

   @Test
   public void mandatory() throws Exception {
      try (TransactionRestClient client = new TransactionRestClient("http://localhost:8080", false)) {
         client.mandatory();
      }
   }

   //*******************************************************************
   // XA Resources
   //*******************************************************************   
   @Test
   public void testCommit() throws Exception {
      try (XAResourceRestClient client = new XAResourceRestClient("http://localhost:8080", false)) {
         client.commit();
      }
   }

}
