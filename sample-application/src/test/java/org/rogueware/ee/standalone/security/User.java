/*
 * User.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.security;

import java.lang.reflect.Method;
import java.util.List;
import static org.junit.Assert.fail;
import org.rogueware.cdi.runnable.security.RolePrincipal;
import org.rogueware.cdi.runnable.security.SecurityFacade;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class User {

   String username;
   List<String> roles;

   public User(String username, List<String> roles) {
      this.username = username;
      this.roles = roles;
   }

   public boolean isUserInRole(String role) {
      return roles.contains(role);
   }

   public SecurityFacade getSecurityFacade() {
      try {
         Method m = User.class.getDeclaredMethod("isUserInRole", String.class);
         SecurityFacade sf = new SecurityFacade(this, m, new RolePrincipal(username));
         return sf;
      } catch (Exception ex) {
         fail("Unable to create SecurityFacade");
         return null;
      }
   }
}
