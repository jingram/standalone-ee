/*
 * SecurityRunAsTest.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.security;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.AfterClass;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;
import org.rogueware.cdi.runnable.CDIRunnable;
import org.rogueware.ee.standalone.App;
import org.rogueware.ee.standalone.thread.runnable.SampleRunnable;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class SecurityTest {

   private  Throwable threadEx = null;

   @BeforeClass
   public static void init() throws Exception {
      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.INFO);

      Logger securityLogger = (Logger) LoggerFactory.getLogger("org.rogueware.cdi.interceptor.security");
      securityLogger.setLevel(Level.TRACE);

      System.setProperty("ConfigFile", "src/test/resources/configuration.xml");
      App.initConfig();
      App.initCDIContainer();
      
   }

   @AfterClass
   public static void shutdown() {
      App.shutdown();
   }

   @Test
   public void testRunnableRoleA() throws Exception {
      threadEx = null;

      Runnable r = CDIRunnable.create(SampleRunnable.class, "roleA");
      Thread t = new Thread(r);
      t.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {

         @Override
         public void uncaughtException(Thread t, Throwable e) {
            if (e instanceof IllegalStateException) {
               threadEx = e.getCause();
            } else {
               threadEx = e;
            }
         }
      });
      t.start();
      t.join();

      if (null != threadEx) {
         fail(String.format("Thread test failed with error %s", threadEx.getMessage()));
      }
   }

   @Test
   public void testRunnableRoleAOwnAuthMechanism() throws Exception {
      threadEx = null;

      Runnable r = CDIRunnable.create(SampleRunnable.class, new User("mrMe", new ArrayList<>(Arrays.asList("Admin", "roleA"))).getSecurityFacade() );
      Thread t = new Thread(r);
      t.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {

         @Override
         public void uncaughtException(Thread t, Throwable e) {
            if (e instanceof IllegalStateException) {
               threadEx = e.getCause();
            } else {
               threadEx = e;
            }
         }
      });
      t.start();
      t.join();

      if (null != threadEx) {
         fail(String.format("Thread test failed with error %s", threadEx.getMessage()));
      }
   }   
   
   
   @Test
   public void testRunnableRoleB() throws Exception {
      threadEx = null;

      Runnable r = CDIRunnable.create(SampleRunnable.class, "roleB");   // RoleB not allowed for rolesAllowable method
      Thread t = new Thread(r);
      t.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {

         @Override
         public void uncaughtException(Thread t, Throwable e) {
            if (e instanceof IllegalStateException) {
               threadEx = e.getCause();
            } else {
               threadEx = e;
            }
         }
      });
      t.start();
      t.join();

      if (null == threadEx || !(threadEx instanceof javax.ws.rs.NotAuthorizedException)) {
         fail("Thread test was supposed to faile with unauthorised!!!");
      }
   }
}
