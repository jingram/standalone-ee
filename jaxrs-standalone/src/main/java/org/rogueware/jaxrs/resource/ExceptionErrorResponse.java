/*
 * ExceptionErrorResponse.java
 * 
 * Defines a class used to wrap exceptions for RESTful communications
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jaxrs.resource;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class ExceptionErrorResponse implements Serializable {

   private String klass;
   private String message;

   public ExceptionErrorResponse() {
   }

   public ExceptionErrorResponse(Exception ex) {
      this.message = ex.getMessage();
      this.klass = ex.getClass().getName();
   }

   public Exception getException() throws Exception {
      try {
         Class exceptionKlass = Class.forName(klass);

         // Get the correct constructor to construct our class
         Class[] constructorArgsClass = new Class[]{String.class};
         Constructor constructor = exceptionKlass.getConstructor(constructorArgsClass);

         // Instantiate the class with correct constructor
         Object[] exceptionConstructorArgs = {message};
         Exception ex = (Exception) constructor.newInstance(exceptionConstructorArgs);
         return ex;
      } catch (Throwable ex) {
         // Unable to load the class
         throw new Exception("Unable to instantiate class '" + klass + "'", ex);
      }
   }

   public String getMessage() {
      return message;
   }

   public void setMessage(String message) {
      this.message = message;
   }

   public String getKlass() {
      return klass;
   }

   public void setKlass(String klass) {
      this.klass = klass;
   }
}
