/*
 * IntegerWrapper.java
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jaxrs.wrappers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegerWrapper {

   @XmlElement(name = "int")
   private int integer;

   public IntegerWrapper() {
   }

   public IntegerWrapper(int integer) {
      this.integer = integer;
   }

   public Integer getInteger() {
      return integer;
   }

   public void setInteger(int integer) {
      this.integer = integer;
   }
}
