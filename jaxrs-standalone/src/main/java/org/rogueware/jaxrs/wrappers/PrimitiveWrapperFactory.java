/*
 * PrimitiveWrapperFactory.java
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jaxrs.wrappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class PrimitiveWrapperFactory {

   public static Response createResponse(Object val) {
      Object res = buildResult(val);

      // Finally return the object as it is
      return Response.ok().entity(res).build();
   }

   public static Response createResponse(ResponseBuilder rb, Object val) {
      Object res = buildResult(val);

      // Finally return the object as it is
      return rb.entity(res).build();
   }

   private static Object buildResult(Object val) {
      Object res = val;
      if (val instanceof String) {
         res = new StringWrapper((String) val);
      } else if (val instanceof Double || val instanceof Float) {
         res = new DoubleWrapper(((Number) val).doubleValue());
      } else if (val instanceof Long) {
         res = new LongWrapper(((Number) val).longValue());
      } else if (val instanceof Integer || val instanceof Short || val instanceof Character || val instanceof Byte) {
         res = new IntegerWrapper(((Number) val).intValue());
      } else if (val instanceof Boolean) {
         res = new BooleanWrapper((boolean) val);
      }
      return res;
   }

   public static <T> T parseResponse(Class<T> klass, Response r) {
      try {
         if (klass.getName().equals(String.class.getName())) {
            return (T) r.readEntity(StringWrapper.class).getString();
         } else if (klass.getName().equals(Double.class.getName()) || klass.getName().equals(double.class.getName())
               || klass.getName().equals(Float.class.getName()) || klass.getName().equals(float.class.getName())) {
            return (T) r.readEntity(DoubleWrapper.class).getDouble();
         } else if (klass.getName().equals(Long.class.getName()) || klass.getName().equals(long.class.getName())) {
            return (T) r.readEntity(LongWrapper.class).getLong();
         } else if (klass.getName().equals(Integer.class.getName()) || klass.getName().equals(int.class.getName())
               || klass.getName().equals(Short.class.getName()) || klass.getName().equals(short.class.getName())
               || klass.getName().equals(Character.class.getName()) || klass.getName().equals(char.class.getName())
               || klass.getName().equals(Byte.class.getName()) || klass.getName().equals(byte.class.getName())) {
            return (T) r.readEntity(IntegerWrapper.class).getInteger();
         } else if (klass.getName().equals(Boolean.class.getName()) || klass.getName().equals(boolean.class.getName())) {
            return (T) r.readEntity(BooleanWrapper.class).getBoolean();
         }

         return null;
      } catch (Exception ex) {
         return null;
      }
   }
}
