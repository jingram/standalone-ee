/*
 * ErrorResponseHelper.java
 * 
 * Defines a class used to wrap exceptions for RESTful communications
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jaxrs.client;

import javax.ws.rs.core.Response;
import org.rogueware.jaxrs.resource.ExceptionErrorResponse;

/**
 *
 * @author Tony Abbott (tabbott@rogueware.org)   
 * @author Johnathan Ingram (jingram@rogueware.org)   
 */
public class ErrorResponseHelper {

   private final Response response;
   private final boolean success;
   private final Exception appException;
   private final Exception decodeException;

   public ErrorResponseHelper(Response response) {
      this.response = response;
      success = Response.Status.Family.SUCCESSFUL == response.getStatusInfo().getFamily();
      Exception ae = null;
      Exception de = null;
      if (!success) {
         try {
            ExceptionErrorResponse er = response.readEntity(ExceptionErrorResponse.class);
            ae = er.getException();
         } catch (Exception ex) {
            de = ex;
         }
      }
      appException = ae;
      decodeException = de;
   }

   public <T extends Exception> void checkAndRaiseSingle(Class<T> exceptionClass) throws T, RemoteRestException {
      checkAndRaise(exceptionClass);
      checkAndRaiseOther();
   }

   public <T extends Exception> ErrorResponseHelper checkAndRaise(Class<T> exceptionClass) throws T, RemoteRestException {
      if (!success) {
         // Handle if could not decode the exception
         if (null == appException) {
            throw new RemoteRestException(response.getStatus(), response.getStatusInfo().getReasonPhrase());
         }

         // Did decode an exception
         if (exceptionClass.isAssignableFrom(appException.getClass())) {
            throw (T) appException;
         }
      }
      return this;
   }

   public void checkAndRaiseOther() throws RemoteRestException {
      if (!success) {
         if (null != appException) {
            // it's an exception, but not of type T, so wrap it in a RemoteRestException
            throw new RemoteRestException(response.getStatus(), response.getStatusInfo().getReasonPhrase(), appException);
         } else {
            // can't decode an exception from the response
            throw new RemoteRestException(response.getStatus(), response.getStatusInfo().getReasonPhrase(), decodeException);
         }
      }
   }
}
