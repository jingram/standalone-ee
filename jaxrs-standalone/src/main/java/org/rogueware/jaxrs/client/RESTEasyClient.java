/*
 * RESTEasyClient.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jaxrs.client;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class RESTEasyClient {
   
      
   public static boolean isRuntimeDelegateAvailable() {
      // Check if the RESTEasy runtime delegate is available and if so set system property
      // See META-INF/services/javax.ws.rs.client.ClientBuilder in lib files
      try {
         Class c = Class.forName("org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder");
         
         // Using RESTEasy then :)
         System.setProperty("javax.ws.rs.ext.RuntimeDelegate", "org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder");
         return true;
      } catch(ClassNotFoundException ex) {
         return false;
      }
   }
   
   public static Object createBasicAuthentication(String username, String password) {
      return new org.jboss.resteasy.client.jaxrs.BasicAuthentication(username, password);
   }   
}
