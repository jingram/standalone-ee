/*
 * ErrorResponseHelper.java
 * 
 * Defines an abstract class for RESTFul client classes to inherit basic
 *   functionality from
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jaxrs.client;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public abstract class AbstractRestClient implements AutoCloseable {

   public enum ClientLibrary {

      JERSEY,
      RESTEASY,
      UNKNOWN
   }

   protected ClientLibrary clientLibrary = ClientLibrary.UNKNOWN;

   protected final String baseUri;
   protected final String path;

   protected final boolean useSsl;
   protected String username;
   protected String password;
   protected Integer connectTimeout;
   protected Integer readTimeout;

   protected final Client client;
   protected final WebTarget webTarget;

   protected AbstractRestClient(String baseUri, String path) throws MalformedURLException, UnknownHostException {
      this(baseUri, path, false, new Object[0]);
   }   
   
   protected AbstractRestClient(String baseUri, String path, Object... providers) throws MalformedURLException, UnknownHostException {
      this(baseUri, path, false, providers);
   }

   protected AbstractRestClient(String baseUri, String path, boolean useSsl) throws MalformedURLException, UnknownHostException {
      this(baseUri, path, false, new Object[0]);
   }

   protected AbstractRestClient(String baseUri, String path, boolean useSsl, Object... providers) throws MalformedURLException, UnknownHostException {
      if (JerseyClient.isRuntimeDelegateAvailable()) {
         clientLibrary = ClientLibrary.JERSEY;
      } else if (RESTEasyClient.isRuntimeDelegateAvailable()) {
         clientLibrary = ClientLibrary.RESTEASY;
      }

      this.baseUri = baseUri;
      this.path = path;
      this.useSsl = useSsl;
      // SSL Workaround (Rewrite URI hostname to ip so SSL does not worry about hostname)
      if (useSsl) {
         baseUri = rewriteUrlToIp(baseUri);
      }

      ClientBuilder cb = ClientBuilder.newBuilder();
      if (useSsl) {
         client = cb
               .sslContext(getSSLContext())
               .hostnameVerifier(getHostnameVerifier())
               .build();
      } else {
         client = cb
               .build();
      }

      // Register any providers with the client
      for (Object provider : providers) {
         client.register(provider);
      }
      
      webTarget = client.target(baseUri).path(path);
      setConnectionTimeoutMs(5000);
      setReadTimeoutMs(30000);
   }

   public void setUsernamePassword(String username, String password) {
      this.username = username;
      this.password = password;

      switch (clientLibrary) {
         case JERSEY:
            webTarget.register(JerseyClient.createBasicAuthentication(username, password));
            break;
         case RESTEASY:
            webTarget.register(RESTEasyClient.createBasicAuthentication(username, password));
            break;
         default:
            this.username = null;
            this.password = null;
            break;
      }
   }

   public void setConnectionTimeoutMs(int timeout) {
      switch (clientLibrary) {
         case JERSEY:
            this.connectTimeout = timeout;
            webTarget.property("jersey.config.client.connectTimeout", timeout);
            break;
         default:
            this.connectTimeout = -1;  // Unable to set !!!
            break;
      }
   }

   public void setReadTimeoutMs(int timeout) {
      switch (clientLibrary) {
         case JERSEY:
            this.readTimeout = timeout;
            webTarget.property("jersey.config.client.readTimeout", timeout);
            break;
         default:
            this.connectTimeout = -1;  // Unable to set !!!
      }
   }

   @Override
   public void close() throws Exception {
      if (null != client) {
         client.close();
      }
   }

   private HostnameVerifier getHostnameVerifier() {
      return new HostnameVerifier() {
         @Override
         public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
            return true;
         }
      };
   }

   private SSLContext getSSLContext() {
      javax.net.ssl.TrustManager x509 = new javax.net.ssl.X509TrustManager() {
         @Override
         public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws java.security.cert.CertificateException {
         }

         @Override
         public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1) throws java.security.cert.CertificateException {
         }

         @Override
         public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
         }
      };
      SSLContext ctx = null;
      try {
         ctx = SSLContext.getInstance("SSL");
         ctx.init(null, new javax.net.ssl.TrustManager[]{
            x509
         }, null);
      } catch (java.security.GeneralSecurityException ex) {
      }
      return ctx;
   }

   private String rewriteUrlToIp(String url) throws MalformedURLException, UnknownHostException {
      // SSL issues: (Work around for self signed certs to use IP and not host)
      URL parse = new URL(url);
      String host = parse.getHost();

      InetAddress address = InetAddress.getByName(host);

      String newUrl = url.replace(host, address.getHostAddress());
      return newUrl;
   }
}
