/*
 * RestInvocationHandler.java
 * 
 * Defines a class used intercept interfaces calls and proxy them onto a RESTful service
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.jaxrs.client;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.ServerException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.rogueware.jaxrs.wrappers.PrimitiveWrapperFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class RestInvocationHandler extends AbstractRestClient implements InvocationHandler {

   public RestInvocationHandler(String baseUri, String path, boolean useSsl) throws MalformedURLException, UnknownHostException {
      super(baseUri, path, useSsl);
   }

   @Override
   public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
      // Called for all methods on the interface this proxy object wraps :)
      // Distinguish between getters and setters
      if (method.getName().startsWith("get") || method.getName().startsWith("is")) {
         return invokeGetter(method, args);
      } else if (method.getName().startsWith("set")) {
         // TODO: Handle setters
      }

      // Invoke any methods on the rest client object instance
      return method.invoke(this, args);
   }

   private Object invokeGetter(Method method, Object[] args) throws Throwable {
      String methodPath = method.getName();
      if (method.getName().startsWith("get")) {
         methodPath = method.getName().substring(3);
      }
      if (method.getName().startsWith("is")) {
         methodPath = method.getName().substring(2);
      }

      if (method.getReturnType().isInterface()) {
         // Create a proxy for the new interface
         RestInvocationHandler ih = new RestInvocationHandler(baseUri, path + "/" + methodPath, useSsl);
         if (null != username && null != password) {
            ih.setUsernamePassword(username, password);
         }
         if (null != readTimeout) {
            ih.setReadTimeoutMs(readTimeout);
         }
         if (null != connectTimeout) {
            ih.setConnectionTimeoutMs(connectTimeout);
         }

         Object proxy = Proxy.newProxyInstance(
               method.getReturnType().getClassLoader(),
               new Class[]{method.getReturnType(), AutoCloseable.class},
               ih);
         return proxy;
      } else {
         // Perform the RESTful call and parse result correctly
         methodPath = methodPath.substring(0, 1).toUpperCase() + methodPath.substring(1);
         Response response = webTarget.path(methodPath).request(MediaType.APPLICATION_JSON).get();

         // Handle null being returned in response
         if (204 == response.getStatus()) {
            return null;
         }

         // Raise the exception if a ServerException occured
         new ErrorResponseHelper(response).checkAndRaiseSingle(ServerException.class);

         // If we returning an interface, return another proxy object to handle requests
         return PrimitiveWrapperFactory.parseResponse(method.getReturnType(), response);
      }
   }
}
