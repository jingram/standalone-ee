/*
 * Defines a class used as a test case
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.automation.home.rest.resource;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.eclipse.persistence.jaxb.JAXBContextProperties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.rogueware.jaxrs.client.ErrorResponseHelper;
import org.rogueware.jaxrs.resource.ExceptionErrorResponse;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class RestExceptionTest {

   @Test
   public void testEncodeDecode() throws Exception {
      XException x = new XException("My test exception");

      ExceptionErrorResponse wrap = new ExceptionErrorResponse(x);

      // To and from JSON
      ByteArrayOutputStream os = new ByteArrayOutputStream(16 * 1024);

      Map<String, Object> properties = new HashMap<>();
      properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
      properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, true);

      // Force using Moxy provider (Dont need jaxb.properties)
      JAXBContext jc = org.eclipse.persistence.jaxb.JAXBContextFactory.createContext(new Class[]{ExceptionErrorResponse.class}, properties);      
      
      Marshaller marshaller = jc.createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      marshaller.marshal(wrap, os);

      String json = new String(os.toByteArray());
      assertEquals("Exception did not serialize to JSON", "{\n" +
"   \"exceptionErrorResponse\" : {\n" +
"      \"klass\" : \"org.rogueware.automation.home.rest.resource.XException\",\n" +
"      \"message\" : \"My test exception\"\n" +
"   }\n" +
"}", json.replaceAll("\r", ""));


      BufferedInputStream is = new BufferedInputStream(new ByteArrayInputStream(os.toByteArray()));
      
      // Create a response object from the json
      Response r = new MockupMoxyResponse(Response.noContent().status(500).entity(is).type(MediaType.APPLICATION_JSON).build() );  

       
       
      try {
         new ErrorResponseHelper(r).checkAndRaiseSingle(XException.class);
         fail("No exception raised, expected XException to be raised");
         
      } catch (XException ex) {
         // Ok
      } catch (Throwable ex) {
         fail("Expected XException to be raised");
      }
   }
}
