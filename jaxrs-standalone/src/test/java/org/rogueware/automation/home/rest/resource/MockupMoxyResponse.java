/*
 * Defines a class used as a mockup for the JAX-RS Response class
 * 
 * Copyright 2013 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.automation.home.rest.resource;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.eclipse.persistence.jaxb.JAXBContextProperties;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class MockupMoxyResponse extends Response {
   private Response r;

   public MockupMoxyResponse(Response r) {
      this.r = r;
   }

   @Override
   public int getStatus() {
      return r.getStatus();
   }

   @Override
   public StatusType getStatusInfo() {
      return r.getStatusInfo();
   }

   @Override
   public Object getEntity() {
      return r.getEntity();
   }

   @Override
   public <T> T readEntity(Class<T> entityType) {
      Object o = r.getEntity();

      if (entityType.isInstance(o)) {
         return (T) o;
      }

      if (r.getEntity() instanceof String) {
         o = new BufferedInputStream(new ByteArrayInputStream(((String) o).getBytes()));
      }

      if (o instanceof InputStream) {
         InputStream is = (InputStream) r.getEntity();

         Map<String, Object> properties = new HashMap<>();
         properties.put(JAXBContextProperties.MEDIA_TYPE, r.getMediaType().toString());

         // Force using Moxy provider (Dont need jaxb.properties)
         try {
            JAXBContext jc = org.eclipse.persistence.jaxb.JAXBContextFactory.createContext(new Class[]{entityType}, properties);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            T t = (T) unmarshaller.unmarshal(is);
            return t;
         } catch (JAXBException ex) {
            Logger.getLogger(MockupMoxyResponse.class.getName()).log(Level.SEVERE, null, ex);
         }
      }

      throw new ProcessingException("Unable to decode the entity using the mockup request");
   }

   @Override
   public <T> T readEntity(GenericType<T> entityType) {
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   }

   @Override
   public <T> T readEntity(Class<T> entityType, Annotation[] annotations) {
      return readEntity(entityType);
   }

   @Override
   public <T> T readEntity(GenericType<T> entityType, Annotation[] annotations) {
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   }

   @Override
   public boolean hasEntity() {
      return r.hasEntity();
   }

   @Override
   public boolean bufferEntity() {
      return r.bufferEntity();
   }

   @Override
   public void close() {
      r.close();
   }

   @Override
   public MediaType getMediaType() {
      return r.getMediaType();
   }

   @Override
   public Locale getLanguage() {
      return r.getLanguage();
   }

   @Override
   public int getLength() {
      return r.getLength();
   }

   @Override
   public Set<String> getAllowedMethods() {
      return r.getAllowedMethods();
   }

   @Override
   public Map<String, NewCookie> getCookies() {
      return r.getCookies();
   }

   @Override
   public EntityTag getEntityTag() {
      return r.getEntityTag();
   }

   @Override
   public Date getDate() {
      return r.getDate();
   }

   @Override
   public Date getLastModified() {
      return r.getLastModified();
   }

   @Override
   public URI getLocation() {
      return r.getLocation();
   }

   @Override
   public Set<Link> getLinks() {
      return r.getLinks();
   }

   @Override
   public boolean hasLink(String relation) {
      return r.hasLink(relation);
   }

   @Override
   public Link getLink(String relation) {
      return r.getLink(relation);
   }

   @Override
   public Link.Builder getLinkBuilder(String relation) {
      return r.getLinkBuilder(relation);
   }

   @Override
   public MultivaluedMap<String, Object> getMetadata() {
      return r.getMetadata();
   }

   @Override
   public MultivaluedMap<String, String> getStringHeaders() {
      return r.getStringHeaders();
   }

   @Override
   public String getHeaderString(String name) {
      return r.getHeaderString(name);
   }
}
