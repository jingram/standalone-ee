/*
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class AssignableFromTest {
   
   @Test
   public void sameClass() {
      SuperException exA = new SuperException();
      SuperException exB = new SuperException();
      
      assertTrue("exA should be assignable from exB", exA.getClass().isAssignableFrom(exB.getClass()));
      assertTrue("exB should be assignable from exA", exB.getClass().isAssignableFrom(exA.getClass()));
      
   }

   @Test
   public void childClass() {
      ChildException exChild = new ChildException();
      SuperException exSuper = new SuperException();
      
      
      assertTrue("exSuper should be assignable from exChild", exSuper.getClass().isAssignableFrom(exChild.getClass()));
      assertTrue("exChild cannot be assignable from exSuper", !exChild.getClass().isAssignableFrom(exSuper.getClass()));
   }
   
}
