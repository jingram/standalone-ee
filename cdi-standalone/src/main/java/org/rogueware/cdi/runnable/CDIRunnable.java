/*
 * CDIRunnable.java
 * 
 * Defines a class to wrap a runnable in a CDI context for runnable to work in a 
 *   CDI container.
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.runnable;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import org.rogueware.cdi.runnable.security.RolePrincipal;
import org.rogueware.cdi.runnable.security.SecurityFacade;
import org.rogueware.cdi.util.CDIUtil;
import org.rogueware.cdi.util.ContextualInstance;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class CDIRunnable<T extends Runnable> implements InvocationHandler {

   private T runnable;
   private Class<T> runnableKlass;

   // Security
   private String runAsRole;
   private SecurityFacade sf;

   private CDIRunnable() {
   }

   private CDIRunnable(Class<T> runnableKlass) {
      this.runnableKlass = runnableKlass;
   }

   private CDIRunnable(Class<T> runnableKlass, String runAsRole) {
      this(runnableKlass);
      this.runAsRole = runAsRole;
   }

   private CDIRunnable(Class<T> runnableKlass, SecurityFacade sf) {
      this(runnableKlass);
      this.sf = sf;
   }

   public static <E extends Runnable> Runnable create(Class<E> klass) {
      // Create the Runnable proxy object
      CDIRunnable<E> instance = new CDIRunnable<>(klass);
      return createProxy(Runnable.class, instance);
   }

   public static <E extends Runnable, I extends Runnable> I create(Class<I> interfaceKlass, Class<E> klass) {
      // Create the Runnable proxy object
      CDIRunnable<E> instance = new CDIRunnable<>(klass);
      return createProxy(interfaceKlass, instance);
   }

   public static <E extends Runnable> Runnable create(Class<E> klass, String runAsRole) {
      // Create the Runnable proxy object
      CDIRunnable<E> instance = new CDIRunnable<>(klass, runAsRole);
      return createProxy(Runnable.class, instance);
   }

   public static <E extends Runnable, I extends Runnable> I create(Class<I> interfaceKlass, Class<E> klass, String runAsRole) {
      // Create the Runnable proxy object
      CDIRunnable<E> instance = new CDIRunnable<>(klass, runAsRole);
      return createProxy(interfaceKlass, instance);
   }

   public static <E extends Runnable> Runnable create(Class<E> klass, SecurityFacade sf) {
      // Create the Runnable proxy object
      CDIRunnable<E> instance = new CDIRunnable<>(klass, sf);
      return createProxy(Runnable.class, instance);
   }

   public static <E extends Runnable, I extends Runnable> I create(Class<I> interfaceKlass, Class<E> klass, SecurityFacade sf) {
      // Create the Runnable proxy object
      CDIRunnable<E> instance = new CDIRunnable<>(klass, sf);
      return createProxy(interfaceKlass, instance);
   }

   private static <E extends Runnable, I extends Runnable> I createProxy(Class<I> interfaceKlass, CDIRunnable<E> instance) {
      I proxy = (I) Proxy.newProxyInstance(
            CDIRunnable.class
            .getClassLoader(),
            new Class[]{interfaceKlass},
            instance
      );

      return proxy;
   }

   @Override
   public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

      // Intercept void run() method
      try {
         if ("run".equals(method.getName()) && 0 == method.getParameterTypes().length && method.getReturnType().equals(Void.TYPE)) {
            // Obtain the bean manager
            BeanManager bm = getBeanManager();

            // Security - RunAs (Create SecurityFacade with run as on top of stack)
            if (null != runAsRole) {
               SecurityFacade sf = new SecurityFacade();
               sf.pushRunAsRole(new RolePrincipal(runAsRole));
               SecurityFacade.setSecurityFacade(sf);
            }

            // Security - Provided security facade (Will use provided method for isUserInRole
            if (null != sf) {
               SecurityFacade.setSecurityFacade(sf);
            }

            // Create instance of runnable in CDI container context
            // Will now have an instance of runnable class type, but with injections in context 
            //   of the thread invoking run() method :)
            ContextualInstance<T> ci = CDIUtil.getContextualInstance(bm, runnableKlass);

            // Activate a RequestScoped Context for the thread
            String providerName = CDI.current().getClass().getName();

            // WELD Specific
            Map<String, Object> requestMap = new HashMap<>();
            if (providerName.startsWith("org.jboss.weld")) {
               org.jboss.weld.context.bound.BoundRequestContext requestContext = CDI.current().select(org.jboss.weld.context.bound.BoundRequestContext.class).get();
               if (!requestContext.isActive()) {
                  requestContext.associate(requestMap);
                  requestContext.activate();
               }
            } else {
               throw new IllegalThreadStateException(String.format("Unknown CDO provider %s. Please implement request scope context. See Apache DeltaSpike for help", providerName));
            }

            runnable = ci.getBean();
            runnable.run();

            // De-Activate a RequestScoped Context for the thread
            // WELD Specific
            if (providerName.startsWith("org.jboss.weld")) {
               org.jboss.weld.context.bound.BoundRequestContext requestContext = CDI.current().select(org.jboss.weld.context.bound.BoundRequestContext.class).get();
               if (requestContext.isActive()) {
                  requestContext.invalidate();
                  requestContext.deactivate();
                  requestContext.dissociate(requestMap);
                  requestMap = null;
               }
            }

            // Cleanup injections
            ci.getCreationalContext().release();
            synchronized (this) {
               runnable = null;
            }
            return null;
         } else {
            // Invoke orignal methods on runnable
            synchronized (this) {
               // Wait up to 5s for runnable to appear
               if (null == runnable) {
                  // NB NB: Can only invoke methods while run() is being called as the context of
                  //        the runnable will be requestScoped, so need to inject with running 
                  //        thread.
                  throw new IllegalThreadStateException("CDI Runnable: Methods can only be invoked while run() method is being invoked by thread");
               }
            }

            return method.invoke(runnable, args);
         }
      } catch (InvocationTargetException ex) {
         throw ex.getCause();
      }
   }

   private BeanManager getBeanManager() {
      try {
         BeanManager bm = CDI.current().getBeanManager();
         return bm;
      } catch (Exception ex) {
         throw new IllegalThreadStateException("Unable to lookup bean manager from CDI");
      }
   }
}
