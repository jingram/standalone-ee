/*
 * SecurityFacade.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.runnable.security;

import java.lang.reflect.Method;
import java.security.Principal;
import java.util.Stack;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class SecurityFacade {

   private transient static final ThreadLocal<SecurityFacade> threadSecurityFacade = new ThreadLocal<>();

   private Stack<Principal> runAsRoles = new Stack<>();

   // NOTE: method takes string param and returns boolean result
   private Principal userPrincipal;
   private Method isUserInRole;    // Scoped to whatever mechanism to test for a user in role
   private Object instance;        // Instance of mechanism to test for user in role

   public static void setSecurityFacade(SecurityFacade sf) {
      threadSecurityFacade.set(sf);
   }

   public static SecurityFacade getSecurityFacade() {
      return threadSecurityFacade.get();
   }

   public SecurityFacade() {
   }

   public SecurityFacade(Object methodInstance, Method isUserInRole) {
      this();
      if (isUserInRole.getParameterTypes().length != 1 || !isUserInRole.getParameterTypes()[0].getName().equals("java.lang.String")) {
         throw new IllegalArgumentException("isUserInRole must take a single java.lang.String argument");
      }

      if (!isUserInRole.getReturnType().getName().equals("boolean")) {
         throw new IllegalArgumentException("isUserInRole must return a boolean type");
      }

      this.instance = methodInstance;
      this.isUserInRole = isUserInRole;
   }

   public SecurityFacade(Object methodInstance, Method isUserInRole, Principal userPrincipal) {
      this(methodInstance, isUserInRole);
      this.userPrincipal = userPrincipal;
   }

   public SecurityFacade(Object methodInstance, Method isUserInRole, Principal userPrincipal, Stack<Principal> runAsRoles) {
      this(methodInstance, isUserInRole, userPrincipal);
      this.runAsRoles = runAsRoles;
   }

   public boolean isUserInRole(String role) {
      // If there are injected roles, check if role matches an injected role
      // Example: RunaAs adds to injected role
      if (runAsRoles.size() > 0 && runAsRoles.peek().getName().equals(role)) {
         return true;
      }

      if (null == isUserInRole) {
         return false;  // No way to test role
      }

      try {
         Object res = isUserInRole.invoke(instance, role);
         if (null != res) {
            return (Boolean) res;
         }
      } catch (Exception ex) {
      }
      return false;
   }

   public void pushRunAsRole(Principal role) {
      runAsRoles.push(role);
   }

   public Principal popRunAsRole() {
      return runAsRoles.pop();
   }

   public Principal getUserPrincipal() {
      // Never return null, rather return anonymous
      if (null == userPrincipal) {
         // Unauthenticated or anonymous access.
         return AnonymousPrincipal.getPrincipal();
      } else {
         return userPrincipal;
      }
   }

}
