/*
 * XADiskSessionProducer.java
 * 
 * Defines a class used to inject an XADisk Session
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.producer.xadisk;

import java.io.Serializable;
import java.lang.reflect.Proxy;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xadisk.bridge.proxies.interfaces.XASession;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class XADiskSessionProducer implements Serializable {

   private static final Logger log = LoggerFactory.getLogger(XADiskSessionProducer.class);

   @Produces
   public XASession createEntityManager(InjectionPoint injectionPoint) {
      // Create the delegate to act on behald of the XA Session
      //  in context of a thread and transaction
      String instanceId = getInstanceIdFromAnnotation(injectionPoint);
      XADiskSessionDelegate xsd = new XADiskSessionDelegate(instanceId);
      XASession xaSession = (XASession) Proxy.newProxyInstance(
            XADiskSessionDelegate.class
            .getClassLoader(),
            new Class[]{XASession.class},
            xsd);
      log.trace(String.format("Created XADisk session delegate injected into %s class member %s", injectionPoint.getMember().getDeclaringClass().getName(), injectionPoint.getMember().getName()));
      return xaSession;
   }

   private String getInstanceIdFromAnnotation(InjectionPoint injectionPoint) {
      XADiskContext annotation = injectionPoint.getAnnotated().getAnnotation(XADiskContext.class);
      if (null != annotation) {
         if (null != annotation.instanceId() && 0 != annotation.instanceId().length()) {
            return annotation.instanceId();
         }
      }
      return null;
   }
}
