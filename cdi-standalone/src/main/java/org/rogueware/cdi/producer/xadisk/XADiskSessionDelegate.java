/*
 * XADiskSessionDelegate.java
 * 
 * Defines a class used to inject an XADisk Session
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.producer.xadisk;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.Synchronization;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.TransactionRequiredException;
import org.rogueware.jta.JTAStandAlone;
import org.rogueware.xadisk.XADiskStandAlone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xadisk.bridge.proxies.interfaces.XASession;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class XADiskSessionDelegate implements InvocationHandler, Serializable {
   
   private transient static final Logger log = LoggerFactory.getLogger(XADiskSessionDelegate.class);
   private transient static final ThreadLocal<Map<Integer, XASession>> threadXASessionByTransaction = new ThreadLocal<>();
   
   private String instanceId;
   
   public XADiskSessionDelegate(String instanceId) {
      this.instanceId = instanceId;
   }
   
   @Override
   public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
      XASession xaSession = getSessionForCurrentTransaction();
      try {
         return method.invoke(xaSession, args);
      } catch (InvocationTargetException ex) {
         throw ex.getCause();
      }
   }
   
   private XASession getSessionForCurrentTransaction() throws SystemException, TransactionRequiredException {
      // Assuming transactions are not being shared across threads :(
      long threadId = Thread.currentThread().getId();
      XASession xaSession = null;

      // Make sure we have the thread var for this thread managing xa sessions
      Map<Integer, XASession> xas = threadXASessionByTransaction.get();
      if (null == xas) {
         xas = new HashMap<>();
         threadXASessionByTransaction.set(xas);
      }
      
      TransactionManager tm = JTAStandAlone.getTransactionManager();
      if (null != tm && null != tm.getTransaction() && tm.getStatus() != Status.STATUS_NO_TRANSACTION) {
         int transactionId = tm.getTransaction().hashCode();
         
         if (xas.containsKey(transactionId)) {
            // return the existing em for the transaction
            xaSession = xas.get(transactionId);
            log.trace(String.format("Returning XA disk session for thread id %d and transaction id %d", threadId, transactionId));
         } else {
            // Create a xa session for the transaction
            // Register synchronization to cleanup xa session when transaction completes
            try {
               tm.getTransaction().registerSynchronization(new XADiskSessionDelegate.XASessionTransactionSynchronization(transactionId));
            } catch (IllegalStateException | RollbackException | SystemException ex) {
               log.error(String.format("Unable to register transaction synchronization for XA disk session cleanup for thread id %d and transaction id %d", threadId, transactionId));
               throw new IllegalStateException(String.format("Unable to register transaction synchronization for XA disk session cleanup for thread id %d and transaction id %d", threadId, transactionId));
            }
            
            if (null == instanceId) {
               xaSession = XADiskStandAlone.createXATransactionSession();
            } else {
               xaSession = XADiskStandAlone.createXATransactionSession(instanceId);
            }
            if (null != xas) {
               xas.put(transactionId, xaSession);
               
               log.trace(String.format("Created XA disk session for thread id %d and transaction id %d", threadId, transactionId));
            } else {
               log.warn(String.format("Unable to create XA disk session for thread id %d and transaction id %d", threadId, transactionId));
            }
         }
      } else {
         // No transaction
         throw new IllegalStateException(String.format("No transaction available to scope XA disk session for thread id %d", threadId));
      }
      
      return xaSession;
   }
   
   private class XASessionTransactionSynchronization implements Synchronization {
      
      private int transactionId;
      
      protected XASessionTransactionSynchronization(int transactionId) {
         this.transactionId = transactionId;
      }
      
      @Override
      public void beforeCompletion() {
         // Do nothing
      }
      
      @Override
      public void afterCompletion(int status) {
         // Don't care about status, just release the XA disk session associated with the transaction
         long threadId = Thread.currentThread().getId();
         
         try {
            Map<Integer, XASession> xas = threadXASessionByTransaction.get();
            if (null != xas && xas.containsKey(transactionId)) {
               XASession xaSession = xas.remove(transactionId);
               xaSession = null;
               log.trace(String.format("Deleted XA disk session for thread id %d and transaction id %s on after transaction completion", threadId, transactionId));
            }
         } catch (Throwable ex) {
            log.error(String.format("Unknown exception after transaction completion for thread id %d and transaction id %s", threadId, transactionId), ex);
         }
      }
   }
   
}
