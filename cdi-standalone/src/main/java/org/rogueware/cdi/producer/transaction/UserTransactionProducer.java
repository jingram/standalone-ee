/*
 * UserTransactionProducer.java
 * 
 * Defines a class used to inject a UserTransaction if JTA present
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.producer.transaction;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.transaction.TransactionManager;
import javax.transaction.TransactionalException;
import javax.transaction.UserTransaction;
import org.rogueware.cdi.interceptor.transaction.TransactionInterceptor;
import org.rogueware.jta.JTAStandAlone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@ApplicationScoped
public class UserTransactionProducer implements Serializable {

   protected final Logger log = LoggerFactory.getLogger(TransactionInterceptor.class);
   protected static TransactionManager tm;

   protected TransactionManager tm() throws TransactionalException {
      synchronized (TransactionInterceptor.class) {
         if (null == tm) {
            try {
               tm = JTAStandAlone.getTransactionManager();
               if (null == tm) {
                  log.error(String.format("UserTransactionProducer: Thread %s Unable to obtain TransactionManager", Thread.currentThread().getName()));
                  throw new TransactionalException("Unable to obtain TransactionManager", new Exception("Transaction Manager is null"));
               }
            } catch (Exception ex) {
               log.error(String.format("UserTransactionProducer: Thread %s Error occured obtaining TransactionManager ", Thread.currentThread().getName()), ex);
               throw new TransactionalException("Error occured obtaining TransactionManager", ex);
            }
         }
      }
      return tm;
   }

   @Produces
   public UserTransaction createUserTransaction() {
      UserTransaction ut = (UserTransaction)new org.rogueware.jta.UserTransaction(tm());
      return ut;
   }

}
