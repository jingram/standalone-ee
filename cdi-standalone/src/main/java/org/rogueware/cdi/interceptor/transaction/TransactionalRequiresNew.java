/*
 * TransactionalRequiresNew.java
 * 
 * Defines a class used to implement JTA 1.2 / CDI 1.1
 *   transaction annotations
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor.transaction;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.transaction.Status;
import javax.transaction.Transaction;
import javax.transaction.Transactional;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Transactional(Transactional.TxType.REQUIRES_NEW)
@Interceptor
public class TransactionalRequiresNew extends TransactionInterceptor {
   /* 
    *  REQUIRES_NEW
    *    If called outside a transaction context, the interceptor must begin a new JTA transaction, 
    *       the managed bean method execution must then continue inside this transaction context, 
    *       and the transaction must be completed by the interceptor.
    *
    *    If called inside a transaction context, the current transaction context will be suspended, 
    *       a new JTA transaction will begin, the managed bean method execution will then continue 
    *       inside this transaction context, the transaction will be committed, and the previously 
    *       suspended transaction will be resumed.
    */

   @AroundInvoke
   public Object transaction(InvocationContext ctx) throws Exception {
      // EE specification validation
      validateContainerEEAnnotation(ctx.getMethod().getDeclaringClass());
      
      // Start
      Transaction t = null;
      if (Status.STATUS_NO_TRANSACTION == tm().getStatus()) {
         tm().begin();
         log.trace(String.format("Transaction REQUIRES_NEW: Thread %s started new transaction %d", Thread.currentThread().getName(), transactionId()));
      }
      else {
         int originalTransactionId = transactionId();
         t = tm().suspend();
         tm().begin();
         log.trace(String.format("Transaction REQUIRES_NEW: Thread %s suspended existing transaction %d and started new transaction %d", Thread.currentThread().getName(), originalTransactionId, transactionId()));         
      }
      
      // Invoke
      Object r = null;
      Throwable ex = null;
      try {
         r = ctx.proceed();
      } catch (Throwable e) {
         ex = e;
         processException(ctx.getMethod(), ex);
      }
  
      // Complete
      completeTransaction();
      if (null != t) {
         tm().resume(t);
         log.trace(String.format("Transaction REQUIRES_NEW: Thread %s resumed suspended transaction %d", Thread.currentThread().getName(), transactionId()));
      }

      if (null != ex)
         throw (Exception)ex;
      return r;
   }
}
