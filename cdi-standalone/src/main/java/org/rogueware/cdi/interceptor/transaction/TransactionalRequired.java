/*
 * TransactionalRequired.java
 * 
 * Defines a class used to implement JTA 1.2 / CDI 1.1
 *   transaction annotations
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor.transaction;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.transaction.Status;
import javax.transaction.Transactional;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Transactional(Transactional.TxType.REQUIRED)
@Interceptor
public class TransactionalRequired extends TransactionInterceptor {
   /* 
    *  REQUIRED
    *    If called outside a transaction context, the interceptor must begin a new JTA transaction, 
    *       the managed bean method execution must then continue inside this transaction context, 
    *       and the transaction must be completed by the interceptor.
    *    If called inside a transaction context, the managed bean method execution must then 
    *       continue inside this transaction context.
    */

   @AroundInvoke
   public Object transaction(InvocationContext ctx) throws Exception {
      // EE specification validation
      validateContainerEEAnnotation(ctx.getMethod().getDeclaringClass());
      
      // Start
      boolean isNewTransaction = false;
      if (Status.STATUS_NO_TRANSACTION == tm().getStatus()) {
         tm().begin();
         isNewTransaction = true;
         
         log.trace(String.format("Transaction REQUIRED: Thread %s started new transaction %d", Thread.currentThread().getName(), transactionId()));
      }
      
      // Invoke
      Object r = null;
      Throwable ex = null;
      try {
         r = ctx.proceed();
      } catch (Throwable e) {
         ex = e;
         processException(ctx.getMethod(), ex);
      }

      // Complete
      if (isNewTransaction) {
         completeTransaction();
      }

      if (null != ex)
         throw (Exception)ex;
      return r;
   }
}
