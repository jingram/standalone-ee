/*
 * TransactionalMandatory.java
 * 
 * Defines a class used to implement JTA 1.2 / CDI 1.1
 *   transaction annotations
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor.transaction;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.transaction.Status;
import javax.transaction.TransactionRequiredException;
import javax.transaction.Transactional;
import javax.transaction.TransactionalException;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Transactional(Transactional.TxType.MANDATORY)
@Interceptor
public class TransactionalMandatory extends TransactionInterceptor {
   /* 
    *  MANDATORY
    *    If called outside a transaction context, a TransactionalException with a nested 
    *       TransactionRequiredException must be thrown.
    *
    *    If called inside a transaction context, managed bean method execution will then 
    *       continue under that context   
    */

   @AroundInvoke
   public Object transaction(InvocationContext ctx) throws Exception {
      // EE specification validation
      validateContainerEEAnnotation(ctx.getMethod().getDeclaringClass());
              
      // Start
      if (Status.STATUS_ACTIVE != tm().getStatus()) {
         log.error(String.format("Transaction MANDATORY: Thread %s no existing transaction", Thread.currentThread().getName()));
         throw new TransactionalException("No existing transaction for transactional type MANDATORY", new TransactionRequiredException("No existing transaction"));
      }

      // Invoke
      Object r = null;
      Throwable ex = null;
      try {
         r = ctx.proceed();
      } catch (Throwable e) {
         ex = e;
         processException(ctx.getMethod(), ex);
      }

      // Complete
      if (null != ex) {
         throw (Exception) ex;
      }
      return r;
   }
}
