/*
 * TransactionalNever.java
 * 
 * Defines a class used to implement JTA 1.2 / CDI 1.1
 *   transaction annotations
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor.transaction;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.transaction.InvalidTransactionException;
import javax.transaction.Status;
import javax.transaction.Transactional;
import javax.transaction.TransactionalException;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Transactional(Transactional.TxType.NEVER)
@Interceptor
public class TransactionalNever extends TransactionInterceptor {
   /* 
    *  NEVER
    *    If called outside a transaction context, managed bean method execution must then continue 
    *       outside a transaction context.
    *
    *    If called inside a transaction context, a TransactionalException with a nested 
    *       InvalidTransactionException must be thrown.
    *
    */

   @AroundInvoke
   public Object transaction(InvocationContext ctx) throws Exception {
      // EE specification validation
      validateContainerEEAnnotation(ctx.getMethod().getDeclaringClass());
      
      // Start
      if (Status.STATUS_NO_TRANSACTION != tm().getStatus()) {
         log.error(String.format("Transaction NEVER: Thread %s existing transaction %d", Thread.currentThread().getName(), transactionId()));
         throw new TransactionalException("Existing transactionfor transactional type NEVER", new InvalidTransactionException("Existing transaction"));
      }

      // Invoke
      return ctx.proceed();
   }
}
