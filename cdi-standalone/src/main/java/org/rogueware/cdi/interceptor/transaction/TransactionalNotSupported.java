/*
 * TransactionalNotSupported.java
 * 
 * Defines a class used to implement JTA 1.2 / CDI 1.1
 *   transaction annotations
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor.transaction;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.transaction.Status;
import javax.transaction.Transaction;
import javax.transaction.Transactional;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Transactional(Transactional.TxType.NOT_SUPPORTED)
@Interceptor
public class TransactionalNotSupported extends TransactionInterceptor {
   /* 
    *  NOT_SUPPORTED
    *    If called outside a transaction context, managed bean method execution must then continue 
    *       outside a transaction context.
    *
    *    If called inside a transaction context, the current transaction context must be suspended, 
    *       the managed bean method execution must then continue outside a transaction context, 
    *       and the previously suspended transaction must be resumed by the interceptor that suspended 
    *       it after the method execution has completed.
    */

   @AroundInvoke
   public Object transaction(InvocationContext ctx) throws Exception {
      // EE specification validation
      validateContainerEEAnnotation(ctx.getMethod().getDeclaringClass());

      // Start
      Transaction t = null;
      if (Status.STATUS_ACTIVE == tm().getStatus()) {
         t = tm().suspend();
         log.trace(String.format("Transaction NOT_SUPPORTED: Thread %s suspended existing transaction %d", Thread.currentThread().getName(), transactionId()));
      }

      // Invoke
      Object r = null;
      Throwable ex = null;
      try {
         r = ctx.proceed();
      } catch (Throwable e) {
         ex = e;
      }

      // Complete
      if (null != t) {
         tm().resume(t);
         log.trace(String.format("Transaction NOT_SUPPORTED: Thread %s resumed suspended transaction %d", Thread.currentThread().getName(), transactionId()));
      }

      if (null != ex)
         throw (Exception)ex;
      return r;
   }
}
