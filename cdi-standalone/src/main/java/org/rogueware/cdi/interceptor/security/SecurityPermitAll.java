/*
 * SecurityPermitAll.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor.security;

import javax.annotation.security.PermitAll;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Interceptor
@PermitAll
public class SecurityPermitAll {

   protected final Logger log = LoggerFactory.getLogger(SecurityPermitAll.class);

   /* 
    *  PermitAll
    *    no security roles are allowed to invoke the specified method(s) - i.e that the methods are to be excluded from execution in the container.
    *
    */
   @AroundInvoke
   public Object securityWrapper(InvocationContext ctx) throws Exception {
      log.trace(String.format("@PermitAll: Thread %s permitted invoking class %s member %s", Thread.currentThread().getName(), ctx.getMethod().getDeclaringClass().getName(), ctx.getMethod().getName()));
 
      // Invoke
      return ctx.proceed();
   }
}
