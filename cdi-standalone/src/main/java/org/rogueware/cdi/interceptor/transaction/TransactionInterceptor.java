/*
 * TransactionInterceptor.java
 * 
 * Defines a class used to implement JTA 1.2 / CDI 1.1
 *   transaction annotations
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor.transaction;

import java.io.Serializable;
import java.lang.reflect.Method;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.InvalidTransactionException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.Transactional;
import javax.transaction.TransactionalException;
import org.rogueware.jta.JTAStandAlone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public abstract class TransactionInterceptor implements Serializable {

   protected final static Logger log = LoggerFactory.getLogger(TransactionInterceptor.class);

   // Define members with annotations
   protected TransactionManager tm() throws TransactionalException {
      return JTAStandAlone.getTransactionManager();
   }

   protected int transactionId() {
      try {
         return (null != tm().getTransaction()) ? tm().getTransaction().hashCode() : -1;
      } catch (SystemException ex) {
         log.error(String.format("Transaction: Thread %s unable to obtain transaction id", Thread.currentThread().getName()), ex);
         return -1;
      }
   }

   protected void validateContainerEEAnnotation(Class beanClass) throws TransactionalException {
      // Make sure even though the annotation is present, the container supports it 
      // EE level specification to determine between container and bean managed 
      // Check if stateless bean has disabled bean managed transactions
      if (beanClass.isAnnotationPresent(TransactionManagement.class)) {
         TransactionManagement tman = (TransactionManagement) beanClass.getAnnotation(TransactionManagement.class);
         boolean containerManagedTransactions = tman.value() == TransactionManagementType.CONTAINER;
         if (!containerManagedTransactions) {
            log.error(String.format("Transaction: Thread %s the EE ejb class %s has a transaction annotation when its set to be bean managed", Thread.currentThread().getName(), beanClass.getName()));
            throw new TransactionalException(String.format("Ejb class %s has a transaction annotation when its set to be bean managed", beanClass.getName()),
                  new InvalidTransactionException("EJB cannot be annoted with transaction annotations if transactions are bean managed"));
         }
      }
   }

   protected void completeTransaction() throws TransactionalException {
      if (null == tm()) {
         log.error(String.format("Transaction: Thread %s no transaction manager available", Thread.currentThread().getName()));
         throw new TransactionalException("No transaction manager available", new InvalidTransactionException("No transaction manager available"));
      }

      int status;
      try {
         status = tm().getStatus();
      } catch (SystemException ex) {
         log.error(String.format("Transaction: Thread %s unable to obtain transaction status", Thread.currentThread().getName()), ex);
         throw new TransactionalException("Unable to obtain transaction status", ex);
      }

      int transactionId = transactionId();
      switch (status) {
         case Status.STATUS_ACTIVE:
            // Commit
            try {
               tm().commit();
               log.trace(String.format("Transaction: Thread %s commited transaction %d", Thread.currentThread().getName(), transactionId));
            } catch (Exception ex) {
               log.error(String.format("Transaction: Thread %s unable to commit transaction %d", Thread.currentThread().getName(), transactionId), ex);
            }
            break;
         //    case Status.STATUS_MARKED_ROLLBACK:
         // TODO: possibly need to commit transaction here???
         default:
            // Rollback
            try {
               tm().rollback();
               log.trace(String.format("Transaction: Thread %s rolledback transaction %d", Thread.currentThread().getName(), transactionId));
            } catch (Exception ex) {
               log.error(String.format("Transaction: Thread %s unable to rollback transaction %d", Thread.currentThread().getName(), transactionId), ex);
            }
            break;
      }
   }

   protected void processException(Method method, Throwable ex) throws TransactionalException {
      /*
       * By default checked exceptions do not result in the transactional interceptor marking the transaction for rollback and 
       *   instances of RuntimeException and its subclasses do.
       * 
       * The dontRollbackOn element can be set to indicate exceptions that must not cause the interceptor to mark the transaction 
       *   for rollback. Conversely, the rollbackOn element can be set to indicate exceptions that must cause the interceptor to 
       *   mark the transaction for rollback. When a class is specified for either of these elements, the designated behavior 
       *   applies to subclasses of that class as well. If both elements are specified, dontRollbackOn takes precedence.      
       */

      // Get annotation. (Method, if not found, then class)
      Transactional annotation = method.getAnnotation(Transactional.class);
      if (null == annotation) {
         annotation = method.getDeclaringClass().getAnnotation(Transactional.class);
      }

      if (null != annotation) {
         // Precedence dontRollbackOn
         for (Class c : annotation.dontRollbackOn()) {
            if (c.isAssignableFrom(ex.getClass())) {
               log.trace(String.format("Transaction: Thread %s transaction %d dontRollbackOn for exception %s", Thread.currentThread().getName(), transactionId(), ex.getClass().getName()));
               return;
            }
         }

         // rollbackOn
         for (Class c : annotation.rollbackOn()) {
            if (c.isAssignableFrom(ex.getClass())) {
               try {
                  tm().setRollbackOnly();
                  log.trace(String.format("Transaction: Thread %s transaction %d rollbackOn exception %s", Thread.currentThread().getName(), transactionId(), ex.getClass().getName()));
                  return;
               } catch (Exception ex2) {
                  log.error(String.format("Transaction: Thread %s transaction %d unable to rollback transaction for rollbackOn exception %s", Thread.currentThread().getName(), transactionId(), ex2.getClass().getName()), ex2);
                  throw new TransactionalException("Unable to rollback transaction ", ex);
               }
            }
         }
      } // end if
      
      // Default: Runtime / unchecked
      if (RuntimeException.class.isAssignableFrom(ex.getClass())) {
         try {
            tm().setRollbackOnly();
            log.trace(String.format("Transaction: Thread %s transaction %d rollback for unchecked exception %s", Thread.currentThread().getName(), transactionId(), ex.getClass().getName()));
         } catch (Exception ex2) {
            log.error(String.format("Transaction: Thread %s transaction %d unable to rollback transaction for unchecked exception %s", Thread.currentThread().getName(), transactionId(), ex2.getClass().getName()), ex2);
            throw new TransactionalException("Unable to rollback transaction ", ex);
         }
      } else {
         log.trace(String.format("Transaction: Thread %s transaction %d not rolling back for checked exception %s", Thread.currentThread().getName(), transactionId(), ex.getClass().getName()));
      }
   }

}
