/*
 * SecurityRunAs.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor.security;

import javax.annotation.security.RunAs;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import org.rogueware.cdi.runnable.security.RolePrincipal;
import org.rogueware.cdi.runnable.security.SecurityFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@RunAs
@Interceptor
public class SecurityRunAs {

   protected final Logger log = LoggerFactory.getLogger(SecurityRunAs.class);

   /* 
    *  RunAs
    *    defines the role of the application during execution in a container.
    *
    */
   @AroundInvoke
   public Object securityWrapper(InvocationContext ctx) throws Exception {

      RunAs ra = ctx.getMethod().getDeclaringClass().getAnnotation(RunAs.class);

      boolean createdSecurityFacade = false;
      SecurityFacade secure = SecurityFacade.getSecurityFacade();
      if (null == secure) {
         secure = new SecurityFacade();
         SecurityFacade.setSecurityFacade(secure);
         createdSecurityFacade = true;
      }

      // Add the role
      secure.pushRunAsRole(new RolePrincipal(ra.value()));
      log.trace(String.format("@RunAs: Thread %s executing as role %s for invoking class %s member %s", ra.value(), Thread.currentThread().getName(), ctx.getMethod().getDeclaringClass().getName(), ctx.getMethod().getName()));

      try {
         return ctx.proceed();
      } finally {
         // Remove the role
         secure.popRunAsRole();
         if (createdSecurityFacade) {
            SecurityFacade.setSecurityFacade(null);
         }
         log.trace(String.format("@RunAs: Thread %s removed role %s for invoking class %s member %s", ra.value(), Thread.currentThread().getName(), ctx.getMethod().getDeclaringClass().getName(), ctx.getMethod().getName()));
      }
   }
}
