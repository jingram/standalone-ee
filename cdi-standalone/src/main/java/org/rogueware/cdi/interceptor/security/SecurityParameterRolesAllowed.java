/*
 * SecurityParameterRolesAllowed.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor.security;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import org.rogueware.annotations.security.ParameterRolesAllowed;
import org.rogueware.cdi.runnable.security.SecurityFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@ParameterRolesAllowed
@Interceptor
public class SecurityParameterRolesAllowed {

   protected final Logger log = LoggerFactory.getLogger(SecurityParameterRolesAllowed.class);

   /* 
    *  RolesAllowed
    */
   @AroundInvoke
   public Object securityWrapper(InvocationContext ctx) throws Exception {
      // Interceptor is called for all methods as roles has a value that we cannot add a non-binding too
      //   First try get annotation from method, then try get from class
      ParameterRolesAllowed ra = ctx.getMethod().getAnnotation(ParameterRolesAllowed.class);
      if (null == ra) {
         ra = ctx.getMethod().getDeclaringClass().getAnnotation(ParameterRolesAllowed.class);

         // Combination - If class has @RolesAllowed and method has @DenyAll or @PermitAll
         //  then don't try roles allowed as its overriden
         if (null != ra && (null != ctx.getMethod().getAnnotation(PermitAll.class) || null != ctx.getMethod().getAnnotation(DenyAll.class))) {
            log.error(String.format("@ParameterRolesAllowed: Thread %s not using @ParameterRolesAllowed as overridden for invoking class %s member %s", Thread.currentThread().getName(), ctx.getMethod().getDeclaringClass().getName(), ctx.getMethod().getName()));
            return ctx.proceed();
         }
      }

      if (null != ra) {
         SecurityFacade secure = SecurityFacade.getSecurityFacade();
         if (null == secure) {
            log.error(String.format("@ParameterRolesAllowed: Thread %s could not find SecurityFacade invoking class %s member %s", Thread.currentThread().getName(), ctx.getMethod().getDeclaringClass().getName(), ctx.getMethod().getName()));
            throw new javax.ws.rs.NotAuthorizedException(String.format("@ParameterRolesAllowed: Thread %s could not find SecurityFacade invoking class %s member %s", Thread.currentThread().getName(), ctx.getMethod().getDeclaringClass().getName(), ctx.getMethod().getName()));
         }

         // We have to apply security
         boolean authorised = false;
         for (String role : ra.value()) {
            // Substitute method parameters into role name
            String expandedRole = "";
            try {
               // Run .toString on all objects to avoid MessageFormat from parsing strings itself.
               // Exmaple: 1278 is formatted as 1,278 
               List<Object> strParams = new ArrayList<>();
               for (Object o : ctx.getParameters()) {
                  strParams.add(o.toString());
               }

               expandedRole = MessageFormat.format(role, strParams.toArray(new Object[]{}));
               log.trace(String.format("@ParameterRolesAllowed: Thread %s expanded role %s to role %s", Thread.currentThread().getName(), role, expandedRole));
            } catch (Exception ex) {
               log.trace(String.format("@ParameterRolesAllowed: Thread %s unable to expand role", Thread.currentThread().getName()), ex);
               throw new javax.ws.rs.NotAuthorizedException(String.format("@ParameterRolesAllowed: Thread %s unable to expand role", Thread.currentThread().getName()), ex);
            }

            if (secure.isUserInRole(expandedRole)) {
               log.trace(String.format("@RolesAllowed: Thread %s authorised with role %s to invoke class %s member %s", Thread.currentThread().getName(), role, ctx.getMethod().getDeclaringClass().getName(), ctx.getMethod().getName()));
               authorised = true;
               break;
            }
         }

         if (!authorised) {
            log.trace(String.format("@RolesAllowed: Thread %s not authorised to invoke class %s member %s", Thread.currentThread().getName(), ctx.getMethod().getDeclaringClass().getName(), ctx.getMethod().getName()));
            throw new javax.ws.rs.NotAuthorizedException(String.format("@RolesAllowed: Thread %s not authorised to invoke class %s member %s", Thread.currentThread().getName(), ctx.getMethod().getDeclaringClass().getName(), ctx.getMethod().getName()));
         }
      }

      // Invoke
      return ctx.proceed();
   }
}
