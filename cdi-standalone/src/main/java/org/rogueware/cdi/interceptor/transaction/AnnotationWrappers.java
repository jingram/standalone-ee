/*
 * AnnotationWrappers.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.interceptor.transaction;

import java.lang.annotation.Annotation;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class AnnotationWrappers {

   private AnnotationWrappers() {
   }

   public static Annotation getTransactionalAnnotation(javax.ejb.TransactionAttribute eeTs) {
      // Constructor to generate a JTA annotation from an EE annotation

      switch (eeTs.value()) {
         case MANDATORY:
            return Transactional_MANDATORY.getAnnotation();
         case NEVER:
            return Transactional_NEVER.getAnnotation();
         case NOT_SUPPORTED:
            return Transactional_NOT_SUPPORTED.getAnnotation();
         case REQUIRED:
            return Transactional_REQUIRED.getAnnotation();
         case REQUIRES_NEW:
            return Transactional_REQUIRES_NEW.getAnnotation();
         case SUPPORTS:
            return Transactional_SUPPORTS.getAnnotation();
         default:
            return Transactional_REQUIRED.getAnnotation();
      }
   }

   @javax.transaction.Transactional
   public static class Transactional {

      public static Annotation getAnnotation() {
         try {
            return Transactional.class.getAnnotation(javax.transaction.Transactional.class);
         } catch (Exception ex) {
            return null;
         }
      }
   }

   @javax.transaction.Transactional(javax.transaction.Transactional.TxType.MANDATORY)
   public static class Transactional_MANDATORY {

      public static Annotation getAnnotation() {
         try {
            return Transactional_MANDATORY.class.getAnnotation(javax.transaction.Transactional.class);
         } catch (Exception ex) {
            return null;
         }
      }
   }

   @javax.transaction.Transactional(javax.transaction.Transactional.TxType.NEVER)
   public static class Transactional_NEVER {

      public static Annotation getAnnotation() {
         try {
            return Transactional_NEVER.class.getAnnotation(javax.transaction.Transactional.class);
         } catch (Exception ex) {
            return null;
         }
      }
   }

   @javax.transaction.Transactional(javax.transaction.Transactional.TxType.NOT_SUPPORTED)
   public static class Transactional_NOT_SUPPORTED {

      public static Annotation getAnnotation() {
         try {
            return Transactional_NOT_SUPPORTED.class.getAnnotation(javax.transaction.Transactional.class);
         } catch (Exception ex) {
            return null;
         }
      }
   }

   @javax.transaction.Transactional(javax.transaction.Transactional.TxType.REQUIRED)
   public static class Transactional_REQUIRED {

      public static Annotation getAnnotation() {
         try {
            return Transactional_REQUIRED.class.getAnnotation(javax.transaction.Transactional.class);
         } catch (Exception ex) {
            return null;
         }
      }
   }

   @javax.transaction.Transactional(javax.transaction.Transactional.TxType.REQUIRES_NEW)
   public static class Transactional_REQUIRES_NEW {

      public static Annotation getAnnotation() {
         try {
            return Transactional_REQUIRES_NEW.class.getAnnotation(javax.transaction.Transactional.class);
         } catch (Exception ex) {
            return null;
         }
      }
   }

   @javax.transaction.Transactional(javax.transaction.Transactional.TxType.SUPPORTS)
   public static class Transactional_SUPPORTS {

      public static Annotation getAnnotation() {
         try {
            return Transactional_SUPPORTS.class.getAnnotation(javax.transaction.Transactional.class);
         } catch (Exception ex) {
            return null;
         }
      }
   }

}
