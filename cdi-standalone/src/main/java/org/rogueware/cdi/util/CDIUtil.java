/*
 * CDIUtil.java
 * 
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.AnnotatedField;
import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class CDIUtil {

   private CDIUtil() {
   }

   /**
    * Creates a new instance within within the CDI container WARNING: Call
    * context.release() to cleanup injections when complete
    *
    * @param <T> Bean type
    * @param manager Bean Manager
    * @param type Class type
    * @return ContextualInstance containing the instance and its context
    */
   public static <T> ContextualInstance<T> getContextualInstance(final BeanManager manager, final Class<T> type) {
      Bean<T> bean = (Bean<T>) manager.resolve(manager.getBeans(type));
      if (bean != null) {
         CreationalContext<T> context = manager.createCreationalContext(bean);

         if (context != null) {
            T result = (T) manager.getReference(bean, type, context);
            return new ContextualInstance<>(result, context);
         }
      }
      return null;
   }

   public static AnnotatedMethod addAnnotation(final AnnotatedMethod am, final Annotation... annotations) {
      return new AnnotatedMethod() {

         @Override
         public Method getJavaMember() {
            return am.getJavaMember();
         }

         @Override
         public List getParameters() {
            return am.getParameters();
         }

         @Override
         public boolean isStatic() {
            return am.isStatic();
         }

         @Override
         public AnnotatedType getDeclaringType() {
            return am.getDeclaringType();
         }

         @Override
         public Type getBaseType() {
            return am.getBaseType();
         }

         @Override
         public Set<Type> getTypeClosure() {
            return am.getTypeClosure();
         }

         @Override
         public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
            for (Annotation a : annotations) {
               if (a.annotationType().equals(annotationType)) {
                  return (T) a;
               }
            }
            return am.getAnnotation(annotationType);
         }

         @Override
         public Set<Annotation> getAnnotations() {
            Set<Annotation> result = new HashSet<>(am.getAnnotations());
            result.addAll(Arrays.asList(annotations));
            return result;
         }

         @Override
         public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
            for (Annotation a : annotations) {
               if (a.annotationType().equals(annotationType)) {
                  return true;
               }
            }

            return am.isAnnotationPresent(annotationType);
         }
      };
   }

   public static AnnotatedMethod removeAnnotation(final AnnotatedMethod am, final Annotation... annotations) {
      return new AnnotatedMethod() {

         @Override
         public Method getJavaMember() {
            return am.getJavaMember();
         }

         @Override
         public List getParameters() {
            return am.getParameters();
         }

         @Override
         public boolean isStatic() {
            return am.isStatic();
         }

         @Override
         public AnnotatedType getDeclaringType() {
            return am.getDeclaringType();
         }

         @Override
         public Type getBaseType() {
            return am.getBaseType();
         }

         @Override
         public Set<Type> getTypeClosure() {
            return am.getTypeClosure();
         }

         @Override
         public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
            for (Annotation a : annotations) {
               if (a.annotationType().equals(annotationType)) {
                  return null;  // Removed
               }
            }
            return am.getAnnotation(annotationType);
         }

         @Override
         public Set<Annotation> getAnnotations() {
            Set<Annotation> result = new HashSet<>();
            for (Annotation a : am.getAnnotations()) {
               boolean removing = false;
               for (Annotation remove : annotations) {
                  if (remove.annotationType().equals(a.annotationType())) {
                     removing = true;
                     break;
                  }
               }
               if (!removing) {
                  result.add(a);
               }
            }
            return result;
         }

         @Override
         public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
            for (Annotation a : annotations) {
               if (a.annotationType().equals(annotationType)) {
                  return false; // Removed
               }
            }

            return am.isAnnotationPresent(annotationType);
         }
      };
   }

   public static AnnotatedField addAnnotation(final AnnotatedField af, final Annotation... annotations) {

      return new AnnotatedField() {
         @Override
         public Field getJavaMember() {
            return af.getJavaMember();
         }

         @Override
         public boolean isStatic() {
            return af.isStatic();
         }

         @Override
         public AnnotatedType getDeclaringType() {
            return af.getDeclaringType();
         }

         @Override
         public Type getBaseType() {
            return af.getBaseType();
         }

         @Override
         public Set<Type> getTypeClosure() {
            return af.getTypeClosure();
         }

         @Override
         public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
            for (Annotation a : annotations) {
               if (a.annotationType().equals(annotationType)) {
                  return (T) a;
               }
            }
            return af.getAnnotation(annotationType);
         }

         @Override
         public Set<Annotation> getAnnotations() {
            Set<Annotation> result = new HashSet<>(af.getAnnotations());
            result.addAll(Arrays.asList(annotations));
            return result;
         }

         @Override
         public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
            for (Annotation a : annotations) {

               if (a.annotationType().equals(annotationType)) {
                  return true;
               }
            }

            return af.isAnnotationPresent(annotationType);
         }
      };
   }
}
