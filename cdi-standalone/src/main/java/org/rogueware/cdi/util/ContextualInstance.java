/*
 * CDIUtil.java
 * 
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.cdi.util;

import javax.enterprise.context.spi.CreationalContext;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class ContextualInstance<T> implements AutoCloseable {

   private T bean;
   private CreationalContext<T> creationalContext;

   public ContextualInstance(T bean, CreationalContext<T> CreationalContext) {
      this.bean = bean;
      this.creationalContext = CreationalContext;
   }

   public T getBean() {
      return bean;
   }

   public CreationalContext<T> getCreationalContext() {
      return creationalContext;
   }

   @Override
   public void close() throws Exception {
      if (null != creationalContext) {
         creationalContext.release();
         creationalContext = null;
         bean = null;
      }
   }
}
