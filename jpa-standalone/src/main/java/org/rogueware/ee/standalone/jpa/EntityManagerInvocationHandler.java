/*
 * EntityManagerInvocationHandler.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.persistence.EntityManager;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class EntityManagerInvocationHandler implements InvocationHandler {

   private final EntityManager em;

   public EntityManagerInvocationHandler(EntityManager em) {
      this.em = em;
   }

   @Override
   public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
      // Make sure there is always a transaction
      if (null != em.getTransaction() && !em.getTransaction().isActive()) {
         em.getTransaction().begin();
      }

      // Intercept close method
      if ("close".equals(method.getName())) {
         if (null != em.getTransaction() && em.getTransaction().isActive()) {
            if (em.getTransaction().getRollbackOnly()) {
               em.getTransaction().rollback();
            } else {
               em.getTransaction().commit();
            }
         }

         // Now original close can be invoked
      }

      // Invoke orignal methods
      try {
         return method.invoke(em, args);
      } catch (InvocationTargetException ex) {
         throw ex.getCause();
      }
   }
}
