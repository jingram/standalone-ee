/*
 * PUEntry.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.configuration.SubnodeConfiguration;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class PUEntry {

   private static final Map<String, String> jpa2PropertyMappings = new HashMap<>();

   private final String puName;

   private final Properties properties = new Properties();

   // Datasource
   private String jndiName;
   private String dataSourceClassName;
   private int maxPoolSize = 5;
   private int minPoolSize = 1;
   private long idleTimeoutSeconds = 180;
   private long maxWaitTimeSeconds = 10;

   static {
      // Easy names that can be used in a config file that can be mapped
      jpa2PropertyMappings.put("URL", "javax.persistence.jdbc.url");
      jpa2PropertyMappings.put("Url", "javax.persistence.jdbc.url");
      jpa2PropertyMappings.put("User", "javax.persistence.jdbc.user");
      jpa2PropertyMappings.put("Username", "javax.persistence.jdbc.user");
      jpa2PropertyMappings.put("Password", "javax.persistence.jdbc.password");
   }

   public PUEntry(SubnodeConfiguration config) {
      puName = config.getString("[@name]");
      minPoolSize = config.getInt("[@min-pool-size]", minPoolSize);
      maxPoolSize = config.getInt("[@max-pool-size]", maxPoolSize);
      idleTimeoutSeconds = config.getLong("[@idle-timeout-in-seconds]", idleTimeoutSeconds);
      maxWaitTimeSeconds = config.getLong("[@max-wait-time-in-seconds]", maxWaitTimeSeconds);

      List names = config.getList("property[@name]");
      List values = config.getList("property[@value]");

      // JDBCDriver
      if (config.containsKey("JDBCDriver[@jdbc-classname]")) {
         properties.put("javax.persistence.jdbc.driver", config.getString("JDBCDriver[@jdbc-classname]"));

         for (int i = 0; i < names.size(); i++) {
            if (jpa2PropertyMappings.containsKey(names.get(i))) {
               properties.put(jpa2PropertyMappings.get(names.get(i)), values.get(i));
            } else {
               properties.put(names.get(i), values.get(i));
            }
         }
      }

      // Datasource
      if (config.containsKey("Datasource[@jndi-name]")) {
         jndiName = config.getString("Datasource[@jndi-name]");
         dataSourceClassName = config.getString("Datasource[@datasource-classname]");

         for (int i = 0; i < names.size(); i++) {
            properties.put(names.get(i), values.get(i));
         }
      }
   }

   /**
    * Constructor used to create a PU with a DATASOURCE persistence unit
    *
    * @param puName
    * @param jndiName
    * @param datasourceClassname
    * @param specifiedProperties
    */
   public PUEntry(String puName, String jndiName, String datasourceClassname, Map<String, String> specifiedProperties) {
      this.puName = puName;
      this.jndiName = jndiName;
      this.dataSourceClassName = datasourceClassname;

      for (String key : specifiedProperties.keySet()) {
         properties.put(key, specifiedProperties.get(key));
      }
   }

   public PUEntry(String puName, String jndiName, String datasourceClassname, int minPoolSize, int maxPoolSize, long idelTimeoutSeconds, long maxWaitTimeSeconds, Map<String, String> specifiedProperties) {
      this.puName = puName;
      this.jndiName = jndiName;
      this.dataSourceClassName = datasourceClassname;

      this.minPoolSize = minPoolSize;
      this.maxPoolSize = maxPoolSize;
      this.idleTimeoutSeconds = idelTimeoutSeconds;
      this.maxWaitTimeSeconds = maxWaitTimeSeconds;

      for (String key : specifiedProperties.keySet()) {
         properties.put(key, specifiedProperties.get(key));
      }
   }

   /**
    * Constructor used to create a PU with a LOCAL_RESOURCE persistence unit
    *
    * @param puName
    * @param jdbcClassname
    * @param specifiedProperties
    */
   public PUEntry(String puName, String jdbcClassname, Map<String, String> specifiedProperties) {
      this.puName = puName;
      properties.put("javax.persistence.jdbc.driver", jdbcClassname);

      for (String key : specifiedProperties.keySet()) {
         if (jpa2PropertyMappings.containsKey(key)) {
            properties.put(jpa2PropertyMappings.get(key), specifiedProperties.get(key));
         } else {
            properties.put(key, specifiedProperties.get(key));
         }
      }
   }

   public String getPuName() {
      return puName;
   }

   public String getJndiName() {
      return jndiName;
   }

   public String getDataSourceClassName() {
      return dataSourceClassName;
   }

   public Properties getProperties() {
      return properties;
   }

   public int getMaxPoolSize() {
      return maxPoolSize;
   }

   public int getMinPoolSize() {
      return minPoolSize;
   }

   public long getIdleTimeoutSeconds() {
      return idleTimeoutSeconds;
   }

   public long getMaxWaitTimeSeconds() {
      return maxWaitTimeSeconds;
   }
}
