/*
 * JPAStandAlone.java
 * 
 * Defined a class to initialize the standard JPA library
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameAlreadyBoundException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import javax.sql.DataSource;
import javax.sql.XADataSource;
import javax.transaction.TransactionManager;
import org.enhydra.jdbc.pool.StandardXAPoolDataSource;
import org.enhydra.jdbc.standard.StandardXADataSource;
import org.rogueware.jta.JTAStandAlone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class JPAStandAlone {

   private static final Logger log = LoggerFactory.getLogger(JPAStandAlone.class);
   private static final Map<String, EntityManagerFactory> emfs = Collections.synchronizedMap(new HashMap<String, EntityManagerFactory>());
   private static String primayPU;

   public static boolean initJPAStandAloneApplication(List<PUEntry> persistenceUnits) throws PersistenceException {
      boolean registeredWithJNDI = false;

      // For each persistence unit defined, create a persitence unit with the global entity manager
      for (PUEntry pu : persistenceUnits) {
         // If there is a datasource, create it and register it with JNDI
         EntityManagerFactory emf;
         if (null != pu.getDataSourceClassName()) {
            // JTA Resource
            try {

               // TODO: Add support for standard DataSource and StandardPoolDataSource
               TransactionManager tm = JTAStandAlone.getTransactionManager();

               Class c = Class.forName(pu.getDataSourceClassName());
               DataSource ds = (DataSource) c.newInstance();

               // TODO: Support other primitives (Int, Double etc later on)
               String username = "";
               String password = "";
               for (String propName : pu.getProperties().stringPropertyNames()) {
                  String setter = "set" + propName;
                  Method m = c.getMethod(setter, String.class);
                  m.invoke(ds, pu.getProperties().getProperty(propName));

                  if (m.getName().contains("User")) {
                     username = pu.getProperties().getProperty(propName);
                  }
                  if (m.getName().contains("Password")) {
                     password = pu.getProperties().getProperty(propName);
                  }
               }
               ((StandardXADataSource) ds).setTransactionManager(tm);

               StandardXAPoolDataSource xaPoolDS = new StandardXAPoolDataSource();
               xaPoolDS.setTransactionManager(tm);         // Always set first
               xaPoolDS.setDataSource((XADataSource) ds);

               xaPoolDS.setUser(username);
               xaPoolDS.setPassword(password);

               xaPoolDS.setMaxSize(pu.getMaxPoolSize());
               xaPoolDS.setMinSize(pu.getMinPoolSize());
               xaPoolDS.setSleepTime(pu.getIdleTimeoutSeconds() * 1000);
               xaPoolDS.setDeadLockMaxWait(pu.getMaxWaitTimeSeconds() * 1000);

               DataSourceDelegate delegateDataSource = new DataSourceDelegate(xaPoolDS);   // Need this to register with JNDI

               // Register with JNDI if present
               try {
                  Context ctx = new InitialContext();
// Binf to EE environment as follows: java:comp/env/jdbc/EmployeeAppDB
                  String[] parts = pu.getJndiName().split("/");
                  String sub = "";
                  for (int i = 0; i < parts.length - 1; i++) {
                     if (i == 0) {
                        sub = parts[0];
                     } else {
                        sub = sub + "/" + parts[i];
                     }

                     try {
                        ctx.createSubcontext(sub);
                     } catch (NameAlreadyBoundException ex) {
                        // Aint gonna loose no sleep
                     }
                  }

                  ctx.rebind(pu.getJndiName(), delegateDataSource);
                  registeredWithJNDI = true;

                  javax.sql.DataSource o = (javax.sql.DataSource) ctx.lookup(pu.getJndiName());

                  log.info(String.format("JPA - Bound datasource %s class %s for persistent unit %s to JNDI context %s", pu.getJndiName(), xaPoolDS.getClass().getName(), pu.getPuName(), pu.getJndiName()));
               } catch (Exception ex) {
                  registeredWithJNDI = false;  // Inited, but not registered
                  log.warn(String.format("JPA - Unable to bind datasource %s class %s for persistent unit %s to JNDI context %s", pu.getJndiName(), pu.getDataSourceClassName(), pu.getPuName(), pu.getJndiName()), ex);
               }

               emf = Persistence.createEntityManagerFactory(pu.getPuName());
               // TODO: Register entity manager factory with JNDI
            } catch (Exception ex) {
               throw new PersistenceException(String.format("Unable to create datasource for classname %s", pu.getDataSourceClassName()), ex);
            }
         } else {
            // Local Resource
            emf = Persistence.createEntityManagerFactory(pu.getPuName(), pu.getProperties());
         }

         if (null == primayPU) {
            primayPU = pu.getPuName();
         }
         emfs.put(pu.getPuName(), emf);
      }

      return registeredWithJNDI;
   }

   public static synchronized void destroySingleton() {
      for (EntityManagerFactory emf : emfs.values()) {
         if (emf.isOpen()) {
            emf.close();
         }
      }
      emfs.clear();
      // TODO: De-register entity manager factory with JNDI
   }

   public static String getDefaultPersistenceUnitName() {
      return primayPU;
   }

   public static List<String> getPersistenceUnitNames() {
      return new ArrayList<>(emfs.keySet());
   }

   public static EntityManager createEntityManager() {
      // Gets the primary PU creatred
      if (null != primayPU) {
         return createEntityManager(primayPU);
      }
      return null;
   }

   public static EntityManager createEntityManager(String persistenceUnitName) {
      EntityManagerFactory emf = null;
      if (null == (emf = emfs.get(persistenceUnitName))) {
         return null;
      }
      EntityManager em = emf.createEntityManager();
      return em;
   }

   public static EntityManagerAutoCloseable createAutoCloseableEntityManager() {
      // Gets the primary PU creatred
      if (null != primayPU) {
         return createAutoCloseableEntityManager(primayPU);
      }
      return null;
   }

   public static EntityManagerAutoCloseable createAutoCloseableEntityManager(String persistenceUnitName) {
      EntityManagerFactory emf = null;
      if (null == (emf = emfs.get(persistenceUnitName))) {
         return null;
      }

      EntityManager em = emf.createEntityManager();
      EntityManagerAutoCloseable proxy = (EntityManagerAutoCloseable) Proxy.newProxyInstance(
            EntityManagerInvocationHandler.class
            .getClassLoader(),
            new Class[]{EntityManagerAutoCloseable.class
            },
            new EntityManagerInvocationHandler(em)
      );
      return proxy;
   }

   public static EntityManagerFactory getEntityManagerFactory() {
      // Gets the primary PU creatred
      if (null != primayPU) {
         return getEntityManagerFactory(primayPU);
      }
      return null;
   }

   public static EntityManagerFactory getEntityManagerFactory(String persistenceUnitName) {
      EntityManagerFactory emf = null;
      if (null == (emf = emfs.get(persistenceUnitName))) {
         return null;
      }
      return emf;
   }

   public static Connection getEntityManagerJDBCConnection(EntityManager em) {
      try {
         // JPA 2 standard
         Connection conn = em.unwrap(Connection.class);
         return conn;
      } catch (Exception ex) {
      }

      return null;   // Unable to get the JDBC Connection
   }
}
