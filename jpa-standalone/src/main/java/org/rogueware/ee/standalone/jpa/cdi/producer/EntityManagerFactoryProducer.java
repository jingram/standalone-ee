/*
 * EntityManagerFactoryProducer.java
 * 
 * Defines a class used to inject an EntityManagerFactory
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa.cdi.producer;

import java.io.Serializable;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import org.rogueware.ee.standalone.jpa.JPAStandAlone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class EntityManagerFactoryProducer implements Serializable {
   protected static final Logger log = LoggerFactory.getLogger(EntityManagerFactoryProducer.class);

   @Produces
   public EntityManagerFactory createEntityManagerFactory(InjectionPoint injectionPoint) {
      EntityManagerFactory emf;
      String unitName = getUnitNameFromAnnotation(injectionPoint);

      if (null == unitName) {
         emf = JPAStandAlone.getEntityManagerFactory();
      } else {
         emf = JPAStandAlone.getEntityManagerFactory(unitName);
      }

      log.trace(String.format("Entity manager factory injected"));
      return emf;
   }

   private String getUnitNameFromAnnotation(InjectionPoint injectionPoint) {
      PersistenceUnit annotation = injectionPoint.getAnnotated().getAnnotation(PersistenceUnit.class);
      if (null != annotation) {
         if (null != annotation.unitName() && 0 != annotation.unitName().length()) {
            return annotation.unitName();
         }
      }
      return null;
   }
}
