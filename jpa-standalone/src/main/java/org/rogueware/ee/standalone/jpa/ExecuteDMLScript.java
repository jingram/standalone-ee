/*
 * ExecuteDMLScript.java
 * 
 * Defined a class to execute DML statements from a script 
 *  Uses input stream from file or resource in JAR
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class ExecuteDMLScript {

   private static final Logger log = LoggerFactory.getLogger(ExecuteDMLScript.class);

   private ExecuteDMLScript() {

   }

   public static void execute(EntityManager em, InputStream is) {
      if (null != is) {
         try {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            boolean processingComments = false;
            String dml = "";

            while (null != (line = br.readLine())) {
               if (line.trim().startsWith("*/") || line.trim().endsWith("*/")) {
                  processingComments = false;
                  continue;
               }

               if (line.trim().startsWith("/*")) {
                  processingComments = true;
                  continue;
               }

               if (processingComments || line.trim().startsWith("--")) {
                  continue;
               }

               // Add the line to the dml statement until we see it ending with ;
               dml += " " + line;
               if (!line.trim().endsWith(";")) {
                  continue;  // Get next piece of DML statement
               }

               if (dml.trim().length() > 0) {
                  dml = dml.trim();
                  dml = dml.substring(0, dml.length()-1);  // Remove ; (Some DB's no like)
                  try {
                     Query q = em.createNativeQuery(dml);
                     int i = q.executeUpdate();

                     log.info(String.format("Executed DML: %s with count %s ", dml, i));
                  } catch (Exception ex) {
                     log.warn(String.format("Unable to execute DML: %s", dml), ex);
                  }
               }
               dml = "";
            }

         } catch (Exception ex) {
            log.error("Unable to initialise database with DML", ex);
         }
      }
   }
}
