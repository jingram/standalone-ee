/*
 * JPAUtils.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class JPAUtils {

   private JPAUtils() {
   }

   public static <T> T returnEntityIfExists(EntityManager em, T entity) {
      // Make sure we have an entity
      if (!isEntityClass(entity.getClass())) {
         return null;
      }

      for (Field field : entity.getClass().getDeclaredFields()) {
         // Primary Keys & Composite Primary Keys
         Id primaryKeyAnnotation = field.getAnnotation(Id.class);
         EmbeddedId compositePrimaryKeyAnnotation = field.getAnnotation(EmbeddedId.class);
         if (null != primaryKeyAnnotation || null != compositePrimaryKeyAnnotation) {
            // Dont test key if the provided value is null.
            Object primaryKeyValue = null;
            try {
               primaryKeyValue = getFieldValue(entity, field);
            } catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            }

            if (null != primaryKeyValue) {
               // @Id = Primary Key
               // Make sure an entity does not exist with the primary key
               String jsql = String.format("SELECT e FROM %s e WHERE e.%s = :primaryKey", entity.getClass().getSimpleName(), field.getName());
               Query q = em.createQuery(jsql);
               q.setParameter("primaryKey", primaryKeyValue);
               try {
                  T res = (T) q.getSingleResult();
                  return res; // It exists, return it
               } catch (NoResultException ex) {
               } catch (Exception ex) {
                  int i = 1;
               }
            }
         }
      }

      // Unique Contraints
      Table tableAnnotation = entity.getClass().getAnnotation(Table.class);
      if (null != tableAnnotation) {
         for (UniqueConstraint constraint : tableAnnotation.uniqueConstraints()) {
            String jsql = String.format("SELECT e FROM %s e WHERE ", entity.getClass().getSimpleName());
            List bindParams = new ArrayList();
            boolean isFirstParam = true;

            int i = 0;
            for (String columnName : constraint.columnNames()) {
               Field field;
               Object fieldValue = null;
               try {
                  field = entity.getClass().getDeclaredField(columnName);
                  if (null == field) {
                     continue;
                  }
                  fieldValue = getFieldValue(entity, field);
               } catch (NoSuchFieldException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
               }

               if (!isFirstParam) {
                  jsql += " AND ";
               }
               isFirstParam = false;
               if (null == fieldValue) {
                  jsql += String.format("e.%s IS NULL", columnName);
               } else {
                  jsql += String.format("e.%s = :param%d", columnName, i);
                  bindParams.add(fieldValue);
                  i++;
               }
            }

            Query q = em.createQuery(jsql);
            for (i = 0; i < bindParams.size(); i++) {
               q.setParameter(String.format("param%s", i), bindParams.get(i));
            }
            try {
               T res = (T) q.getSingleResult();
               return res; // It exists, return it
            } catch (NoResultException ex) {
            }
         }
      }

      return null;  // Entity does not exist
   }

   public static <T> T returnEntityIfExists(EntityManager em, Class<T> klass, Object key) {
      // Make sure we have an entity and the key is not null
      if (!isEntityClass(klass) || key == null) {
         return null;
      }

      // Did not use em.find as if key type does not match, it flags transaction for rollback
      for (Field field : klass.getDeclaredFields()) {
         // Primary Keys 
         Id primaryKeyAnnotation = field.getAnnotation(Id.class);
         if (null != primaryKeyAnnotation) {
            // Dont test key if the provided value  not of the same type            
            if (!field.getType().getCanonicalName().equals(key.getClass().getCanonicalName())) {
               continue;
            }

            // @Id = Primary Key
            // Make sure an entity does not exist with the primary key
            String jsql = String.format("SELECT e FROM %s e WHERE e.%s = :primaryKey", klass.getSimpleName(), field.getName());
            Query q = em.createQuery(jsql);
            q.setParameter("primaryKey", key);
            try {
               T res = (T) q.getSingleResult();
               return res; // It exists, return it
            } catch (NoResultException ex) {
            } catch (Exception ex) {
            }
         }
      }
      
      // Try lookup using any unique constraints
      // Unique Contraints
      Table tableAnnotation = klass.getAnnotation(Table.class);
      if (null != tableAnnotation) {
         for (UniqueConstraint constraint : tableAnnotation.uniqueConstraints()) {
            String jsql = String.format("SELECT e FROM %s e WHERE ", klass.getSimpleName());

            // Single key provided, only support unique contraints with a single field
            if (1 != constraint.columnNames().length) {
               continue;
            }

            try {
               String columnName = constraint.columnNames()[0];
               Field field = klass.getDeclaredField(columnName);

               // Must be the same type!!
               if (!field.getType().getCanonicalName().equals(key.getClass().getCanonicalName())) {
                  continue;
               }

               jsql += String.format("e.%s = :param", columnName);
               Query q = em.createQuery(jsql);
               q.setParameter(String.format("param"), key);
               T res = (T) q.getSingleResult();
               return res; // It exists, return it
            } catch (NoSuchFieldException | NoResultException | IllegalArgumentException ex) {
            }
         }
      }

      return null;  // Entity does not exist
   }

   public static boolean isEntityClass(Class<?> klass) {
      Entity entityAnnotation = klass.getAnnotation(Entity.class);
      return null != entityAnnotation;
   }

   private static Object getFieldValue(Object entity, Field field) throws NoSuchFieldException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
      if (!Modifier.isPublic(field.getModifiers())) {
         // Try getter (get | is)
         String suffix = field.getName();
         suffix = suffix.substring(0, 1).toUpperCase() + suffix.substring(1);
         Method method = null;
         try {
            method = entity.getClass().getMethod("get" + suffix);
         } catch (NoSuchMethodException ex) {
            try {
               method = entity.getClass().getMethod("is" + suffix);
            } catch (NoSuchMethodException ex2) {
            }
         }

         if (null != method) {
            return method.invoke(entity);
         }

         // Try setting access level
         field.setAccessible(true);
         return field.get(entity);
      }

      throw new NoSuchFieldException();
   }
}
