/*
 * Test Case
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa.local;

import org.rogueware.jpa.DBTableThree;
import org.rogueware.jpa.DBTableTwo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.rogueware.ee.standalone.jpa.EntityManagerAutoCloseable;
import org.rogueware.ee.standalone.jpa.JPAStandAlone;
import org.rogueware.ee.standalone.jpa.PUEntry;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class JPAStandAloneAutoCloseableTest {

   private List<PUEntry> persistenceUnits;

   @Before
   public void init()  {
      persistenceUnits = new ArrayList<>();
      
      Map<String, String> properties = new HashMap<>();
      properties.put("DriverClass", "org.h2.Driver");
      properties.put("URL", "jdbc:h2:mem:jpaSample");
      properties.put("Username", "sa");
      
      persistenceUnits.add(new PUEntry("TestPU", "org.h2.Driver", properties));
      persistenceUnits.add(new PUEntry("OtherPU", "org.h2.Driver", properties));
   }

   @Test
   public void testAutoCloseable() {
      try {
         JPAStandAlone.initJPAStandAloneApplication(persistenceUnits);

         // AutoClosable (Creates transaction and commits / rollsback)
         // If an error occures on the connection, rollback is flagged.
         // To force rollback, do em.getTransaction().setRollbackOnly() .... EE style
         // TestPU
         try (EntityManagerAutoCloseable em = JPAStandAlone.createAutoCloseableEntityManager("TestPU")) {
            DBTableThree d = new DBTableThree(100, "Test autoclosable with autocommit");
            em.persist(d);
            em.flush();
         }

         // Explicit Rollback
         try (EntityManagerAutoCloseable em = JPAStandAlone.createAutoCloseableEntityManager("TestPU")) {
            DBTableThree d = new DBTableThree(101, "Only 1");
            em.persist(d);
            em.flush();
            
            em.getTransaction().setRollbackOnly();
         }

         // Error occured on connection Rollback
         try (EntityManagerAutoCloseable em = JPAStandAlone.createAutoCloseableEntityManager("TestPU")) {
            DBTableTwo d = new DBTableTwo(101, "What are you talking about");   // DBTableTwo does not exist, will auto rollback
            em.persist(d);            
            // Assume rollback when persist throws
         }
         catch (Exception ex) {
            // Ya, thats what we wanted
         }

         
         try (EntityManagerAutoCloseable em = JPAStandAlone.createAutoCloseableEntityManager("TestPU")) {
            Query q = em.createQuery("SELECT e FROM DBTableThree e");

            List<DBTableThree> res = q.getResultList();
            assertEquals("Incorrect result returned", 1, res.size());
         }

      } catch (PersistenceException ex) {
         ex.printStackTrace(System.err);
         fail(String.format("JPA Init failed: %s", ex.getMessage()));
      }
   }
}
