/*
 * MonitorBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa.jta;

import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
public class MonitorBean {

   private static final Logger log = LoggerFactory.getLogger(MonitorBean.class);

   @Inject
   private DatabaseBean databaseBean;

   public void testCreateTwoDatabasesOneTransaction() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting Two Databases One Transaction Test");
      log.info("-------------------------------------------------------------------------");

      databaseBean.createTableRecords("MyRecord", false);
      if (!databaseBean.databaseOneRecordExists("MyRecord")) {
         log.error("MyRecord does not exist in database 1");
         throw new InterruptedException("MyRecord does not exist in database 1");
      }
      if (!databaseBean.databaseTwoRecordExists("MyRecord")) {
         log.error("MyRecord does not exist in database 2");
         throw new InterruptedException("MyRecord does not exist in database 2");
      }

      if (1 != databaseBean.databaseOneCount()) {
         log.error("Incorrect number of records in database 1");
         throw new InterruptedException("Incorrect number of records in database 1");
      }
      if (1 != databaseBean.databaseTwoCount()) {
         log.error("Incorrect number of records in database 2");
         throw new InterruptedException("Incorrect number of records in database 2");
      }

      // Delete the records
      databaseBean.deleteDatabases();
      if (0 != databaseBean.databaseOneCount()) {
         log.error("Unable to delete records in database 1");
         throw new InterruptedException("Unable to delete records in database 1");
      }
      if (0 != databaseBean.databaseTwoCount()) {
         log.error("Unable to delete records in database 2");
         throw new InterruptedException("Unable to delete records in database 2");
      }

      if (databaseBean.getErrorOccured()) {
         throw new InterruptedException("Detected error during test");
      }

      log.info("-------------------------------------------------------------------------");
   }

   public void testRollbackTwoDatabasesOneTransaction() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting Two Databases One Transaction Rollback Test");
      log.info("-------------------------------------------------------------------------");

      int startCountOne = databaseBean.databaseOneCount();
      int startCountTwo = databaseBean.databaseTwoCount();

      try {
         databaseBean.createTableRecords("RollmeBack", true);   // Rollback
      } catch (RuntimeException ex) {
         // Expected
      }

      if (databaseBean.databaseOneRecordExists("RollmeBack")) {
         log.error("RollmeBack SHOULD NOT exist in database 1");
         throw new InterruptedException("RollmeBack SHOULD NOT exist in database 1");
      }
      if (databaseBean.databaseTwoRecordExists("RollmeBack")) {
         log.error("RollmeBack SHOULD NOT exist in database 2");
         throw new InterruptedException("RollmeBack SHOULD NOT exist in database 2");
      }

      if (startCountOne != databaseBean.databaseOneCount()) {
         log.error("Incorrect record count after rollback in database 1");
         throw new InterruptedException("Incorrect record count after rollback in database 1");
      }
      if (startCountTwo != databaseBean.databaseTwoCount()) {
         log.error("Incorrect record count after rollback in database 2");
         throw new InterruptedException("Incorrect record count after rollback in database 2");
      }

      if (databaseBean.getErrorOccured()) {
         throw new InterruptedException("Detected error during test");
      }

      log.info("-------------------------------------------------------------------------");
   }
}
