/*
 * Test Case
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa.local;

import org.rogueware.jpa.DBTableTwo;
import org.rogueware.jpa.DBTable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.rogueware.ee.standalone.jpa.JPAStandAlone;
import org.rogueware.ee.standalone.jpa.PUEntry;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class JPAStandAloneTest {

   private List<PUEntry> persistenceUnits;

   @Before
   public void init()  {
      persistenceUnits = new ArrayList<>();
      
      Map<String, String> properties = new HashMap<>();
      properties.put("DriverClass", "org.h2.Driver");
      properties.put("URL", "jdbc:h2:mem:jpaSample");
      properties.put("Username", "sa");
      properties.put("Password", "");
      
      persistenceUnits.add(new PUEntry("TestPU", "org.h2.Driver", properties));
      persistenceUnits.add(new PUEntry("OtherPU", "org.h2.Driver", properties));
   }

   @Test
   public void initEntityManager() {
      try {
         JPAStandAlone.initJPAStandAloneApplication(persistenceUnits);

         EntityManager em = null;
         EntityTransaction t = null;

         // TestPU
         try {
            em = JPAStandAlone.createEntityManager("TestPU");
            t = em.getTransaction();
            t.begin();

            DBTable d = new DBTable(1, "Hello");
            em.persist(d);

            d = new DBTable(2, "Friend");
            em.persist(d);
         } finally {
            if (em != null) {
               t.commit();
               em.close();
            }
         }

         // OtherPU
         try {
            em = JPAStandAlone.createEntityManager("OtherPU");
            t = em.getTransaction();
            t.begin();

            DBTableTwo d = new DBTableTwo(1, "Goodbye");
            em.persist(d);

            d = new DBTableTwo(2, "Dude");
            em.persist(d);
         } finally {
            if (em != null) {
               t.commit();
               em.close();
            }
         }

         // Query
         try {
            em = JPAStandAlone.createEntityManager("TestPU");
            Query q = em.createQuery("SELECT e FROM DBTable e");

            List<DBTable> res = q.getResultList();
            assertEquals("Incorrect result returned", 2, res.size());
            for (DBTable d : (List<DBTable>) q.getResultList()) {
               System.out.println(d.toString());
            }
         } finally {
            if (em != null) {
               em.close();
            }
         }
      } catch (PersistenceException ex) {
         ex.printStackTrace(System.err);
         fail(String.format("JPA Init failed: %s", ex.getMessage()));
      }
   }
}
