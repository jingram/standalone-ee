/*
 * DatabaseBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa.jta;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.rogueware.jpa.DBTable;
import org.rogueware.jpa.DBTableTwo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
@Transactional
public class DatabaseBean {

   private static final Logger log = LoggerFactory.getLogger(DatabaseBean.class);

   // Test injecting with and without @Inject
   @PersistenceContext(unitName = "DataSourceOnePU")
   private EntityManager emOne;

   @PersistenceContext(unitName = "DataSourceTwoPU")
   private EntityManager emTwo;

   private final AtomicBoolean errorOccured = new AtomicBoolean(false);

   public void createTableRecords(String name, boolean abort) {
      try {
         // Database One
         DBTable recordA = new DBTable();
         recordA.setName(name);
         emOne.persist(recordA);

         // Database Two
         DBTableTwo recordB = new DBTableTwo();
         recordB.setName(name);
         emTwo.persist(recordB);

         emOne.flush();
         log.info(String.format("Created database ONE record name %s with id %d", name, recordA.getId()));

         emTwo.flush();
         log.info(String.format("Created database TWO record name %s with id %d", name, recordB.getId()));
      } catch (Exception ex) {
         errorOccured.set(true);
         log.error("JPA - Error occured inserting records into databases", ex);
         throw new RuntimeException("JPA - Error occured inserting records into databases", ex);
      }

      if (abort) {
         throw new RuntimeException("Aborting transaction");
      }
   }

   public boolean databaseOneRecordExists(String name) {
      try {
         Query q = emOne.createQuery("SELECT COUNT(e.id) FROM DBTable e where e.name = :name");
         q.setParameter("name", name);
         Number count = (Number) q.getSingleResult();
         return count.intValue() == 1;
      } catch (Exception ex) {
         errorOccured.set(true);
         log.error("JPA - Error occured checking records in database ONE", ex);
         throw new RuntimeException("JPA - Error occured checking records in database ONE", ex);
      }
   }

   public boolean databaseTwoRecordExists(String name) {
      try {
         Query q = emTwo.createQuery("SELECT COUNT(e.id) FROM DBTableTwo e where e.name = :name");
         q.setParameter("name", name);
         Number count = (Number) q.getSingleResult();
         return count.intValue() == 1;
      } catch (Exception ex) {
         errorOccured.set(true);
         log.error("JPA - Error occured checking records in database TWO", ex);
         throw new RuntimeException("JPA - Error occured checking records in database TWO", ex);
      }
   }

   public int databaseOneCount() {
      try {
         Query q = emOne.createQuery("SELECT COUNT(e.id) FROM DBTable e");
         Number count = (Number) q.getSingleResult();
         return count.intValue();
      } catch (Exception ex) {
         errorOccured.set(true);
         log.error("JPA - Error occured counting records in database ONE", ex);
         throw new RuntimeException("JPA - Error occured counting records in database ONE", ex);
      }
   }

   public int databaseTwoCount() {
      try {
         Query q = emTwo.createQuery("SELECT COUNT(e.id) FROM DBTableTwo e");
         Number count = (Number) q.getSingleResult();
         return count.intValue();
      } catch (Exception ex) {
         errorOccured.set(true);
         log.error("JPA - Error occured counting records in database TWO", ex);
         throw new RuntimeException("JPA - Error occured counting records in database TWO", ex);
      }
   }

   public void deleteDatabases() {
      try {
         Query q1 = emOne.createQuery("DELETE FROM DBTable");
         int count = q1.executeUpdate();
         log.info(String.format("Deleting %d records from database ONE", count));

         Query q2 = emTwo.createQuery("DELETE FROM DBTableTwo");
         count = q2.executeUpdate();
         log.info(String.format("Deleting %d records from database TWO", count));
      } catch (Exception ex) {
         errorOccured.set(true);
         log.error("JPA - Error occured deleting records from databases", ex);
         throw new RuntimeException("JPA - Error occured deleting records from databases", ex);
      }
   }

   public boolean getErrorOccured() {
      return errorOccured.get();
   }

}
