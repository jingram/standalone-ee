/*
 * TestJTAJPA.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jpa.jta;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.naming.Context;
import javax.naming.InitialContext;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.rogueware.cdi.util.CDIUtil;
import org.rogueware.cdi.util.ContextualInstance;
import org.rogueware.ee.standalone.jpa.JPAStandAlone;
import org.rogueware.ee.standalone.jpa.PUEntry;
import org.rogueware.jta.JTAStandAlone;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestJTAJPA {

   private static Weld weld;
   private static BeanManager bm;
   private static List<PUEntry> persistenceUnits = new ArrayList<>();

   @BeforeClass
   public static void init() throws Exception {
      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.INFO);

      Logger jpaLogger = (Logger) LoggerFactory.getLogger("org.rogueware.ee.standalone.jpa");
      jpaLogger.setLevel(Level.TRACE);

      // JNDI
      System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");
      InitialContext ctx = new InitialContext();
      ctx.createSubcontext("java:");  
      
      // JTA
      JTAStandAlone.initJTAStandAloneApplication();

      // Derby
      // JPA - Datasource One
      Map<String, String> propertiesOne = new HashMap<>();
      propertiesOne.put("DriverName", "org.apache.derby.jdbc.EmbeddedDriver");
      propertiesOne.put("Url", "jdbc:derby:memory:oneDB;create=true");
      propertiesOne.put("User", "");
      propertiesOne.put("Password", "");
      persistenceUnits.add(new PUEntry("DataSourceOnePU", "jdbc/DataSourceOnePU", "org.enhydra.jdbc.standard.StandardXADataSource", 2, 5, 180, 10, propertiesOne));
     
      
      // MySQL
      // JPA - Datasource One
/*      Map<String, String> propertiesOne = new HashMap<>();
      propertiesOne.put("DriverName", "com.mysql.jdbc.Driver");
      propertiesOne.put("Url", "jdbc:mysql://localhost:3306/oneDB?zeroDateTimeBehavior=convertToNull");
      propertiesOne.put("User", "root");
      propertiesOne.put("Password", "112233");
      persistenceUnits.add(new PUEntry("DataSourceOnePU", "jdbc/DataSourceOnePU", "org.enhydra.jdbc.standard.StandardXADataSource", 2, 5, 180, 10, propertiesOne));
  */          
      
      // Derby
      // JPA - Datasource Two
      Map<String, String> propertiesTwo = new HashMap<>();
      propertiesTwo.put("DriverName", "org.apache.derby.jdbc.EmbeddedDriver");
      propertiesTwo.put("Url", "jdbc:derby:memory:twoDB;create=true");
      propertiesTwo.put("User", "");
      propertiesTwo.put("Password", "");
      persistenceUnits.add(new PUEntry("DataSourceTwoPU", "jdbc/DataSourceTwoPU", "org.enhydra.jdbc.standard.StandardXADataSource", 5, 15, 180, 10, propertiesTwo));

      
      // MySQL
      // JPA - Datasource Two
/*      Map<String, String> propertiesTwo = new HashMap<>();
      propertiesTwo.put("DriverName", "com.mysql.jdbc.Driver");
      propertiesTwo.put("Url", "jdbc:mysql://localhost:3306/twoDB?zeroDateTimeBehavior=convertToNull");
      propertiesTwo.put("User", "root");
      propertiesTwo.put("Password", "112233");
      persistenceUnits.add(new PUEntry("DataSourceTwoPU", "jdbc/DataSourceTwoPU", "org.enhydra.jdbc.standard.StandardXADataSource", 5, 15, 180, 10, propertiesTwo));
*/    
      
      JPAStandAlone.initJPAStandAloneApplication(persistenceUnits);

         
      // Weld
      weld = new Weld();
      WeldContainer weldContainer = weld.initialize();

      bm = CDI.current().getBeanManager();
      if (null == bm) {
         throw new Exception("Unable to initialize CDI container");
      }
   }

   @AfterClass
   public static void shutdown() {
      try {
         if (null != weld) {
            weld.shutdown();
         }
      } catch (Exception ex) {
      }
      
      JPAStandAlone.destroySingleton();
   }

   @Test
   public void testDatabaseJTA() throws Exception {
      try (ContextualInstance<MonitorBean> ci = CDIUtil.getContextualInstance(bm, MonitorBean.class);) {
         MonitorBean mb = ci.getBean();
         mb.testCreateTwoDatabasesOneTransaction();
         mb.testRollbackTwoDatabasesOneTransaction();
      }
   }
}
