/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.rogueware.ee.standalone.timer.service;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author jingram
 */
public class TimerServiceThreadFactory implements ThreadFactory {

   private static final AtomicInteger poolNumber = new AtomicInteger(1);
 
   @Override
   public Thread newThread(Runnable r) {
      Thread t = new Thread(r, String.format("timer-service-thread-%d", poolNumber.getAndIncrement()));
      return t;
   }

}
