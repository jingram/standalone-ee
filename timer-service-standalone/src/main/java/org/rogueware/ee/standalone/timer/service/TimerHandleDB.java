/*
 * TimerHandleDB.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.EJBException;
import javax.ejb.NoSuchObjectLocalException;
import javax.ejb.Schedule;
import javax.ejb.ScheduleExpression;
import javax.ejb.Schedules;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.transaction.TransactionManager;
import javax.xml.bind.annotation.XmlRootElement;
import org.rogueware.ee.standalone.timer.service.contrib.TimerSchedule;
import org.rogueware.jta.JTAStandAlone;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Entity
@XmlRootElement
@Table(name = "Timer",
      uniqueConstraints = {
         @UniqueConstraint(columnNames = {"uuid"})
      },
      indexes = {
         @Index(columnList = "klass"),
         @Index(columnList = "singleActionExpires"),
         @Index(columnList = "schedule"),
         @Index(columnList = "intervalStart"),
         @Index(columnList = "intervalMs")}
)
public class TimerHandleDB implements Serializable, javax.ejb.TimerHandle {

   private static final long serialVersionUID = 1L;

   @Id
   @Column(name = "uuid", nullable = false)
   private String uuid;

   @Basic
   @Column(name = "klass", nullable = false, length = 1024)
   private String klass;

   @Basic
   @Column(name = "method", nullable = false, length = 255)
   private String method;

   @Basic
   @Column(name = "timerParameter", nullable = false, length = 1)
   private String timerParameter;

   @Lob
   @Column(name = "serializableInfo", length = 65536)
   private byte[] serializableInfo;

   // Calendar / Schedule: second # minute # hour # dow # dom # month # year)
   @Basic
   @Column(name = "schedule", length = 2048)
   private String schedule;

   // Interval Timer
   @Column(name = "intervalStart")
   @Temporal(TemporalType.TIMESTAMP)
   private Date intervalStart;

   @Basic
   @Column(name = "intervalMs")
   private Long intervalMs;

   // Single Action Timer
   @Temporal(TemporalType.TIMESTAMP)
   @Column(name = "singleActionExpires")
   private Date singleActionExpires;

   public TimerHandleDB() {
   }

   public TimerHandleDB(Method method, ScheduleExpression se, TimerConfig timerConfig) {
      // Constructor for creating handle for annotations
      this.klass = method.getDeclaringClass().getName();
      this.method = method.getName();
      this.timerParameter = method.getParameterTypes().length == 0 ? "F" : "T";
      setSerializableInfo(timerConfig.getInfo());
      this.schedule = new TimerSchedule(se).getScheduleAsString();
      this.intervalStart = null;
      this.intervalMs = null;
      this.singleActionExpires = null;
   }   
   
   public String getUuid() {
      return uuid;
   }

   public void setUuid(String uuid) {
      this.uuid = uuid;
   }

   public String getKlass() {
      return klass;
   }

   public void setKlass(String klass) {
      this.klass = klass;
   }

   public String getMethod() {
      return method;
   }

   public void setMethod(String method) {
      this.method = method;
   }

   public String getTimerParameter() {
      return timerParameter;
   }

   public void setTimerParameter(String timerParameter) {
      this.timerParameter = timerParameter;
   }

   public Serializable getSerializableInfo() {
      try (ByteArrayInputStream bis = new ByteArrayInputStream(this.serializableInfo);
            ObjectInput in = new ObjectInputStream(bis)) {
         return (Serializable) in.readObject();
      } catch (Exception ex) {
      }
      return null;
   }

   public void setSerializableInfo(Serializable serializableInfo) {
      try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos)) {
         out.writeObject(serializableInfo);
         this.serializableInfo = bos.toByteArray();
      } catch (Exception ex) {
      }
   }

   public String getSchedule() {
      return schedule;
   }

   public void setSchedule(String schedule) {
      this.schedule = schedule;
   }

   public Date getIntervalStart() {
      return intervalStart;
   }

   public void setIntervalStart(Date intervalStart) {
      this.intervalStart = intervalStart;
   }

   public Long getIntervalMs() {
      return intervalMs;
   }

   public void setIntervalMs(Long intervalMs) {
      this.intervalMs = intervalMs;
   }

   public Date getSingleActionExpires() {
      return singleActionExpires;
   }

   public void setSingleActionExpires(Date singleActionExpires) {
      this.singleActionExpires = singleActionExpires;
   }

   @Override
   public int hashCode() {
      int hash = 0;
      hash += (null != uuid ? uuid.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object) {
      // TODO: Warning - this method won't work in the case the id fields are not set
      if (!(object instanceof TimerHandleDB)) {
         return false;
      }
      TimerHandleDB other = (TimerHandleDB) object;
      if ((this.uuid == null && other.uuid != null) || (this.uuid != null && !this.uuid.equals(other.uuid))) {
         return false;
      }
      return true;
   }

   @Override
   public String toString() {
      return "org.rogueware.ee.standalone.timer.service.jpa.TimerDB[ uuid=" + uuid + " ]";
   }

   @Override
   public Timer getTimer() throws IllegalStateException, NoSuchObjectLocalException, EJBException {
      // Make sure the class exists
      Class c;
      try {
         c = Class.forName(klass);
      } catch (ClassNotFoundException ex) {
         throw new EJBException(String.format("Timer class %s does not exist", klass));
      }

      // Make sure the timer method exists and is annotated correctly
      Method timerMethod;
      try {
         if ("T".equals(timerParameter)) {
            timerMethod = c.getMethod(method, javax.ejb.Timer.class);
         } else {
            timerMethod = c.getMethod(method);
         }
      } catch (NoSuchMethodException | SecurityException ex) {
         if ("T".equals(timerParameter)) {
            throw new EJBException(String.format("Unable to find timer method %s(javax.ejb.Timer timer) on class %s", method, klass));
         } else {
            throw new EJBException(String.format("Unable to find timer method %s on class %s ", method, klass));
         }
      }
      
      if (null == timerMethod.getAnnotation(Timeout.class) && 
          null == timerMethod.getAnnotation(Schedule.class) && 
          null == timerMethod.getAnnotation(Schedules.class)) {
            throw new EJBException(String.format("Timer method %s on class %s is not annotated with either @Timeout, @Schedule or @Schedules", method, klass));         
      }

      TimerService ts = TimerService.getTimerService(c);
      TimerConfig timerConfig = new TimerConfig(getSerializableInfo(), true);

      org.rogueware.ee.standalone.timer.service.Timer timer = null;
      if (null != singleActionExpires) {
         Date expires = new Date(singleActionExpires.getTime() + Calendar.getInstance().getTimeZone().getOffset(singleActionExpires.getTime()));  // UTC to local
         timer = new org.rogueware.ee.standalone.timer.service.Timer(uuid, timerConfig, expires, ts, timerMethod);
      } else if (null != intervalStart) {
         Date start = new Date(intervalStart.getTime() + Calendar.getInstance().getTimeZone().getOffset(intervalStart.getTime()));  // UTC to local
         timer = new org.rogueware.ee.standalone.timer.service.Timer(uuid, timerConfig, start, intervalMs, ts, timerMethod);
      } else if (null != schedule) {
         TimerSchedule timerSchedule = new TimerSchedule(schedule);
         timer = new org.rogueware.ee.standalone.timer.service.Timer(uuid, timerConfig, timerSchedule.getScheduledExpression(), ts, timerMethod);
      }

      if (ts.isDuplicateTimer(timer)) {
         //    log.warn(String.format("Unable to create programmatic timer on class %s as its a duplicate timer, same execution time and same info", timerServiceClass.getName()));
         throw new EJBException(String.format("Unable to create programmatic timer on class %s as its a duplicate timer, same execution time and same info", klass));
      }

      // Register a synchronization with the transaction to only add and schedule the timer once the transaction is commited
      try {
         TransactionManager tm = JTAStandAlone.getTransactionManager();
         if (null != tm && null != tm.getTransaction()) {
            TimerStartSynchronization transSync = new TimerStartSynchronization(timer);
            tm.getTransaction().registerSynchronization(transSync);
         }
      } catch (Exception ex) {
         //     log.error("Unable to register timer start synchronization with transaction", ex);
         throw new EJBException("Unable to register timer start synchronization with transaction", ex);
      }

      return (Timer) timer;
   }
}
