/*
 * Timer.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.service;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.ejb.EJBException;
import javax.ejb.NoMoreTimeoutsException;
import javax.ejb.NoSuchObjectLocalException;
import javax.ejb.ScheduleExpression;
import javax.ejb.TimerConfig;
import javax.ejb.TimerHandle;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.persistence.EntityManager;
import javax.transaction.TransactionManager;
import org.rogueware.cdi.util.CDIUtil;
import org.rogueware.cdi.util.ContextualInstance;
import static org.rogueware.ee.standalone.timer.service.TimerService.ENTITY_MANAGER_PU;
import org.rogueware.ee.standalone.timer.service.contrib.TimerSchedule;
import org.rogueware.ee.standalone.jpa.JPAStandAlone;
import org.rogueware.jta.JTAStandAlone;
import org.rogueware.jta.TransactionScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class Timer implements javax.ejb.Timer, Runnable {

   private transient static final Logger log = LoggerFactory.getLogger(Timer.class);

   private String uuid;
   private final TimerConfig timerConfig;

   private ScheduleExpression scheduleExpression;
   private Date intervalStart;
   private Long interval;
   private Date expires;

   private final TimerService timerService;
   private final Method expiresMethod;

   private final AtomicBoolean cancelling = new AtomicBoolean(false);
   private final AtomicBoolean cancelled = new AtomicBoolean(false);
   private ScheduledFuture<?> sf;
   private Long lastExpired;

   public Timer(TimerConfig timerConfig, ScheduleExpression scheduleExpression, TimerService timerService, Method expiresMethod) {
      // Creates calendar / scheduled timer not persistent
      this.timerConfig = timerConfig;
      this.scheduleExpression = scheduleExpression;
      this.timerService = timerService;
      this.expiresMethod = expiresMethod;
   }

   public Timer(String uuid, TimerConfig timerConfig, ScheduleExpression scheduleExpression, TimerService timerService, Method expiresMethod) {
      // Creates calendar / scheduled timer persistent
      this(timerConfig, scheduleExpression, timerService, expiresMethod);
      this.uuid = uuid;
   }

   public Timer(TimerConfig timerConfig, Date expires, TimerService timerService, Method expiresMethod) {
      // Single action timer not persistent
      this.timerConfig = timerConfig;
      this.expires = expires;
      this.timerService = timerService;
      this.expiresMethod = expiresMethod;
   }

   public Timer(String uuid, TimerConfig timerConfig, Date expires, TimerService timerService, Method expiresMethod) {
      // Single action timer persistent
      this(timerConfig, expires, timerService, expiresMethod);
      this.uuid = uuid;
   }

   public Timer(TimerConfig timerConfig, Date start, long intervalMs, TimerService timerService, Method expiresMethod) {
      // Inerval timer not persistent
      this.timerConfig = timerConfig;
      this.intervalStart = start;
      this.interval = intervalMs;
      this.timerService = timerService;
      this.expiresMethod = expiresMethod;

      Date now = new Date();
      if (start.after(now) || start.equals(now)) {
         expires = start;
      } else {
         // Get next inerval after now, aligned to start
         double offsetFromStart = (double) (System.currentTimeMillis() - intervalStart.getTime());
         double offsetIntervals = offsetFromStart / (double) interval;
         long nextOffsetInterval = (long) Math.ceil(offsetIntervals);
         long nextOffset = nextOffsetInterval * interval;

         long next = (intervalStart.getTime() + nextOffset);
         expires = new Date(next);
      }

   }

   public Timer(String uuid, TimerConfig timerConfig, Date start, long intervalMs, TimerService timerService, Method expiresMethod) {
      // Inerval timer persistent
      this(timerConfig, start, intervalMs, timerService, expiresMethod);
      this.uuid = uuid;
   }

   public String getUuid() {
      return uuid;
   }

   public TimerConfig getTimerConfig() {
      return timerConfig;
   }

   public ScheduleExpression getScheduleExpression() {
      return scheduleExpression;
   }

   public synchronized Date getExpires() {
      return expires;
   }

   public Method getExpiresMethod() {
      return expiresMethod;
   }

   public TimerService getTimerService() {
      return timerService;
   }

   public boolean isCancelled() {
      return cancelled.get();
   }

   @Override
   public synchronized void cancel() throws IllegalStateException, NoSuchObjectLocalException, EJBException {
      CDITimerServiceExtension.assertNotInitialised();
      if (cancelled.get() || cancelling.get()) {
         return; // Already cancelled
      }

      try {
         // Register a synchronization with the transaction to only cancel once the transaction is commited
         TimerCancelSynchronization transSync = new TimerCancelSynchronization(this);
         TransactionManager tm = JTAStandAlone.getTransactionManager();
         tm.getTransaction().registerSynchronization(transSync);
         cancelling.set(true);

         log.debug(String.format("Cancelling timer for class %s method %s", expiresMethod.getDeclaringClass().getName(), expiresMethod.getName()));
      } catch (Exception ex) {
         log.error(String.format("Unable to cancel timer for class %s method %s", expiresMethod.getDeclaringClass().getName(), expiresMethod.getName()), ex);
         throw new EJBException(String.format("Unable to cancel timer for class %s method %s", expiresMethod.getDeclaringClass().getName(), expiresMethod.getName()));
      }
   }

   protected synchronized void _cancel() {
      // Remove the entity from the database in the transaction
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRES_NEW)) {
         if (null != uuid) {

            // If timer persistent, remove from database
            EntityManager em = JPAStandAlone.getEntityManagerFactory(ENTITY_MANAGER_PU).createEntityManager();

            TimerHandleDB handleJpa = em.find(TimerHandleDB.class, uuid);
            if (null != handleJpa) {
               em.remove(handleJpa);
               log.debug(String.format("Removing persistent timer with UUID %s from persistence store", uuid));
            }
         }

         cancelled.set(true);
         timerService._removeTimer(this);

         if (null != sf) {
            sf.cancel(false);
            sf = null;
         }
      } catch (Exception ex) {
         log.warn(String.format("Unable to remove persistent timer with UUID %s from persistence store", uuid), ex);
      }

   }

   protected synchronized void _abortScheduledFuture() {
      if (null != sf) {
         sf.cancel(false);
         sf = null;
      }
   }

   protected synchronized void _abortCancel() {
      cancelling.set(false);
   }

   @Override
   public synchronized long getTimeRemaining() throws IllegalStateException, NoSuchObjectLocalException, NoMoreTimeoutsException, EJBException {
      CDITimerServiceExtension.assertNotInitialised();
      if (cancelled.get()) {
         throw new NoSuchObjectLocalException();
      }
      if (null != expires) {
         long remaining = expires.getTime() - System.currentTimeMillis();
         if (remaining < 0) {
            remaining = 0;
         }
         return remaining;
      }
      throw new NoMoreTimeoutsException();
   }

   @Override
   public synchronized Date getNextTimeout() throws IllegalStateException, NoSuchObjectLocalException, NoMoreTimeoutsException, EJBException {
      CDITimerServiceExtension.assertNotInitialised();
      return expires;
   }

   @Override
   public ScheduleExpression getSchedule() throws IllegalStateException, NoSuchObjectLocalException, EJBException {
      return scheduleExpression;
   }

   @Override
   public boolean isPersistent() throws IllegalStateException, NoSuchObjectLocalException, EJBException {
      return timerConfig.isPersistent();
   }

   @Override
   public boolean isCalendarTimer() throws IllegalStateException, NoSuchObjectLocalException, EJBException {
      return (null != scheduleExpression);
   }

   @Override
   public Serializable getInfo() throws IllegalStateException, NoSuchObjectLocalException, EJBException {
      return timerConfig.getInfo();
   }

   @Override
   public TimerHandle getHandle() throws IllegalStateException, NoSuchObjectLocalException, EJBException {
      // Create a persitable version of this timer!!!
      TimerHandleDB thdb = new TimerHandleDB();
      if (null != uuid) {
         thdb.setUuid(uuid);
      } else {
         thdb.setUuid(UUID.randomUUID().toString()); // Generate a key
      }
      thdb.setIntervalMs(interval);
      thdb.setIntervalStart(intervalStart);
      thdb.setKlass(expiresMethod.getDeclaringClass().getName());
      thdb.setMethod(expiresMethod.getName());
      thdb.setTimerParameter(expiresMethod.getParameterTypes().length == 0 ? "F" : "T");
      thdb.setSchedule(new TimerSchedule(scheduleExpression).getScheduleAsString());
      thdb.setSerializableInfo(timerConfig.getInfo());
      thdb.setSingleActionExpires((null == intervalStart && null == scheduleExpression) ? expires : null);

      return thdb;
   }

   @Override
   public boolean equals(Object obj) {
      if (obj == null || !(obj instanceof Timer)) {
         return false;
      }

      Timer other = (Timer) obj;
      if (!this.expiresMethod.equals(other.expiresMethod)) {
         return false;
      }

      // Calendar timer
      if ((this.scheduleExpression == null && other.scheduleExpression != null) || (this.scheduleExpression != null
            && !(new TimerSchedule(this.scheduleExpression).getScheduleAsString().equals(new TimerSchedule(other.scheduleExpression).getScheduleAsString())))) {
         return false;

      }

      // Interval timer
      if ((this.interval == null && other.interval != null) || (this.interval != null
            && !this.interval.equals(other.interval))) {
         return false;
      }

      // Single action timer
      if ((this.expires == null && other.expires != null) || (this.expires != null
            && !(this.expires.equals(other.expires) && this.timerConfig.getInfo().equals(other.timerConfig.getInfo())))) {
         return false;
      }

      return true;
   }

   /**
    * Schedules the timers next expiry
    */
   protected synchronized void schedule() {
      try {
         // If the timer is cancelled or the service is no longer initialised, shutting down, dont re-schedule
         if (cancelled.get() || !CDITimerServiceExtension.getInitialised()) {
            return;
         }

         // Cancel current scheduled future if there is one
         if (null != sf) {
            sf.cancel(false);
            sf = null;
         }

         if (null != scheduleExpression) {
            // Scheduled
            TimerSchedule ts = new TimerSchedule(scheduleExpression);
            Calendar nextTimeout;
            if (null == lastExpired) {
               nextTimeout = ts.getNextTimeout();
            } else {
               nextTimeout = ts.getNextTimeout(new Date(lastExpired));
            }

            expires = nextTimeout.getTime();
         } else if (null != interval) {
            // Interval (Aligned)
            if (null != lastExpired) {
               // Get next interval after now, aligned to start (Interval is always aligned to the start time)
               double offsetFromStart = (double) (System.currentTimeMillis()+1 - intervalStart.getTime());  // +1: Sanity: Fast executing timer processes in < 1ms will schedule the same time again
               double offsetIntervals = offsetFromStart / (double) interval;
               long nextOffsetInterval = (long) Math.ceil(offsetIntervals);
               long nextOffset = nextOffsetInterval * interval;

               long delay = (intervalStart.getTime() + nextOffset) - System.currentTimeMillis();
               expires = new Date(System.currentTimeMillis() + delay);
            }
         }

         // Single Action will have expires set, and set to null on run
         if (null != expires) {
            long delay = expires.getTime() - System.currentTimeMillis();
            if (delay < 0) {
               delay = 0;
            }

                     
            ScheduledExecutorService ses = timerService.getScheduledExecutorService();
            if (!ses.isShutdown() && !ses.isTerminated()) {
               sf = ses.schedule(this, delay, TimeUnit.MILLISECONDS);
               log.trace(String.format("Scheduled timer with delay %dms", delay));
               
               // Make sure the timer is always part of the timers if scheduled!!
               timerService._addTimer(this);
            } else {
               log.warn(String.format("Unable to schedule timer on class %s method %s as scheduled executor is shutdown", expiresMethod.getDeclaringClass().getName(), expiresMethod.getName()));
            }
         } else {
            // Timer is completed as one shot or no further schedules. Cancel timer
            _cancel();
         }
      } catch (Exception ex) {
         log.error(String.format("Unable to schedule timer on class %s method %s. Timer is aborted!!", expiresMethod.getDeclaringClass().getName(), expiresMethod.getName()), ex);
         _cancel();
      }
   }

   @Override
   public void run() {
      if (cancelled.get() || !CDITimerServiceExtension.getInitialised()) {
         return;
      }

      long startTime = System.currentTimeMillis();
      try {
         BeanManager bm = getBeanManager();
         try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRES_NEW);
               ContextualInstance ci = CDIUtil.getContextualInstance(bm, expiresMethod.getDeclaringClass());) {
            try {
// TODO: If EJB, ask EJB container for the bean insance, not CDI
               Object instance = ci.getBean();

               // Last minute check not to execute if shutting down
               if (!CDITimerServiceExtension.getInitialised()) {
                  return;
               }
               if (expiresMethod.getParameterTypes().length == 0) {
                  expiresMethod.invoke(instance);
               } else {
                  expiresMethod.invoke(instance, this);
               }
            } catch (Throwable threadEx) {
               log.error(String.format("Timer execution on class %s method %s raised exception. Cancelling timer...", expiresMethod.getDeclaringClass().getName(), expiresMethod.getName()), threadEx);
               _cancel(); // Sets cancelled = true, so synchronization will end up doing nothing
               return;
            }

            // If the timer got cancelled while executing, rollback the transaction
            if (cancelled.get()) {
               log.info(String.format("Timer was cancelled during execution on class %s method %s raised exception. Rolling back transaction", expiresMethod.getDeclaringClass().getName(), expiresMethod.getName()));
               _cancel(); // Sets cancelled = true, so synchronization will end up doing nothing
               ts.getTransaction().rollback();
               return;
            }
         } // Close resources (Commit transaction, free CDI bean resources)

      } catch (Throwable ex) {
         // Any error occuring outside of invoking timeout method, log and reschedule as not client problem.
         log.error(String.format("Failed to execute timer on class %s method %s. Timer will re-schedule", expiresMethod.getDeclaringClass().getName(), expiresMethod.getName()), ex);
      }

      // Schedule next execution
      synchronized (this) {
         lastExpired = System.currentTimeMillis();    // When last executed (Successfully executed now)
         expires = null;                              // Need to reschedule
         log.debug(String.format("Timer on class %s method %s executed in %d ms", expiresMethod.getDeclaringClass().getName(), expiresMethod.getName(), (System.currentTimeMillis() - startTime)));
         schedule();
      }

   }

   private BeanManager getBeanManager() {
      try {
         BeanManager bm = CDI.current().getBeanManager();
         return bm;
      } catch (Exception ex) {
         throw new IllegalThreadStateException("Unable to lookup bean manager from CDI");
      }
   }
}
