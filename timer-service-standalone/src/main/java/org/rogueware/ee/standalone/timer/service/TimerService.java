/*
 * TimerService.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.service;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import javax.ejb.EJBException;
import javax.ejb.ScheduleExpression;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.persistence.EntityManager;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import org.rogueware.ee.standalone.timer.service.contrib.TimerSchedule;
import org.rogueware.ee.standalone.jpa.JPAStandAlone;
import org.rogueware.jta.TransactionScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org) Resource: NOTE: There is a
 * TimerService instance per class with timers
 * http://docs.oracle.com/javaee/6/tutorial/doc/bnboy.html
 */
public class TimerService implements javax.ejb.TimerService {

   private transient static final Logger log = LoggerFactory.getLogger(TimerService.class);
   public static final String ENTITY_MANAGER_PU = "TimerServicePU";

   private static Map<Class, TimerService> timerServiceClasses = Collections.synchronizedMap(new HashMap<Class, TimerService>());

   private final Class timerServiceClass;
   private final List<Timer> timers = Collections.synchronizedList(new ArrayList<Timer>());
   private final ScheduledExecutorService scheduledExecutorService;

   private Method timeoutMethod;

   public TimerService(Class timerServiceClass, ScheduledExecutorService scheduledExecutorService) {
      this.timerServiceClass = timerServiceClass;
      this.scheduledExecutorService = scheduledExecutorService;

      // Find an annotated @Timeout method for this class if it has one
      for (Method m : timerServiceClass.getMethods()) {
         if (null != m.getAnnotation(Timeout.class)) {
            if (isMethodValid(m)) {
               // Check for 2 methods declared and warn we only using first method!!!
               if (null != timeoutMethod) {
                  log.warn(String.format("Ignoring duplicate @Timeout annotated method on class %s method %s for programmatic timers on class", timerServiceClass.getName(), m.getName()));
               } else {
                  log.info(String.format("Using class %s method %s annotated with @Timeout as timeout method for programmatic timers on class", timerServiceClass.getName(), m.getName()));
                  timeoutMethod = m;
               }
            } else {
               log.warn("Ignoring invalid @Timeout annotated method on class %s method %s for programmatic timers on class", timerServiceClass.getName(), m.getName());
            }
         }
      }
   }

   protected static void resetTimerService() {
      timerServiceClasses.clear();
   }

   public static TimerService getTimerService(Class klass) {
      TimerService ts;
      synchronized (timerServiceClasses) {
         if (null == (ts = timerServiceClasses.get(klass))) {
            ts = new TimerService(klass, CDITimerServiceExtension.getTimerServiceScheduledExecutor());
            timerServiceClasses.put(klass, ts);
         }
      }
      return ts;
   }

   protected ScheduledExecutorService getScheduledExecutorService() {
      return scheduledExecutorService;
   }

   public Class getTimerServiceClass() {
      return timerServiceClass;
   }

   protected Method getTimeoutMethod() {
      return timeoutMethod;
   }

   @Override
   public Timer createTimer(long duration, Serializable info) throws IllegalArgumentException, IllegalStateException, EJBException {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRED)) {
         // Timer are persistent by default!!!
         if (duration < 0) {
            ts.rollback();
            throw new IllegalArgumentException();
         }
         Date expiration = new Date(System.currentTimeMillis() + duration);
         TimerConfig timerConfig = new TimerConfig(info, true);
         return _createSingleActionTimer(expiration, timerConfig, ts);
      } catch (SystemException | NotSupportedException ex) {
         throw new EJBException(ex);
      }
   }

   @Override
   public Timer createSingleActionTimer(long duration, TimerConfig timerConfig) throws IllegalArgumentException, IllegalStateException, EJBException {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRED)) {
         if (duration < 0 || null == timerConfig) {
            ts.rollback();
            throw new IllegalArgumentException();
         }
         Date expiration = new Date(System.currentTimeMillis() + duration);
         return _createSingleActionTimer(expiration, timerConfig, ts);
      } catch (SystemException | NotSupportedException ex) {
         throw new EJBException(ex);
      }
   }

   @Override
   public Timer createTimer(long initialDuration, long intervalDuration, Serializable info) throws IllegalArgumentException, IllegalStateException, EJBException {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRED)) {
         if (initialDuration < 0 || intervalDuration < 0) {
            ts.rollback();
            throw new IllegalArgumentException();
         }
         Date expiration = new Date(System.currentTimeMillis() + initialDuration);
         TimerConfig timerConfig = new TimerConfig(info, true);
         return _createIntervalTimer(expiration, intervalDuration, timerConfig, ts);
      } catch (SystemException | NotSupportedException ex) {
         throw new EJBException(ex);
      }
   }

   @Override
   public Timer createIntervalTimer(long initialDuration, long intervalDuration, TimerConfig timerConfig) throws IllegalArgumentException, IllegalStateException, EJBException {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRED)) {
         if (initialDuration < 0 || intervalDuration < 0) {
            ts.rollback();
            throw new IllegalArgumentException();
         }
         Date expiration = new Date(System.currentTimeMillis() + initialDuration);
         return _createIntervalTimer(expiration, intervalDuration, timerConfig, ts);
      } catch (SystemException | NotSupportedException ex) {
         throw new EJBException(ex);
      }
   }

   @Override
   public Timer createTimer(Date expiration, Serializable info) throws IllegalArgumentException, IllegalStateException, EJBException {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRED)) {
         // Timer are persistent by default!!!
         if (expiration.getTime() < 0) {
            ts.rollback();
            throw new IllegalArgumentException();
         }
         TimerConfig timerConfig = new TimerConfig(info, true);
         return _createSingleActionTimer(expiration, timerConfig, ts);
      } catch (SystemException | NotSupportedException ex) {
         throw new EJBException(ex);
      }
   }

   @Override
   public Timer createSingleActionTimer(Date expiration, TimerConfig timerConfig) throws IllegalArgumentException, IllegalStateException, EJBException {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRED)) {
         if (expiration.getTime() < 0 || null == timerConfig) {
            ts.rollback();
            throw new IllegalArgumentException();
         }
         return _createSingleActionTimer(expiration, timerConfig, ts);
      } catch (SystemException | NotSupportedException ex) {
         throw new EJBException(ex);
      }
   }

   @Override
   public Timer createTimer(Date initialExpiration, long intervalDuration, Serializable info) throws IllegalArgumentException, IllegalStateException, EJBException {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRED)) {
         if (intervalDuration < 0) {
            ts.rollback();
            throw new IllegalArgumentException();
         }
         TimerConfig timerConfig = new TimerConfig(info, true);
         return _createIntervalTimer(initialExpiration, intervalDuration, timerConfig, ts);
      } catch (SystemException | NotSupportedException ex) {
         throw new EJBException(ex);
      }
   }

   @Override
   public Timer createIntervalTimer(Date initialExpiration, long intervalDuration, TimerConfig timerConfig) throws IllegalArgumentException, IllegalStateException, EJBException {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRED)) {
         if (intervalDuration < 0) {
            ts.rollback();
            throw new IllegalArgumentException();
         }
         return _createIntervalTimer(initialExpiration, intervalDuration, timerConfig, ts);
      } catch (SystemException | NotSupportedException ex) {
         throw new EJBException(ex);
      }
   }

   @Override
   public Timer createCalendarTimer(ScheduleExpression schedule) throws IllegalArgumentException, IllegalStateException, EJBException {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRED)) {
         TimerConfig timerConfig = new TimerConfig("", true);
         return _createScheduleTimer(schedule, timerConfig, ts);
      } catch (SystemException | NotSupportedException ex) {
         throw new EJBException(ex);
      }
   }

   @Override
   public Timer createCalendarTimer(ScheduleExpression schedule, TimerConfig timerConfig) throws IllegalArgumentException, IllegalStateException, EJBException {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRED)) {
         return _createScheduleTimer(schedule, timerConfig, ts);
      } catch (SystemException | NotSupportedException ex) {
         throw new EJBException(ex);
      }
   }

   @Override
   public Collection<Timer> getTimers() throws IllegalStateException, EJBException {
      synchronized (timers) {
         return new ArrayList<>(timers);
      }
   }

   @Override
   public Collection<Timer> getAllTimers() throws IllegalStateException, EJBException {
      CDITimerServiceExtension.assertNotInitialised();
      return _getAllTimers();
   }

   protected static Collection<Timer> _getAllTimers() {
      List<Timer> tms = new ArrayList<>();
      synchronized (timerServiceClasses) {
         // Get all timers for all timer services
         for (TimerService ts : timerServiceClasses.values()) {
            tms.addAll(ts.getTimers());
         }
      }
      return tms;
   }

   protected synchronized void _addTimer(Timer timer) {
      if (!timers.contains(timer)) {
         timers.add(timer);
      }
   }

   protected synchronized void _removeTimer(Timer timer) {
      if (timers.contains(timer)) {
         timers.remove(timer);
      }
   }

   protected synchronized Timer _createSingleActionTimer(Date expires, TimerConfig timerConfig, TransactionScope ts) throws IllegalStateException, EJBException {
      return _createTimer(expires, null, null, null, timerConfig, ts);
   }

   protected synchronized Timer _createIntervalTimer(Date start, long interval, TimerConfig timerConfig, TransactionScope ts) throws IllegalStateException, EJBException {
      return _createTimer(null, start, interval, null, timerConfig, ts);
   }

   protected synchronized Timer _createScheduleTimer(ScheduleExpression se, TimerConfig timerConfig, TransactionScope ts) throws IllegalStateException, EJBException {
      return _createTimer(null, null, null, se, timerConfig, ts);
   }

   protected synchronized Timer _createTimer(Date expires, Date start, Long interval, ScheduleExpression se, TimerConfig timerConfig, TransactionScope ts) throws IllegalStateException, EJBException {
      CDITimerServiceExtension.assertNotInitialised();
      try {
         if (null == timeoutMethod) {
            log.error(String.format("Unable to create programmatic timer on class %s as no method annotated with @Timeout is present", timerServiceClass.getName()));
            throw new EJBException(String.format("Unable to create programmatic timer on class %s as no method annotated with @Timeout is present", timerServiceClass.getName()));
         }

         // Make sure info is not null
         timerConfig.setInfo(timerConfig.getInfo() == null ? "" : timerConfig.getInfo());

         TimerHandleDB thdb = new TimerHandleDB();
         thdb.setUuid((timerConfig.isPersistent()) ? UUID.randomUUID().toString() : null);
         thdb.setKlass(timerServiceClass.getName());
         thdb.setMethod(timeoutMethod.getName());
         thdb.setTimerParameter(timeoutMethod.getParameterTypes().length == 0 ? "F" : "T");
         thdb.setSerializableInfo(timerConfig.getInfo());
         thdb.setSingleActionExpires((null != expires) ? new Date(expires.getTime() - Calendar.getInstance().getTimeZone().getOffset(expires.getTime())) : null);  // UTC
         thdb.setIntervalStart((null != start) ? new Date(start.getTime() - Calendar.getInstance().getTimeZone().getOffset(start.getTime())) : null);  // UTC
         thdb.setIntervalMs(interval);
         thdb.setSchedule((null != se) ? new TimerSchedule(se).getScheduleAsString() : null);

         org.rogueware.ee.standalone.timer.service.Timer timer = (org.rogueware.ee.standalone.timer.service.Timer) thdb.getTimer();
         if (timerConfig.isPersistent()) {
            try {
               // Assume JTA managed
               EntityManager em = JPAStandAlone.getEntityManagerFactory(ENTITY_MANAGER_PU).createEntityManager();
               em.persist(thdb);
               em.flush();
               log.debug(String.format("Creating new persistent timer with UUID %s in database", timer.getUuid()));
            } catch (Exception ex) {
               log.error(String.format("Unable to persist timer in database"), ex);
               throw new EJBException(String.format("Unable to persist timer in database"), ex);
            }
         }

         if (null != expires) {
            log.debug(String.format("Created programmatic single action timer for class %s method %s expires %s", timerServiceClass.getName(), timeoutMethod.getName(), expires.toString()));
         } else if (null != start) {
            log.debug(String.format("Created programmatic interval timer for class %s method %s starts %s, interval %dms", timerServiceClass.getName(), timeoutMethod.getName(), start.toString(), interval));
         } else if (null != se) {
            log.debug(String.format("Created programmatic schedule timer for class %s method %s schedule %s", timerServiceClass.getName(), timeoutMethod.getName(), se.toString()));
         }

         return timer;
      } catch (EJBException ex) {
         ts.rollback();
         throw ex;
      }
   }

   protected synchronized void _createAnnotatedScheduleTimer(Method method, ScheduleExpression se, TimerConfig timerConfig) throws IllegalStateException, EJBException {
      // These timers are not created in context of a transaction as they are created during startup
      CDITimerServiceExtension.assertNotInitialised();

      if (!isMethodValid(method)) {
         return;
      }

      // NB: Annotated timers are persistent in terms that the annotations are persistent between restarts, but not persisted to the database
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRES_NEW)) {
         org.rogueware.ee.standalone.timer.service.Timer timer = (org.rogueware.ee.standalone.timer.service.Timer)new TimerHandleDB(method, se, timerConfig).getTimer(); // Registers synchronization to register timer on transaction commit
         
         log.debug(String.format("Creating annotated scheduled (calendar) timer for class %s method %s schedule %s", method.getDeclaringClass().getName(), method.getName(), se.toString()));
      } catch (Exception ex) {
         log.error(String.format("Unable to create annotated schedule timer"), ex);
      }
   }

   protected synchronized boolean _recreatePersistentTimer(TimerHandleDB handle) throws IllegalStateException, EJBException {
      // These timers are not created in context of a localised transaction as they are created during startup
      CDITimerServiceExtension.assertNotInitialised();

      if (null == timeoutMethod) {
         log.warn(String.format("Removed persistent timer with uuid %s on class %s as class does not contain an @Timeout annotated method.", handle.getUuid(), handle.getKlass()));
         return true;
      }

      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRES_NEW)) {
         org.rogueware.ee.standalone.timer.service.Timer timer = (org.rogueware.ee.standalone.timer.service.Timer)handle.getTimer(); // Registers synchronization to register timer on transaction commit

         if (null != handle.getSingleActionExpires()) {
            log.info(String.format("Recreating persistent single action timer with uuid %s for class %s method %s expires %s", handle.getUuid(), timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName(), timer.getExpires().toString()));
         } else if (null != handle.getIntervalStart()) {
            log.info(String.format("Recreating persistent interval timer with uuid %s for class %s method %s starts %s, interval %dms ", handle.getUuid(), timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName(), handle.getIntervalStart().toString(), handle.getIntervalMs()));
         } else if (null != handle.getSchedule()) {
            log.info(String.format("Recreating persistent calendar timer with uuid %s for class %s method %s schedule %s", handle.getUuid(), timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName(), new TimerSchedule(handle.getSchedule()).getScheduledExpression().toString() ));
         }
         return false;
      } catch (Exception ex) {
         log.error(String.format("Unable to create persistent timer with uuid %s on class %s", handle.getUuid(), handle.getKlass()), ex);
         return false;
      }
   }

   protected boolean isDuplicateTimer(org.rogueware.ee.standalone.timer.service.Timer timer) {
      for (Timer t : timers) {
         if (t.equals(timer)) {
            return true;
         }
      }
      return false;
   }

   private boolean isMethodValid(Method m) {
      if (!m.getReturnType().equals(void.class)) {
         log.error(String.format("Removed timer on class %s method %s as method must return type void", m.getDeclaringClass().getName(), m.getName()));
         return false;
      }

      if (m.getParameterTypes().length > 1) {
         log.error(String.format("Removed timer on class %s method %s as method can take 0 or exactly one javax.ejb.Timer parameter", m.getDeclaringClass().getName(), m.getName()));
         return false;
      }

      if (m.getParameterTypes().length == 1 && !m.getParameterTypes()[0].equals(javax.ejb.Timer.class)) {
         log.error(String.format("Removed timer on class %s method %s as method can take 0 or exactly one javax.ejb.Timer parameter", m.getDeclaringClass().getName(), m.getName()));
         return false;
      }

      return true;
   }

}
