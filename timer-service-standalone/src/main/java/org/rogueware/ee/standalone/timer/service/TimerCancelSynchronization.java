/*
 * TimerCancelSynchronization.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.service;

import javax.persistence.EntityManager;
import javax.transaction.Status;
import javax.transaction.Synchronization;
import static org.rogueware.ee.standalone.timer.service.TimerService.ENTITY_MANAGER_PU;
import org.rogueware.ee.standalone.jpa.JPAStandAlone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TimerCancelSynchronization implements Synchronization {

   private transient static final Logger log = LoggerFactory.getLogger(TimerCancelSynchronization.class);

   private Timer timer;

   public TimerCancelSynchronization(Timer timer) {
      this.timer = timer;
   }

   @Override
   public void beforeCompletion() {
   }

   @Override
   public void afterCompletion(int status) {
      // If we have commited, cancel the timer
      //  only if the timer is not already cancelled.
      if (!timer.isCancelled()) {
         switch (status) {
            case Status.STATUS_COMMITTED:
               // Cancel the timer
               timer._cancel();
               log.info(String.format("Commited cancelling timer for class %s method %s", timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName()));
               break;

            case Status.STATUS_ROLLEDBACK:
               timer._abortCancel();
               log.info(String.format("Rolledback cancelling timer for class %s method %s", timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName()));
               break;

            default:
               timer._abortCancel();
               log.info(String.format("Aborted cancelling timer for class %s method %s for transaction in unknown state %d", timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName(), status));
               break;
         }
      }
   }

}
