/*
 * CDITimerServiceExtension.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.service;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.ScheduleExpression;
import javax.ejb.Schedules;
import javax.ejb.TimerConfig;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterDeploymentValidation;
import javax.enterprise.inject.spi.AnnotatedConstructor;
import javax.enterprise.inject.spi.AnnotatedField;
import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.BeforeShutdown;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.ProcessAnnotatedType;
import javax.enterprise.inject.spi.ProcessInjectionTarget;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.rogueware.cdi.annotations.AnnotationWrappers;
import org.rogueware.cdi.util.CDIUtil;
import org.rogueware.ee.standalone.jpa.JPAStandAlone;
import org.rogueware.jta.TransactionScope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class CDITimerServiceExtension implements Extension {

   private transient static final Logger log = LoggerFactory.getLogger(CDITimerServiceExtension.class);

   private static List<Method> discoveredAnnotatedMethods = Collections.synchronizedList(new ArrayList<Method>());
   private static final AtomicBoolean initialised = new AtomicBoolean(false);

   private static ScheduledExecutorService scheduledExecutorService;

   // Discover all bean methods annotated with either Schedule or Schedules
   // Ensure all java.ejb.TimerService fields with @Resource are annotated @Inject
   public <T> void processTimerServiceAnnotations(@Observes ProcessAnnotatedType<T> pat) {
      final AnnotatedType<T> at = pat.getAnnotatedType();

      // Look for methods annotated with @Schedule or @Schedules
      for (AnnotatedMethod am : at.getMethods()) {
         if (am.isAnnotationPresent(Schedule.class) || am.isAnnotationPresent(Schedules.class)) {
            discoveredAnnotatedMethods.add(am.getJavaMember());
            log.info(String.format("Detected scheduled class %s method %s to timer service", am.getJavaMember().getDeclaringClass().getName(), am.getJavaMember().getName()));
         }
      }

      // Override any @Resource javax.ejb.TimerService annotations with @Inject
      AnnotatedType<T> wrapped = new AnnotatedType<T>() {

         @Override
         public Class<T> getJavaClass() {
            return at.getJavaClass();
         }

         @Override
         public Set<AnnotatedConstructor<T>> getConstructors() {
            return at.getConstructors();
         }

         @Override
         public Set<AnnotatedMethod<? super T>> getMethods() {
            return at.getMethods();
         }

         @Override
         public Set<AnnotatedField<? super T>> getFields() {
            Set<AnnotatedField<? super T>> result = new HashSet<>();
            for (final AnnotatedField af : at.getFields()) {
               // If there is a field with the @Resource of type javax.ejb.TimerService,
               //   make sure the field has the @Inject 
               if (javax.ejb.TimerService.class.equals(af.getJavaMember().getType())
                       && af.isAnnotationPresent(Resource.class)) {
                  result.add(CDIUtil.addAnnotation(af, AnnotationWrappers.Inject.getAnnotation()));
               } else {
                  result.add(af);
               }
            }

            return result;
         }

         @Override
         public Type getBaseType() {
            return at.getBaseType();
         }

         @Override
         public Set<Type> getTypeClosure() {
            return at.getTypeClosure();
         }

         @Override
         public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
            return at.getAnnotation(annotationType);
         }

         @Override
         public Set<Annotation> getAnnotations() {
            return at.getAnnotations();
         }

         @Override
         public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
            return at.isAnnotationPresent(annotationType);
         }
      };

      pat.setAnnotatedType(wrapped);
   }

   public void startTimerService(@Observes AfterDeploymentValidation event) {
      if (!initialised.get()) {
         // Create scheduled executor, with timeout (At some point we will reach
         //   correct pool size with threads that are not terminated
         ScheduledThreadPoolExecutor pool = new ScheduledThreadPoolExecutor(10, new TimerServiceThreadFactory());
         pool.setKeepAliveTime(30, TimeUnit.SECONDS);
         pool.allowCoreThreadTimeOut(true);
         scheduledExecutorService = pool;

         initialised.set(true);

         // Create all Schedule / Schedules annotated timers
         createMethodAnnotatedTimers();

         // Create persistent timers
         createPersistentTimers();

         // Bind to JNDI
         bindJNDI();
         
         log.info("Timer service intialised");
      }
   }

   public void shutdownTimerService(@Observes BeforeShutdown event) {
      if (initialised.get()) {
         initialised.set(false);

         if (null != scheduledExecutorService) {
            // Abort all our known timers scheduled futures
            for (javax.ejb.Timer t : TimerService._getAllTimers()) {
               Timer timer = (Timer) t;
               timer._abortScheduledFuture();
            }

            try {
               scheduledExecutorService.shutdown();
               scheduledExecutorService.awaitTermination(30, TimeUnit.SECONDS);
               List<Runnable> tasks = scheduledExecutorService.shutdownNow();
            } catch (InterruptedException ex) {
               scheduledExecutorService.shutdownNow();
               log.warn("Thread interrupted while waiting for timer service to shutdown. Tasks where terminated and transactions should not commit");
            }
            scheduledExecutorService = null;
         }

         // Reset
         discoveredAnnotatedMethods = Collections.synchronizedList(new ArrayList<Method>());
         TimerService.resetTimerService();
         log.info("Timer service shutdown");
      }
   }

   public void processInjectionTarget(@Observes ProcessInjectionTarget pit) {

   }

   public static ScheduledExecutorService getTimerServiceScheduledExecutor() {
      return scheduledExecutorService;
   }

   public static boolean getInitialised() {
      return initialised.get();
   }

   public static void assertNotInitialised() throws IllegalStateException {
      if (!initialised.get()) {
         throw new IllegalStateException("Timer Service not initialised");
      }
   }

   private void createMethodAnnotatedTimers() {
      for (Method m : discoveredAnnotatedMethods) {
         {
            Schedule s = m.getAnnotation(Schedule.class);
            if (null != s) {
               createScheduledTimer(s, m);
            }
         }

         {
            Schedules ss = m.getAnnotation(Schedules.class);
            if (null != ss) {
               for (Schedule s : ss.value()) {
                  createScheduledTimer(s, m);
               }
            }
         }
      }
   }

   private void createScheduledTimer(Schedule s, Method m) {
      Serializable info = s.info() == null ? "" : s.info();
      TimerConfig tc = new TimerConfig(info, true);  // Always consider annotated as persistent

      ScheduleExpression se = new ScheduleExpression().
              second(s.second()).
              minute(s.minute()).
              hour(s.hour()).
              dayOfMonth(s.dayOfMonth()).
              month(s.month()).
              dayOfWeek(s.dayOfWeek()).
              year(s.year()).
              timezone(s.timezone()).
              start(null).
              end(null);

      TimerService timerService = TimerService.getTimerService(m.getDeclaringClass());
      timerService._createAnnotatedScheduleTimer(m, se, tc);
   }

   private void createPersistentTimers() {
      try (TransactionScope ts = new TransactionScope(TransactionScope.Scope.REQUIRES_NEW)) {
         EntityManager em = JPAStandAlone.getEntityManagerFactory(TimerService.ENTITY_MANAGER_PU).createEntityManager();
         Query q = em.createQuery("SELECT e FROM TimerHandleDB e");
         List<TimerHandleDB> res = (List<TimerHandleDB>) q.getResultList();

         for (TimerHandleDB handle : res) {
            try {
               Class c = Class.forName(handle.getKlass());
               TimerService timerService = TimerService.getTimerService(c);
               if (timerService._recreatePersistentTimer(handle)) {
                  em.remove(handle);
                  log.info("Timer service removed persistent timer with uuid %s on class %s", handle.getUuid(), handle.getKlass());
               }
            } catch (Exception ex) {
               log.error(String.format("Timer service unable to recreate persistent timer with uuid %s on class %s", handle.getUuid(), handle.getKlass()), ex);
            }
         }
      } catch (Exception ex) {
         log.error("Timer service unable to recreate persistent timers", ex);
      }
   }

   
   public void bindJNDI() {
      // Register the timer service interface with JNDI
      try {
         Context ctx = new InitialContext();

// TODO: java:comp/Timerservice ... this may need to live on the EJB container intercepting method call to add and remove ..., not here
// Note: The TimerService is the instance for the component         
         
         
//         ctx.rebind("java:/TransactionManager", jtaImpl.getTransactionManager());
//         log.info(String.format("JTA - Bound transaction manager to JNDI context java:/TransactionManager"));
  
         
         
         
      } catch (Exception ex) {
         log.warn(String.format("JTA - Unable to bind transaction manager to JNDI context java:/TransactionManager or /TransactionManager or  java:jboss/TransactionManager or java:appserver/TransactionManager"));
      }
   }

   
   public void unbindJNDI() {
   }
}
