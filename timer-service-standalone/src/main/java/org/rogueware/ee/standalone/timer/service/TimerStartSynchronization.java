/*
 * TimerStartSynchronization.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.service;

import javax.transaction.Status;
import javax.transaction.Synchronization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TimerStartSynchronization implements Synchronization {

   private transient static final Logger log = LoggerFactory.getLogger(TimerStartSynchronization.class);

   private Timer timer;

   public TimerStartSynchronization(Timer timer) {
      this.timer = timer;
   }

   @Override
   public void beforeCompletion() {
   }

   @Override
   public void afterCompletion(int status) {
      // If we have commited, register and schedule the timer
      switch (status) {
         case Status.STATUS_COMMITTED:
            // Add the timer and start it
            timer.getTimerService()._addTimer(timer);
            timer.schedule();
            if (null != timer.getUuid()) {
               log.info(String.format("Commited timer with uuid %s for class %s method %s", timer.getUuid(), timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName()));
            } else {
               log.info(String.format("Commited timer for class %s method %s", timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName()));
            }
            break;

         case Status.STATUS_ROLLEDBACK:
            timer._cancel();
            if (null != timer.getUuid()) {
               log.info(String.format("Rolledback timer with uuid %s for class %s method %s", timer.getUuid(), timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName()));
            } else {
               log.info(String.format("Rolledback timer for class %s method %s", timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName()));
            }
            break;

         default:
            timer._cancel();
            if (null != timer.getUuid()) {
               log.info(String.format("Cancelled timer for with uuid %s class %s method %s for transaction in unknown state %d", timer.getUuid(), timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName(), status));
            } else {
               log.info(String.format("Cancelled timer for class %s method %s for transaction in unknown state %d", timer.getExpiresMethod().getDeclaringClass().getName(), timer.getExpiresMethod().getName(), status));
            }
            break;
      }
   }

}
