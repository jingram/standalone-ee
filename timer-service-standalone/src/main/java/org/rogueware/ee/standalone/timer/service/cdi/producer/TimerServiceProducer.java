/*
 * TimerServiceProducer.java
 * 
 * Defines a class used to inject a class scoped TimerService
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.service.cdi.producer;

import java.io.Serializable;
import javax.ejb.TimerService;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TimerServiceProducer implements Serializable {

   private static final Logger log = LoggerFactory.getLogger(TimerServiceProducer.class);

   @Produces
   public TimerService injecTimerService(InjectionPoint injectionPoint) {
      log.trace(String.format("TimerService injected into %s class member %s", injectionPoint.getMember().getDeclaringClass().getName(), injectionPoint.getMember().getName()));      
      return org.rogueware.ee.standalone.timer.service.TimerService.getTimerService(injectionPoint.getMember().getDeclaringClass());
   }
}
