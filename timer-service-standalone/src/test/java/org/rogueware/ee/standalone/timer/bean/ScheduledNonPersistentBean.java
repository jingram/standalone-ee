/*
 * ScheduledNonPersistentBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.bean;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Schedule;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
@ApplicationScoped
public class ScheduledNonPersistentBean {

   private static final Logger log = LoggerFactory.getLogger(ScheduledNonPersistentBean.class);

   private AtomicInteger numScheduledTicks = new AtomicInteger(0);
   private AtomicBoolean programaticTimerExecuted = new AtomicBoolean(false);
   private AtomicBoolean errorOccured = new AtomicBoolean(false);
   private String state;

   @Resource
   private TimerService timerService;
   
   @Inject
   UserTransaction ut;

   @PostConstruct
   public void init() {
      state = "Broken";
      
      // Make sure the correct timer service is injected
      if (null == timerService) {
         errorOccured.set(true);
         log.error("Timer Service - Unable to inject timer service for class");         
      }
      
      org.rogueware.ee.standalone.timer.service.TimerService ts = (org.rogueware.ee.standalone.timer.service.TimerService)timerService;
      if (!this.getClass().equals(ts.getTimerServiceClass())) {
         errorOccured.set(true);
         log.error("Timer Service - Incorrect timer service injected for class");                  
      }
   }

   @Schedule(hour = "*", minute = "*", second = "*/1", info = "scheduledTimer")
   public void scheduledTwoSecondsCancel(Timer timer) throws SystemException {      
      log.info("Running Timer!!");
      
      // Make sure we have a transaction started by the Timer Service!!!
      if (null == ut || ut.getStatus() != Status.STATUS_ACTIVE) {
         errorOccured.set(true);
         log.error("Timer Service - An active transaction is required to have started for the timer service");
         throw new RuntimeException("Timer Service - An active transaction is required to have started for the timer service");
      }

      if (0 != timer.getTimeRemaining()) {
         errorOccured.set(true);
         log.error("Timer Service - getTimeRemaining did not return 0 for expiring timer");
         throw new RuntimeException("Timer Service - getTimeRemaining did not return 0 for expiring timer");         
      }
      
      // Make sure the timer is listed (Timers for this class!!)
      boolean found = false;
      for (Timer t : timerService.getTimers()) {
         if (t.isCalendarTimer()) {
            found = true;
         }
      }
      if (!found) {
         errorOccured.set(true);
         log.error("Timer Service - Timer not listed!!!");
         throw new RuntimeException("Timer Service - Timer not listed!!!");
      }

      numScheduledTicks.addAndGet(1);
      state = "Application scope is preserved with timer :)";

      if (2 == numScheduledTicks.get()) {
         // Create programatic timer
         // Cant create it when cancelling timer as the creation will rollback!!!
         TimerConfig tc = new TimerConfig("twoSecondSingleShot", false);
         timerService.createSingleActionTimer(2000, tc);   // tick 1 second after last tick on this timer     
      }
      
      if (3 == numScheduledTicks.get()) {
         timer.cancel();  // Will rollback transaction on this timer tick !!!!
      }
   }

   @Timeout
   public void timeout(Timer timer) {
      // Make sure the scheduled timer is NOT listed (Timers for this class!!)
      boolean found = false;
      for (Timer t : timerService.getTimers()) {
         if ("scheduledTimer".equals(t.getInfo())) {
            found = true;
         }
      }
      if (found) {
         errorOccured.set(true);
         log.error("Timer Service - Timer 'scheduledTimer' should have been removed!!!");
         throw new RuntimeException("Timer Service - Timer 'scheduledTimer' should have been removed!!!");
      }
      
      
      // Make sure the programmatic timer is listed (Timers for this class!!)
      found = false;
      for (Timer t : timerService.getTimers()) {
         if ("twoSecondSingleShot".equals(t.getInfo()) ) {
            found = true;
         }
      }
      if (!found) {
         errorOccured.set(true);
         log.error("Timer Service - Programmatic timer not listed!!!");
         throw new RuntimeException("Timer Service - Programmatic timer not listed!!!");
      }      
      
      
      // Make sure this is the programmatic timer expiring
      if ("twoSecondSingleShot".equals(timer.getInfo())) {
         programaticTimerExecuted.set(true);
      }
      
      
      // Make sure as a singleton state was perserved
      if (null == state || "Broken".equals(state)) {
         errorOccured.set(true);
         log.error("Timer Service - State was not preserved on the application scoped singleton!!!");
         throw new RuntimeException("Timer Service - State was not preserved on the application scoped singleton!!!");
      }
   }

   public int getNumScheduledTicks() {
      return numScheduledTicks.get();
   }

   public boolean getProgramaticTimerExecuted() {
      return programaticTimerExecuted.get();
   }

   public boolean getErrorOccured() {
      return errorOccured.get();
   }

}
