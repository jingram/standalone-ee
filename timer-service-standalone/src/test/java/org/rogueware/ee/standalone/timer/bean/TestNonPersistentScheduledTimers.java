/*
 * TestNonPersistentScheduledTimers.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.bean;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.naming.Context;
import javax.naming.InitialContext;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.rogueware.cdi.util.CDIUtil;
import org.rogueware.cdi.util.ContextualInstance;
import org.rogueware.ee.standalone.jpa.JPAStandAlone;
import org.rogueware.ee.standalone.jpa.PUEntry;
import org.rogueware.jta.JTAStandAlone;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestNonPersistentScheduledTimers {

   private static Weld weld;
   private static BeanManager bm;
   //  private static org.jnp.server.Main jndiServer;

   @BeforeClass
   public static void init() throws Exception {
      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.INFO);

      Logger timerLogger = (Logger) LoggerFactory.getLogger("org.rogueware.ee.standalone.timer");
      timerLogger.setLevel(Level.TRACE);

      // JNDI
      System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");
      InitialContext ctx = new InitialContext();
      ctx.createSubcontext("java:");  
      
      // JTA
      JTAStandAlone.initJTAStandAloneApplication();

      // JPA - Datasource
      List<PUEntry> persistenceUnits = new ArrayList<>();
      Map<String, String> properties = new HashMap<>();
      properties.put("DriverName", "org.apache.derby.jdbc.EmbeddedDriver");
      properties.put("Url", "jdbc:derby:memory:timerDb;create=true");
      properties.put("User", "");
      properties.put("Password", "");

      persistenceUnits.add(new PUEntry("TimerServicePU", "TimerDataSource", "org.enhydra.jdbc.standard.StandardXADataSource", properties));
      JPAStandAlone.initJPAStandAloneApplication(persistenceUnits);

      // Weld
      weld = new Weld();
      WeldContainer weldContainer = weld.initialize();

      bm = CDI.current().getBeanManager();
      if (null == bm) {
         throw new Exception("Unable to initialize CDI container");
      }
   }

   @AfterClass
   public static void shutdown() {
      try {
         if (null != weld) {
            weld.shutdown();
         }
      } catch (Exception ex) {
      }
   }

   @Test
   public void testNonPersistentTimers() throws Exception {
      try (ContextualInstance<MonitorBean> ci = CDIUtil.getContextualInstance(bm, MonitorBean.class);) {
         MonitorBean mb = ci.getBean();
         mb.monitorNonPersistenceScheduled();
      }
   }
}
