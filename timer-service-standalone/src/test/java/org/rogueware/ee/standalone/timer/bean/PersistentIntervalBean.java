/*
 * PersistentIntervalBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.bean;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;
import org.rogueware.ee.standalone.timer.service.TimerHandleDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
@ApplicationScoped
public class PersistentIntervalBean {

   private static final Logger log = LoggerFactory.getLogger(PersistentIntervalBean.class);

   private final AtomicBoolean errorOccured = new AtomicBoolean(false);

   private long lastCreateTimerInitialDuration = -1;
   private final AtomicInteger ticksCreateTimerInitialDuration = new AtomicInteger(0);

   private long lastCreateIntervalTimerInitialDuration = -1;
   private final AtomicInteger ticksCreateIntervalTimerInitialDuration = new AtomicInteger(0);

   private long lastCreateTimerInitialExpiration = -1;
   private final AtomicInteger ticksCreateTimerInitialExpiration = new AtomicInteger(0);

   private long lastCreateIntervalTimerInitialExpiration = -1;
   private final AtomicInteger ticksCreateIntervalTimerInitialExpiration = new AtomicInteger(0);

   private final AtomicInteger expireAfterTicks = new AtomicInteger(Integer.MAX_VALUE);
   private final AtomicBoolean cancelAll = new AtomicBoolean(false);

   @Resource
   @Inject
   private TimerService timerService;

   @Inject
   UserTransaction ut;

   @Inject
   @PersistenceContext(unitName = "TimerServicePU")
   EntityManager em;

   @PostConstruct
   public void init() {
   }

   @Transactional(Transactional.TxType.REQUIRES_NEW)
   public void createTimers() {
      // If there is no transaction, the timerService will create and commit one per create timer call if its persistent, otherwise will work with an existing transaction
      lastCreateTimerInitialDuration = System.currentTimeMillis();
      timerService.createTimer(100, 100, "createTimer_initialDuration");

      lastCreateIntervalTimerInitialDuration = System.currentTimeMillis();
      timerService.createIntervalTimer(200, 200, new TimerConfig("createIntervalTimer_initialDuration", true));

      lastCreateTimerInitialExpiration = System.currentTimeMillis();
      timerService.createTimer(new Date(System.currentTimeMillis() + 300), 300, "createTimer_initialExpiration");

      lastCreateIntervalTimerInitialExpiration = System.currentTimeMillis();
      timerService.createIntervalTimer(new Date(System.currentTimeMillis() + 400), 400, new TimerConfig("createIntervalTimer_initialExpiration", true));
   }

   @Transactional(Transactional.TxType.REQUIRES_NEW)
   public void validateTimers(int quantity) {
      try {
         Query q = em.createQuery("SELECT e FROM TimerHandleDB e WHERE e.intervalStart IS NOT NULL");
         List<TimerHandleDB> res = (List<TimerHandleDB>) q.getResultList();

         if (quantity != res.size()) {
            errorOccured.set(true);
            log.error(String.format("Timer Service - Expected %d interval timers in database", quantity));
            throw new RuntimeException(String.format("Timer Service - Expected %d interval timers in database", quantity));
         }

         if (quantity != timerService.getTimers().size()) {
            errorOccured.set(true);
            log.error(String.format("Timer Service - Expected %d interval timers in timer service for class", quantity));
            throw new RuntimeException(String.format("Timer Service - Expected %d interval timers in timer service for class", quantity));
         }
      } catch (Exception ex) {
         errorOccured.set(true);
         log.error("Timer Service - Error occured determining if timers in database", ex);
         throw new RuntimeException("Timer Service - Error occured determining if timers in database", ex);
      }
   }

   @Timeout
   public void timeout(Timer timer) {
      long interval = 0;

      if (cancelAll.get()) {
         timer.cancel();
         return;
      }

      if (timer.getInfo().equals("createTimer_initialDuration")) {
         ticksCreateTimerInitialDuration.getAndIncrement();
         interval = System.currentTimeMillis() - lastCreateTimerInitialDuration;
         if (-1 == lastCreateTimerInitialDuration) {  // Handle initial startup after persistent timers are restored
            lastCreateTimerInitialDuration = System.currentTimeMillis();
            return;
         }
         lastCreateTimerInitialDuration = System.currentTimeMillis();
         if (!(interval >= 75 && interval <= 150)) {
            errorOccured.set(true);
            log.error(String.format("Timer Service - createTimer_initialDuration interval %dms occurance out of range", interval));
            throw new RuntimeException("Timer Service - createTimer_initialDuration interval occurance out of range");
         }
      } else if (timer.getInfo().equals("createIntervalTimer_initialDuration")) {
         ticksCreateIntervalTimerInitialDuration.getAndIncrement();
         interval = System.currentTimeMillis() - lastCreateIntervalTimerInitialDuration;
         if (-1 == lastCreateIntervalTimerInitialDuration) {  // Handle initial startup after persistent timers are restored
            lastCreateIntervalTimerInitialDuration = System.currentTimeMillis();
            return;
         }
         lastCreateIntervalTimerInitialDuration = System.currentTimeMillis();
         if (!(interval >= 175 && interval <= 250)) {
            errorOccured.set(true);
            log.error(String.format("Timer Service - createIntervalTimer_initialDuration interval %dms occurance out of range", interval));
            throw new RuntimeException("Timer Service - createIntervalTimer_initialDuration interval occurance out of range");
         }
      } else if (timer.getInfo().equals("createTimer_initialExpiration")) {
         ticksCreateTimerInitialExpiration.getAndIncrement();
         interval = System.currentTimeMillis() - lastCreateTimerInitialExpiration;
         if (-1 == lastCreateTimerInitialExpiration) {  // Handle initial startup after persistent timers are restored
            lastCreateTimerInitialExpiration = System.currentTimeMillis();
            return;
         }

         // Simulate 150ms of processing time on each 4th iteration
         if (ticksCreateTimerInitialExpiration.get() == 2 || ticksCreateTimerInitialExpiration.get() == 5) {
            try {
               log.info("Timer sleeping 150ms to simulate work load");
               Thread.sleep(150);
            } catch (InterruptedException ex) {
            }
         }
         lastCreateTimerInitialExpiration = System.currentTimeMillis(); // Should only schedule timer 150ms after this as interval is from start, not from end of execution!!!!

         // If after every x iteration, check processing lag for last tick as 4th iteration simulating 150ms of processing time, if event, check normal interval
         if (ticksCreateTimerInitialExpiration.get() == 3 || ticksCreateTimerInitialExpiration.get() == 6) {
            if (!(interval >= 135 && interval <= 200)) {
               errorOccured.set(true);
               log.error(String.format("Timer Service - createTimer_initialExpiration interval %dms occurance out of range simulating processing time", interval));
               throw new RuntimeException("Timer Service - createTimer_initialExpiration interval occurance out of range simulating processing time");
            }
         } else {
            if (!(interval >= 275 && interval <= 350)) {
               errorOccured.set(true);
               log.error(String.format("Timer Service - createTimer_initialExpiration interval %dms occurance out of range", interval));
               throw new RuntimeException("Timer Service - createTimer_initialExpiration interval occurance out of range");
            }
         }
      } else if (timer.getInfo().equals("createIntervalTimer_initialExpiration")) {
         ticksCreateIntervalTimerInitialExpiration.getAndIncrement();
         interval = System.currentTimeMillis() - lastCreateIntervalTimerInitialExpiration;
         if (-1 == lastCreateIntervalTimerInitialExpiration) {  // Handle initial startup after persistent timers are restored
            lastCreateIntervalTimerInitialExpiration = System.currentTimeMillis();
            return;
         }
         lastCreateIntervalTimerInitialExpiration = System.currentTimeMillis();
         if (!(interval >= 375 && interval <= 450)) {
            errorOccured.set(true);
            log.error(String.format("Timer Service - createIntervalTimer_initialExpiration interval %dms occurance out of range", interval));
            throw new RuntimeException("Timer Service - createIntervalTimer_initialExpiration interval occurance out of range");
         }
      }

      // Cancel after 10 ticks (Testing a timer cancel)
      if (timer.getInfo().equals("createTimer_initialDuration") && ticksCreateTimerInitialDuration.get() == expireAfterTicks.get()) {
         timer.cancel();
      }
      if (timer.getInfo().equals("createTimer_initialDuration") && ticksCreateTimerInitialDuration.get() > expireAfterTicks.get()) {
         errorOccured.set(true);
         log.error("Timer Service - createTimer_initialDuration should have been cancelled");
         throw new RuntimeException("Timer Service - createTimer_initialDuration should have been cancelled");
      }

      log.info(String.format("Interval timer expired after %dms interval", interval));
   }

   public int getTicksCreateTimerInitialDuration() {
      return ticksCreateTimerInitialDuration.get();
   }

   public int getTicksCreateIntervalTimerInitialDuration() {
      return ticksCreateIntervalTimerInitialDuration.get();
   }

   public int getTicksCreateTimerInitialExpiration() {
      return ticksCreateTimerInitialExpiration.get();
   }

   public int getTicksCreateIntervalTimerInitialExpiration() {
      return ticksCreateIntervalTimerInitialExpiration.get();
   }

   public boolean getErrorOccured() {
      return errorOccured.get();
   }

   public void setExpireAfterTicks(int expireAfterTicks) {
      this.expireAfterTicks.set(expireAfterTicks);
   }

   public void setCancelAll(boolean cancelAll) {
      this.cancelAll.set(cancelAll);
   }

}
