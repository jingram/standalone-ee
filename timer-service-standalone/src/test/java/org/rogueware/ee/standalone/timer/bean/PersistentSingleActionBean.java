/*
 * PersistentSingleActionBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.bean;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;
import org.rogueware.ee.standalone.timer.service.TimerHandleDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
@ApplicationScoped
public class PersistentSingleActionBean {

   private static final Logger log = LoggerFactory.getLogger(PersistentSingleActionBean.class);

   private AtomicBoolean errorOccured = new AtomicBoolean(false);
   private AtomicInteger programaticTimerExecuted = new AtomicInteger(0);

   
   @Resource
   private TimerService timerService;

   @Inject
   UserTransaction ut;

   @Inject
   @PersistenceContext(unitName = "TimerServicePU")
   EntityManager em;

   @PostConstruct
   public void init() {
   }

   @Transactional(Transactional.TxType.REQUIRES_NEW)
   public void createFailedTimers() {
      // If there is no transaction, the timerService will create and commit one per create timer call if its persistent, otherwise will work with an existing one
      timerService.createTimer(4800, "failed");
      throw new IllegalArgumentException("Rollback");
   }   
   
   @Transactional(Transactional.TxType.REQUIRES_NEW)
   public void createTimers() {
      // If there is no transaction, the timerService will create and commit one per create timer call if its persistent, otherwise will work with an existing one
      timerService.createTimer(4800, "createTimer_duration");
      timerService.createTimer(new Date(System.currentTimeMillis() + 4900), "createTimer_expiration");
      timerService.createSingleActionTimer(5000, new TimerConfig("createSingleActionTimer_duration", true));
      timerService.createSingleActionTimer(new Date(System.currentTimeMillis() + 5100), new TimerConfig("createSingleActionTimer_expiration", true));
   }

   @Transactional(Transactional.TxType.REQUIRES_NEW)
   public void validateTimers(int quantity) {
      try {
         Query q = em.createQuery("SELECT e FROM TimerHandleDB e WHERE e.singleActionExpires IS NOT NULL");
         List<TimerHandleDB> res = (List<TimerHandleDB>) q.getResultList();

         if (quantity != res.size()) {
            errorOccured.set(true);
            log.error(String.format("Timer Service - Expected %d single action timers in database", quantity));
            throw new RuntimeException(String.format("Timer Service - Expected %d single action timers in database", quantity));
         }
         
         if (quantity != timerService.getTimers().size()){
            errorOccured.set(true);
            log.error(String.format("Timer Service - Expected %d single action timers in timer service for class", quantity));
            throw new RuntimeException(String.format("Timer Service - Expected %d single action timers in timer service for class", quantity));
         }
      } catch (Exception ex) {
         errorOccured.set(true);
         log.error("Timer Service - Error occured determining if timers in database", ex);
         throw new RuntimeException("Timer Service - Error occured determining if timers in database", ex);
      }
   }   

   @Timeout
   public void timeout(Timer timer) {
      if (timer.getInfo().equals("createTimer_duration"))
         programaticTimerExecuted.getAndIncrement();
      else if (timer.getInfo().equals("createTimer_expiration"))
         programaticTimerExecuted.getAndIncrement();
      else if (timer.getInfo().equals("createSingleActionTimer_duration"))
         programaticTimerExecuted.getAndIncrement();
      else if (timer.getInfo().equals("createSingleActionTimer_expiration"))
         programaticTimerExecuted.getAndIncrement();
      log.info(String.format("Timeout called %d", programaticTimerExecuted.get()));
   }

   public int getProgramaticTimerExecuted() {
      return programaticTimerExecuted.get();
   }

   public boolean getErrorOccured() {
      return errorOccured.get();
   }

}
