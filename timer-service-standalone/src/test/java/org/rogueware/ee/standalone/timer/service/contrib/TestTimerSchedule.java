/*
 * TestTimerSchedule.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.service.contrib;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.ScheduleExpression;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestTimerSchedule {

   private static SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

   @Test
   public void testTimerScheduleEvery30Seconds() {
      ScheduleExpression se = getDefault();
      se.second("*/30");
      se.minute("*");
      se.hour("*");
      
      TimerSchedule ts = new TimerSchedule(se);
      Calendar nextTimeout = ts.getNextTimeout();

      Calendar cmp = Calendar.getInstance();
      if (cmp.get(Calendar.SECOND) >= 30) {
         cmp.set(Calendar.SECOND, 0);
         cmp.add(Calendar.MINUTE, 1);
      } else {
         cmp.set(Calendar.SECOND, 30);
      }

      System.out.println(String.format("Current: %s - Next: %s", iso.format(new Date()), iso.format(nextTimeout.getTime())));
      assertEquals("Every 30s schedule is incorrect", iso.format(cmp.getTime()), iso.format(nextTimeout.getTime()));

      for (int i = 0; i < 100; i++) {
         cmp.setTime(nextTimeout.getTime());
         cmp.add(Calendar.SECOND, 30);
         nextTimeout = ts.getNextTimeout(nextTimeout.getTime());
         System.out.println(String.format("Prev: %s - Next: %s", iso.format(cmp.getTime()), iso.format(nextTimeout.getTime())));
         assertEquals("Every 30s schedule is incorrect", iso.format(cmp.getTime()), iso.format(nextTimeout.getTime()));
      }
   }

   @Test
   public void testTimerScheduleSpecified() {
      ScheduleExpression se = getDefault();
      se.second("45");
      se.minute("30");
      se.hour("14");
      se.dayOfWeek("Tue,Thu,Sun");

      TimerSchedule ts = new TimerSchedule(se);
      
      Calendar start = Calendar.getInstance();
      start.set(Calendar.SECOND, 0);
      start.set(Calendar.MINUTE, 0);
      start.set(Calendar.HOUR, 0);
      start.set(Calendar.DAY_OF_MONTH, 3);  // Monday 3rd March 2014
      start.set(Calendar.MONTH, 2);
      start.set(Calendar.YEAR, 2014);
      
      
      Calendar nextTimeout = ts.getNextTimeout(start.getTime());
      System.out.println(String.format("Next: %s", iso.format(nextTimeout.getTime())));
      assertEquals("Specified schedule is incorrect","2014-03-04 14:30:45", iso.format(nextTimeout.getTime()));
      
      nextTimeout = ts.getNextTimeout(nextTimeout.getTime());
      System.out.println(String.format("Next: %s", iso.format(nextTimeout.getTime())));
      assertEquals("Specified schedule is incorrect","2014-03-06 14:30:45", iso.format(nextTimeout.getTime()));
      
      nextTimeout = ts.getNextTimeout(nextTimeout.getTime());
      System.out.println(String.format("Next: %s", iso.format(nextTimeout.getTime())));
      assertEquals("Specified schedule is incorrect","2014-03-09 14:30:45", iso.format(nextTimeout.getTime()));
      
      nextTimeout = ts.getNextTimeout(nextTimeout.getTime());
      System.out.println(String.format("Next: %s", iso.format(nextTimeout.getTime())));
      assertEquals("Specified schedule is incorrect","2014-03-11 14:30:45", iso.format(nextTimeout.getTime()));
   }

  // @Test   ... Damn test broken. Assumes schedule has to continue forever ... need to ponder if this is ok or not
   public void testTimerScheduleSpecifiedBecomesNull() {
      ScheduleExpression se = getDefault();
      se.second("45");
      se.minute("30");
      se.hour("14");
      se.month("3");
      //se.year("2014");
      se.dayOfMonth("30");
      se.dayOfWeek("Sun");
   
      TimerSchedule ts = new TimerSchedule(se);
      
      Calendar start = Calendar.getInstance();
      start.set(Calendar.SECOND, 0);
      start.set(Calendar.MINUTE, 0);
      start.set(Calendar.HOUR, 0);
      start.set(Calendar.DAY_OF_MONTH, 27);  // Thursday 27th March 2014  (One more sunday in month)
      start.set(Calendar.MONTH, 2);
      start.set(Calendar.YEAR, 2014);  
      
      Calendar nextTimeout = ts.getNextTimeout(start.getTime());
      System.out.println(String.format("Next: %s", iso.format(nextTimeout.getTime())));
      assertEquals("Specified schedule null is incorrect","2014-03-30 14:30:45", iso.format(nextTimeout.getTime()));
      
      nextTimeout = ts.getNextTimeout(nextTimeout.getTime());
      System.out.println(String.format("Next: %s", iso.format(nextTimeout.getTime())));
      assertNull("Specified schedule null is incorrect", nextTimeout);
   }
   
   private ScheduleExpression getDefault() {
      return new ScheduleExpression().
            second("0").
            minute("0").
            hour("0").
            dayOfMonth("*").
            month("*").
            dayOfWeek("*").
            year("*").
            timezone("").
            start(null).
            end(null);
   }
}
