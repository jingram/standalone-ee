/*
 * MonitorBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.bean;

import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
public class MonitorBean {

   private static final Logger log = LoggerFactory.getLogger(MonitorBean.class);

   @Inject
   private ScheduledNonPersistentBean scheduledNonPersistentBean;

   @Inject
   private PersistentSingleActionBean persistentSingleActionBean;

   @Inject
   private PersistentIntervalBean persistenIntervalBean;

   @Inject
   private PersistentCalendarBean persistenCalendarBean;
   
   public void createPersistentCalendarTimer() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting calendar persistence Timer Service Test");
      log.info("-------------------------------------------------------------------------");

      persistenCalendarBean.validateTimers(0);

      // Create the timers
      persistenCalendarBean.createTimers();
      persistenCalendarBean.validateTimers(2);
  
      long start = System.currentTimeMillis();
      boolean allPassed = false;
      while (System.currentTimeMillis() - start < 5000 && !allPassed) {
         if (persistenCalendarBean.getErrorOccured()) {
            throw new InterruptedException("Detected error in persistenCalendarBean timer service test ");
         }
         // All timers must at least fire twice before we simulate server restart
         allPassed = (persistenCalendarBean.getTicksEveryTwo() > 1
               && persistenCalendarBean.getTicksEveryFive() > 0);
         Thread.sleep(25);
      }

      if (!allPassed) {
         throw new InterruptedException("Detected timeout while waiting for persistenCalendarBean timer service test to complete");
      }
      log.info("-------------------------------------------------------------------------");
   }   
   
     public void monitorPersistentCalendarTimer() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Monitoring calendar persistence Timer Service Test");
      log.info("-------------------------------------------------------------------------");

      // Should have been restored after restart
      persistenCalendarBean.validateTimers(2);

      
      long start = System.currentTimeMillis();
      boolean allPassed = false;
      while (System.currentTimeMillis() - start < 10000 && !allPassed) {
         if (persistenCalendarBean.getErrorOccured()) {
            throw new InterruptedException("Detected error in persistenCalendarBean timer service test ");
         }
         allPassed = (persistenCalendarBean.getTicksEveryTwo() > 1
               && persistenCalendarBean.getTicksEveryFive() > 0);
         Thread.sleep(25);
      }

      if (!allPassed) {
         throw new InterruptedException("Detected timeout while waiting for persistenCalendarBean timer service test to complete");
      }
      // Wait for timers transctions to complete and delete
      Thread.sleep(500);
      persistenCalendarBean.validateTimers(2);

      log.info("-------------------------------------------------------------------------");
   }    
   
   public void createPersistentIntervalTimer() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting interval persistence Timer Service Test");
      log.info("-------------------------------------------------------------------------");

      persistenIntervalBean.validateTimers(0);

      // Create the timers
      persistenIntervalBean.createTimers();
      persistenIntervalBean.validateTimers(4);
  
      long start = System.currentTimeMillis();
      boolean allPassed = false;
      while (System.currentTimeMillis() - start < 5000 && !allPassed) {
         if (persistenIntervalBean.getErrorOccured()) {
            throw new InterruptedException("Detected error in persistenIntervalBean timer service test ");
         }
         // All timers must at least fire twice before we simulate server restart
         allPassed = (persistenIntervalBean.getTicksCreateTimerInitialDuration() > 1
               && persistenIntervalBean.getTicksCreateIntervalTimerInitialDuration() > 1
               && persistenIntervalBean.getTicksCreateTimerInitialExpiration() > 1
               && persistenIntervalBean.getTicksCreateIntervalTimerInitialExpiration() > 1);
         Thread.sleep(25);
      }
     
      if (!allPassed) {
         throw new InterruptedException("Detected timeout while waiting for persistenIntervalBean timer service test to complete");
      }
      log.info("-------------------------------------------------------------------------");
   }

   public void monitorPersistentIntervalTimer() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Monitoring interval persistence Timer Service Test");
      log.info("-------------------------------------------------------------------------");

      // Should have been restored after restart
      persistenIntervalBean.validateTimers(4);
      persistenIntervalBean.setExpireAfterTicks(10);
      
      long start = System.currentTimeMillis();
      boolean allPassed = false;
      while (System.currentTimeMillis() - start < 10000 && !allPassed) {
         if (persistenIntervalBean.getErrorOccured()) {
            throw new InterruptedException("Detected error in persistenIntervalBean timer service test ");
         }
         allPassed = (persistenIntervalBean.getTicksCreateTimerInitialDuration() == 10
               && persistenIntervalBean.getTicksCreateIntervalTimerInitialDuration() > 5
               && persistenIntervalBean.getTicksCreateTimerInitialExpiration() > 5
               && persistenIntervalBean.getTicksCreateIntervalTimerInitialExpiration() > 5);
         Thread.sleep(25);
      }

      if (!allPassed) {
         throw new InterruptedException("Detected timeout while waiting for persistenIntervalBean timer service test to complete");
      }
      // Wait for timers transctions to complete and delete
      Thread.sleep(200);
      persistenIntervalBean.validateTimers(3);   // One got cancelled after 20 ticks

      log.info("Cancelling timers");
      persistenIntervalBean.setCancelAll(true);
      Thread.sleep(500);
      persistenIntervalBean.validateTimers(0);   // All are now cancelled
      

      log.info("-------------------------------------------------------------------------");
   }   
   
   public void createPersistentSingleActionTimer() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting single action persistence Timer Service Test");
      log.info("-------------------------------------------------------------------------");

      persistentSingleActionBean.validateTimers(0);
      try {
         persistentSingleActionBean.createFailedTimers();
         throw new InterruptedException("Method createFailedTimers should have thrown exception to rollback transaction");
      } catch (Exception ex) {
         // Creation of timers should have been rolled back
      }
      persistentSingleActionBean.validateTimers(0);  // No timers should be created!!!

      // Create the timers
      persistentSingleActionBean.createTimers();
      persistentSingleActionBean.validateTimers(4);
      log.info("-------------------------------------------------------------------------");
   }

   public void monitorPersistentSingleActionTimer() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Monitoring single action persistence Timer Service Test");
      log.info("-------------------------------------------------------------------------");

      // Should have been restored after restart
      persistentSingleActionBean.validateTimers(4);

      long start = System.currentTimeMillis();
      boolean allPassed = false;
      while (System.currentTimeMillis() - start < 10000 && !allPassed) {
         if (persistentSingleActionBean.getErrorOccured()) {
            throw new InterruptedException("Detected error in persistentSingleActionBean timer service test ");
         }
         allPassed = (4 == persistentSingleActionBean.getProgramaticTimerExecuted());
         Thread.sleep(25);
      }

      if (!allPassed) {
         throw new InterruptedException("Detected timeout while waiting for persistentSingleActionBean timer service test to complete");
      }
      // Wait for timers transctions to complete and delete
      Thread.sleep(500);
      persistentSingleActionBean.validateTimers(0);

      log.info("-------------------------------------------------------------------------");
   }

   public void monitorNonPersistenceScheduled() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Monitoring non-persistence Timer Service Test");
      log.info("-------------------------------------------------------------------------");

      long start = System.currentTimeMillis();
      boolean allPassed = false;
      while (System.currentTimeMillis() - start < 5000 && !allPassed) {
         if (scheduledNonPersistentBean.getErrorOccured()) {
            throw new InterruptedException("Detected error in scheduledNonPersistentBean timer service test ");
         }
         allPassed = (!scheduledNonPersistentBean.getErrorOccured() && scheduledNonPersistentBean.getProgramaticTimerExecuted() && 3 == scheduledNonPersistentBean.getNumScheduledTicks());
         Thread.sleep(25);
      }

      if (!allPassed) {
         throw new InterruptedException("Detected timeout while waiting for scheduledNonPersistentBean timer service test to complete");
      }

      // Make sure there are no more timers for class.
      log.info("-------------------------------------------------------------------------");
   }

}
