/*
 * PersistentCalendarBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.timer.bean;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.transaction.UserTransaction;
import org.rogueware.ee.standalone.timer.service.TimerHandleDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
@ApplicationScoped
public class PersistentCalendarBean {

   private static final Logger log = LoggerFactory.getLogger(PersistentCalendarBean.class);

   private final AtomicBoolean errorOccured = new AtomicBoolean(false);

   private final AtomicInteger ticksEveryTwo = new AtomicInteger(0);
   private final AtomicInteger ticksEveryFive = new AtomicInteger(0);
   
   private AtomicBoolean cancelAll = new AtomicBoolean(false);

   @Resource
   private TimerService timerService;

   @Inject
   UserTransaction ut;

   @Inject
   @PersistenceContext(unitName = "TimerServicePU")
   EntityManager em;

   @PostConstruct
   public void init() {
   }

   @Transactional(Transactional.TxType.REQUIRES_NEW)
   public void createTimers() {
      // If there is no transaction, the timerService will create and commit one per create timer call if its persistent, otherwise will work with an existing transaction
      
      ScheduleExpression se1 = getDefaultScheduleExpression();
      se1.second("*/2");   // Every two seconds
      se1.minute("*");
      se1.hour("*");
      timerService.createCalendarTimer(se1);

      ScheduleExpression se2 = getDefaultScheduleExpression();
      se2.second("*/5");   // Every five seconds
      se2.minute("*");
      se2.hour("*");
      timerService.createCalendarTimer(se2, new TimerConfig("createCalendarTimer", true));
   }

   @Transactional(Transactional.TxType.REQUIRES_NEW)
   public void validateTimers(int quantity) {
      try {
         Query q = em.createQuery("SELECT e FROM TimerHandleDB e WHERE e.schedule IS NOT NULL");
         List<TimerHandleDB> res = (List<TimerHandleDB>) q.getResultList();

         if (quantity != res.size()) {
            errorOccured.set(true);
            log.error(String.format("Timer Service - Expected %d calendar timers in database", quantity));
            throw new RuntimeException(String.format("Timer Service - Expected %d calendar timers in database", quantity));
         }

         if (quantity != timerService.getTimers().size()) {
            errorOccured.set(true);
            log.error(String.format("Timer Service - Expected %d calendar timers in timer service for class", quantity));
            throw new RuntimeException(String.format("Timer Service - Expected %d calendar timers in timer service for class", quantity));
         }
      } catch (Exception ex) {
         errorOccured.set(true);
         log.error("Timer Service - Error occured determining if timers in database", ex);
         throw new RuntimeException("Timer Service - Error occured determining if timers in database", ex);
      }
   }

   @Timeout
   public void timeout(Timer timer) {
      long interval = 0;

      if (cancelAll.get()) {
         timer.cancel();
         return;
      }

      if (timer.getInfo().equals("")) {
         ticksEveryTwo.getAndIncrement();
         Calendar c = Calendar.getInstance();
         if (0 != c.get(Calendar.SECOND) % 2){
            errorOccured.set(true);
            log.error("Timer Service - Schedule 2 second timer did not tick on a second divisible by 2");
            throw new RuntimeException("Timer Service - Schedule 2 second timer did not tick on a second divisible by 2");
         }  
      } else if (timer.getInfo().equals("createCalendarTimer")) {
         ticksEveryFive.getAndIncrement();
         Calendar c = Calendar.getInstance();
         if (0 != c.get(Calendar.SECOND) % 5){
            errorOccured.set(true);
            log.error("Timer Service - Schedule 2 second timer did not tick on a second divisible by 5");
            throw new RuntimeException("Timer Service - Schedule 5 second timer did not tick on a second divisible by 5");
         }  
      } 

      log.info(String.format("Schedule timer run on second %s", Calendar.getInstance().get(Calendar.SECOND)));
   }

   public boolean getErrorOccured() {
      return errorOccured.get();
   }

   public int getTicksEveryTwo() {
      return ticksEveryTwo.get();
   }

   public int getTicksEveryFive() {
      return ticksEveryFive.get();
   }  
   
   private ScheduleExpression getDefaultScheduleExpression() {
      return new ScheduleExpression().
            second("0").
            minute("0").
            hour("0").
            dayOfMonth("*").
            month("*").
            dayOfWeek("*").
            year("*").
            timezone("").
            start(null).
            end(null);
   }

}
