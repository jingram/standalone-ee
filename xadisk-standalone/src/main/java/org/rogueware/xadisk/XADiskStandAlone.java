/*
 * XADiskStandAlone.java
 * 
 * Defined a class to initialize the XADisk environment
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.xadisk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;
import org.rogueware.jta.JTAStandAlone;
import org.xadisk.bridge.proxies.interfaces.Session;
import org.xadisk.bridge.proxies.interfaces.XAFileSystem;
import org.xadisk.bridge.proxies.interfaces.XAFileSystemProxy;
import org.xadisk.bridge.proxies.interfaces.XASession;
import org.xadisk.filesystem.standalone.StandaloneFileSystemConfiguration;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class XADiskStandAlone {

   private static final Map<String, XAFileSystem> xafss = Collections.synchronizedMap(new HashMap<String, XAFileSystem>());
   private static String primayInstanceId;

   public static void initJPAStandAloneApplication(List<FileSystemConfigurationEntry> instances) throws InterruptedException {
      // For each instance defined, create a XA file system
      for (FileSystemConfigurationEntry fsc : instances) {
         StandaloneFileSystemConfiguration sfsc = new StandaloneFileSystemConfiguration(fsc.getInstanceId(), fsc.getXaDiskSystemDirectory());
         XAFileSystem xafs = XAFileSystemProxy.bootNativeXAFileSystem(sfsc);
         xafs.waitForBootup(-1);

         // TODO: Register XA File System factory with JNDI
         xafss.put(fsc.getInstanceId(), xafs);
         if (null == primayInstanceId)
            primayInstanceId = fsc.getInstanceId();
      }
   }

   public static synchronized void destroySingleton() {
      xafss.clear();
      // TODO: De-register XA File System factory with JNDI
   }

   public static List<String> getXADiskInstanceIds() {
      return new ArrayList<>(xafss.keySet());
   }

   public static Session createLocalTransactionSession() {
      // Gets the primary PU creatred
      if (null != primayInstanceId) {
         return createLocalTransactionSession(primayInstanceId);
      }
      return null;
   }

   public static Session createLocalTransactionSession(String instanceId) {
      XAFileSystem xafs = null;
      if (null == (xafs = xafss.get(instanceId))) {
         return null;
      }

      Session session = xafs.createSessionForLocalTransaction();
      return session;
   }

   public static XASession createXATransactionSession() {
      // Gets the primary PU creatred
      if (null != primayInstanceId) {
         return createXATransactionSession(primayInstanceId);
      }
      return null;
   }

   public static XASession createXATransactionSession(String instanceId) {
      XAFileSystem xafs = null;
      if (null == (xafs = xafss.get(instanceId))) {
         return null;
      }

      // Register with the current transaction
      TransactionManager tm = JTAStandAlone.getTransactionManager();
      try {
         if (null == tm || null == tm.getTransaction()) {
            return null;
         }

         XASession xaSession = xafs.createSessionForXATransaction();
         XAResource xarXADisk = xaSession.getXAResource();
         tm.getTransaction().enlistResource(xarXADisk);
         return xaSession;
      } catch (SystemException | IllegalStateException | RollbackException ex) {
         return null;
      }
   }

   public static XAFileSystem getXAFileSystem() {
      // Gets the primary PU creatred
      if (null != primayInstanceId) {
         return getXAFileSystem(primayInstanceId);
      }
      return null;
   }

   public static XAFileSystem getXAFileSystem(String instanceId) {
      XAFileSystem xafs = null;
      if (null == (xafs = xafss.get(instanceId))) {
         return null;
      }
      return xafs;
   }
}
