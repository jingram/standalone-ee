/*
 * FileSystemConfigurationEntry.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.xadisk;

import org.apache.commons.configuration.SubnodeConfiguration;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class FileSystemConfigurationEntry {

   private final String instanceId;
   private final String xaDiskSystemDirectory;

   public FileSystemConfigurationEntry(SubnodeConfiguration config) {
      instanceId = config.getString("[@id]");
      xaDiskSystemDirectory = config.getString("[@xaDiskSystemDirectory]");
   }

   public FileSystemConfigurationEntry(String instanceId, String xaDiskSystemDirectory) {
      this.instanceId = instanceId;
      this.xaDiskSystemDirectory = xaDiskSystemDirectory;

   }

   public String getInstanceId() {
      return instanceId;
   }

   public String getXaDiskSystemDirectory() {
      return xaDiskSystemDirectory;
   }

}
