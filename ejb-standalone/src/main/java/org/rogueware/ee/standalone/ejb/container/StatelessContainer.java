/*
 * StatelessContainer.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.container;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJBException;
import javax.ejb.SessionContext;
import javax.enterprise.inject.Vetoed;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.naming.Context;
import javax.naming.NamingException;
import net.sf.cglib.proxy.Enhancer;
import org.rogueware.cdi.util.CDIUtil;
import org.rogueware.cdi.util.ContextualInstance;
import org.rogueware.ee.standalone.jndi.JNDI;
import org.rogueware.ee.standalone.jndi.ThreadContextFactory;
import org.rogueware.ee.standalone.jndi.cdi.extension.JNDIResourceInjectionUtility;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 * @param <T>
 */
@Vetoed
public class StatelessContainer<T> {

   private transient static final org.slf4j.Logger log = LoggerFactory.getLogger(StatelessContainer.class);

   private static final int DEFAULT_CONCURRENT_COUNT = 2;
   private static final int DEFAULT_WAIT_TIMEOUT = 5000;

   private StatelessEJBDelegate<T> statelessEjbDelegate;
   private T concreteSuperClassDelegate;
   private StatelessSessionContext<T> sessionContext;

   private final Class<T> ejbClass;
   private final List<ContextualInstance<T>> cdiBeans = new ArrayList<>();    // List of all known beans
   private BlockingQueue<T> beans;                                      // List of available beans
   private final AtomicBoolean running = new AtomicBoolean(false);

   // Thread Var management to re-allocate same instance of bean to the same thread
   private final ThreadLocal threadAllocatedBean = new ThreadLocal();
   private final ThreadLocal<AtomicInteger> threadAllocatedBeanReference = new ThreadLocal<>();

   public StatelessContainer(Class<T> klass) throws IllegalThreadStateException {
      this.ejbClass = klass;

      createProxy();

      // Create the session
      this.sessionContext = new StatelessSessionContext<>(ejbClass, statelessEjbDelegate);
   }

   public T getConcreteSuperClassDelegate() {
      return concreteSuperClassDelegate;
   }

   public Class<T> getEjbClass() {
      return ejbClass;
   }

   public SessionContext getSessionContext() {
      return sessionContext;
   }

   public boolean isRunning() {
      return running.get();
   }

   public synchronized void startup() {
      // TODO: Add system properties to override size (override default or per class)
      beans = new ArrayBlockingQueue<>(DEFAULT_CONCURRENT_COUNT, true);

      // Populate beans calling @PostCreate life cycle event
      Exception lastException = null;

      try {
         setupComponentJNDI();
         for (int i = 0; i < DEFAULT_CONCURRENT_COUNT; i++) {
            try {
               ContextualInstance<T> ctxI = CDIUtil.getContextualInstance(getBeanManager(), ejbClass);

               // Process injections and @PostConstruct
               try {
                  // Inject any resources
                  JNDIResourceInjectionUtility.injectJNDIResources(ctxI.getBean(), ejbClass, true);
                  
                  // Inject any EJB references
                  EJBInjectionUtility.injectEJBDelegates(ctxI.getBean(), ejbClass);
                  
                  fireLifeCycleEvent(ctxI.getBean(), PostConstruct.class);
               } catch (RuntimeException ex) {
                  log.error(String.format("Stateless EJB container for class %s threw RuntimeException while processing @PostConstruct life cycle event, aborting EJB instance creation", ejbClass.getName()), ex);
                  continue;
               }

               cdiBeans.add(ctxI);
               beans.add(ctxI.getBean());              
            } catch (Exception ex) {
               // Avoid logging duplicate exceptions
               if (null == lastException || !ex.getClass().equals(lastException.getClass())) {
                  log.warn(String.format("Stateless EJB container for class %s threw exception while creating EJB instance, aborting EJB instance creation", ejbClass.getName()), ex);
               }
               lastException = ex;
            }
         }
      } catch (NamingException ex) {
         log.error(String.format("Stateless EJB container for class %s threw NamingException while processing @PostConstruct life cycle event, aborting EJB instance creation", ejbClass.getName()), ex);         
      } finally {
         cleanupComponentJNDI();
      }

      // If we have < 50% of the available beans, cancel the stateless container
      if (0 == beans.size() || beans.size() < DEFAULT_CONCURRENT_COUNT / 2) {
         log.error(String.format("Stateless EJB container for class %s could not instantiate more than 50%% of instances, aborting container", ejbClass.getName()));
         throw new IllegalThreadStateException(String.format("Stateless EBJ container for class %s aborted", ejbClass.getName()));
      }

      running.set(true);  // Container has WELD proxies, now start returning concrete class proxies!!!
      log.info(String.format("Stateless EJB container for class %s created %d bean instances", ejbClass.getName(), beans.size()));
   }

   public void shutdown(long timeoutMs) {
      running.set(false);

// TODO: Remove delegate from JNDI      
      // Wait for all beans to return or timeout
      long timeout = System.currentTimeMillis() + timeoutMs;
      while (cdiBeans.size() != beans.size() && System.currentTimeMillis() <= timeout) {
         try {
            Thread.sleep(50);
         } catch (InterruptedException ex) {
            Logger.getLogger(StatelessContainer.class.getName()).log(Level.SEVERE, null, ex);
         }
      }

      if (System.currentTimeMillis() > timeout && cdiBeans.size() != beans.size()) {
         log.warn(String.format("Stateless EJB container for class %s timeout while waiting for all bean executions to complete. Forcing shutdown", ejbClass.getName()));
      }

      if (null != beans) {
         beans.clear();
      }

      try {
         setupComponentJNDI();
         for (ContextualInstance<T> ctxI : cdiBeans) {
            try {
               // Process @PreDestroy
               try {
                  // Inject any resources
                  JNDIResourceInjectionUtility.injectJNDIResources(ctxI.getBean(), ejbClass, true);
                  fireLifeCycleEvent(ctxI.getBean(), PreDestroy.class);
               } catch (RuntimeException ex) {
                  log.error(String.format("Stateless EJB container for class %s threw RuntimeException while processing @PreDestroy life cycle event", ejbClass.getName()), ex);
               }

               ctxI.close();
            } catch (Exception ex) {
               log.warn(String.format("Stateless EJB container for class %s was unable to destroy EJB instance", ejbClass.getName()), ex);
            }
         }
      } catch (NamingException ex) {
      } finally {
         cleanupComponentJNDI();
      }

      cdiBeans.clear();
      log.info(String.format("Stateless EJB container for class %s shutdown", ejbClass.getName()));
   }

   public int getSize() {
      return cdiBeans.size();
   }

   public int getAvailabe() {
      return beans.size();
   }

   public T requestBean() throws EJBException {

      if (!running.get()) {
         log.warn(String.format("Stateless EJB container for class %s shutdown, not returning EJB", ejbClass.getName()));
         throw new EJBException(String.format("Stateless EJB container for class %s shutdown, not returning EJB", ejbClass.getName()));
      }

      try {
         // If thread already allocated bean, increment reference and return the same bean instance
         if (null != threadAllocatedBean.get()) {
            T bean = (T) threadAllocatedBean.get();

            log.trace(String.format("Stateless EJB container for class %s, returned already allocated EJB to thread, reference count %d", ejbClass.getName(),
                    threadAllocatedBeanReference.get().incrementAndGet()));
            return bean;
         }

         // TODO: Add system property to override default timeout
         T bean = beans.poll(DEFAULT_WAIT_TIMEOUT, TimeUnit.MILLISECONDS);
         if (null == bean) {
            log.error(String.format("Stateless EJB container for class %s, timeout occured while waiting for available EJB", ejbClass.getName()));
            throw new EJBException(String.format("Stateless EJB container for class %s, timeout occured while waiting for available EJB", ejbClass.getName()));
         }

         // Inject any resources
         JNDIResourceInjectionUtility.injectJNDIResources(bean, ejbClass, true);

         // Increment reference to bean for thread
         threadAllocatedBean.set(bean);
         threadAllocatedBeanReference.set(new AtomicInteger(1));

         log.trace(String.format("Stateless EJB container for class %s, allocated available EJB and performed resource injections, %d of %d available", ejbClass.getName(), getAvailabe(), getSize()));
         return bean;
      } catch (InterruptedException ex) {
         log.error(String.format("Stateless EJB container for class %s interrupted whilel obtaining available EJB", ejbClass.getName()));
         throw new EJBException(String.format("Stateless EJB container for class %s interrupted whilel obtaining available EJB", ejbClass.getName()));
      }
   }

   public void returnBean(T bean) {

      // Dereference thread and if 0, return bean to pool
      if (0 == threadAllocatedBeanReference.get().decrementAndGet()) {
         beans.offer(bean);
         threadAllocatedBean.set(null);
         threadAllocatedBeanReference.set(null);

         log.trace(String.format("Stateless EJB container for class %s, returned EJB, %d of %d available", ejbClass.getName(), getAvailabe(), getSize()));
      } else {
         log.trace(String.format("Stateless EJB container for class %s, dereferenced already allocated EJB to thread, reference count %d", ejbClass.getName(),
                 threadAllocatedBeanReference.get().get()));
      }
   }

   protected void setupComponentJNDI() throws NamingException {
      // Push any existing thread JNDI contexts or create one if non existent      
      try {
         ThreadContextFactory.pushContext();
         Context ctx = ThreadContextFactory.getInitialContext(null, new Hashtable<>());
         JNDI.rebindWithContextCreation(ctx, "java:comp/EJBContext", sessionContext);
// TODO: Populate the thread context with the specific bean JNDI env / comp requirements 
      } catch (NamingException ex) {
         log.error("Unable to push JNDI thread context and bind required component objects", ex);
         throw ex;
      }
   }

   protected void cleanupComponentJNDI() {
      // Pop the thread context created for this call to cleanup
      try {
         ThreadContextFactory.popContext();
      } catch (NamingException ex) {
         log.error("Unable to pop JNDI thread context", ex);
      }
   }

   private void createProxy() {
      // Create the concreate superclass proxy / delegate
      try {
         // We need to create a concrete superclass proxy as the EJB does not have an interface!!!!
         // Using CGILIB with ASM to generate new byte code class on the fly.
         // Only create this once as thread safe and preserving resources
         statelessEjbDelegate = new StatelessEJBDelegate<>(this);

         Enhancer e = new Enhancer();
         e.setClassLoader(ejbClass.getClassLoader());
         e.setSuperclass(ejbClass);
         e.setCallback(statelessEjbDelegate);
         concreteSuperClassDelegate = (T) e.create();
      } catch (Exception ex) {
         log.error(String.format("Stateless EJB container for class %s could not create concrete superclass proxy / delegate for injection references", ejbClass.getName()), ex);
         throw new IllegalThreadStateException(String.format("Stateless EBJ container for class %s aborted", ejbClass.getName()));
      }

      // TODO: Register delegate with JNDI  (Log JNDI info)          
   }

   private BeanManager getBeanManager() {
      try {
         BeanManager bm = CDI.current().getBeanManager();
         return bm;
      } catch (Exception ex) {
         throw new IllegalThreadStateException("Unable to lookup bean manager from CDI");
      }
   }

   private <A extends Annotation> void fireLifeCycleEvent(T bean, Class<A> annotation) throws RuntimeException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
      Method eventMethod = null;
      for (Method m : ejbClass.getMethods()) {
         if (m.isAnnotationPresent(annotation)) {
            eventMethod = m;
            break;
         }
      }

      if (null != eventMethod) {
         eventMethod.setAccessible(true);
         try {
            eventMethod.invoke(bean);
         } catch (InvocationTargetException ex) {
            if (ex.getCause() instanceof RuntimeException) {
               throw (RuntimeException) ex.getCause();
            } else {
               throw new IllegalArgumentException(ex.getCause());
            }
         }
      }
   }
}
