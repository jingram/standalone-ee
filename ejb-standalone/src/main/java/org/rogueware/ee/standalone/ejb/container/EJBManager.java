/*
 * CDIEJBExtension.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.container;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.enterprise.inject.Vetoed;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class EJBManager {

   private transient static final org.slf4j.Logger log = LoggerFactory.getLogger(EJBManager.class);

   private static final Map<Class, StatelessContainer> statelessEJBs = new HashMap<>();
   //private static final Map<Class, ContextualInstance> singletonEJBs = new HashMap<>();

   private static final EJBManager singleton = new EJBManager();

   public static EJBManager getEJBManager() {
      return singleton;
   }

   public void registerStatelessEJB(Class ejbKlass) {
      if (null == statelessEJBs.get(ejbKlass)) {
         try {
            StatelessContainer sc = new StatelessContainer(ejbKlass);
            statelessEJBs.put(ejbKlass, sc);
         } catch (Exception ex) {
            log.error(String.format("Unable to create stateless EJB container for class %s", ejbKlass.getName()), ex);
         }
      }
   }

   public void startUp() {
      List<Class> failedEjbs = new ArrayList<>();
      for (StatelessContainer sc : statelessEJBs.values()) {
         try {
            if (!sc.isRunning()) {
               sc.startup();
            }
         } catch (Exception ex) {
            log.error(String.format("Unable to create stateless EJB container for class %s", sc.getEjbClass().getName()), ex);
            failedEjbs.add(sc.getEjbClass());
         }
      }

      // Remove failed EJB's as available stateless EJB's
      for (Class c : failedEjbs) {
         statelessEJBs.remove(c);
      }
   }

   public void shutdown() {
      for (StatelessContainer sc : statelessEJBs.values()) {
         sc.shutdown(30000);
      }

      statelessEJBs.clear();
   }

   public boolean isValidEJB(Class ejbKlass) {
// TODDO      return statelessEJBs.containsKey(ejbKlass) || singletonEJBs.containsKey(ejbKlass);
      return statelessEJBs.containsKey(ejbKlass);
   }

   public boolean isStatelessEJB(Class ejbKlass) {
      return statelessEJBs.containsKey(ejbKlass);
   }

   public boolean isSingletonEJB(Class ejbKlass) {
      // TODO return singletonEJBs.containsKey(ejbKlass);
      return false;
   }

   public StatelessContainer getStatelessEJBContainer(Class ejbKlass) {
      /*      StatelessContainer sc = statelessEJBs.get(ejbKlass);
       if (null != sc && !sc.isRunning()) {
       try {
       sc.startup();
       } catch (Exception ex) {
       log.error(String.format("Unable to create stateless EJB container for class %s", sc.getEjbClass().getName()), ex);
       statelessEJBs.remove(sc);
       }
       }*/
      return statelessEJBs.get(ejbKlass);
   }

   public Set<Class> getStatelessEJBs() {
      return statelessEJBs.keySet();
   }
}
