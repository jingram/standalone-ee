/*
 * EJBInjectionUtility.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.container;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class EJBInjectionUtility {

   private static final Logger log = LoggerFactory.getLogger(EJBInjectionUtility.class);

   private EJBInjectionUtility() {
      // Cannot instantiate
   }

   public static void injectEJBDelegates(Object target, Class targetKlass) {
      List<Field> annotatedFields = new ArrayList<>();

      // Recurse inheritance
      Class c = targetKlass;
      while (null != c && !c.equals(Object.class)) {
         // Fields
         for (Field f : c.getDeclaredFields()) {
            if (f.isAnnotationPresent(EJB.class)) {
               if (Modifier.isStatic(f.getModifiers())) {
                  log.error(String.format("Static field %s in class %s cannot be annotated with @EJB, igoring ejb injection", f.getName(), targetKlass.getName()));
                  continue;
               }
               if (Modifier.isFinal(f.getModifiers())) {
                  log.error(String.format("Final field %s in class %s cannot be annotated with @EJB, igoring ejb injection", f.getName(), targetKlass.getName()));
                  continue;
               }

               annotatedFields.add(f);
            }
         }
         c = c.getSuperclass();
      }

      // Inject EJB delegates
      for (Field field : annotatedFields) {
         // Obtain delegate from container for EJB class
         if (!EJBManager.getEJBManager().isValidEJB(field.getType())) {
            log.error(String.format("Field %s in class %s annotated with @EJB, but no valid EJB container found", field.getName(), targetKlass.getName()));
            continue;
         }

         try {
            Object delegate = EJBManager.getEJBManager().getStatelessEJBContainer(field.getType()).getConcreteSuperClassDelegate();
            field.setAccessible(true);
            field.set(target, delegate);
         } catch (Exception ex) {
            log.error(String.format("Field %s in class %s annotated with @EJB was unable to inject delegate", field.getName(), targetKlass.getName()), ex);
         }
      }
   }

}
