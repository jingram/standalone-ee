/*
 * StatelessEJBDelegate.java
 * 
 * Defines a class used to wrap an entity manager transaction scoped
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.container;

import java.io.Serializable;
import java.lang.reflect.Method;
import javax.ejb.EJBException;
import javax.enterprise.inject.Vetoed;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class StatelessEJBDelegate<T> implements MethodInterceptor, Serializable {

   private transient static final Logger log = LoggerFactory.getLogger(StatelessEJBDelegate.class);

   private final StatelessContainer<T> container;

   public StatelessEJBDelegate(StatelessContainer<T> container) {
      this.container = container;
   }

   @Override
   public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
      T bean = null;

      container.setupComponentJNDI();
      try {
         bean = container.requestBean();

         // Invoke the method
         return proxy.invoke(bean, args);   // Exceptions are not wrapped in InvocationTargetExcetion like Dynamic Proxy
      } catch (EJBException ex) {
         log.error(String.format("Unable to obtain a Stateless EJB bean for class %s to invoke method %s", container.getEjbClass().getName(), method.getName()), ex);
         throw ex;
      } catch (Throwable ex) {
         log.error(String.format("Unhandled exception on Stateless EJB bean for class %s invoking method %s", container.getEjbClass().getName(), method.getName()), ex);
         throw new EJBException(new Exception(ex));
      } finally {
         if (null != bean) {
            container.returnBean(bean);
         }

         // Pop the thread context created for this call to cleanup
         container.cleanupComponentJNDI();
      }
   }
}
