/*
 * CDIEJBExtension.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.cdi.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.MessageDriven;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.spi.AfterDeploymentValidation;
import javax.enterprise.inject.spi.AnnotatedConstructor;
import javax.enterprise.inject.spi.AnnotatedField;
import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.BeforeShutdown;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.ProcessAnnotatedType;
import javax.enterprise.inject.spi.WithAnnotations;
import javax.inject.Named;
import org.rogueware.cdi.util.CDIUtil;
import org.rogueware.ee.standalone.ejb.container.EJBManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org) NOTE:
 * http://docs.oracle.com/javaee/7/tutorial/doc/ejb-intro007.htm
 * http://docs.oracle.com/javaee/7/tutorial/doc/transactions003.htm#BNCIJ
 * https://blogs.oracle.com/arungupta/entry/what_s_new_in_ejb
 * http://www.physics.usyd.edu.au/~rennie/javaEEReferenceSheet.html
 */
public class DiscoverEJBExtension implements Extension {

   private transient static final Logger log = LoggerFactory.getLogger(DiscoverEJBExtension.class);

   // Discover all EJB Beans and convert them to CDI with correct CDI annotations
   public <T> void processEJBAnnotatedBeans(@Observes @WithAnnotations({Stateless.class, Singleton.class, MessageDriven.class}) ProcessAnnotatedType<T> pat) {
      Class c = pat.getAnnotatedType().getJavaClass();
      // Determine type and register accordingly
      if (c.isAnnotationPresent(MessageDriven.class)) {
         // Message Driven

      } else if (c.isAnnotationPresent(Singleton.class)) {
         // Singleton

      } else if (c.isAnnotationPresent(Stateless.class)) {
         // Stateless      
         if (!validateStatelessEJB(pat.getAnnotatedType())) {
            return;
         }

         // EJB Stateless Bean: mapped onto @Named and all @TransactionAttribute mapped onto @Transaction
         //                   : bean has a default REQUIRED transaction unless specified by @TransactionAttribute on class         
         pat.setAnnotatedType(mergeStatelessEJBIntoCDIAnnotations(pat.getAnnotatedType()));
         EJBManager.getEJBManager().registerStatelessEJB(c);
      }
   }

   // Start the EJB "Containers"
   public void startEJBContainers(@Observes AfterDeploymentValidation event) {
      EJBManager.getEJBManager().startUp();
   }

   public void shutdownEJBContainers(@Observes BeforeShutdown event) {
      EJBManager.getEJBManager().shutdown();
   }

   private <T> boolean validateStatelessEJB(AnnotatedType<T> at) {
      // Validate single life cycle events
      int postConstructLifeCycles = 0;
      int preDestroyLifeCycles = 0;

      for (AnnotatedMethod am : at.getMethods()) {
         postConstructLifeCycles += am.isAnnotationPresent(PostConstruct.class) ? 1 : 0;
         preDestroyLifeCycles += am.isAnnotationPresent(PreDestroy.class) ? 1 : 0;

         // Transaction semantics for @PostConstruct must be unspecified
         if (am.isAnnotationPresent(PostConstruct.class)) {
            if (am.isAnnotationPresent(TransactionAttribute.class)) {
               log.error(String.format("Stateless EJB class %s REJECTED as single life cycle event @PostConstruct cannot specify transaction attribute @TransactionAttribute", at.getJavaClass().getName()));
               return false;
            }
            if (0 != am.getJavaMember().getParameterTypes().length) {
               log.error(String.format("Stateless EJB class %s REJECTED as single life cycle event @PostConstruct cannot take parameters", at.getJavaClass().getName()));
               return false;
            }
         }

         // Transaction semantics for @PreDestroy must be unspecified
         if (am.isAnnotationPresent(PreDestroy.class)) {
            if (am.isAnnotationPresent(TransactionAttribute.class)) {
               log.error(String.format("Stateless EJB class %s REJECTED as single life cycle event @PreDestroy cannot specify transaction attribute @TransactionAttribute", at.getJavaClass().getName()));
               return false;
            }
            if (0 != am.getJavaMember().getParameterTypes().length) {
               log.error(String.format("Stateless EJB class %s REJECTED as single life cycle event @PreDestroy cannot take parameters", at.getJavaClass().getName()));
               return false;
            }
         }
      }

      if (postConstructLifeCycles > 1) {
         log.error(String.format("Stateless EJB class %s REJECTED as has multiple life cycle events for the single life cycle event @PostConstruct", at.getJavaClass().getName()));
         return false;
      }

      if (preDestroyLifeCycles > 1) {
         log.error(String.format("Stateless EJB class %s REJECTED as has multiple life cycle events for the single life cycle event @PreDestroy", at.getJavaClass().getName()));
         return false;
      }

      return true;
   }

   private <T> AnnotatedType<T> mergeStatelessEJBIntoCDIAnnotations(final AnnotatedType<T> at) {
      final List<Annotation> injectClassAnnotations;
      injectClassAnnotations = new ArrayList<>();

      // @Default
      if (!at.getJavaClass().isAnnotationPresent(Default.class)) {
         injectClassAnnotations.add(org.rogueware.cdi.annotations.AnnotationWrappers.Default.getAnnotation());
      }
      
      // @Named
      if (!at.getJavaClass().isAnnotationPresent(Named.class)) {
         injectClassAnnotations.add(org.rogueware.cdi.annotations.AnnotationWrappers.Named.getAnnotation());
      }


/*      // @RequestScoped
      // This is a trick to make sure CDI allows us to recursivly reference @EJB as in the end the delegate is actually application scoped
      if (!at.getJavaClass().isAnnotationPresent(RequestScoped.class)) {
         injectClassAnnotations.add(org.rogueware.cdi.annotations.AnnotationWrappers.RequestScoped.getAnnotation());
      }
*/
      
      // @TransactionAttribute -> @Transaction 
      // Don't add if bean managed!!!
      boolean isBeanManagedTransaction = at.getJavaClass().isAnnotationPresent(TransactionManagement.class)
            && ((TransactionManagement) at.getJavaClass().getAnnotation(TransactionManagement.class)).value() == TransactionManagementType.BEAN;
      if (!isBeanManagedTransaction) {
         if (!at.getJavaClass().isAnnotationPresent(TransactionAttribute.class)) {
            // Container managed using default REQUIRED scope
            injectClassAnnotations.add(org.rogueware.cdi.interceptor.transaction.AnnotationWrappers.Transactional_REQUIRED.getAnnotation());
         } else {
            // Container managed using the scope specified in the EE transaction attribute annotation
            injectClassAnnotations.add(org.rogueware.cdi.interceptor.transaction.AnnotationWrappers.getTransactionalAnnotation((TransactionAttribute) at.getJavaClass().getAnnotation(TransactionAttribute.class)));
         }
      }

      AnnotatedType<T> wrapped = new AnnotatedType<T>() {

         @Override
         public Class<T> getJavaClass() {
            return at.getJavaClass();
         }

         @Override
         public Set<AnnotatedConstructor<T>> getConstructors() {
            return at.getConstructors();
         }

         @Override
         public Set<AnnotatedMethod<? super T>> getMethods() {
            Set<AnnotatedMethod<? super T>> result = new HashSet<>();
            for (AnnotatedMethod am : at.getMethods()) {
               if (am.isAnnotationPresent(PostConstruct.class)) {
                  // Make sure @PostConstruct is non-transactional 
                  AnnotatedMethod aModified = CDIUtil.addAnnotation(am, org.rogueware.cdi.interceptor.transaction.AnnotationWrappers.Transactional_NOT_SUPPORTED.getAnnotation());
                  result.add(aModified);
               } else if (am.isAnnotationPresent(PreDestroy.class)) {
                  // Make sure @PreDestroy is non-transactional 
                  AnnotatedMethod aModified = CDIUtil.addAnnotation(am, org.rogueware.cdi.interceptor.transaction.AnnotationWrappers.Transactional_NOT_SUPPORTED.getAnnotation());
                  result.add(aModified);
               }
               if (am.isAnnotationPresent(TransactionAttribute.class)) {
                  // Inject JTA annotations to implement @TransactionAttribute EE annotations in CDI
                  result.add(CDIUtil.addAnnotation(am, org.rogueware.cdi.interceptor.transaction.AnnotationWrappers.getTransactionalAnnotation((TransactionAttribute) am.getAnnotation(TransactionAttribute.class))));
               } else {
                  result.add(am);
               }
            }
            return result;
         }

         @Override
         public Set<AnnotatedField<? super T>> getFields() {
            return at.getFields();
         }

         @Override
         public Type getBaseType() {
            return at.getBaseType();
         }

         @Override
         public Set<Type> getTypeClosure() {
            return at.getTypeClosure();
         }

         @Override
         public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
            for (Annotation a : injectClassAnnotations) {
               if (a.annotationType().equals(annotationType)) {
                  return (T) a;
               }
            }
            return at.getAnnotation(annotationType);
         }

         @Override
         public Set<Annotation> getAnnotations() {
            Set<Annotation> result = new HashSet<>(at.getAnnotations());
            result.addAll(injectClassAnnotations);
            return result;
         }

         @Override
         public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
            for (Annotation a : injectClassAnnotations) {
               if (a.annotationType().equals(annotationType)) {
                  return true;
               }
            }
            return at.isAnnotationPresent(annotationType);
         }
      };

      return wrapped;
   }

}
