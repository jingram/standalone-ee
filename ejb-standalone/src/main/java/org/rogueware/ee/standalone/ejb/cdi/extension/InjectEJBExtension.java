/*
 * CDIInjectEJBExtension.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.cdi.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.MessageDriven;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AnnotatedConstructor;
import javax.enterprise.inject.spi.AnnotatedField;
import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.enterprise.inject.spi.InjectionTarget;
import javax.enterprise.inject.spi.ProcessAnnotatedType;
import javax.enterprise.inject.spi.ProcessInjectionPoint;
import javax.enterprise.inject.spi.ProcessInjectionTarget;
import javax.enterprise.inject.spi.WithAnnotations;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;
import org.rogueware.cdi.util.CDIUtil;
import org.rogueware.ee.standalone.ejb.container.EJBManager;
import org.rogueware.ee.standalone.ejb.container.StatelessContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class InjectEJBExtension implements Extension {

   private transient static final Logger log = LoggerFactory.getLogger(InjectEJBExtension.class);

   // Ensure all fields annotated with @EJB are annotated @Inject
   public <T> void processEJBAnnotations(@Observes @WithAnnotations({EJB.class}) ProcessAnnotatedType<T> pat) {
      final AnnotatedType<T> at = pat.getAnnotatedType();
      final Class klass = pat.getAnnotatedType().getJavaClass();

      // Add @Inject to any field declated with @EJB
      AnnotatedType<T> wrapped = new AnnotatedType<T>() {

         @Override
         public Class<T> getJavaClass() {
            return at.getJavaClass();
         }

         @Override
         public Set<AnnotatedConstructor<T>> getConstructors() {
            return at.getConstructors();
         }

         @Override
         public Set<AnnotatedMethod<? super T>> getMethods() {
            return at.getMethods();
         }

         @Override
         public Set<AnnotatedField<? super T>> getFields() {
            //  if within an EE entity, dont add @Inject as EE container will inject for @EJB
            if (klass.isAnnotationPresent(Stateless.class) || klass.isAnnotationPresent(Singleton.class) || klass.isAnnotationPresent(MessageDriven.class)) {
               return at.getFields();
            }

            Set<AnnotatedField<? super T>> result = new HashSet<>();
            for (final AnnotatedField af : at.getFields()) {
               // If there is a field with the @EJB, add @Inject as its within CDI
               if (af.isAnnotationPresent(EJB.class)) {
                  result.add(CDIUtil.addAnnotation(af, new AnnotationLiteral<Inject>() {
                  }));
               } else {
                  result.add(af);
               }
            }

            return result;
         }

         @Override
         public Type getBaseType() {
            return at.getBaseType();
         }

         @Override
         public Set<Type> getTypeClosure() {
            return at.getTypeClosure();
         }

         @Override
         public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
            return at.getAnnotation(annotationType);
         }

         @Override
         public Set<Annotation> getAnnotations() {
            return at.getAnnotations();
         }

         @Override
         public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
            return at.isAnnotationPresent(annotationType);
         }
      };

      pat.setAnnotatedType(wrapped);
   }

   public <T, X> void processEJBInjectionTarget(@Observes ProcessInjectionPoint<T, X> pip) {
      final InjectionPoint ip = pip.getInjectionPoint();
      if (ip.getAnnotated().isAnnotationPresent(EJB.class)) {
         /*         pip.setInjectionPoint(new InjectionPoint() {

          @Override
          public Type getType() {
          return ip.getType();
          }

          @Override
          public Set<Annotation> getQualifiers() {
          return ip.getQualifiers();
          }

          @Override
          public Bean<?> getBean() {
          return ip.getBean();
          }

          @Override
          public Member getMember() {
          return ip.getMember();
          }

          @Override
          public Annotated getAnnotated() {
          return ip.getAnnotated();
          }

          @Override
          public boolean isDelegate() {
          // This is a delegate, not the actual bean
          return true;
          }

          @Override
          public boolean isTransient() {
          return ip.isTransient();
          }
          });*/
      }

   }

   public <T> void processEJBInjectionTarget(@Observes ProcessInjectionTarget<T> pit) {
      final InjectionTarget<T> it = pit.getInjectionTarget();
      final Class injectionTargetKlass = pit.getAnnotatedType().getJavaClass();

// TODO: Add singleton etc      
      if (null != injectionTargetKlass.getAnnotation(Stateless.class)) {
         pit.setInjectionTarget(new InjectionTarget<T>() {

            @Override
            public void inject(T instance, CreationalContext<T> ctx) {
               it.inject(instance, ctx);
            }

            @Override
            public void postConstruct(T instance) {

               // Disabled @PostContruct in CDI as EE container will fire event
            }

            @Override
            public void preDestroy(T instance) {
               // Disabled @PreDestroy in CDI as EE container will fire event
            }

            @Override
            public T produce(CreationalContext<T> ctx) {
               // Stateless Bean
               // If the container is creating cached instances, return the normal WELD resolved bean
               if (null == EJBManager.getEJBManager().getStatelessEJBContainer(injectionTargetKlass)
                       || !EJBManager.getEJBManager().getStatelessEJBContainer(injectionTargetKlass).isRunning()) {
                  // We are in making ready phase, so CDI needs to produce the actual instance of the bean for the Stateless Container,
                  // not return our concrete proxy
                  return it.produce(ctx);
               }

               // We are in normal mode injecting into another Bean, inject a delegate and not the actual Bean
               if (EJBManager.getEJBManager().isStatelessEJB(injectionTargetKlass)) {
                  // Stateless EJB Delegate 
                  try {
                     StatelessContainer<T> container = EJBManager.getEJBManager().getStatelessEJBContainer(injectionTargetKlass);
                     if (null == container) {
                        throw new EJBException(String.format("Unable able to obtain a valid Stateless EJB container for class %s. Possibly not a valid Stateless EJB being injected", injectionTargetKlass.getName()));
                     }

                     T delegate = container.getConcreteSuperClassDelegate();
                     log.trace(String.format("Created Stateless EJB delegate injected into %s class", injectionTargetKlass.getName()));
                     return delegate;
                  } catch (Throwable ex) {
                     log.error(String.format("Unable to create Stateless EJB delegate to be injected into %s class", injectionTargetKlass.getName()), ex);
                  }
               }

               // Singleton EJB Delegate
//               if (EJBManager.getEJBManager().isSingletonEJB(injectionTargetKlass)) {
// TODO: Singleton EJB Delegate
//               }
// TODO: Final error as cannot find delegate for type                
               return null;
            }

            @Override
            public void dispose(T instance) {
               // Do not dispose delegates (concrete superclass proxy) as they are not CDI beans, only dispose the CDI beans
               if (instance.getClass().equals(injectionTargetKlass)) {
                  it.dispose(instance);
               }
            }

            @Override
            public Set<InjectionPoint> getInjectionPoints() {
               return it.getInjectionPoints();
            }

         });
      }
   }

}
