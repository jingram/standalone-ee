/*
 * SessionContext.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.container;

import java.security.Identity;
import java.security.Principal;
import java.util.Map;
import java.util.Properties;
import javax.ejb.EJBHome;
import javax.ejb.EJBLocalHome;
import javax.ejb.EJBLocalObject;
import javax.ejb.EJBObject;
import javax.ejb.TimerService;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.inject.Vetoed;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.xml.rpc.handler.MessageContext;
import org.rogueware.cdi.runnable.security.AnonymousPrincipal;
import org.rogueware.cdi.runnable.security.SecurityFacade;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Vetoed
public class StatelessSessionContext<T> implements javax.ejb.SessionContext {

   private transient static final org.slf4j.Logger log = LoggerFactory.getLogger(StatelessSessionContext.class);

   private Class ejbClass;
   private StatelessEJBDelegate<T> delegate;

   private boolean containerManagedTransactions;
   private UserTransaction ut;

   public StatelessSessionContext(Class ejbClass, StatelessEJBDelegate<T> delegate) {
      this.ejbClass = ejbClass;
      this.delegate = delegate;

      // Check if stateless bean has disabled bean managed transactions
      if (ejbClass.isAnnotationPresent(TransactionManagement.class)) {
         TransactionManagement tman = (TransactionManagement) ejbClass.getAnnotation(TransactionManagement.class);
         containerManagedTransactions = tman.value() == TransactionManagementType.CONTAINER;
      }

      try {
         Context ctx = new InitialContext();
         ut = (UserTransaction) ctx.lookup("java:comp/UserTransaction");
      } catch (NamingException ex) {
         log.warn("Unable to lookup user transaction at java:comp/UserTransaction, context methods requiring the user transaction will fail");
      }
   }

   @Override
   public EJBLocalObject getEJBLocalObject() throws IllegalStateException {
      // Stateful Session Bean stuff
      throw new IllegalStateException("EJBLocalObject not available");
   }

   @Override
   public EJBObject getEJBObject() throws IllegalStateException {
      // Stateful Session Bean stuff
      throw new IllegalStateException("EJBObject not available");
   }

   @Override
   public MessageContext getMessageContext() throws IllegalStateException {
      // Message Bean stuff
      throw new IllegalStateException("MessageContext not available");
   }

   @Override
   public <T> T getBusinessObject(Class<T> businessInterface) throws IllegalStateException {
      try {
         T res = (T) delegate;
         return res;
      } catch (Exception ex) {
         // Cannot cast to the business interface
         throw new IllegalStateException(String.format("Invalid business interface : %s for ejb %s", businessInterface.getName(), ejbClass.getSimpleName()));
      }
   }

   @Override
   public Class getInvokedBusinessInterface() throws IllegalStateException {
      return ejbClass;
   }

   @Override
   public boolean wasCancelCalled() throws IllegalStateException {
// TODO: Check whether a client invoked the cancel method on the client Future object corresponding to the currently executing asynchronous business method.      
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   }

   @Override
   public EJBHome getEJBHome() throws IllegalStateException {
      throw new IllegalStateException("EJBHome not available");
   }

   @Override
   public EJBLocalHome getEJBLocalHome() throws IllegalStateException {
      throw new IllegalStateException("EJBLocalHome not available");
   }

   @Override
   public Properties getEnvironment() {
// TODO: (Build properties from bean class JNDI env      
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   }

   @Override
   public Identity getCallerIdentity() {
      throw new UnsupportedOperationException("getCallerIdentity() depricated, use getCallerPrincipal() instead.");
   }

   @Override
   public Principal getCallerPrincipal() throws IllegalStateException {
      SecurityFacade secure = SecurityFacade.getSecurityFacade();
      if (null != secure) {
         return secure.getUserPrincipal();
      } else {
         return AnonymousPrincipal.getPrincipal();
      }
   }

   @Override
   public boolean isCallerInRole(Identity role) {
      throw new UnsupportedOperationException("isCallerInRole(Identity role) depricated, use isCallerInRole(String roleName)  instead.");
   }

   @Override
   public boolean isCallerInRole(String roleName) throws IllegalStateException {
      SecurityFacade secure = SecurityFacade.getSecurityFacade();
      if (null != secure) {
         return secure.isUserInRole(roleName);
      }
      return false;
   }

   @Override
   public UserTransaction getUserTransaction() throws IllegalStateException {
      if (containerManagedTransactions) {
         throw new IllegalStateException("getUserTransaction() not supported on instance of a bean with container-managed transactions. " + ""
                 + "Annotate bean with @TransactionManagement(TransactionManagementType.BEAN) to enable user transaction interface");
      }

      // bean managed container
      if (null == ut) {
         throw new IllegalStateException("Unable to lookup java:comp/UserTransaction during context initialisation");
      }

      return ut;
   }

   @Override
   public void setRollbackOnly() throws IllegalStateException {
      if (!containerManagedTransactions) {
         throw new IllegalStateException("setRollbackOnly() not supported on instance of a bean with bean-managed transactions. " + ""
                 + "Annotate bean with @TransactionManagement(TransactionManagementType.CONTAINER) to enable");
      }

      try {
         if (ut.getStatus() == Status.STATUS_ACTIVE) {
            ut.setRollbackOnly();
         }
      } catch (SystemException ex) {
         log.error("Unable to set rollback only status for current transaction", ex);
         throw new IllegalStateException("setRollbackOnly() unable to set rollback only status for current transaction");
      }
   }

   @Override
   public boolean getRollbackOnly() throws IllegalStateException {
      if (!containerManagedTransactions) {
         throw new IllegalStateException("getRollbackOnly() not supported on instance of a bean with bean-managed transactions. " + ""
                 + "Annotate bean with @TransactionManagement(TransactionManagementType.CONTAINER) to enable");
      }

      try {
         return ut.getStatus() == Status.STATUS_ACTIVE;
      } catch (SystemException ex) {
         log.error("Unable to obtain rollback only for current transaction", ex);
         throw new IllegalStateException("getRollbackOnly() unable to obtain rollback only for current transaction");
      }
   }

   @Override
   public TimerService getTimerService() throws IllegalStateException {
// TODO      
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      
//      return (TimerService) org.rogueware.ee.standalone.timer.service.TimerService.getTimerService(ejbClass);
   }

   @Override
   public Object lookup(String name) throws IllegalArgumentException {
      // java: namespace, if unqualified string, lookup relative to java:comp/env/ 
      String search = name;
      if (!name.startsWith("java:") && !name.startsWith("/java:")) {
         if (!name.startsWith("/")) {
            search = "java:comp/env/" + name;
         } else {
            search = "java:comp/env" + name;
         }
      }

      try {
         Context ctx = new InitialContext();
         return ctx.lookup(search);
      } catch (NamingException ex) {
         log.error(String.format("Unable to resolve %s within components environment", search));
         throw new IllegalArgumentException(String.format("Unable to resolve %s within components environment", search));
      }
   }

   @Override
   public Map<String, Object> getContextData() {
// TODO: Need to work out scope (Looks like only available either per thread or per EJB .. would need to write test app to figure out)      
      // Could use reference counting for push on wrapped calls to EJB's and pop until no more left, but that would break if outer containee entered another bean again 
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   }
}
