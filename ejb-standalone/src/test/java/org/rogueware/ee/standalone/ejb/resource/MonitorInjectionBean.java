/*
 * MonitorInjectionBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.resource;

import javax.ejb.EJB;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
public class MonitorInjectionBean {

   private static final Logger log = LoggerFactory.getLogger(MonitorInjectionBean.class);

   @EJB
   private StartEJB startEJBRef;


   public void runStatelessEJBInject() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting Stateless EJB Resource Injection Test");
      log.info("-------------------------------------------------------------------------");

      if (null == startEJBRef) {
         throw new InterruptedException("StartEJB was not injected and is null");
      }

      if (!startEJBRef.getClass().getName().contains("EnhancerByCGLIB")) {
         throw new InterruptedException("StartEJB did not inject instance of EJB delegate");
      }
      

      startEJBRef.startStateless();

      if (!StartEJB.correctEJBDelegate.get()) {
         throw new InterruptedException("StartEJB did not inject correct EJB delegates");
      }
      
      if (StartEJB.componentScopeCreep.get()) {
         throw new InterruptedException("StartEJB can see JNDI component items from InjectStatelessEJB");
      }

      
      if (!InjectStatelessEJB.correctEJBDelegate.get()) {
         throw new InterruptedException("InjectStatelessEJB did not inject correct EJB delegates");
      }
      
      if (!InjectStatelessEJB.resourcesPopulated.get()) {
         throw new InterruptedException("InjectStatelessEJB did not populate built in resources");
      }

      if (!InjectStatelessEJB.envResourcesPopulated.get()) {
         throw new InterruptedException("InjectStatelessEJB did not populate environment from inherited class");
      }

      if (!InjectStatelessEJB.lookupsPassed.get()) {
         throw new InterruptedException("InjectStatelessEJB JNDI lookups did not pass");
      }

      if (!InjectStatelessEJB.readOnlyJNDIPassed.get()) {
         throw new InterruptedException("InjectStatelessEJB JNDI context was not read-only");
      }
      
      if (InjectStatelessEJB.brokenPropertyOnePopulated.get()) {
         throw new InterruptedException("InjectStatelessEJB injected resource into broken property one");
      }
      if (InjectStatelessEJB.brokenPropertyTwoPopulated.get()) {
         throw new InterruptedException("InjectStatelessEJB injected resource into broken property two");
      }

      if (InjectStatelessEJB.brokenMethodOneCalled.get()) {
         throw new InterruptedException("InjectStatelessEJB injected resource into broken method one");
      }
      if (InjectStatelessEJB.brokenMethodTwoCalled.get()) {
         throw new InterruptedException("InjectStatelessEJB injected resource into broken method two");
      }
      if (InjectStatelessEJB.brokenMethodThreeCalled.get()) {
         throw new InterruptedException("InjectStatelessEJB injected resource into broken method three");
      }
      if (InjectStatelessEJB.brokenMethodFourCalled.get()) {
         throw new InterruptedException("InjectStatelessEJB injected resource into broken method four");
      }      
      
      log.info("-------------------------------------------------------------------------");
   }
}
