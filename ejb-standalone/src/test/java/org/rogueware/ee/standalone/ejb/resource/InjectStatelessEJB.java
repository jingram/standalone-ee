/*
 * InjectStatelessEJB.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.resource;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.enterprise.inject.spi.BeanManager;
import javax.naming.InitialContext;
import static org.rogueware.ee.standalone.ejb.resource.StartEJB.correctEJBDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Stateless
public class InjectStatelessEJB extends EnvInject {

   private static final Logger log = LoggerFactory.getLogger(InjectStatelessEJB.class);

   public static AtomicBoolean correctEJBDelegate = new AtomicBoolean(false);
   
   public static AtomicBoolean resourcesPopulated = new AtomicBoolean(false);
   public static AtomicBoolean envResourcesPopulated = new AtomicBoolean(false);
   public static AtomicBoolean lookupsPassed = new AtomicBoolean(false);
   public static AtomicBoolean readOnlyJNDIPassed = new AtomicBoolean(false);

   public static AtomicBoolean brokenPropertyOnePopulated = new AtomicBoolean(false);
   public static AtomicBoolean brokenPropertyTwoPopulated = new AtomicBoolean(false);

   public static AtomicBoolean brokenMethodOneCalled = new AtomicBoolean(false);
   public static AtomicBoolean brokenMethodTwoCalled = new AtomicBoolean(false);
   public static AtomicBoolean brokenMethodThreeCalled = new AtomicBoolean(false);
   public static AtomicBoolean brokenMethodFourCalled = new AtomicBoolean(false);

   
   @EJB
   StartEJB startEJBRef;
   
   // Built in
   @Resource
   SessionContext sessionContext;

   @Resource(name = "ctx")
   SessionContext ctx;

   @Resource(name = "other/ctx")
   EJBContext otherCtx;


   @Resource(name = "java:global/mrBean")
   BeanManager otherBm;

   BeanManager bm;
   
   @Resource
   public void setMrBean(BeanManager bm) {
      this.bm = bm;
   }

   
   // Lookup
   @Resource(name="outside", lookup = "java:global/customVal")
   String outside;
   
   
   
   // Broken .. static
   @Resource
   static SessionContext brokenOne;

   // Broken .. final
   @Resource
   final SessionContext brokenTwo = null;
   
   // Broken ... too many parameters
   @Resource
   public void setMrBrokenOne(BeanManager bm, SessionContext ctx) {
      brokenMethodOneCalled.set(true);
   }

   // Broken ... non void return type
   @Resource
   public boolean setMrBrokenTwo(BeanManager bm) {
      brokenMethodTwoCalled.set(true);
      return true;
   }

   // Broken ... static method
   @Resource
   public static void setMrBrokenThree(BeanManager bm) {
      brokenMethodThreeCalled.set(true);
   }

   // Broken ... method not setter naming convention
   @Resource
   public void mrBrokenFour(BeanManager bm) {
      brokenMethodFourCalled.set(true);
   }

   public void checkStateless() {
      correctEJBDelegate.set(startEJBRef.getClass().getName().contains("EnhancerByCGLIB"));
      
      
      // Resources populated
      resourcesPopulated.set(null != sessionContext && null != ctx && null != otherCtx && null != bm && null != otherBm);
      envResourcesPopulated.set(null != myString && myString.equals("String") && 22 == myInteger && 99.5 == myFloat);  // Also tests inheritance
      
      
      // Check broken properties
      brokenPropertyOnePopulated.set(brokenOne != null);
      brokenPropertyTwoPopulated.set(brokenTwo != null);
      
      // Check JNDI bindings are bound correctly                 
      try {
         InitialContext ic = new InitialContext();
         SessionContext lSessionContext = (SessionContext) ic.lookup("java:comp/env/org.rogueware.ee.standalone.ejb.resource.InjectStatelessEJB/sessionContext");
         SessionContext lCtx = (SessionContext) ic.lookup("java:comp/env/ctx");
         SessionContext lOtherCtx = (SessionContext) ic.lookup("java:comp/env/other/ctx");

         BeanManager lBm = (BeanManager) ic.lookup("java:comp/env/org.rogueware.ee.standalone.ejb.resource.InjectStatelessEJB/mrBean");

         String lOutside = (String) ic.lookup("java:comp/env/outside");
         
         Integer lMyInteger = (Integer) ic.lookup("java:comp/env/org.rogueware.ee.standalone.ejb.resource.InjectStatelessEJB/myInteger");
         Double lMyFloat = (Double) ic.lookup("java:comp/env/floatMe");

         

         lookupsPassed.set(true);
      } catch (Exception ex) {
         log.error("Unable to lookup JNDI entry", ex);
      }

      
      // Make sure the JNDI context is read-only
      try {
         InitialContext ic = new InitialContext();
         BeanManager lBm = (BeanManager) ic.lookup("java:comp/env/org.rogueware.ee.standalone.ejb.resource.InjectStatelessEJB/mrBean");

         ic.rebind("ava:comp/env/org.rogueware.ee.standalone.ejb.resource.InjectStatelessEJB/mrBean", otherBm);
         
         log.error("JNDI context is not read-only inside the bean");
      } catch (Exception ex) {
         readOnlyJNDIPassed.set(true);
      }
      
      
      // Fire callback to make sure JNDI scoped correctly
      startEJBRef.statelessCallback();
   }
}
