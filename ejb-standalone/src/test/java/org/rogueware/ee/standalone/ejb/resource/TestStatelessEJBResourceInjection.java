/*
 * TestStatelessEJBResourceInjection.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.resource;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.naming.Context;
import javax.naming.InitialContext;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.rogueware.cdi.util.CDIUtil;
import org.rogueware.cdi.util.ContextualInstance;
import org.rogueware.ee.standalone.jndi.JNDI;
import org.rogueware.jta.JTAStandAlone;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestStatelessEJBResourceInjection {

   private static Weld weld;
   private static BeanManager bm;

   @BeforeClass
   public static void init() throws Exception {

      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.TRACE);

      Logger ejbLogger = (Logger) LoggerFactory.getLogger("org.rogueware.ee.standalone.ejb");
      ejbLogger.setLevel(Level.TRACE);

      Logger jtaLogger = (Logger) LoggerFactory.getLogger("org.rogueware.cdi.interceptor.transaction");
      jtaLogger.setLevel(Level.TRACE);

      // Log4J
      org.apache.log4j.BasicConfigurator.configure();

      // JNDI
      System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");
      InitialContext ctx = new InitialContext();

      // Add environment variables and other lookup
      InitialContext ic = new InitialContext();
      JNDI.rebindWithContextCreation(ic, "java:global/customVal", "Hello World");

      // Simulate environment variables (Exampe deployment descriptor setting values for EJB)
      JNDI.rebindWithContextCreation(ic, "java:comp/env/org.rogueware.ee.standalone.ejb.resource.InjectStatelessEJB/myString", "String");
      JNDI.rebindWithContextCreation(ic, "java:comp/env/org.rogueware.ee.standalone.ejb.resource.InjectStatelessEJB/myInteger", 22);
      JNDI.rebindWithContextCreation(ic, "java:comp/env/org.rogueware.ee.standalone.ejb.resource.InjectStatelessEJB/myFloat", 99.5);

      
      // JTA
      JTAStandAlone.initJTAStandAloneApplication();

      // Weld
      weld = new Weld();
      WeldContainer weldContainer = weld.initialize();

      bm = CDI.current().getBeanManager();
      if (null == bm) {
         throw new Exception("Unable to initialize CDI container");
      }
   }

   @AfterClass
   public static void shutdown() throws Exception {
      try {
         if (null != weld) {
            weld.shutdown();
         }
      } catch (Exception ex) {
      }
   }

   @Test
   public void testInjection() throws Exception {
      
      // Activate a RequestScoped Context for the thread
      String providerName = CDI.current().getClass().getName();

/*      // WELD Specific
      Map<String, Object> requestMap = new HashMap<>();
      if (providerName.startsWith("org.jboss.weld")) {
         org.jboss.weld.context.bound.BoundRequestContext requestContext = CDI.current().select(org.jboss.weld.context.bound.BoundRequestContext.class).get();
         if (!requestContext.isActive()) {
            requestContext.associate(requestMap);
            requestContext.activate();
         }
      } else {
         throw new IllegalThreadStateException(String.format("Unknown CDO provider %s. Please implement request scope context. See Apache DeltaSpike for help", providerName));
      }
*/
      try (ContextualInstance<MonitorInjectionBean> ci = CDIUtil.getContextualInstance(bm, MonitorInjectionBean.class);) {
         MonitorInjectionBean mb = ci.getBean();
         mb.runStatelessEJBInject();
      }

/*      // De-Activate a RequestScoped Context for the thread
      // WELD Specific
      if (providerName.startsWith("org.jboss.weld")) {
         org.jboss.weld.context.bound.BoundRequestContext requestContext = CDI.current().select(org.jboss.weld.context.bound.BoundRequestContext.class).get();
         if (requestContext.isActive()) {
            requestContext.invalidate();
            requestContext.deactivate();
            requestContext.dissociate(requestMap);
            requestMap = null;
         }
      }*/
   }
}
