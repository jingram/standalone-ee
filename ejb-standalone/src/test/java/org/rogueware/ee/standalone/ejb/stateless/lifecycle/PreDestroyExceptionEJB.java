/*
/*
 * PostCreateExceptionEJB.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.stateless.lifecycle;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.PreDestroy;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Stateless
public class PreDestroyExceptionEJB {
   private static final Logger log = LoggerFactory.getLogger(PreDestroyExceptionEJB.class);
   
   public static AtomicBoolean threwError = new AtomicBoolean(false);
   
   @PreDestroy
   public void preDestroyWithError() {
      log.info("Throwing error on life cycle event for @PreDestroy");
      threwError.set(true);
      throw new EJBException("@PreDestroy exception");
   }
   
}
