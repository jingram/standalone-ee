/*
 * TestLifeCycleException.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.stateless.lifecycle;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;
import javax.naming.Context;
import javax.naming.InitialContext;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.rogueware.cdi.util.CDIUtil;
import org.rogueware.cdi.util.ContextualInstance;
import org.rogueware.jta.JTAStandAlone;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestStatelessLifeCycleException {

   private static Weld weld;
   private static BeanManager bm;

   @BeforeClass
   public static void init() throws Exception {

      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.INFO);

      Logger timerLogger = (Logger) LoggerFactory.getLogger("org.rogueware.ee.standalone.ejb");
      timerLogger.setLevel(Level.TRACE);

      // JNDI
      System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
      System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");
      InitialContext ctx = new InitialContext();
      ctx.createSubcontext("java:");

      // JTA
      JTAStandAlone.initJTAStandAloneApplication();

      // Weld
      weld = new Weld();
      WeldContainer weldContainer = weld.initialize();

      bm = CDI.current().getBeanManager();
      if (null == bm) {
         throw new Exception("Unable to initialize CDI container");
      }
   }

   @AfterClass
   public static void shutdown() {
      try {
         if (null != weld) {
            weld.shutdown();
         }
      } catch (Exception ex) {
      }
   }

   @Test
   public void testPostCreateException() throws Exception {
      try (ContextualInstance<MonitorLifeCycleBean> ci = CDIUtil.getContextualInstance(bm, MonitorLifeCycleBean.class);) {
         MonitorLifeCycleBean mb = ci.getBean();
         mb.runPostCreateException();
      }
   }

   @Test
   public void testPreDestroyException() throws Exception {
      try (ContextualInstance<MonitorLifeCycleBean> ci = CDIUtil.getContextualInstance(bm, MonitorLifeCycleBean.class);) {
         MonitorLifeCycleBean mb = ci.getBean();
         mb.runPreDestroyException();
      }
   }
}
