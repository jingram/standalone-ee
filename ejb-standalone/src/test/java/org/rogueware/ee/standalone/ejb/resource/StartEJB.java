/*
 * StartEJB.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.resource;

import static java.lang.StrictMath.log;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Stateless
public class StartEJB {
   private static final Logger log = LoggerFactory.getLogger(StartEJB.class);

   public static AtomicBoolean correctEJBDelegate = new AtomicBoolean(false);
   public static AtomicBoolean componentScopeCreep = new AtomicBoolean(false);

   
   @Resource
   SessionContext ctx;
   
   @EJB
   InjectStatelessEJB injectStatelessEJBRef;

   public void startStateless() {
      correctEJBDelegate.set(injectStatelessEJBRef.getClass().getName().contains("EnhancerByCGLIB"));
      
      
      // Make sure context not bound to resource 
      try {
         InitialContext ic = new InitialContext();
         SessionContext lCtx = (SessionContext) ic.lookup("java:comp/env/ctx");
         componentScopeCreep.set(true);
         log.error("java:comp/env/ctx should not be bound");
      } catch (Exception ex) {
         // :)         
      }

      injectStatelessEJBRef.checkStateless();
   }

   public void statelessCallback() {
      try {
         InitialContext ic = new InitialContext();
         SessionContext lCtx = (SessionContext) ic.lookup("java:comp/env/ctx");
         
         componentScopeCreep.set(true);
         log.error("java:comp/env/ctx should not be bound");
      } catch (Exception ex) {
         // :)         
      }
   }

}
