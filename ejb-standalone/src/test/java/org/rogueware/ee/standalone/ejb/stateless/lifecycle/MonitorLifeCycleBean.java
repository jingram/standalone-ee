/*
 * MonitorLifeCycleBean.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.stateless.lifecycle;

import javax.ejb.EJB;
import javax.inject.Named;
import org.rogueware.ee.standalone.ejb.container.EJBManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Named
public class MonitorLifeCycleBean {

   private static final Logger log = LoggerFactory.getLogger(MonitorLifeCycleBean.class);

   @EJB
   private DefaultStatelessEJB defaultStatelessEJBRef;

   public void runPostCreateException() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting @PostCreate Exception Test");
      log.info("-------------------------------------------------------------------------");

      if (!PostCreateExceptionEJB.threwError.get()) {
         throw new InterruptedException("@PostConstruct life cycle event not called on PostCreateExceptionEJB and exception not thrown");
      }
      if (EJBManager.getEJBManager().isValidEJB(PostCreateExceptionEJB.class)) {
         throw new InterruptedException("PostCreateExceptionEJB should not have a valid container");
      }

      log.info("-------------------------------------------------------------------------");
   }

   public void runPreDestroyException() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting @PreDestroy Exception Test");
      log.info("-------------------------------------------------------------------------");

      if (!PostCreateExceptionEJB.threwError.get()) {
         throw new InterruptedException("@PreDestroy life cycle event not called on PreDestroyExceptionEJB and exception not thrown");
      }

      log.info("-------------------------------------------------------------------------");
   }

   public void runDefaultStatelessEJB() throws InterruptedException {
      log.info("-------------------------------------------------------------------------");
      log.info("Starting Default Stateless EJB Test");
      log.info("-------------------------------------------------------------------------");

      if (null == defaultStatelessEJBRef) {
         throw new InterruptedException("DefaultStatelessEJB was not injected and is null");
      }

      if (!defaultStatelessEJBRef.getClass().getName().contains("EnhancerByCGLIB")) {
         throw new InterruptedException("DefaultStatelessEJB did not inject instance of EJB delegate");
      }

      if (!DefaultStatelessEJB.postCreateCalled.get()) {
         throw new InterruptedException("@PostConstruct life cycle event not called on DefaultStatelessEJB");
      }
      if (!DefaultStatelessEJB.postCreateResourcesInjected.get()) {
         throw new InterruptedException("@PostConstruct life cycle event on DefaultStatelessEJB did not inject resources");
      }
      
      defaultStatelessEJBRef.businessMethodOne();
            
      log.info("-------------------------------------------------------------------------");
   }
}
