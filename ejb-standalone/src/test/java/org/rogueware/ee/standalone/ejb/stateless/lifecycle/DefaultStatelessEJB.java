/*
 /*
 * DefaultStatelessEJB.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.ejb.stateless.lifecycle;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.spi.BeanManager;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;
import org.rogueware.jta.JTAStandAlone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
@Stateless
public class DefaultStatelessEJB {

   private static final Logger log = LoggerFactory.getLogger(DefaultStatelessEJB.class);

   public static AtomicBoolean postCreateCalled = new AtomicBoolean(false);
   public static AtomicBoolean preDestroyCalled = new AtomicBoolean(false);

   public static AtomicBoolean postCreateResourcesInjected = new AtomicBoolean(false);
   public static AtomicBoolean preDestroyResourcesInjected = new AtomicBoolean(false);
   
   @Resource
   SessionContext ctx;
   
   @Resource
   UserTransaction ut;

   @Resource
   BeanManager bm;
   
      
   @PostConstruct
   public void init() {
      postCreateCalled.set(true);
      postCreateResourcesInjected.set(null != ctx && null != ut && null != bm);
   }

   @PreDestroy
   public void finalize() {
      preDestroyCalled.set(true);
      preDestroyResourcesInjected.set(null != ctx && null != ut && null != bm);
   }

   public void businessMethodOne() {
      assertUserTransaction();

      try {
         if (ut.getStatus() != Status.STATUS_ACTIVE) {
            log.error("businessMethodOne - An active transaction is required");
            throw new EJBException("businessMethodOne - An active transaction is required");
         }
      } catch (SystemException ex) {
         throw new EJBException(ex);
      }
   }

   @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
   public void businessMethodWithNewTransaction(String transactionId) {

   }

   private void assertUserTransaction() {
      // Make sure the user transaction injected
      if (null == ut) {
         log.error("User transaction not injected");
         throw new EJBException("User transaction not injected");
      }

      if (!(ut instanceof UserTransaction)) {
         log.error("User transaction not of type CDIUserTransaction");
         throw new EJBException("User transaction not of type CDIUserTransaction");
      }

      // Make sure the user transaction matches the current thread transaction
      try {
         TransactionManager tm = JTAStandAlone.getTransactionManager();
         org.rogueware.jta.UserTransaction cut = (org.rogueware.jta.UserTransaction) ut;

         // Check transaction status the same
         if (null != tm.getTransaction() && null != cut.getTransaction()) {
            if (!tm.getTransaction().toString().equals(cut.getTransaction().toString())) {
               log.error("User transaction does not match the current thread transaction");
               throw new EJBException("User transaction does not match the current thread transaction");
            }
         } else if (null != tm.getTransaction() && null != cut.getTransaction()) {
            log.error("User transaction does not match the current thread transaction");
            throw new EJBException("User transaction does not match the current thread transaction");
         }
      } catch (SystemException ex) {
         log.error("Error occured while comparing user transaction to current thread transaction", ex);
         throw new EJBException("Error occured while comparing user transaction to current thread transaction", ex);
      }
   }
}
