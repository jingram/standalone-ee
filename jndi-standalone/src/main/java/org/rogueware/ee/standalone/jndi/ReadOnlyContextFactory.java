/*
 * ReadOnlyContextFactory.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class ReadOnlyContextFactory implements InitialContextFactory {

   private static final Logger log = LoggerFactory.getLogger(ReadOnlyContextFactory.class);

   private Context ic;

   public ReadOnlyContextFactory() {
   }

   public ReadOnlyContextFactory(Context ic) {
      // Specify context to wrap read-only
      this.ic = ic;
   }

   @Override
   public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
      // Get the default initial context and wrap it as read only
      if (null == ic) {
         ic = new InitialContext();
      }
      return getInitialContext(ic, environment);
   }

   public static Context getInitialContext(Context ic, Hashtable<?, ?> environment) throws NamingException {
      // Get the default initial context
      if (null == ic) {
         ic = new InitialContext();
      }

      return new ReadOnlyContext(ic);
   }
}
