/*
 * JNDIResourceInjectionUtility.java
 * 
 * Defines a utility class used to inject JNDI resources
 *  into a class with the @Resource annotation according to 
 *  EE rules if EJB component or just using lookup if not
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi.cdi.extension;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.MessageDrivenContext;
import javax.ejb.SessionContext;
import javax.enterprise.inject.spi.BeanManager;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;
import org.rogueware.ee.standalone.jndi.JNDI;
import org.rogueware.ee.standalone.jndi.ThreadContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class JNDIResourceInjectionUtility {

   private static final Logger log = LoggerFactory.getLogger(JNDIResourceInjectionUtility.class);

   private static final Map<Class, String> RESOURCE_MAPPED_TYPES = new HashMap<>();

   private static final Map<Class, List<Field>> classResourceFieldsCache = Collections.synchronizedMap(new HashMap<Class, List<Field>>());
   private static final Map<Class, List<Method>> classResourceMethodsCache = Collections.synchronizedMap(new HashMap<Class, List<Method>>());

   static {
      // EE7 specification section 5
      RESOURCE_MAPPED_TYPES.put(SessionContext.class, "java:comp/EJBContext");
      RESOURCE_MAPPED_TYPES.put(EJBContext.class, "java:comp/EJBContext");
      RESOURCE_MAPPED_TYPES.put(MessageDrivenContext.class, "java:comp/EJBContext");
      RESOURCE_MAPPED_TYPES.put(UserTransaction.class, "java:comp/UserTransaction");
      RESOURCE_MAPPED_TYPES.put(BeanManager.class, "java:comp/BeanManager");
// TODO: Add as implemented
   }

   private JNDIResourceInjectionUtility() {
      // Cannot instantiate
   }

   public static void injectJNDIResources(Object target, boolean isEEComponent) {
      injectJNDIResources(target, target.getClass(), isEEComponent);
   }
   
   public static void injectJNDIResources(Object target, Class targetKlass, boolean isEEComponent) {
      try {
         populateCache(targetKlass);

         // Get initial context
         InitialContext ic = new InitialContext();

         // Process field injection
         for (Field f : classResourceFieldsCache.get(targetKlass)) {
            injectIntoField(ic, isEEComponent, target, targetKlass, f);
         }

         // Process method injection
         for (Method m : classResourceMethodsCache.get(targetKlass)) {
            injectIntoMethod(ic, isEEComponent, target, targetKlass, m);
         }

         // TODO: Class annotated @Resources or @Resource 
      } catch (Exception ex) {
         log.warn(String.format("An error occured during JNDI resource injection on class %s", targetKlass.getName()), ex);
      }
   }

   private static void injectIntoField(InitialContext ic, boolean isEEComponent, Object target, Class targetKlass, Field field) {
      Package targetPackage = targetKlass.getPackage();
      Resource resourceAnnotation = field.getAnnotation(Resource.class);

      // Resource name ... if not specified, default, otherwise bind name under java:comp/env
      String name = null;
      if (isEEComponent) {
         name = resourceAnnotation.name();
         if (null == name || name.isEmpty()) {
            name = String.format("java:comp/env/%s/%s", targetKlass.getName(), field.getName());
         } else {
            if (!name.startsWith("java:comp/env")) {
               name = String.format("java:comp/env/%s", name);
            }
         }
      }

      // Resource lookup ... if not specified, default
      String lookup = resourceAnnotation.lookup();
      if (null == lookup || lookup.isEmpty()) {
         // If known type, use known lookup
         if (RESOURCE_MAPPED_TYPES.containsKey(field.getType())) {
            lookup = RESOURCE_MAPPED_TYPES.get(field.getType());
         } else {
            lookup = String.format("java:comp/env/%s/%s", targetKlass.getName(), field.getName());
         }
      }

      // Lookup and set value on target
      Object val = null;
      try {
         val = ic.lookup(lookup);
      } catch (NamingException ex) {
         log.debug(String.format("Unable to lookup JNDI value for %s to inject into field %s in class %s. Defined default will be used", lookup, field.getName(), targetKlass.getName()));
      }

      try {
         if (null != val) {
            field.setAccessible(true);
            field.set(target, val);
         }
      } catch (Exception ex) {
         log.warn(String.format("Unable to inject value for %s for field %s in class %s", field.getType().getName(), field.getName(), targetKlass.getName()), ex);
      }

      // Only rebind if EE
      if (isEEComponent) {
         if (null != val) {
            try {
               // Must be a thread context, so get thread context and rebind
               Context ctx = ThreadContextFactory.getInitialContext(null, new Hashtable<>());
               JNDI.rebindWithContextCreation(ctx, name, val);
            } catch (NamingException ex) {
               log.debug(String.format("Unable to rebind JNDI value for %s for field %s in class %s", name, field.getName(), targetKlass.getName()));
            }
         }
      }
   }

   private static void injectIntoMethod(InitialContext ic, boolean isEEComponent, Object target, Class targetKlass, Method method) {
      Package targetPackage = targetKlass.getPackage();
      Resource resourceAnnotation = method.getAnnotation(Resource.class);

      String propName = method.getName().replaceFirst("set", "");
      propName = propName.substring(0, 1).toLowerCase() + propName.substring(1);

      // Resource name ... if not specified, default, otherwise bind name under java:comp/env
      String name = null;
      if (isEEComponent) {
         name = resourceAnnotation.name();
         if (null == name || name.isEmpty()) {
            // Property name is setter method name transformed to property name using Java Beans convention
            name = String.format("java:comp/env/%s/%s", targetKlass.getName(), propName);
         } else {
            if (!name.startsWith("java:comp/env")) {
               name = String.format("java:comp/env/%s", name);
            }
         }
      }

      // Resource lookup ... if not specified, default
      String lookup = resourceAnnotation.lookup();
      if (null == lookup || lookup.isEmpty()) {
         // If known type, use known lookup
         if (RESOURCE_MAPPED_TYPES.containsKey(method.getParameterTypes()[0])) {
            lookup = RESOURCE_MAPPED_TYPES.get(method.getParameterTypes()[0]);
         } else {
            lookup = String.format("java:comp/env/%s/%s", targetKlass.getName(), propName);
         }
      }

      // Lookup and set value on target
      Object val = null;
      try {
         val = ic.lookup(lookup);
      } catch (NamingException ex) {
         log.debug(String.format("Unable to lookup JNDI value for %s to inject into method %s in class %s. Defined default will be used", lookup, method.getName(), targetKlass.getName()));
      }

      try {
         if (null != val) {
            method.setAccessible(true);
            method.invoke(target, val);
         }
      } catch (Exception ex) {
         log.warn(String.format("Unable to inject value for %s for method %s in class %s", method.getParameterTypes()[0].getName(), method.getName(), targetKlass.getName()), ex);
      }

      // Only rebind if EE
      if (isEEComponent) {
         if (null != val) {
            try {
               // Must be a thread context, so get thread context and rebind
               Context ctx = ThreadContextFactory.getInitialContext(null, new Hashtable<>());
               JNDI.rebindWithContextCreation(ctx, name, val);
            } catch (NamingException ex) {
               log.debug(String.format("Unable to rebind JNDI value for %s for method %s in class %s", name, method.getName(), targetKlass.getName()));
            }
         }
      }
   }

   private static void populateCache(Class targetClass) {
      if (classResourceFieldsCache.containsKey(targetClass) && classResourceMethodsCache.containsKey(targetClass)) {
         return;  // Already cached
      }

      List<Field> annotatedFields = new ArrayList<>();
      List<Method> annotatedMethods = new ArrayList<>();

      // Recurse inheritance
      Class c = targetClass;
      while (null != c && !c.equals(Object.class)) {
         // Fields
         for (Field f : c.getDeclaredFields()) {
            if (f.isAnnotationPresent(Resource.class)) {
               if (Modifier.isStatic(f.getModifiers())) {
                  log.error(String.format("Static field %s in class %s cannot be annotated with @Resource, igoring resource injection", f.getName(), targetClass.getName()));
                  continue;
               }
               if (Modifier.isFinal(f.getModifiers())) {
                  log.error(String.format("Final field %s in class %s cannot be annotated with @Resource, igoring resource injection", f.getName(), targetClass.getName()));
                  continue;
               }
               if (null != f.getAnnotation(Resource.class).mappedName() && !f.getAnnotation(Resource.class).mappedName().isEmpty()) {
                  log.error(String.format("@Resource annotated field %s in class %s ignoring non-portable mappedName", f.getName(), targetClass.getName()));
               }

               annotatedFields.add(f);
            }
         }

         // Methods
         for (Method m : c.getDeclaredMethods()) {
            if (m.isAnnotationPresent(Resource.class)) {
               if (Modifier.isStatic(m.getModifiers())) {
                  log.error(String.format("Static method %s in class %s cannot be annotated with @Resource, igoring resource injection", m.getName(), targetClass.getName()));
                  continue;
               }
               if (!m.getName().startsWith("set") || !m.getReturnType().equals(void.class) || m.getParameterTypes().length != 1) {
                  log.error(String.format("Method %s in class %s name must following JavaBeans conventions for property names, name must begin with set, have a void return type, and have only one parameter", m.getName(), targetClass.getName()));
                  continue;
               }
               if (null != m.getAnnotation(Resource.class).mappedName() && !m.getAnnotation(Resource.class).mappedName().isEmpty()) {
                  log.error(String.format("@Resource annotated method %s in class %s ignoring non-portable mappedName", m.getName(), targetClass.getName()));
               }

               annotatedMethods.add(m);
            }
         }

         c = c.getSuperclass();
      }

      classResourceFieldsCache.put(targetClass, annotatedFields);
      classResourceMethodsCache.put(targetClass, annotatedMethods);
   }
}
