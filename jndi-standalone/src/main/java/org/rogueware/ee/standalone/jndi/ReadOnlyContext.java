/*
 * ReadOnlyContext.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Queue;
import javax.naming.Binding;
import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.NameClassPair;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.OperationNotSupportedException;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class ReadOnlyContext implements javax.naming.Context {

   private final Context wrapCtx;

   public ReadOnlyContext(Context wrapCtx) {
      this.wrapCtx = wrapCtx;
   }

   @Override
   public Object lookup(Name name) throws NamingException {
      Object res = wrapCtx.lookup(name);
      if (res instanceof Context) {
         return new ReadOnlyContext((Context) res);
      } else {
         return res;
      }
   }

   @Override
   public Object lookup(String name) throws NamingException {
      Object res = wrapCtx.lookup(name);
      if (res instanceof Context) {
         return new ReadOnlyContext((Context) res);
      } else {
         return res;
      }
   }

   @Override
   public void bind(Name name, Object obj) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public void bind(String name, Object obj) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public void rebind(Name name, Object obj) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public void rebind(String name, Object obj) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public void unbind(Name name) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public void unbind(String name) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public void rename(Name oldName, Name newName) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public void rename(String oldName, String newName) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public NamingEnumeration<NameClassPair> list(Name name) throws NamingException {
      NamingEnumerationImpl<NameClassPair> res = (NamingEnumerationImpl) wrapCtx.list(name);
      Queue<NameClassPair> newElements = new LinkedList<>();
      for (NameClassPair ncp : res.elements) {
         if (Context.class.getName().equals(ncp.getClassName())) {
            NameClassPair newNcp = new NameClassPair(ncp.getName(), ReadOnlyContext.class.getName(), ncp.isRelative());
            newElements.offer(newNcp);
         } else {
            newElements.offer(ncp);
         }
      }
      res.elements = newElements;
      return res;
   }

   @Override
   public NamingEnumeration<NameClassPair> list(String name) throws NamingException {
      CompositeName cn = new CompositeName(0 == name.length() ? "/" : name);
      return list(cn);
   }

   @Override
   public NamingEnumeration<Binding> listBindings(Name name) throws NamingException {
      NamingEnumerationImpl<Binding> res = (NamingEnumerationImpl) wrapCtx.listBindings(name);
      Queue<Binding> newElements = new LinkedList<>();
      for (Binding bnd : res.elements) {
         if (bnd.getObject() instanceof Context) {
            Binding newBnd = new Binding(bnd.getName(), ReadOnlyContext.class.getName(), new ReadOnlyContext((Context) bnd.getObject()), bnd.isRelative());
            newElements.offer(newBnd);
         } else {
            newElements.offer(bnd);
         }
      }
      res.elements = newElements;
      return res;
   }

   @Override
   public NamingEnumeration<Binding> listBindings(String name) throws NamingException {
      CompositeName cn = new CompositeName(0 == name.length() ? "/" : name);
      return listBindings(cn);
   }

   @Override
   public void destroySubcontext(Name name) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public void destroySubcontext(String name) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public Context createSubcontext(Name name) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public Context createSubcontext(String name) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public Object lookupLink(Name name) throws NamingException {
      return lookup(name);
   }

   @Override
   public Object lookupLink(String name) throws NamingException {
      return lookup(name);
   }

   @Override
   public NameParser getNameParser(Name name) throws NamingException {
      return wrapCtx.getNameParser(name);
   }

   @Override
   public NameParser getNameParser(String name) throws NamingException {
      return wrapCtx.getNameParser(name);
   }

   @Override
   public Name composeName(Name name, Name prefix) throws NamingException {
      return wrapCtx.composeName(name, prefix);
   }

   @Override
   public String composeName(String name, String prefix) throws NamingException {
      return wrapCtx.composeName(name, prefix);
   }

   @Override
   public Object addToEnvironment(String propName, Object propVal) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public Object removeFromEnvironment(String propName) throws NamingException {
      throw new OperationNotSupportedException("Context is read-only");
   }

   @Override
   public Hashtable<?, ?> getEnvironment() throws NamingException {
      return wrapCtx.getEnvironment();
   }

   @Override
   public void close() throws NamingException {
      wrapCtx.close();
   }

   @Override
   public String getNameInNamespace() throws NamingException {
      return wrapCtx.getNameInNamespace();
   }
}
