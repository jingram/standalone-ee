/*
 * AbstractContext.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import javax.naming.Binding;
import javax.naming.CompositeName;
import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.NameClassPair;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.spi.NamingManager;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public abstract class AbstractContext implements javax.naming.Context {

   @Override
   public Object lookup(String name) throws NamingException {
      CompositeName cn = new CompositeName(0 == name.length() ? "/" : name);
      return lookup(cn);
   }

   @Override
   public void bind(Name name, Object obj) throws NamingException {
      Object bindObj = obj;
      if (null != obj) {
         bindObj = NamingManager.getStateToBind(obj, new CompositeName(name.get(name.size() - 1)), this, getEnvironment());
      }

      _bind(name, bindObj, false);
   }

   @Override
   public void bind(String name, Object obj) throws NamingException {
      CompositeName cn = new CompositeName(0 == name.length() ? "/" : name);
      bind(cn, obj);
   }

   @Override
   public void rebind(Name name, Object obj) throws NamingException {
      Object bindObj = obj;
      if (null != obj) {
         bindObj = NamingManager.getStateToBind(obj, new CompositeName(name.get(name.size() - 1)), this, getEnvironment());
      }

      _bind(name, bindObj, true);
   }

   @Override
   public void rebind(String name, Object obj) throws NamingException {
      CompositeName cn = new CompositeName(0 == name.length() ? "/" : name);
      rebind(cn, obj);
   }

   @Override
   public void unbind(Name name) throws NamingException {
      _unbind(name);
   }

   @Override
   public void unbind(String name) throws NamingException {
      CompositeName cn = new CompositeName(0 == name.length() ? "/" : name);
      unbind(cn);
   }

   @Override
   public void rename(String oldName, String newName) throws NamingException {
      CompositeName cnOld = new CompositeName(0 == oldName.length() ? "/" : oldName);
      CompositeName cnNew = new CompositeName(0 == newName.length() ? "/" : newName);
      rename(cnOld, cnNew);
   }

   @Override
   public NamingEnumeration<NameClassPair> list(String name) throws NamingException {
      CompositeName cn = new CompositeName(0 == name.length() ? "/" : name);
      return list(cn);
   }

   @Override
   public NamingEnumeration<Binding> listBindings(String name) throws NamingException {
      CompositeName cn = new CompositeName(0 == name.length() ? "/" : name);
      return listBindings(cn);
   }

   @Override
   public void destroySubcontext(Name name) throws NamingException {
      _unbind(name);
   }

   @Override
   public void destroySubcontext(String name) throws NamingException {
      CompositeName cn = new CompositeName(0 == name.length() ? "/" : name);
      destroySubcontext(cn);
   }

   @Override
   public Object lookupLink(Name name) throws NamingException {
      // Implementation does not support links, so always returns the object
      return lookup(name);
   }

   @Override
   public Object lookupLink(String name) throws NamingException {
      CompositeName cn = new CompositeName(name);
      return lookupLink(cn);
   }
   
   @Override
   public void close() throws NamingException {
      // Nothing to really release in terms of resources :)
   }   
   
   @Override
   public String getNameInNamespace() throws NamingException {
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   }   
   
   protected abstract Object _bind(Name name, Object obj, boolean rebind) throws NamingException;

   protected abstract void _unbind(Name name) throws NameNotFoundException, InvalidNameException, NamingException;
}
