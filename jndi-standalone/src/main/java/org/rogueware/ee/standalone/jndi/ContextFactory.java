/*
 * ContextFactory.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org) Note:
 * http://docs.oracle.com/javase/jndi/tutorial/trailmap.html
 * http://docs.oracle.com/javase/7/docs/technotes/guides/jndi/spec/spi/jndispi.fm.html
 * http://docs.oracle.com/cd/E19644-01/817-5449/djjndi.html 
 */
public class ContextFactory implements InitialContextFactory {

   // Structure stores all bindings (objects) 
   // This allows heirachal structure mapped to a flat memory implementation
   private static final Map<String, Object> allBindings = Collections.synchronizedMap(new HashMap<String, Object>(4096));
   
   
   private static Context initialContext;
   
   
   @Override
   public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
      synchronized (ContextFactory.class) {
         if (null == initialContext) {
            initialContext = new Context("", environment, allBindings);
            allBindings.put("/", initialContext);
         }
      }
      return initialContext;
   }


   public static void clearInitialContext() {
      allBindings.clear();
   }
   
}
