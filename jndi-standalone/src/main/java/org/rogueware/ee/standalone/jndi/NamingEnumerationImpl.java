/*
 * NamingEnumerationImpl.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import java.util.NoSuchElementException;
import java.util.Queue;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class NamingEnumerationImpl<T> implements NamingEnumeration<T> {

   protected Queue<T> elements;

   protected NamingEnumerationImpl(Queue<T> elements) {
      this.elements = elements;
   }

   @Override
   public T next() throws NamingException {
      if (elements.isEmpty()) {
         throw new NoSuchElementException("No more elements are available");
      }
      return elements.poll();
   }

   @Override
   public boolean hasMore() throws NamingException {
      return !elements.isEmpty();
   }

   @Override
   public void close() throws NamingException {
      elements.clear();
   }

   @Override
   public boolean hasMoreElements() {
      return !elements.isEmpty();
   }

   @Override
   public T nextElement() {
      if (elements.isEmpty()) {
         return null;
      }
      return elements.poll();
   }
}
