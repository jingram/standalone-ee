/*
 * JNDI.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class JNDI {

   private JNDI() {
      // Cannot instantiate
   }

   public static boolean subContextCreation(Context ic, String context) throws NamingException {
      try {
         if (null == ic) {
            ic = new InitialContext();
         }
      } catch (Exception ex) {
         throw new NamingException(String.format("Unable to create JNDI initial context for context %s creation: %s", context, ex.getMessage()));
      }

      boolean created = false;
      try {
         Context c = ic;
         for (String newCtx : context.split("/")) {
            // Obtain context ... if cannot create it
            try {
               c = (Context) c.lookup(newCtx);
            } catch (NamingException ex) {
               c = c.createSubcontext(newCtx);
               created = true;
            }
         }
      } catch (Exception ex) {
         throw new NamingException(String.format("Unhandled exception trying to create JNDI initial context for context %s creation: %s", context, ex.getMessage()));
      }

      return created;
   }

   public static void rebindWithContextCreation(Context ic, String name, Object val) throws NamingException {
      try {
         if (null == ic) {
            ic = new InitialContext();
         }
      } catch (Exception ex) {
         throw new NamingException(String.format("Unable to create JNDI initial context for rebinding %s: %s", name, ex.getMessage()));
      }

      // Make sure the sub contexts exist
      int lastInd = name.lastIndexOf('/');
      String context = name.substring(0, lastInd);
      subContextCreation(ic, context);

      ic.rebind(name, val);
   }
}
