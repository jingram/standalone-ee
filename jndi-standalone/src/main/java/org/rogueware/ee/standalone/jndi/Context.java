/*
 * Context.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import javax.naming.Binding;
import javax.naming.CompositeName;
import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NameClassPair;
import javax.naming.NameNotFoundException;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.spi.NamingManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class Context extends AbstractContext implements javax.naming.Context {

   private static final Logger log = LoggerFactory.getLogger(Context.class);

   protected final Map<String, Object> bindings;

   // Context environment
   private final Hashtable env;
   protected String contextName;
   protected List<String> children = new ArrayList<>();

   protected Context(String contextName, Hashtable<?, ?> env, Map<String, Object> bindings) {
      this.contextName = contextName;
      this.env = new Hashtable(env);
      this.bindings = bindings;
   }

   @Override
   public Object lookup(Name name) throws NamingException {
      String n = createContextName(name);
      if (!bindings.containsKey(n)) {
         log.trace(String.format("Name %s is not bound", n));
         throw new NameNotFoundException(String.format("Name %s is not bound", n));
      }

      Object res = bindings.get(n);
      if (null != res) {
         try {
            res = NamingManager.getObjectInstance(res, new CompositeName(name.get(name.size() - 1)), this, env);
         } catch (Exception ex) {
            throw new NamingException(ex.getMessage());
         }
      }

      log.trace(String.format("Looked up %s of type %s", n, res.getClass().getName()));
      return res;
   }

   @Override
   public void rename(Name oldName, Name newName) throws NamingException {
      String nOld = createContextName(oldName);
      String nNew = createContextName(newName);

      if (!bindings.containsKey(nOld)) {
         log.trace(String.format("Name %s is not bound", nOld));
         throw new NameNotFoundException(String.format("Name %s is not bound", nOld));
      }

      if (bindings.containsKey(nNew)) {
         log.trace(String.format("Name %s is already bound", nNew));
         throw new NameAlreadyBoundException(String.format("Name %s is already bound", nNew));
      }

      // Make sure all contexts exist before this one on the destination context
      for (int i = 1; i < newName.size(); i++) {
         String n = createContextName(newName.getPrefix(i));
         if (!bindings.containsKey(n)) {
            log.trace(String.format("Context %s is not bound within context %s", n, contextName));
            throw new NameNotFoundException(String.format("Context %s is not bound within context %s", n, contextName));
         }
      }

      synchronized (bindings) {
         Object val = bindings.remove(nOld);
         bindings.put(nNew, val);

         // Rename the child name on the parent(s)
         Name parentOldN = new CompositeName(nOld);
         Name parentNewN = new CompositeName(nNew);

         Context parent;
         String pN;

         // Remove from old parent
         pN = parentOldN.getPrefix(parentOldN.size() - 1).toString();
         if (null != (parent = (Context) bindings.get(pN))) {
            parent.children.remove(oldName.get(oldName.size() - 1));
         }
         // Add to new parent
         pN = parentNewN.getPrefix(parentNewN.size() - 1).toString();
         if (null != (parent = (Context) bindings.get(pN))) {
            parent.children.add(newName.get(newName.size() - 1));
         }

         if (val instanceof Context) {
            ((Context) val).contextName = nNew;

            // Rename all children
            List<String> renameKeys = new ArrayList<>();
            for (String key : bindings.keySet()) {
               if (key.startsWith(nOld)) {
                  renameKeys.add(key);
               }
            }
            for (String key : renameKeys) {
               val = bindings.remove(key);
               String newKey = key.replaceFirst(nOld, nNew);
               bindings.put(newKey, val);
               if (val instanceof Context) {
                ((Context) val).contextName = newKey;                  
               }
            }
         }
      }
   }

   @Override
   public NamingEnumeration<NameClassPair> list(Name name) throws NamingException {
      Object o = lookup(name);
      if (!(o instanceof Context)) {
         log.trace(String.format("%s is not a context", name.toString()));
         throw new NamingException(String.format("%s is not a context", name.toString()));
      }

      Context ctx = (Context) o;
      Queue<NameClassPair> queue = new LinkedList<>();
      for (String child : ctx.children) {
         o = ctx.lookup(child);
         queue.offer(new NameClassPair(child, o.getClass().getName(), true));
      }
      return new NamingEnumerationImpl<>(queue);
   }

   @Override
   public NamingEnumeration<Binding> listBindings(Name name) throws NamingException {
      Object o = lookup(name);
      if (!(o instanceof Context)) {
         log.trace(String.format("%s is not a context", name.toString()));
         throw new NamingException(String.format("%s is not a context", name.toString()));
      }

      Context ctx = (Context) o;
      Queue<Binding> queue = new LinkedList<>();
      for (String child : ctx.children) {
         o = ctx.lookup(child);
         queue.offer(new Binding(child, o.getClass().getName(), o, true));
      }
      return new NamingEnumerationImpl<>(queue);
   }

   @Override
   public Context createSubcontext(Name name) throws NamingException {
      return (Context) _bind(name, new Context(createContextName(name), env, bindings), false);
   }

   @Override
   public Context createSubcontext(String name) throws NamingException {
      CompositeName cn = new CompositeName(name);
      return createSubcontext(cn);
   }

   @Override
   public NameParser getNameParser(Name name) throws NamingException {
      String n = name.toString();
      if (!bindings.containsKey(n) || !(bindings.get(n) instanceof javax.naming.Context)) {
         throw new NameNotFoundException(String.format("Context %s not found", n));
      }

      Context ctx = (Context) bindings.get(n);
      if (this != ctx) {
         return ctx.getNameParser(name);
      }

      return new NameParserImpl();  // This can change per context if required
   }

   @Override
   public NameParser getNameParser(String name) throws NamingException {
      if (0 == name.length()) {
         name = "/";
      }
      CompositeName cn = new CompositeName(name);
      return getNameParser(cn);
   }

   @Override
   public Name composeName(Name name, Name prefix) throws NamingException {
      return prefix.addAll(name);
   }

   @Override
   public String composeName(String name, String prefix) throws NamingException {
      if (0 == name.length()) {
         name = "/";
      }
      if (0 == prefix.length()) {
         prefix = "/";
      }
      CompositeName cn = new CompositeName(name);
      CompositeName cp = new CompositeName(prefix);
      return composeName(cn, cp).toString();
   }

   @Override
   public Object addToEnvironment(String propName, Object propVal) throws NamingException {
      return env.put(propName, propVal);
   }

   @Override
   public Object removeFromEnvironment(String propName) throws NamingException {
      return env.remove(propName);
   }

   @Override
   public Hashtable<?, ?> getEnvironment() throws NamingException {
      // Make a copy as cannot modify environment using returned reference 
      Hashtable<?, ?> res = new Hashtable<>(env);
      return res;
   }

   @Override
   protected Object _bind(Name name, Object obj, boolean rebind) throws NameNotFoundException, NameAlreadyBoundException, InvalidNameException {
      // Make sure all contexts exist before this one
      for (int i = 1; i < name.size(); i++) {
         String n = createContextName(name.getPrefix(i));
         if (!bindings.containsKey(n)) {
            log.trace(String.format("Context %s is not bound within context %s", n, contextName));
            throw new NameNotFoundException(String.format("Context %s is not bound within context %s", n, contextName));
         }
      }

      synchronized (bindings) {
         // Make sure the one we are binding does not already exist, unless we are rebinding
         String n = createContextName(name.getPrefix(name.size()));
         if (!rebind) {
            if (bindings.containsKey(n)) {
               log.trace(String.format("Name %s is already bound within context %s", n, contextName));
               throw new NameAlreadyBoundException(String.format("Name %s is already bound within context %s", n, contextName));
            }
         }

         bindings.put(n, obj);
         // Make sure the parent context is tracking all its children
         Name parentN = new CompositeName(n);
         String pN = parentN.getPrefix(parentN.size() - 1).toString();
         Context parent;
         if (null != (parent = (Context) bindings.get(pN))) {
            if (!parent.children.contains(name.get(name.size() - 1))) {
               parent.children.add(name.get(name.size() - 1));
            }
         }
         log.trace(String.format("Bound name %s using object type %s", n, obj.getClass().getName()));

         return obj;
      }
   }

   @Override
   protected void _unbind(Name name) throws NameNotFoundException, InvalidNameException, NamingException {
      // Make sure the intermediate contexts exist      
      String n = createContextName(name);
      CompositeName cn = new CompositeName(n);

      // Context cannot unbind / destroy itself
      if (n.equals(createContextName(new CompositeName("/")))) {
         log.trace(String.format("Context %s cannot destroy itself", n));
         throw new NamingException(String.format("Context %s cannot destroy itself", n));
      }

      String intermediateN = cn.getPrefix(cn.size() - 1).toString();
      if (!bindings.containsKey(intermediateN) || !(bindings.get(intermediateN) instanceof Context)) {
         log.trace(String.format("Intermediate context %s is not bound", n));
         throw new NameNotFoundException(String.format("Intermediate context %s is not bound", n));
      }

      // Idempotent
      if (!bindings.containsKey(n)) {
         return;
      }

      synchronized (bindings) {
         Object val = bindings.remove(n);
         // Remove the child name from the parent
         Name parentN = new CompositeName(n);
         String pN = parentN.getPrefix(parentN.size() - 1).toString();
         Context parent;
         if (null != (parent = (Context) bindings.get(pN))) {
            parent.children.remove(name.get(name.size() - 1));
         }

         if (val instanceof Context) {
            // Delete all children 
            List<String> delKeys = new ArrayList<>();
            for (String key : bindings.keySet()) {
               if (key.startsWith(n)) {
                  delKeys.add(key);
               }
            }
            for (String key : delKeys) {
               Object del = bindings.remove(key);
               del = null;
            }
         }
         val = null;
      }
   }

   protected String createContextName(Name name) {
      String n = name.toString();
      String res;
      if (contextName.endsWith("/") && n.startsWith("/")) {
         res = contextName.substring(0, contextName.length() - 1) + n;
      } else if (!contextName.endsWith("/") && !n.startsWith("/")) {
         res = contextName + "/" + n;
      } else {
         res = contextName + n;
      }

      // Cant end with / unless its the only item
      if (res.length() > 1 && res.endsWith("/")) {
         res = res.substring(0, res.length() - 1);
      }

      // Replace any // with /
      while (res.contains("//")) {
         res = res.replaceAll("//", "/");
      }

      return res;
   }

   protected String createContextName(String prefix, Name name) {
      String n = name.toString();
      String res;
      if (prefix.endsWith("/") && n.startsWith("/")) {
         res = prefix.substring(0, prefix.length() - 1) + n;
      } else if (!contextName.endsWith("/") && !n.startsWith("/")) {
         res = prefix + "/" + n;
      } else {
         res = prefix + n;
      }

      // Cant end with / unless its the only item
      if (res.length() > 1 && res.endsWith("/")) {
         res = res.substring(0, res.length() - 1);
      }

      // Replace any // with /
      while (res.contains("//")) {
         res = res.replaceAll("//", "/");
      }

      return res;
   }   
   
   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(createContextName(new CompositeName()));
      sb.append("[\n");
      for (String child : children) {
         sb.append(child);
         sb.append("\n");
      }
      sb.append("]");
      return sb.toString();
   }
}
