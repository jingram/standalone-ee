/*
 * ThreadContextFactory.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class ThreadContextFactory implements InitialContextFactory {

   private static final Logger log = LoggerFactory.getLogger(ThreadContextFactory.class);

   // Structure stores all bindings (objects) scoped only to a thread!!!!
   private static final ThreadLocal<Map<String, Object>> threadBindings = new ThreadLocal<>();
   private static final ThreadLocal<Deque<Map<String, Object>>> threadBindingsStack = new ThreadLocal<>();
   private static final ThreadLocal<Boolean> threadContextCreatedByPush = new ThreadLocal<>();

   @Override
   public Context getInitialContext(Hashtable<?, ?> environment) throws NamingException {
      // Get the default initial context
      InitialContext ic = new InitialContext();
      return getInitialContext(ic, environment);
   }

   public static Context getInitialContext(Context ic, Hashtable<?, ?> environment) throws NamingException {
      // Get the default initial context
      if (null == ic) {
         ic = new InitialContext();
      }

      // Obtain or init thread vars
      Map<String, Object> localThreadBindings = threadBindings.get();
      if (null == localThreadBindings) {
         localThreadBindings = new HashMap<>();
         threadBindings.set(localThreadBindings);
      }

      Deque<Map<String, Object>> localThreadBindingsStack = threadBindingsStack.get();
      if (null == localThreadBindingsStack) {
         localThreadBindingsStack = new ArrayDeque<>();
         threadBindingsStack.set(localThreadBindingsStack);
      }

      return new ThreadContext(ic, "/", new Hashtable(), localThreadBindings);
   }

   public static void destoyThreadContext() {
      Map<String, Object> localThreadBindings = threadBindings.get();
      if (null != localThreadBindings) {
         localThreadBindings.clear();
      }
      Deque<Map<String, Object>> localThreadBindingsStack = threadBindingsStack.get();
      if (null != localThreadBindingsStack) {
         localThreadBindingsStack.clear();
      }

      localThreadBindings = null;
      threadBindings.set(null);
      threadBindingsStack.set(null);
      threadContextCreatedByPush.set(null);
   }

   public static boolean hasThreadContext() {
      return null != threadBindings.get();
   }

   public static void pushContext() throws NamingException {
      // Push the current thread context and start with a clean context
      if (null == threadBindings.get() || null == threadBindingsStack.get()) {
         threadContextCreatedByPush.set(true);
         getInitialContext(null, new Hashtable());
      }

      Map<String, Object> localThreadBindings = threadBindings.get();
      Deque<Map<String, Object>> localThreadBindingsStack = threadBindingsStack.get();

      // Don't destroy reference of localThreadBindings as other objects may already have this reference
      localThreadBindingsStack.push(new HashMap<>(localThreadBindings));
      localThreadBindings.clear();
   }

   public static void pushAndPreserveContext() throws NamingException {
      // Push the current thread context and preserbe the current context
      if (null == threadBindings.get() || null == threadBindingsStack.get()) {
         threadContextCreatedByPush.set(true);
         getInitialContext(null, new Hashtable());
      }

      Map<String, Object> localThreadBindings = threadBindings.get();
      Deque<Map<String, Object>> localThreadBindingsStack = threadBindingsStack.get();

      localThreadBindingsStack.push(new HashMap<>(localThreadBindings));
      // Keep existing copy the same
   }

   public static void popContext() throws NamingException {
      // Discard the current context and restore it from the last pushed context
      if (null == threadBindings.get() || null == threadBindingsStack.get()) {
         throw new NamingException("ThreadContext has not been initialized or pushed to");
      }

      Map<String, Object> localThreadBindings = threadBindings.get();
      Deque<Map<String, Object>> localThreadBindingsStack = threadBindingsStack.get();
      if (localThreadBindingsStack.isEmpty()) {
         throw new NamingException("ThreadContext has no pushed state to pop");
      }

      localThreadBindings.clear();
      localThreadBindings.putAll(localThreadBindingsStack.pop());

      // Check if we need to destroy the thread context if it was created using push
      if (localThreadBindingsStack.isEmpty() && null != threadContextCreatedByPush.get() && threadContextCreatedByPush.get()) {
         // Destroy the thread context as this is the last pop and context was created in terms of a stack
         destoyThreadContext();
      }
   }
}
