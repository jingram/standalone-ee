/*
 * EEContextFactory.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import java.util.Hashtable;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;
import javax.naming.spi.InitialContextFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class EEInitialContextFactoryBuilder implements InitialContextFactoryBuilder {

   private transient static final Logger log = LoggerFactory.getLogger(EEInitialContextFactoryBuilder.class);
   private transient static Hashtable initialProviderJndiEnv;

   public EEInitialContextFactoryBuilder() {
      // Preserve any JNDI implementation that is being asked to be used
      try {
         InitialContext ctx = new InitialContext();
         initialProviderJndiEnv = ctx.getEnvironment();

         if (null != initialProviderJndiEnv.get(javax.naming.Context.INITIAL_CONTEXT_FACTORY)) {
            log.info(String.format("Detected existing JNDI provider context factory %s", initialProviderJndiEnv.get(javax.naming.Context.INITIAL_CONTEXT_FACTORY)));
         } else {
            log.info(String.format("Detected existing JNDI provider context factory, but could not determine the context factory class. Please ensure %s is set", javax.naming.Context.INITIAL_CONTEXT_FACTORY));
         }
      } catch (Exception ex) {
         // No existing JNDI provider, use in-memory provider
         initialProviderJndiEnv = new Hashtable();
         initialProviderJndiEnv.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");
         log.info("No existing JNDI provider context factory detected, using JNDI provider context factory org.rogueware.ee.standalone.jndi.ContextFactory");
      }
   }

   @Override
   public InitialContextFactory createInitialContextFactory(Hashtable<?, ?> environment) throws NamingException {
      String ctxFactoryClass = (String) initialProviderJndiEnv.get(javax.naming.Context.INITIAL_CONTEXT_FACTORY);
      if (null != ctxFactoryClass) {
         InitialContextFactory icf = null;
         try {
            Class<?> icfClass = Class.forName(ctxFactoryClass);
            icf = (InitialContextFactory) icfClass.newInstance();
         } catch (Exception ex) {
            log.error(String.format("Unable to create context factory class %s", ctxFactoryClass), ex);
            throw new NamingException(String.format("Unable to create context factory class %s", ctxFactoryClass));
         }

         // Check between thread context active or not active
         // Thread context is active when thread executing through EE container
         if (ThreadContextFactory.hasThreadContext()) {
            // Thread executing inside an EE container, wrap the thread context as read-only
            javax.naming.Context c = icf.getInitialContext(environment);
            javax.naming.Context tc = ThreadContextFactory.getInitialContext(c, environment);
            return new ReadOnlyContextFactory(tc);
         } else {
            // Return the normal context factory the application was initialized with
            return icf;
         }

      } else {
         throw new NamingException(String.format("No context factory class set. Please ensure %s is set", javax.naming.Context.INITIAL_CONTEXT_FACTORY));
      }

   }

}
