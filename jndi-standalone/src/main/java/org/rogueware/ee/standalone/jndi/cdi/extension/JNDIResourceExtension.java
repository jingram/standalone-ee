/*
 * JNDIResourceExtension.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi.cdi.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import javax.annotation.Resources;
import javax.ejb.MessageDriven;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterDeploymentValidation;
import javax.enterprise.inject.spi.AnnotatedConstructor;
import javax.enterprise.inject.spi.AnnotatedField;
import javax.enterprise.inject.spi.AnnotatedMethod;
import javax.enterprise.inject.spi.AnnotatedType;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.BeforeBeanDiscovery;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.ProcessAnnotatedType;
import javax.enterprise.inject.spi.WithAnnotations;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.NamingManager;
import org.rogueware.ee.standalone.jndi.EEInitialContextFactoryBuilder;
import org.rogueware.ee.standalone.jndi.JNDI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class JNDIResourceExtension implements Extension {

   private transient static final Logger log = LoggerFactory.getLogger(JNDIResourceExtension.class);

   private static final List<String> CONTEXTS = new ArrayList<>(Arrays.asList("java:comp/env", "java:module", "java:global"));

   @ResourceRequired
   public static class ResourceRequiredWrapper {

      public static Annotation getAnnotation() {
         try {
            return ResourceRequiredWrapper.class.getAnnotation(ResourceRequired.class);
         } catch (Exception ex) {
            return null;
         }
      }
   }

   // Initialiase JNDI for EE
   public void initJNDI(@Observes BeforeBeanDiscovery bbd) {
      // Set the initial context factory builder to allow the application to understand JNDI in EE
      InitialContext ic = null;
      try {
         NamingManager.setInitialContextFactoryBuilder(new EEInitialContextFactoryBuilder());
         ic = new InitialContext();
         log.info("Installed JNDI initial context factory builder to handle Java EE container requirements");
      } catch (Exception ex) {
         log.error("Unable to initialize JNDI for EE", ex);
      }

      // Create the required contexts if not already created
      boolean created;
      for (String ctx : CONTEXTS) {
         try {
            created = JNDI.subContextCreation(ic, ctx);
            if (created) {
               log.info(String.format("Created JNDI context %s", ctx));
            }
         } catch (NamingException ex) {
            log.error(String.format("Unable to create JNDI context %s", ctx), ex);
         }
      }
   }

   // Start the EJB "Containers"
   public void jndiBindings(@Observes AfterDeploymentValidation event, BeanManager bm) {
      // Bind any required values
      try {
         InitialContext ic = new InitialContext();
         ic.rebind("java:comp/BeanManager", bm);
      } catch (NamingException ex) {
         log.error("Unable to bind CDI Bean Manager to JNDI name java:comp/BeanManager", ex);
      }
   }

   public <T> void processResourceAnnotations(@Observes @WithAnnotations({Resource.class, Resources.class}) ProcessAnnotatedType<T> pat) {
      final AnnotatedType<T> at = pat.getAnnotatedType();
      Class javaClass = at.getJavaClass();

      // Add the ResourceRequired annotation to any beans that require resource injects for interceptor to handle
      // (@Resources or @Resource present)
      // This will not be added to EJB's as the container will handle injection
      if (!javaClass.isAnnotationPresent(Stateless.class) && !javaClass.isAnnotationPresent(Singleton.class) && !javaClass.isAnnotationPresent(MessageDriven.class)) {
         pat.setAnnotatedType(new AnnotatedType<T>() {

            @Override
            public Class<T> getJavaClass() {
               return at.getJavaClass();
            }

            @Override
            public Set<AnnotatedConstructor<T>> getConstructors() {
               return at.getConstructors();
            }

            @Override
            public Set<AnnotatedMethod<? super T>> getMethods() {
               return at.getMethods();
            }

            @Override
            public Set<AnnotatedField<? super T>> getFields() {
               return at.getFields();
            }

            @Override
            public Type getBaseType() {
               return at.getBaseType();
            }

            @Override
            public Set<Type> getTypeClosure() {
               return at.getTypeClosure();
            }

            @Override
            public <T extends Annotation> T getAnnotation(Class<T> annotationType) {
               if (annotationType.equals(ResourceRequired.class)) {
                  return (T) ResourceRequiredWrapper.getAnnotation();
               }
               return at.getAnnotation(annotationType);
            }

            @Override
            public Set<Annotation> getAnnotations() {
               Set<Annotation> result = new HashSet<>(at.getAnnotations());
               result.add(ResourceRequiredWrapper.getAnnotation());
               return result;
            }

            @Override
            public boolean isAnnotationPresent(Class<? extends Annotation> annotationType) {
               if (ResourceRequired.class.equals(annotationType)) {
                  return true;
               }
               return at.isAnnotationPresent(annotationType);
            }
         });
      }
   }

}
