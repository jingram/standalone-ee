/*
 * ThreadContext.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import javax.naming.Binding;
import javax.naming.CompositeName;
import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NameClassPair;
import javax.naming.NameNotFoundException;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.OperationNotSupportedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 * NOTE: This context writes are only recorded in the current thread!!! 
 * Any delete to the main context will fail. 
 * Any bindings over the main context will be scoped for the life of
 * the thread context
 */
public class ThreadContext extends AbstractContext implements javax.naming.Context {

   private static final Logger log = LoggerFactory.getLogger(ThreadContext.class);

   private final javax.naming.Context mainCtx;
   private final Context threadCtx;

   // Context environment
   protected ThreadContext(javax.naming.Context mainCtx, String contextName, Hashtable<?, ?> env, Map<String, Object> bindings) {
      // NOTE: Both contexts share the same name !!!!
      this.mainCtx = mainCtx;
      this.threadCtx = new org.rogueware.ee.standalone.jndi.Context(contextName, env, bindings);
   }

   @Override
   public Object lookup(Name name) throws NamingException {
      Object res;
      try {
         res = threadCtx.lookup(name);
      } catch (NameNotFoundException ex) {
         if (null != mainCtx) {
            res = mainCtx.lookup(name);
         } else {
            throw ex;
         }
      }

      if (res instanceof javax.naming.Context && !(res instanceof ThreadContext)) {
         res = new ThreadContext((javax.naming.Context) res, threadCtx.createContextName(name), new Hashtable(), threadCtx.bindings);
         threadCtx.bindings.put(threadCtx.createContextName(name), res);
      }

      return res;
   }

   @Override
   public void rename(Name oldName, Name newName) throws NamingException {
      String nOld = threadCtx.createContextName(oldName);
      String nNew = threadCtx.createContextName(newName);

      try {
         mainCtx.lookup(nOld);
         if (!threadCtx.bindings.containsKey(nOld)) {
            log.trace(String.format("Thread context cannot change underlying main context binding %s", nOld));
            throw new OperationNotSupportedException(String.format("Thread context cannot change underlying main context binding %s", nOld));
         }
      } catch (NameNotFoundException ex) {
         if (!threadCtx.bindings.containsKey(nOld)) {
            log.trace(String.format("Name %s is not bound", nOld));
            throw new NameNotFoundException(String.format("Name %s is not bound", nOld));
         }
      }

      try {
         mainCtx.lookup(nNew);
         log.trace(String.format("Name %s is already bound", nNew));
         throw new NameAlreadyBoundException(String.format("Name %s is already bound", nNew));
      } catch (NameNotFoundException ex) {
         if (threadCtx.bindings.containsKey(nNew)) {
            log.trace(String.format("Name %s is already bound", nNew));
            throw new NameAlreadyBoundException(String.format("Name %s is already bound", nNew));
         }
      }

      // Make sure all contexts exist before this one on the destination context and
      //  make sure if it exists on the main context, that we create a ThreadContext to wrap it
      try {
         String newParent = threadCtx.createContextName(newName.getPrefix(newName.size() - 1));
         if (!threadCtx.bindings.containsKey(newParent)) {
            Context p = (Context) mainCtx.lookup(newParent);
            threadCtx.bindings.put(newParent, new ThreadContext(p, newParent, new Hashtable(), threadCtx.bindings));
         }
      } catch (NameNotFoundException ex) {
         log.trace(String.format("Context %sd not bound %s", newName.getPrefix(newName.size() - 1).toString()));
         throw new NameNotFoundException(String.format("Context %sd not bound %s", newName.getPrefix(newName.size() - 1).toString()));
      }

      synchronized (threadCtx.bindings) {
         Object val = threadCtx.bindings.remove(nOld);
         threadCtx.bindings.put(nNew, val);

         // Rename the child name on the parent(s)
         Name parentOldN = new CompositeName(nOld);
         Name parentNewN = new CompositeName(nNew);

         ThreadContext parent;
         String pN;

         // Remove from old parent
         pN = parentOldN.getPrefix(parentOldN.size() - 1).toString();
         if (null != (parent = (ThreadContext) threadCtx.bindings.get(pN))) {
            parent.threadCtx.children.remove(oldName.get(oldName.size() - 1));
         }
         // Add to new parent
         pN = parentNewN.getPrefix(parentNewN.size() - 1).toString();
         if (null != (parent = (ThreadContext) threadCtx.bindings.get(pN))) {
            parent.threadCtx.children.add(newName.get(newName.size() - 1));
         }

         if (val instanceof ThreadContext) {
            ((ThreadContext) val).threadCtx.contextName = nNew;

            // Rename all children
            List<String> renameKeys = new ArrayList<>();
            for (String key : threadCtx.bindings.keySet()) {
               if (key.startsWith(nOld)) {
                  renameKeys.add(key);
               }
            }
            for (String key : renameKeys) {
               val = threadCtx.bindings.remove(key);
               String newKey = key.replaceFirst(nOld, nNew);
               threadCtx.bindings.put(newKey, val);
               if (val instanceof Context) {
                  ((Context) val).contextName = newKey;
               }
            }
         }
      }
   }

   @Override
   public NamingEnumeration<NameClassPair> list(Name name) throws NamingException {
      Queue<NameClassPair> queue = new LinkedList<>();

      try {
         if (!(mainCtx.lookup(name) instanceof javax.naming.Context)) {
            log.trace(String.format("%s is not a context", name.toString()));
            throw new NamingException(String.format("%s is not a context", name.toString()));
         }

         NamingEnumeration<NameClassPair> mainNe = mainCtx.list(name);
         while (mainNe.hasMore()) {
            queue.offer(mainNe.next());
         }
      } catch (NameNotFoundException ex) {
         // We will only use the thread context entries as there is no underlying main context ...
         //  unless it does not exist on the thread
         threadCtx.lookup(name);
      }

      // Add the threads listing
      try {
         Object o = lookup(name);
         if (!(o instanceof ThreadContext)) {
            log.trace(String.format("%s is not a context", name.toString()));
            throw new NamingException(String.format("%s is not a context", name.toString()));
         }

         ThreadContext ctx = (ThreadContext) o;
         for (String child : ctx.threadCtx.children) {
            o = ctx.lookup(child);
            queue.offer(new NameClassPair(child, o.getClass().getName(), true));
         }
      } catch (NameNotFoundException ex) {
         // Just use main context listing
      }
      return new NamingEnumerationImpl<>(queue);
   }

   @Override
   public NamingEnumeration<Binding> listBindings(Name name) throws NamingException {
      Queue<Binding> queue = new LinkedList<>();

      try {
         if (!(mainCtx.lookup(name) instanceof Context)) {
            log.trace(String.format("%s is not a context", name.toString()));
            throw new NamingException(String.format("%s is not a context", name.toString()));
         }

         NamingEnumeration<Binding> mainNe = mainCtx.listBindings(name);
         while (mainNe.hasMore()) {
            queue.offer(mainNe.next());
         }
      } catch (NameNotFoundException ex) {
         // We will only use the thread context entries as there is no underlying main context ...
         //  unless it does not exist on the thread
         threadCtx.lookup(name);
      }

      // Add the threads listing
      try {

         Object o = lookup(name);
         if (!(o instanceof ThreadContext)) {
            log.trace(String.format("%s is not a context", name.toString()));
            throw new NamingException(String.format("%s is not a context", name.toString()));
         }

         ThreadContext ctx = (ThreadContext) o;

         for (String child : ctx.threadCtx.children) {
            o = ctx.lookup(child);
            queue.offer(new Binding(child, o.getClass().getName(), o, true));
         }
      } catch (NameNotFoundException ex) {
         // Just use main context listing
      }
      return new NamingEnumerationImpl<>(queue);
   }

   @Override
   public ThreadContext createSubcontext(Name name) throws NamingException {
      return (ThreadContext) _bind(name, new ThreadContext(null, threadCtx.createContextName(name), getEnvironment(), threadCtx.bindings), false);
   }

   @Override
   public ThreadContext createSubcontext(String name) throws NamingException {
      CompositeName cn = new CompositeName(name);
      return createSubcontext(cn);
   }

   @Override
   public NameParser getNameParser(Name name) throws NamingException {
      return threadCtx.getNameParser(name);
   }

   @Override
   public NameParser getNameParser(String name) throws NamingException {
      return threadCtx.getNameParser(name);
   }

   @Override
   public Name composeName(Name name, Name prefix) throws NamingException {
      return threadCtx.composeName(name, prefix);
   }

   @Override
   public String composeName(String name, String prefix) throws NamingException {
      return threadCtx.composeName(name, prefix);
   }

   @Override
   public Object addToEnvironment(String propName, Object propVal) throws NamingException {
      if (null != mainCtx && mainCtx.getEnvironment().containsKey(propName)) {
         throw new OperationNotSupportedException(String.format("Thread context cannot change underlying main context property %s", propName));
      }
      return threadCtx.addToEnvironment(propName, propVal);
   }

   @Override
   public Object removeFromEnvironment(String propName) throws NamingException {
      if (null != mainCtx && mainCtx.getEnvironment().contains(propName)) {
         throw new OperationNotSupportedException(String.format("Thread context cannot change underlying main context property %s", propName));
      }
      return threadCtx.removeFromEnvironment(propName);
   }

   @Override
   public Hashtable<?, ?> getEnvironment() throws NamingException {
      // Merge the two contexts (make a copy)
      Hashtable res = new Hashtable();

      if (null != mainCtx) {
         for (Object key : mainCtx.getEnvironment().keySet()) {
            res.put(key, mainCtx.getEnvironment().get(key));
         }
      }

      for (Object key : threadCtx.getEnvironment().keySet()) {
         res.put(key, threadCtx.getEnvironment().get(key));
      }
      return res;
   }

   @Override
   protected Object _bind(Name name, Object obj, boolean rebind) throws NamingException {
      // Make sure all contexts exist before the name being bound
      for (int i = 1; i < name.size(); i++) {
         Name nm = name.getPrefix(i);
         try {
            threadCtx.lookup(nm);
         } catch (NameNotFoundException ex) {
            if (null != mainCtx) {
               mainCtx.lookup(nm);
            } else {
               throw ex;
            }
         }
      }

      // Make sure the one we are binding does not already exist, unless we are rebinding
      String n = threadCtx.createContextName(name.getPrefix(name.size()));
      if (!rebind) {
         try {
            threadCtx.lookup(name);
            log.trace(String.format("Name %s is already bound within context %s", name.toString(), threadCtx.contextName));
            throw new NameAlreadyBoundException(String.format("Name %s is already bound within context %s", n, threadCtx.contextName));
         } catch (NameNotFoundException ex) {
            // :) 
         }
         if (null != mainCtx) {
            try {
               mainCtx.lookup(name);
               log.trace(String.format("Name %s is already bound within context %s", name.toString(), threadCtx.contextName));
               throw new NameAlreadyBoundException(String.format("Name %s is already bound within context %s", n, threadCtx.contextName));
            } catch (NameNotFoundException ex) {
               // :) 
            }
         }
      }

      synchronized (threadCtx.bindings) {
         threadCtx.bindings.put(n, obj);

         // Make sure the parent context is tracking all its additional children
         Name parentN = new CompositeName(n);
         String pN = parentN.getPrefix(parentN.size() - 1).toString();
         org.rogueware.ee.standalone.jndi.ThreadContext parent;

         if (!threadCtx.bindings.containsKey(pN)) {
            // Add a thread context wrapper for the context the new binding appeared under if its not already wrapped
            javax.naming.Context pCntx = (javax.naming.Context) mainCtx.lookup(pN);
            threadCtx.bindings.put(pN, new ThreadContext(pCntx, pN, pCntx.getEnvironment(), threadCtx.bindings));
         }

         if (null != (parent = (org.rogueware.ee.standalone.jndi.ThreadContext) threadCtx.bindings.get(pN))) {
            if (!parent.threadCtx.children.contains(name.get(name.size() - 1))) {
               parent.threadCtx.children.add(name.get(name.size() - 1));
            }
         }
         log.trace(String.format("Bound name %s using object type %s", n, obj.getClass().getName()));

         return obj;
      }
   }

   @Override
   protected void _unbind(Name name) throws NameNotFoundException, InvalidNameException, NamingException {
      // Make sure the intermediate contexts exist      
      String n = threadCtx.createContextName(name);
      CompositeName cn = new CompositeName(n);

      // Context cannot unbind / destroy itself
      if (n.equals(threadCtx.createContextName(new CompositeName("/")))) {
         log.trace(String.format("Context %s cannot destroy itself", n));
         throw new NamingException(String.format("Context %s cannot destroy itself", n));
      }

      String intermediateN = cn.getPrefix(cn.size() - 1).toString();
      try {
         mainCtx.lookup(intermediateN);
      } catch (NameNotFoundException ex) {
         if (!threadCtx.bindings.containsKey(intermediateN) || !(threadCtx.bindings.get(intermediateN) instanceof Context)) {
            log.trace(String.format("Intermediate context %s is not bound", n));
            throw new NameNotFoundException(String.format("Intermediate context %s is not bound", n));
         }
      }

      // Idempotent
      try {
         Object o = mainCtx.lookup(name);
         // Cannot unbind any context from the main contect
         if (o instanceof javax.naming.Context) {
            log.trace(String.format("Thread context cannot change underlying main context binding %s", n));
            throw new OperationNotSupportedException(String.format("Thread context cannot change underlying main context binding %s", n));
         }

         // Main has it, thread does not and want to unbind
         if (!threadCtx.bindings.containsKey(n)) {
            log.trace(String.format("Thread context cannot change underlying main context binding %s", n));
            throw new OperationNotSupportedException(String.format("Thread context cannot change underlying main context binding %s", n));
         }
      } catch (NameNotFoundException ex) {
         if (!threadCtx.bindings.containsKey(n)) {
            return;
         }
      }

      synchronized (threadCtx.bindings) {
         Object val = threadCtx.bindings.remove(n);
         // Remove the child name from the parent
         Name parentN = new CompositeName(n);
         String pN = parentN.getPrefix(parentN.size() - 1).toString();
         ThreadContext parent;
         if (null != (parent = (ThreadContext) threadCtx.bindings.get(pN))) {
            parent.threadCtx.children.remove(name.get(name.size() - 1));
         }

         if (val instanceof ThreadContext) {
            // Delete all children 
            List<String> delKeys = new ArrayList<>();
            for (String key : threadCtx.bindings.keySet()) {
               if (key.startsWith(n)) {
                  delKeys.add(key);
               }
            }
            for (String key : delKeys) {
               Object del = threadCtx.bindings.remove(key);
               del = null;
            }
         }
         val = null;
      }
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      if (null != mainCtx) {
         sb.append(mainCtx.toString());
         sb.append("\n");
      }
      sb.append("THREAD ");
      sb.append(Thread.currentThread().getName());
      sb.append(threadCtx.createContextName(new CompositeName()));
      sb.append("[\n");
      for (String child : threadCtx.children) {
         sb.append(child);
         sb.append("\n");
      }
      sb.append("]");
      return sb.toString();
   }
}
