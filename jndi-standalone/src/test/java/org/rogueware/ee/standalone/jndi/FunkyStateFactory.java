/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.rogueware.ee.standalone.jndi;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.spi.StateFactory;

/**
 *
 * @author jingram
 */
public class FunkyStateFactory implements StateFactory {

   @Override
   public Object getStateToBind(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment) throws NamingException {
      if (obj instanceof Funky) {
         Funky funky = (Funky) obj;
         return funky.getRef();
      } else {
         return null;
      }
   }

}
