/*
 * TestContext.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import javax.naming.Binding;
import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NameClassPair;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestContext {

   @BeforeClass
   public static void init() throws Exception {
      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.INFO);

      Logger jndiLogger = (Logger) LoggerFactory.getLogger("org.rogueware.ee.standalone.jndi");
      jndiLogger.setLevel(Level.TRACE);
   }

   @Test
   public void testJNDI() throws Exception {
      Hashtable env = new Hashtable();
      env.put(Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");
      //  env.put(Context.OBJECT_FACTORIES, "org.rogueware.ee.standalone.jndi.FunkyObjectFactory");
      env.put(Context.STATE_FACTORIES, "org.rogueware.ee.standalone.jndi.FunkyStateFactory");
      runJNDITest(env);
   }

   @Test
   public void testCompareJNDI() throws Exception {
      Hashtable env = new Hashtable();
      env.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
      //env.put(Context.OBJECT_FACTORIES, "org.rogueware.ee.standalone.jndi.FunkyObjectFactory");
      env.put(Context.STATE_FACTORIES, "org.rogueware.ee.standalone.jndi.FunkyStateFactory");
      runJNDITest(env);
   }

   private void runJNDITest(Hashtable env) throws Exception {
      InitialContext ctx = new InitialContext(env);

      createContext(ctx);
      bindings(ctx);
      naming(ctx);
      environment(ctx);
      listing(ctx);
      listBindings(ctx);
      destroyContext(ctx);
   }

   private void createContext(InitialContext ctx) throws Exception {
      // Create context
      Context ctxJava = ctx.createSubcontext("java:");
      Context ctxA = ctxJava.createSubcontext("a");
      ctx.createSubcontext("/java:/a1");
      Context ctxB = ctxA.createSubcontext("b");
      Context ctxC = ctxJava.createSubcontext("a/b/c");

      assertNotNull("java: not bound", ctx.lookup("java:"));
      assertNotNull("java: not bound", ctxJava.lookup("/"));
      assertTrue(ctxJava.lookup("/") instanceof Context);

      assertNotNull("java:/a not bound", ctx.lookup("java:/a"));
      assertNotNull("java:/a not bound", ctxJava.lookup("a"));
      assertNotNull("java:/a not bound", ctxA.lookup("/"));
      assertTrue(ctxA.lookup("/") instanceof Context);
      try {
         ctxJava.lookup("b");
         fail(":java/b is not supposed to be bound and throw NameNotFoundException");
      } catch (NameNotFoundException ex) {
         // :)
      }

      assertNotNull("java:/a1 not bound", ctx.lookup("java:/a1"));
      assertTrue(ctx.lookup("java:/a1") instanceof Context);

      assertNotNull("java:/a/b not bound", ctx.lookup("/java:/a/b"));
      assertNotNull("java:/a/b not bound", ctxJava.lookup("/a/b"));
      assertNotNull("java:/a/b not bound", ctxA.lookup("/b"));
      assertNotNull("java:/a/b not bound", ctxB.lookup("/"));
      assertTrue(ctxA.lookup("/b") instanceof Context);

      assertNotNull("java:/a/b/c not bound", ctx.lookup("/java:/a/b/c"));
      assertNotNull("java:/a/b/c not bound", ctxJava.lookup("/a/b/c"));
      assertNotNull("java:/a/b/c not bound", ctxA.lookup("/b/c"));
      assertNotNull("java:/a/b/c not bound", ctxB.lookup("/c"));
      assertNotNull("java:/a/b/c not bound", ctxC.lookup(""));
      assertTrue(ctxC.lookup("") instanceof Context);

      // Duplicate
      try {
         ctxB.createSubcontext("c");
         fail(":java/a/b/b is supposed to be bound and throw NameAlreadyBoundException");
      } catch (NameAlreadyBoundException ex) {
         // :)
      }
   }

   private void bindings(InitialContext ctx) throws Exception {
      String aBabyStr = "a";
      String aStr = "A";
      String bStr = "BB";
      String bTreeMoveStr = "BBTreeMove";
      String cStr = "CCC";
      Funky funky = new Funky("Testing", 203);
      assertFalse("State change must be false on original Funky object", funky.isStateChanged());

      Context ctxJava = (Context) ctx.lookup(new CompositeName("java:"));
      Context ctxB = (Context) ctxJava.lookup(new CompositeName("a/b"));
      ctxJava.bind("/a/aVal", aBabyStr);
      ctxB.bind("bOOOPSVal", bStr);
      ctxB.bind("c/cVal", cStr);
      ctxB.bind("bMoveTree", bTreeMoveStr);

      assertNotNull("java:/a/aVal not bound", ctx.lookup("/java:/a/aVal"));
      assertTrue(ctx.lookup("/java:/a/aVal") instanceof String);
      assertTrue(ctx.lookupLink("/java:/a/aVal") instanceof String);
      assertNotNull("java:/a/b/bOOOPSVal not bound", ctxJava.lookup("a/b/bOOOPSVal"));
      assertTrue(ctxJava.lookup("a/b/bOOOPSVal") instanceof String);
      assertTrue(ctxJava.lookupLink("a/b/bOOOPSVal") instanceof String);
      assertNotNull("java:/a/b/bMoveTree not bound", ctxJava.lookup("a/b/bMoveTree"));
      assertTrue(ctxJava.lookup("a/b/bMoveTree") instanceof String);
      assertTrue(ctxJava.lookupLink("a/b/bMoveTree") instanceof String);
      assertNotNull("java:/a/b/c/cVal not bound", ctxB.lookup("/c/cVal"));
      assertTrue(ctxB.lookup("/c/cVal") instanceof String);
      assertTrue(ctxB.lookupLink("/c/cVal") instanceof String);

      // Duplicate bind / rebind
      assertEquals("aVal <> a", aBabyStr, ctx.lookup("/java:/a/aVal").toString());
      try {
         ctx.bind("/java:/a/aVal", aStr);
         fail("/java:/a/aVal should already be bound");
      } catch (NameAlreadyBoundException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }
      ctx.rebind("java:/a/aVal", aStr);
      assertEquals("aVal <> A", aStr, ctx.lookup("/java:/a/aVal").toString());

      // Rename
      try {
         ctxJava.rename("a/unknown/bOOOPSVal", "/a/b/bVal");
         fail("a/unknown/bOOOPSVal should not exist");
      } catch (NameNotFoundException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }
      try {
         ctxJava.rename("a/b/bOOOPSVal", "/a/aVal");
         fail("/a/aVal should be bound");
      } catch (NameAlreadyBoundException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      ctxJava.rename("/a/b", "/a/BigB");
      assertNotNull("java:/a/BigB/c/cVal not bound", ctxB.lookup("/c/cVal"));
      ctxB.lookup("/c/cVal");

      ctxJava.rename("/a/BigB/bOOOPSVal", "/a/BigB/bVal");
      try {
         ctx.lookup("/java:/a/BigB/bOOOPSVal");
      } catch (NameNotFoundException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }
      assertNotNull("java:/a/BigB/bVal not bound", ctxJava.lookup("a/BigB/bVal"));
      assertTrue(ctxJava.lookup("a/BigB/bVal") instanceof String);
      assertEquals("bVal <> BB", bStr, ctx.lookup("/java:/a/BigB/bVal").toString());

      
      // Rename value from one part of tree to another part
      ctx.rename("java:/a/BigB/bMoveTree", "java:/a/BigB/c/bMovedTree");
      assertNotNull("java:/a/BigB/c/bMovedTree not bound", ctx.lookup("java:/a/BigB/c/bMovedTree"));
      // Make sure listings have been adjusted
      boolean foundOld = false;
      NamingEnumeration<NameClassPair> ne = ctx.list("java:/a/BigB");
      while (ne.hasMoreElements()) {
         if ("bMovedTree".equals(ne.next().getName())) {
            foundOld = true;
         }
      }
      assertFalse("Listing in old tree not updated", foundOld);
      
      boolean foundNew = false;
      ne = ctx.list("java:/a/BigB/c");
      while (ne.hasMoreElements()) {
         if ("bMovedTree".equals(ne.next().getName())) {
            foundNew = true;
         }
      }
      assertTrue("Listing in new tree not updated", foundNew);

      
      
      
      // Reference conversion
      ctx.bind("java:/a1/MrFunk", funky);
      Funky recreatedFunky = (Funky) ctx.lookup("java:/a1/MrFunk");  // Cast would fail if wrong object
      assertNotNull("java:/a1/MrFunk not bound", ctxJava.lookup("a1/MrFunk"));
      assertTrue("State change must be true on Funky object object from JNDI", recreatedFunky.isStateChanged());  // Used State and Object factories
      assertTrue("Incorrect number of messages", recreatedFunky.getMessage().size() == 203);

      // Unbind
      try {
         ctx.unbind("java:/aaa/MrFunk");
      } catch (NameNotFoundException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      ctx.unbind("java:/a/BigB");   // Rip out a sub context with children
      try {
         ctxJava.lookup("a/BigB/c/cVal");
      } catch (NameNotFoundException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      ctx.unbind("java:/a1/MrFunk");
      try {
         ctxJava.lookup("a1/MrFunk");
      } catch (NameNotFoundException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }
   }

   private void naming(InitialContext ctx) throws Exception {
      assertEquals("Initial context should ignore prefix", "cn=homedir", ctx.composeName("cn=homedir", "This will be ingored"));

      Context ctxA = (Context) ctx.lookup("java:/a");
      assertEquals("Incorrect initial context", "/aa/aaa", ctxA.composeName("aa/aaa", ""));
      assertEquals("Incorrect composeName", "/bla/doh///aa/aaa", ctxA.composeName("/aa/aaa", "/bla/doh/"));
   }

   private void environment(InitialContext ctx) throws Exception {
      Context ctxA = (Context) ctx.lookup("java:/a");
      assertNull("Environment property should not exist", ctxA.addToEnvironment("myProperty", "something"));
      assertEquals("Should return prev property value", "something", ctxA.addToEnvironment("myProperty", "else"));
      assertEquals("Get value failed", "else", ctxA.getEnvironment().get("myProperty"));
      assertNull("Environment property should not exist", ctx.getEnvironment().get("myProperty"));  // Should not exist on initial context

      Context ctxAOtherRef = (Context) ctx.lookup("java:/a");
      assertEquals("Get value failed", "else", ctxAOtherRef.getEnvironment().get("myProperty"));

      assertEquals("Remove value failed", "else", ctxAOtherRef.removeFromEnvironment("myProperty"));
      assertNull("Environment property should not exist", ctxA.getEnvironment().get("myProperty"));
   }

   private void listing(InitialContext ctx) throws Exception {
      Context ctxA = (Context) ctx.lookup("java:/a");
      NameClassPair ncp;

      NamingEnumeration<NameClassPair> ne = ctx.list("/java:");
      assertTrue("/java: should have a child 'hasMore'", ne.hasMore());
      assertTrue("/java: should have a child 'hasMoreElements'", ne.hasMoreElements());
      ncp = ne.next();
      assertTrue("/java: should have another child 'hasMore'", ne.hasMore());
      assertTrue("/java: should have another child 'hasMoreElements'", ne.hasMoreElements());
      assertNotNull("NameClassPair should not be null", ncp);
      assertEquals("Name of NameClassPair should be a", "a", ncp.getName());
      assertTrue("Name of NameClassPair class should contain Context", ncp.getClassName().contains("Context"));
      ncp = ne.next();
      assertFalse("/java: should not have any more elements 'hasMore'", ne.hasMore());
      assertFalse("/java: should not have any more elements 'hasMoreElements'", ne.hasMoreElements());
      assertNotNull("NameClassPair should not be null", ncp);
      assertEquals("Name of NameClassPair should be a1", "a1", ncp.getName());
      assertTrue("Name of NameClassPair class should contain Context", ncp.getClassName().contains("Context"));

      try {
         ncp = ne.next();
         fail("There should be no more elementes");
      } catch (NoSuchElementException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      ne = ctxA.list("");
      assertTrue("ContextA should have a child 'hasMore'", ne.hasMore());
      assertTrue("ContextA should have a child 'hasMoreElements'", ne.hasMoreElements());
      ncp = ne.next();
      assertFalse("ContextA should not have any more elements 'hasMore'", ne.hasMore());
      assertFalse("ContextA  should not have any more elements 'hasMoreElements'", ne.hasMoreElements());
      assertTrue("ContextA AVal should be relative", ncp.isRelative());
      assertNotNull("NameClassPair should not be null", ncp);
      assertEquals("Name of NameClassPair should be aVal", "aVal", ncp.getName());
      assertEquals("Name of NameClassPair class should be java.lang.String", "java.lang.String", ncp.getClassName());

      try {
         ncp.getNameInNamespace();
         fail("Exception must be thrown");
      } catch (UnsupportedOperationException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      try {
         ne = ctx.list("java:/unknown");
         fail("Exception must be thrown");
      } catch (NameNotFoundException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      try {
         ne = ctx.list("java:/a/aVal");
         fail("Exception must be thrown");
      } catch (NamingException ex) {
         // :)  Name not bound to context
      } catch (Exception ex) {
         fail("Incorrect exception");
      }
   }

   private void listBindings(InitialContext ctx) throws Exception {
      Context ctxA = (Context) ctx.lookup("java:/a");
      Binding bnd;

      NamingEnumeration<Binding> ne = ctx.listBindings("/java:");
      assertTrue("/java: should have a child 'hasMore'", ne.hasMore());
      assertTrue("/java: should have a child 'hasMoreElements'", ne.hasMoreElements());
      bnd = ne.next();
      assertTrue("/java: should have another child 'hasMore'", ne.hasMore());
      assertTrue("/java: should have another child 'hasMoreElements'", ne.hasMoreElements());
      assertNotNull("Binding should not be null", bnd);
      assertEquals("Name of Binfing should be a", "a", bnd.getName());
      assertTrue("Name of Binding class should contain Context", bnd.getClassName().contains("Context"));
      assertTrue("Type of Binding class should be Context", bnd.getObject() instanceof Context);
      bnd = ne.next();
      assertFalse("/java: should not have any more elements 'hasMore'", ne.hasMore());
      assertFalse("/java: should not have any more elements 'hasMoreElements'", ne.hasMoreElements());
      assertNotNull("Binding should not be null", bnd);
      assertEquals("Name of Binding should be a1", "a1", bnd.getName());
      assertTrue("Name of Binding class should contain Context", bnd.getClassName().contains("Context"));
      assertTrue("Type of Binding class should be Context", bnd.getObject() instanceof Context);

      try {
         bnd = ne.next();
         fail("There should be no more elementes");
      } catch (NoSuchElementException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      ne = ctxA.listBindings("");
      assertTrue("ContextA should have a child 'hasMore'", ne.hasMore());
      assertTrue("ContextA should have a child 'hasMoreElements'", ne.hasMoreElements());
      bnd = ne.next();
      assertFalse("ContextA should not have any more elements 'hasMore'", ne.hasMore());
      assertFalse("ContextA  should not have any more elements 'hasMoreElements'", ne.hasMoreElements());
      assertTrue("ContextA AVal should be relative", bnd.isRelative());
      assertNotNull("Binding should not be null", bnd);
      assertEquals("Name of Binding should be aVal", "aVal", bnd.getName());
      assertEquals("Name of Binding class should be java.lang.String", "java.lang.String", bnd.getClassName());
      assertTrue("Type of Binding class should be String", bnd.getObject() instanceof String);

      try {
         bnd.getNameInNamespace();
         fail("Exception must be thrown");
      } catch (UnsupportedOperationException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      try {
         ne = ctx.listBindings("java:/unknown");
         fail("Exception must be thrown");
      } catch (NameNotFoundException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      try {
         ne = ctx.listBindings("java:/a/aVal");
         fail("Exception must be thrown");
      } catch (NamingException ex) {
         // :)  Name not bound to context
      } catch (Exception ex) {
         fail("Incorrect exception");
      }
   }

   public void destroyContext(InitialContext ctx) throws Exception {
      Context ctxA = (Context) ctx.lookup("java:/a");

      try {
         ctxA.destroySubcontext("");
         fail("Context cannot detroy itself");
      } catch (NamingException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      try {
         ctxA.destroySubcontext("/unknown/val");
         fail("intermediate context not found");
      } catch (NamingException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      ctx.destroySubcontext("/java:/a");
      try {
         ctx.destroySubcontext("/java:/a");   // Idempotent
         System.out.println("destroySubcontext is implement as idempotent");
      } catch (Exception ex) {
         System.err.println("destroySubcontext is NOT implement as idempotent");
      }

      try {
         ctx.lookup("java:/a/aVal");
         fail("Context a should be deleted");
      } catch (NameNotFoundException ex) {
         // :)
      } catch (Exception ex) {
         fail("Incorrect exception");
      }

      // Implementation interpretation
      try {
         ctxA.lookup("aVal");      // Deleted, but looked up context still in scope
         System.err.println("Implementation DID NOT destroy existing context reference when deleted using another context");
      } catch (NamingException ex) {
         System.out.println("Implementation destroyed existing context reference when deleted using another context");
      }
   }

}
