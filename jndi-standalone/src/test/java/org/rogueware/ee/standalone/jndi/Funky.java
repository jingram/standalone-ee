/*
 * Funky.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import java.util.ArrayList;
import java.util.List;
import javax.naming.RefAddr;
import javax.naming.Reference;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class Funky {

   private List<String> message;
   private boolean stateChanged;

   public Funky(String message, int times) {
      this.message = new ArrayList<>();
      for (int i = 0; i < times; i++) {
         this.message.add(String.format("%d:%s", (i + 1), message));
      }
   }

   public Funky(String ref) {
      this(ref.split("__")[0], Integer.parseInt(ref.split("__")[1]));
      stateChanged = true;
   }

   public List<String> getMessage() {
      return message;
   }

   public boolean isStateChanged() {
      return stateChanged;
   }

   public Reference getRef() {
      RefAddr ra = new RefAddr("serialized") {

         @Override
         public Object getContent() {
            return message.get(0).split(":")[1] + "__" + message.size();
         }
      };
      Reference r = new Reference(Funky.class.getName(), ra, FunkyObjectFactory.class.getName(), null);
      return r;
   }
}
