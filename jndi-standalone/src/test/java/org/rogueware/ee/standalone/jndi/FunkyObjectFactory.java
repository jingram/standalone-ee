/*
 * FunkyObjectFactory.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class FunkyObjectFactory implements ObjectFactory {

   @Override
   public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment) throws Exception {
      if (obj instanceof Reference) {
         Reference r = (Reference) obj;
         if (null != r.getFactoryClassName() && FunkyObjectFactory.class.getName().equals(r.getFactoryClassName())) {
            return new Funky((String) r.get("serialized").getContent());
         }
      }
      return null;
   }

}
