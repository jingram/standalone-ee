/*
 * TestContext.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import java.util.Hashtable;
import javax.naming.Binding;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.OperationNotSupportedException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestReadOnlyContext {

   private static InitialContext mainCtx;

   @BeforeClass
   public static void init() throws Exception {
      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.INFO);

      Logger jndiLogger = (Logger) LoggerFactory.getLogger("org.rogueware.ee.standalone.jndi");
      jndiLogger.setLevel(Level.TRACE);
   }

   @Test
   public void testReadOnlyContext() throws Throwable {
      // Set default JNDI provider
      System.setProperty(javax.naming.Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");

      setupMainContext();

      // Setup a read-only context wrapping the normal context
      Hashtable env = new Hashtable();
      env.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ReadOnlyContextFactory");
      InitialContext ctx = new InitialContext(env);

      // Check can read
      assertNotNull("java:global/a not bound", ctx.lookup("java:global/a"));
      assertNotNull("java:global/c not bound", ctx.lookup("java:global/c"));
      assertTrue(ctx.lookup("java:comp") instanceof javax.naming.Context);
      assertEquals("aaVal1 <> aaVal1", "aaVal1", ctx.lookup("java:global/a/aa/aaVal1").toString());

      // Can see main environment
      assertEquals("global <> main", "main", ctx.getEnvironment().get("global"));

      // Test cannot write
      try {
         ctx.createSubcontext("java:global/zzz");
         fail("Read context should have failed write");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.destroySubcontext("java:app");
         fail("Read context should have failed write");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.addToEnvironment("bla", "valye");
         fail("Read context should have failed write");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.removeFromEnvironment("global");
         fail("Read context should have failed write");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.bind("java:global/zzz", "zzzVal");
         fail("Read context should have failed write");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.rebind("java:global/gVal1", "gValNew");
         fail("Read context should have failed write");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.unbind("java:global/gVal1");
         fail("Read context should have failed write");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.rename("java:global/gVal1", "java:global/gValOther");
         fail("Read context should have failed write");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      // Lookup a sub context (should be wrapped read-only)
      javax.naming.Context ctxC = (javax.naming.Context) ctx.lookup("java:global/c");
      assertNotNull("cVal2 not bound", ctxC.lookup("cVal2"));

      try {
         ctxC.createSubcontext("cNew");
         fail("Read context should have failed write");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.bind("cNewVal", "cNewVal");
         fail("Read context should have failed write");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      // Listing context's should be read only
      NamingEnumeration<Binding> ne = ctx.listBindings("java:global");
      while (ne.hasMore()) {
         Binding b = ne.next();
         if (b.getObject() instanceof javax.naming.Context) {
            javax.naming.Context c = (javax.naming.Context) b.getObject();
            try {
               c.createSubcontext("cNew");
               fail("Read context should have failed write");
            } catch (OperationNotSupportedException ex) {
               // :)
            }
         }
      }

   }

   private void setupMainContext() throws NamingException {
      mainCtx = new InitialContext();

      mainCtx.addToEnvironment("global", "main");

      mainCtx.createSubcontext("java:global");
      mainCtx.createSubcontext("java:global/a");
      mainCtx.createSubcontext("java:global/a/aa");
      mainCtx.createSubcontext("java:global/b");
      mainCtx.createSubcontext("java:global/c");
      mainCtx.createSubcontext("java:app");
      mainCtx.createSubcontext("java:comp");

      mainCtx.bind("java:global/gVal1", "gVal1");
      mainCtx.bind("java:global/a/aVal1", "aVal1");
      mainCtx.bind("java:global/a/aVal2", "aVal2");
      mainCtx.bind("java:global/a/aa/aaVal1", "aaVal1");
      mainCtx.bind("java:global/b/bVal1", "bVal1");
      mainCtx.bind("java:global/c/cVal1", "cVal1");
      mainCtx.bind("java:global/c/cVal2", "cVal2");
   }
}
