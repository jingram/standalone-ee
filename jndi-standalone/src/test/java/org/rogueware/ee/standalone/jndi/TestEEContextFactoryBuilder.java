/*
 * TestJNDI.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.spi.NamingManager;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestEEContextFactoryBuilder {

   @BeforeClass
   public static void init() throws Exception {
      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.INFO);

      Logger jndiLogger = (Logger) LoggerFactory.getLogger("org.rogueware.ee.standalone.jndi");
      jndiLogger.setLevel(Level.TRACE);

      // Set the context factory builder
      NamingManager.setInitialContextFactoryBuilder(new EEInitialContextFactoryBuilder());
      System.setProperty(javax.naming.Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");
      setupMainContext();
   }

   @Test
   public void testThreadContextPushPop() throws Throwable {
      // Outside of any EE container (Should be our main provider)
      InitialContext ic = new InitialContext();
      javax.naming.Context c = (javax.naming.Context)ic.lookup("");
      
      assertTrue("Initial context should be main provider", c instanceof org.rogueware.ee.standalone.jndi.Context);

      // Simulate call into EE container
      ThreadContextFactory.pushContext();  // Call creates thread context
      
      ic = new InitialContext();
      c = (javax.naming.Context)ic.lookup("");
      assertTrue("Initial context should read only as in EE container", c instanceof org.rogueware.ee.standalone.jndi.ReadOnlyContext);

      
      // Simulate call exited EE container
      ThreadContextFactory.popContext();// Call destroys thread context  (Note, this can be last of recursive calls into container)
      ic = new InitialContext();
      c = (javax.naming.Context)ic.lookup("");
      assertTrue("Initial context should be main provider", c instanceof org.rogueware.ee.standalone.jndi.Context);            
   }

   private static void setupMainContext() throws NamingException {
      // Always clear out memory provider
      ContextFactory.clearInitialContext();

      InitialContext mainCtx = new InitialContext();

      mainCtx.addToEnvironment("global", "main");

      mainCtx.createSubcontext("java:global");
      mainCtx.createSubcontext("java:global/a");
      mainCtx.createSubcontext("java:global/a/aa");
      mainCtx.createSubcontext("java:global/b");
      mainCtx.createSubcontext("java:global/c");
      mainCtx.createSubcontext("java:app");
      mainCtx.createSubcontext("java:comp");

      mainCtx.bind("java:global/gVal1", "gVal1");
      mainCtx.bind("java:global/a/aVal1", "aVal1");
      mainCtx.bind("java:global/a/aVal2", "aVal2");
      mainCtx.bind("java:global/a/aa/aaVal1", "aaVal1");
      mainCtx.bind("java:global/b/bVal1", "bVal1");
      mainCtx.bind("java:global/c/cVal1", "cVal1");
      mainCtx.bind("java:global/c/cVal2", "cVal2");
   }
}
