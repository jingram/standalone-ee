/*
 * TestContext.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import java.util.Hashtable;
import javax.naming.InitialContext;
import static org.junit.Assert.assertNotNull;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestJNDIUtility {
   
   @BeforeClass
   public static void init() throws Exception {
      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.INFO);

      Logger jndiLogger = (Logger) LoggerFactory.getLogger("org.rogueware.ee.standalone.jndi");
      jndiLogger.setLevel(Level.TRACE);
   }
   
   @Test
   public void testJNDIUtility() throws Exception {
      System.setProperty(javax.naming.Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");      
      InitialContext ctx = new InitialContext();
      
      JNDI.subContextCreation(ctx, "java:comp/env");
      JNDI.subContextCreation(null, "java:comp/env/one/two/three");
            
      assertNotNull("java:comp not bound", ctx.lookup("java:comp"));
      assertNotNull("java:comp/env not bound", ctx.lookup("java:comp/env"));
      assertNotNull("java:comp/env/one not bound", ctx.lookup("java:comp/env/one"));
      assertNotNull("java:comp/env/one/two not bound", ctx.lookup("java:comp/env/one/two"));
      assertNotNull("java:comp/env/one/two/three not bound", ctx.lookup("java:comp/env/one/two/three"));            
   }   
   
}
