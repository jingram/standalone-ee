/*
 * TestJNDI.java
 * 
 * Copyright 2014 Johnathan Ingram (jingram@rogueware.org)
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. *
 * 
 */
package org.rogueware.ee.standalone.jndi;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.OperationNotSupportedException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Johnathan Ingram (jingram@rogueware.org)
 */
public class TestThreadContext {

   private static InitialContext mainCtx;

   public enum ThreadState {

      POPULATE,
      POPULATED,
      OTHER_THREAD_CHECK,
      OTHER_THREAD_CHECKED,
      FINISHED
   }

   private static ThreadState threadOneState = ThreadState.POPULATE;
   private static Throwable threadOneError;
   private static ThreadState threadTwoState = ThreadState.POPULATE;
   private static Throwable threadTwoError;

   @BeforeClass
   public static void init() throws Exception {
      Logger rootLogger = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
      rootLogger.setLevel(Level.INFO);

      Logger jndiLogger = (Logger) LoggerFactory.getLogger("org.rogueware.ee.standalone.jndi");
      jndiLogger.setLevel(Level.TRACE);
   }

   @Test
   public void testThreadContextPushPop() throws Throwable {
      System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");
      setupMainContext();

      // Wrap context with thread context
      Hashtable env = new Hashtable();
      env.put(Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ThreadContextFactory");
      InitialContext ctx = new InitialContext(env);
      assertTrue("Thread context should exist for thread", ThreadContextFactory.hasThreadContext());

      // State 1
      ctx.bind("java:global/state1", "state1");

      // State 2
      ThreadContextFactory.pushContext();
      try {
         ctx.lookup("java:global/state1");
         fail("java:global/state1 should not be bound as state was pushed without preserve");
      } catch (NameNotFoundException ex) {
         // :)
      }
      ctx.bind("java:global/state2", "state2");

      // State 3
      ThreadContextFactory.pushAndPreserveContext();
      // state2 should exist, as preserved the state during push
      assertEquals("state2 <> state2", "state2", ctx.lookup("java:global/state2").toString());
      try {
         ctx.lookup("java:global/state1");
         fail("java:global/state1 should not be bound as state was pushed without preserve");
      } catch (NameNotFoundException ex) {
         // :)
      }
      ctx.bind("java:global/state3", "state3");

      // Back to state 2
      ThreadContextFactory.popContext();
      assertEquals("state2 <> state2", "state2", ctx.lookup("java:global/state2").toString());
      try {
         ctx.lookup("java:global/state3");
         fail("java:global/state3 should not be bound as state was poped back to state 2");
      } catch (NameNotFoundException ex) {
         // :)
      }
      try {
         ctx.lookup("java:global/state1");
         fail("java:global/state1 should not be bound as state was poped back to state 2");
      } catch (NameNotFoundException ex) {
         // :)
      }

      // Back to state 1
      ThreadContextFactory.popContext();
      assertEquals("state1 <> state1", "state1", ctx.lookup("java:global/state1").toString());
      try {
         ctx.lookup("java:global/state2");
         fail("java:global/state2 should not be bound as state was poped back to state 1");
      } catch (NameNotFoundException ex) {
         // :)
      }

      // Error on too many pops
      try {
         ThreadContextFactory.popContext();
         fail("Should not be able to pop more states than where pushed");
      } catch (NamingException ex) {
         // :)
      }
   }

   @Test
   public void testThreadContextCreateOnPushDestroyOnPop() throws Throwable {
      System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");
      setupMainContext();

      // Make sure starting clean
      ThreadContextFactory.destoyThreadContext();
      assertFalse("Thread context should not exist", ThreadContextFactory.hasThreadContext());

      ThreadContextFactory.pushContext();  // First push creates thread context
      assertTrue("Thread context should exist for thread", ThreadContextFactory.hasThreadContext());
      ThreadContextFactory.pushContext();
      ThreadContextFactory.pushContext();

      ThreadContextFactory.popContext();
      ThreadContextFactory.popContext();
      assertTrue("Thread context should exist for thread", ThreadContextFactory.hasThreadContext());
      ThreadContextFactory.popContext(); // Last pop destroys thread context                 

      assertFalse("Thread context should be destroyed", ThreadContextFactory.hasThreadContext());
   }

   @Test
   public void testThreadContext() throws Throwable {
      System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ContextFactory");
      setupMainContext();

      Thread t1 = new Thread(new Runnable() {
         @Override
         public void run() {
            try {
               runThreadOneContextTest();
            } catch (Throwable ex) {
               threadOneError = ex;
            }
         }
      });

      Thread t2 = new Thread(new Runnable() {
         @Override
         public void run() {
            try {
               runThreadTwoContextTest();
            } catch (Throwable ex) {
               threadTwoError = ex;
            }
         }
      });

      t1.start();
      t2.start();

      t1.join(5000);
      t2.join(5000000);

      if (null != threadOneError) {
         throw threadOneError;
      }
      if (null != threadTwoError) {
         throw threadTwoError;
      }

      if (threadOneState != ThreadState.FINISHED) {
         fail("Thread 1 did not complete test");
      }
      if (threadTwoState != ThreadState.FINISHED) {
         fail("Thread 2 did not complete test");
      }
   }

   @Test
   public void testThreadContextWithOtherJNDISPI() throws Throwable {
      System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.naming.java.javaURLContextFactory");
      setupMainContext();

      Thread t1 = new Thread(new Runnable() {
         @Override
         public void run() {
            try {
               runThreadOneContextTest();
            } catch (Throwable ex) {
               threadOneError = ex;
            }
         }
      });

      Thread t2 = new Thread(new Runnable() {
         @Override
         public void run() {
            try {
               runThreadTwoContextTest();
            } catch (Throwable ex) {
               threadTwoError = ex;
            }
         }
      });

      t1.start();
      t2.start();

      try {
         t1.join(5000);
         t2.join(5000);
      } catch (InterruptedException ex) {

      }

      if (null != threadOneError) {
         throw threadOneError;
      }
      if (null != threadTwoError) {
         throw threadTwoError;
      }

      if (threadOneState != ThreadState.FINISHED) {
         fail("Thread 1 did not complete test");
      }
      if (threadTwoState != ThreadState.FINISHED) {
         fail("Thread 2 did not complete test");
      }
   }

   private static void setupMainContext() throws NamingException {
      // Always clear out memory provider
      ContextFactory.clearInitialContext();

      mainCtx = new InitialContext();

      mainCtx.addToEnvironment("global", "main");

      mainCtx.createSubcontext("java:global");
      mainCtx.createSubcontext("java:global/a");
      mainCtx.createSubcontext("java:global/a/aa");
      mainCtx.createSubcontext("java:global/b");
      mainCtx.createSubcontext("java:global/c");
      mainCtx.createSubcontext("java:app");
      mainCtx.createSubcontext("java:comp");

      mainCtx.bind("java:global/gVal1", "gVal1");
      mainCtx.bind("java:global/a/aVal1", "aVal1");
      mainCtx.bind("java:global/a/aVal2", "aVal2");
      mainCtx.bind("java:global/a/aa/aaVal1", "aaVal1");
      mainCtx.bind("java:global/b/bVal1", "bVal1");
      mainCtx.bind("java:global/c/cVal1", "cVal1");
      mainCtx.bind("java:global/c/cVal2", "cVal2");
   }

   private void runThreadOneContextTest() throws Exception {
      // Create a thread scoped context (All changes are thread scoped, can see main context, but not modify it)
      Hashtable env = new Hashtable();
      env.put(Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ThreadContextFactory");
      InitialContext ctx = new InitialContext(env);

      // Create some contexts
      ctx.createSubcontext("java:global/b/t1ba");
      ctx.createSubcontext("java:global/b/t1ba/t1bba");
      ctx.createSubcontext("java:global/t1d");
      ctx.createSubcontext("java:comp/env");   // Same name for both threads

      // Create some bindings
      ctx.bind("java:comp/env/envVal", "t1envVal");
      ctx.bind("java:global/b/t1ba/t1baVal1", "t1baVal1");
      ctx.bind("java:global/b/t1ba/t1bba/t1bbaVal1", "t1bbaVal1");

      // Environment
      ctx.addToEnvironment("global1", "thread1");
      Context envCtx = (Context) ctx.lookup("java:comp/env");
      envCtx.addToEnvironment("envProp1", "thread1");

      // Can see main context
      assertNotNull("java:global/a not bound", ctx.lookup("java:global/a"));
      assertNotNull("java:global/c not bound", ctx.lookup("java:global/c"));
      assertTrue(ctx.lookup("java:comp") instanceof Context);
      assertEquals("aaVal1 <> aaVal1", "aaVal1", ctx.lookup("java:global/a/aa/aaVal1").toString());

      // Can see main environment
      assertEquals("global <> main", "main", ctx.getEnvironment().get("global"));

      // Can see own thread context items
      assertNotNull("java:global/b/t1ba not bound", ctx.lookup("java:global/b/t1ba"));
      assertNotNull("java:comp/env not bound", ctx.lookup("java:comp/env"));
      assertTrue(ctx.lookup("java:global/t1d") instanceof Context);
      assertEquals("t1bbaVal1 <> t1bbaVal1", "t1bbaVal1", ctx.lookup("java:global/b/t1ba/t1bba/t1bbaVal1").toString());
      assertEquals("envVal <> t1envVal", "t1envVal", ctx.lookup("java:comp/env/envVal").toString());

      // Can see own environment
      assertEquals("global1 <> thread1", "thread1", ctx.getEnvironment().get("global1").toString());
      assertEquals("envProp1 == thread1", "thread1", envCtx.getEnvironment().get("envProp1").toString());

      threadOneState = ThreadState.POPULATED;

      // WAIT for Thread 2 to check our values
      while (threadTwoState.ordinal() < ThreadState.OTHER_THREAD_CHECKED.ordinal()) {
         Thread.sleep(50);
      }

      // Check thread 2 state
      threadOneState = ThreadState.OTHER_THREAD_CHECK;

      InitialContext ctxCleanCheck = new InitialContext(env);
      try {
         ctxCleanCheck.lookup("java:global/b/t2ba");  // Only exist in thread 2 context
         fail("java:global/b/t2ba is not supposed to be bound in thread 1");
      } catch (NameNotFoundException ex) {
         // :)
      }
      try {
         ctxCleanCheck.lookup("java:global/b/t2ba/t2bba/t2bbaVal1");  // Only exist in thread 2 context
         fail("java:global/b/t2ba/t2bba/t2bbaVal1 is not supposed to be bound in thread 1");
      } catch (NameNotFoundException ex) {
         // :)
      }
      assertNotEquals("envVal == t2envVal", "t2envVal", ctx.lookup("java:comp/env/envVal").toString());   // Must = our thread value

      assertNull("global exists", ctx.getEnvironment().get("global2"));

      threadOneState = ThreadState.OTHER_THREAD_CHECKED;

      // Rebind
      ctx.rebind("java:comp/env/envVal", "rebindVal");
      assertEquals("envVal <> rebindVal", "rebindVal", ctx.lookup("java:comp/env/envVal").toString());

      // Rename      
      Context bCtx = (Context) ctx.lookup("java:global/b");
      bCtx.rename("/t1ba/t1baVal1", "t1ba/t1baVal1Moved");     // from and to thread context
      assertNotNull("java:global/b/t1ba/t1baVal1Moved not bound", ctx.lookup("java:global/b/t1ba/t1baVal1Moved"));
      try {
         ctx.lookup("java:global/b/t1ba/t1baVal1");
         fail("java:global/b/t1ba/t1baVal1 exists");
      } catch (NameNotFoundException ex) {
         // :)
      }

      // Rename with listing check
      ctx.rename("java:global/b/t1ba/t1bba/t1bbaVal1", "java:global/c/t1bbaVal1Moved");                      // From thread context under a main context
      assertNotNull("java:global/c/t1bbaVal1Moved not bound", ctx.lookup("java:global/c/t1bbaVal1Moved"));
      try {
         ctx.lookup("java:global/b/t1ba/t1bba/t1bbaVal1");
         fail("java:global/b/t1ba/t1bba/t1bbaVal1exists");
      } catch (NameNotFoundException ex) {
         // :)
      }
      boolean foundOld = false;
      NamingEnumeration<NameClassPair> ne = ctx.list("java:global/b/t1ba/t1bba");
      while (ne.hasMoreElements()) {
         if ("t1bbaVal1".equals(ne.next().getName())) {
            foundOld = true;
         }
      }
      assertFalse("Listing in old tree not updated", foundOld);

      boolean foundNew = false;
      ne = ctx.list("java:global/c");
      while (ne.hasMoreElements()) {
         if ("t1bbaVal1Moved".equals(ne.next().getName())) {
            foundNew = true;
         }
      }
      assertTrue("Listing in new tree not updated", foundNew);

      // Unbind
      ctx.unbind("java:comp/env");
      try {
         ctx.lookup("java:comp/env");
         fail("java:comp/env exists");
      } catch (NameNotFoundException ex) {
         // :)
      }

      threadOneState = ThreadState.FINISHED;
   }

   private void runThreadTwoContextTest() throws Exception {
      // Create a thread scoped context (All changes are thread scoped, can see main context, but not modify it)
      Hashtable env = new Hashtable();
      env.put(Context.INITIAL_CONTEXT_FACTORY, "org.rogueware.ee.standalone.jndi.ThreadContextFactory");
      InitialContext ctx = new InitialContext(env);

      // Create some contexts
      ctx.createSubcontext("java:global/b/t2ba");
      ctx.createSubcontext("java:global/b/t2ba/t2bba");
      ctx.createSubcontext("java:global/t2d");
      ctx.createSubcontext("java:comp/env");     // Same name for both threads

      // Create some bindings
      ctx.bind("java:comp/env/envVal", "t2envVal");  // Same name, different values

      // Environment
      ctx.addToEnvironment("global2", "thread2");
      Context envCtx = (Context) ctx.lookup("java:comp/env");
      envCtx.addToEnvironment("envProp2", "thread2");

      // Can see main context
      assertNotNull("java:global/a not bound", ctx.lookup("java:global/a"));
      assertNotNull("java:global/c not bound", ctx.lookup("java:global/c"));
      assertTrue(ctx.lookup("java:comp") instanceof Context);
      assertEquals("aaVal1 <> aaVal1", "aaVal1", ctx.lookup("java:global/a/aa/aaVal1").toString());

      // Can see main environment
      assertEquals("global <> main", "main", ctx.getEnvironment().get("global"));

      // Can see own thread context items
      assertNotNull("java:global/b/t2ba not bound", ctx.lookup("java:global/b/t2ba"));
      assertNotNull("java:comp/env not bound", ctx.lookup("java:comp/env"));
      assertTrue(ctx.lookup("java:global/t2d") instanceof Context);
      assertEquals("envVal <> t2envVal", "t2envVal", ctx.lookup("java:comp/env/envVal").toString());

      // Can see own environment
      assertEquals("global2 <> thread2", "thread2", ctx.getEnvironment().get("global2"));

      threadTwoState = ThreadState.POPULATED;

      // WAIT for Thread 1 to populate
      while (threadOneState.ordinal() < ThreadState.POPULATED.ordinal()) {
         Thread.sleep(50);
      }

      // Check thread 1 state
      threadTwoState = ThreadState.OTHER_THREAD_CHECK;

      InitialContext ctxCleanCheck = new InitialContext(env);
      try {
         ctxCleanCheck.lookup("java:global/b/t1ba");  // Only exist in thread 1 context
         fail("java:global/b/t1ba is not supposed to be bound in thread 2");
      } catch (NameNotFoundException ex) {
         // :)
      }
      try {
         ctxCleanCheck.lookup("java:global/b/t1ba/t1bba/t1bbaVal1");  // Only exist in thread 1 context
         fail("java:global/b/t1ba/t1bba/t1bbaVal1 is not supposed to be bound in thread 2");
      } catch (NameNotFoundException ex) {
         // :)
      }
      assertNotEquals("envVal == t1envVal", "t1envVal", ctx.lookup("java:comp/env/envVal").toString());   // Must = our thread value

      assertNull("global1 exists", ctx.getEnvironment().get("global1"));

      threadTwoState = ThreadState.OTHER_THREAD_CHECKED;

      // WAIT for Thread 1 to check our values
      while (threadOneState.ordinal() < ThreadState.OTHER_THREAD_CHECKED.ordinal()) {
         Thread.sleep(50);
      }

      // Negative tests
      // Try manipulate underlying main context
      try {
         ctx.destroySubcontext("java:global/a");
         fail("Operation not supported on main context");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.unbind("java:global/a/aVal1");
         fail("Operation not supported on main context");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.rename("java:global/a/aa/aaVal1", "java:global/b/t2ba/aaVal1");
         fail("Operation not supported on main context");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.rename("java:global/a/aVal1", "java:global/a/renamedAVal1");
         fail("Operation not supported on main context");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      try {
         ctx.addToEnvironment("global", "myNewVal");
         fail("Operation not supported on main context");
      } catch (OperationNotSupportedException ex) {
         // :)
      }

      // Destroy the context
      ThreadContextFactory.destoyThreadContext();
      ctxCleanCheck = new InitialContext(env);
      try {
         ctxCleanCheck.lookup("java:comp/env");  // Should be cleaned up
         fail("java:comp/envis not supposed to be bound");
      } catch (NameNotFoundException ex) {
         // :)
      }

      threadTwoState = ThreadState.FINISHED;
   }
}
